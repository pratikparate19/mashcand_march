(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Dashboard/aboutus/aboutus.component.css":
/*!*********************************************************!*\
  !*** ./src/app/Dashboard/aboutus/aboutus.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Dashboard/aboutus/aboutus.component.html":
/*!**********************************************************!*\
  !*** ./src/app/Dashboard/aboutus/aboutus.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left: 12%\">\r\n  <div class=\"row\" style=\"margin-top: 20px;\">\r\n    <div class=\"col-md-12\" style=\"text-align: center\">\r\n      <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/MineMark+Logo-Orange.svg\">\r\n    </div>\r\n  </div>\r\n  <div class=\"row\" style=\"margin-top: 20px;\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <div>\r\n                <h3 style=\"font-weight: bold;font-family: sans-serif;\">About Minemark</h3>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <p style=\" text-align: justify;font-family: sans-serif;font-size: 16px;\">Lorem ipsum dolor sit amet,\r\n                consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\r\n                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip\r\n                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore\r\n                eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia\r\n                deserunt mollit anim id est laborum.</p>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n  <div class=\"row\" style=\"margin-top: 20px;\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <div>\r\n                <h3 style=\"font-weight: bold;font-family: sans-serif;\">Address</h3>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <p style=\" text-align: justify;font-family: sans-serif;font-size: 16px;\">MineMark Solutions Pvt. Ltd.</p>\r\n              <p style=\" text-align: justify;font-family: sans-serif;font-size: 16px;\">ShriRang, S.No: 25/2/1, Vishal\r\n                Nagar, Pune-411027</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <div>\r\n                <h3 style=\"font-weight: bold;font-family: sans-serif;\">Contact us at</h3>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-8\">\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <div><img class=\"img-responsive\" src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/assets/phone.svg\"></div>\r\n                  <div style=\"margin-top: -14px;\"><a style=\"margin-left: 20px;\">+918446913250</a></div>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <div><img class=\"img-responsive\" src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/assets/phone.svg\"></div>\r\n                  <div style=\"margin-top: -14px;\"><a style=\"margin-left: 20px;\">+919075006223</a></div>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <div>\r\n                &nbsp;\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n              <div class=\"col-md-8\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <div><img class=\"img-responsive\" src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/assets/baseline-email-24px.png\" style=\"width: 20px;height: 20px;\"></div>\r\n                    <div style=\"margin-top: -14px;\"><a style=\"margin-left: 20px;\">example@example.com</a></div>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                   &nbsp;\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-4\">\r\n                <div>\r\n                  &nbsp;\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n\r\n        </div>\r\n\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n    &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/aboutus/aboutus.component.ts":
/*!********************************************************!*\
  !*** ./src/app/Dashboard/aboutus/aboutus.component.ts ***!
  \********************************************************/
/*! exports provided: AboutusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutusComponent", function() { return AboutusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutusComponent = /** @class */ (function () {
    function AboutusComponent() {
    }
    AboutusComponent.prototype.ngOnInit = function () {
    };
    AboutusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-aboutus',
            template: __webpack_require__(/*! ./aboutus.component.html */ "./src/app/Dashboard/aboutus/aboutus.component.html"),
            styles: [__webpack_require__(/*! ./aboutus.component.css */ "./src/app/Dashboard/aboutus/aboutus.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutusComponent);
    return AboutusComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/demo/demo.component.css":
/*!***************************************************!*\
  !*** ./src/app/Dashboard/demo/demo.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:hover {\r\n\r\n  cursor:pointer;\r\n  color: red;\r\n}\r\ntextarea.ng-invalid {\r\n  background-color: pink;\r\n}\r\n/* The container */\r\n.container {\r\n  display: block;\r\n  position: relative;\r\n  padding-left: 35px;\r\n  margin-bottom: 12px;\r\n  cursor: pointer;\r\n  font-size: 18px;;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  font-family: sans-serif;\r\n}\r\n/* Hide the browser's default radio button */\r\n.container input {\r\n  position: absolute;\r\n  opacity: 0;\r\n  cursor: pointer;\r\n}\r\n/* Create a custom radio button */\r\n.checkmark {\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  height: 25px;\r\n  width: 25px;\r\n  background-color: #eee;\r\n  border-radius: 50%;\r\n}\r\n/* On mouse-over, add a grey background color */\r\n.container:hover input ~ .checkmark {\r\n  background-color: #ccc;\r\n}\r\n/* When the radio button is checked, add a blue background */\r\n.container input:checked ~ .checkmark {\r\n  background-color: #FF4F33;\r\n}\r\n/* Create the indicator (the dot/circle - hidden when not checked) */\r\n.checkmark:after {\r\n  content: \"\";\r\n  position: absolute;\r\n  display: none;\r\n}\r\n/* Show the indicator (dot/circle) when checked */\r\n.container input:checked ~ .checkmark:after {\r\n  display: block;\r\n}\r\n/* Style the indicator (dot/circle) */\r\n.container .checkmark:after {\r\n top: 9px;\r\nleft: 9px;\r\nwidth: 8px;\r\nheight: 8px;\r\nborder-radius: 50%;\r\nbackground: white;\r\n}\r\n.btnsubmit{\r\n  background-color: #FF4F33;width: 179px;font-size: 18px;\r\n            height: 50px;color: #ffffff;border-radius: 25px;\r\n}"

/***/ }),

/***/ "./src/app/Dashboard/demo/demo.component.html":
/*!****************************************************!*\
  !*** ./src/app/Dashboard/demo/demo.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <app-full-layout></app-full-layout>\r\n <div class=\"container\" style=\"margin-top: 50px\">\r\n   <div class=\"row\">\r\n     <div class=\"col-md-6\" value=\"submit\"  style=\"text-align: center; color: #FF4F33;font-size: 18px\">Cancel Test</div>\r\n     <div class=\"col-md-6\" style=\"text-align: center; color: #FF4F33;font-size: 18px\"><img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/if_1-09_511570.jpg\"\r\n         style=\"width: 50px;height: 40px;\">\r\n       <countdown id=\"countdown\" [config]=\"{leftTime: 100 * 27, notify: [1,300]}\">$!m!:$!s!</countdown>\r\n     </div>\r\n   </div>\r\n   <div class=\"row\" style=\"margin-top: 30px\"  >\r\n\r\n     <div class=\"col-md-6 offset-3\">\r\n       <div class=\"card bg-light\">\r\n         <div class=\"card-body\">\r\n           <div class=\"row\" style=\"margin-top: 20px;\">\r\n             <div class=\"col-md-12\">\r\n               <p style=\"text-align: center;\">Question {{QuestionCount}}</p>\r\n             </div>\r\n           </div>\r\n           <div class=\"row\" style=\"margin-top: 20px;\">\r\n             <div class=\"col-md-12\">\r\n               <p style=\"text-align: center;\"> {{que.valueData}}</p>\r\n             </div>\r\n           </div>\r\n           <form>\r\n             <div class=\"row\" style=\"margin-top: 20px;\">\r\n               <div class=\"col-md-12 form-group\" >\r\n                 <label class=\"container\">\r\n                   <p>{{que.option_1}}</p>\r\n                   <input type=\"radio\" checked=\"checked\" [(ngModel)]=\"given_answer\" [value]=1 name=\"radio\">\r\n                   <span class=\"checkmark\"></span>\r\n                 </label>\r\n                 <label class=\"container\">\r\n                   <p>{{que.option_2}}</p>\r\n                   <input type=\"radio\" [value]=2 name=\"radio\" [(ngModel)]=\"given_answer\">\r\n                   <span class=\"checkmark\"></span>\r\n                 </label>\r\n                 <label class=\"container\">\r\n                   <p>{{que.option_3}}</p>\r\n                   <input type=\"radio\" [value]=3 name=\"radio\" [(ngModel)]=\"given_answer\">\r\n                   <span class=\"checkmark\"></span>\r\n                 </label>\r\n                 <label class=\"container\">\r\n                   <p>{{que.option_4}}</p>\r\n                   <input type=\"radio\" [value]=4 name=\"radio\" [(ngModel)]=\"given_answer\">\r\n                   <span class=\"checkmark\"></span>\r\n                 </label>\r\n               </div>\r\n             </div>\r\n           </form>\r\n\r\n           <div class=\"row\" style=\"margin-top: 20px;\">\r\n             <div class=\"col-md-12 offset-4\">\r\n               <button class=\"btn btnsubmit\" value=\"submit\" (click)=\"submit($event.target.value)\">Submit</button>\r\n             </div>\r\n           </div>\r\n           <div class=\"row\" style=\"margin-top: 20px;\">\r\n             <div class=\"col-md-12 offset-5\">\r\n               <button style=\"color:#FF4F33;font-size: 16px;\" value=\"skip\" (click)=\"skip($event.target.value)\">Skip the question</button>\r\n             </div>\r\n           </div>\r\n         </div>\r\n       </div>\r\n     </div>\r\n   </div>\r\n </div>\r\n <footer class=\"footer\" style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n   &copy; <a style=\"color:black;font-size: 16px;\">Minemark Solutions Private Limited</a> 2019 </footer>\r\n\r\n"

/***/ }),

/***/ "./src/app/Dashboard/demo/demo.component.ts":
/*!**************************************************!*\
  !*** ./src/app/Dashboard/demo/demo.component.ts ***!
  \**************************************************/
/*! exports provided: DemoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DemoComponent", function() { return DemoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DemoComponent = /** @class */ (function () {
    function DemoComponent(toastr) {
        this.toastr = toastr;
        this.que = [];
        this.QuestionCount = 0;
        this.menu = [
            { valueData: "What is the output of 2*1 = ?", option_1: '2', option_2: '2', option_3: '6', option_4: '8', value: '1' },
            { valueData: "What is the output of 2*2 = ?", option_1: '8', option_2: '6', option_3: '4', option_4: '10', value: '2' },
            { valueData: "What is the output of 2*3 = ?", option_1: '6', option_2: '10', option_3: '12', option_4: '8', value: '3' },
            { valueData: "What is the output of 2*4 = ?", option_1: '4', option_2: '8', option_3: '10', option_4: '6', value: '4' },
            { valueData: "What is the output of 2*5 = ?", option_1: '14', option_2: '10', option_3: '6', option_4: '2', value: '5' },
            { valueData: "What is the output of 2*6 = ?", option_1: '8', option_2: '2', option_3: '10', option_4: '12', value: '6' },
            { valueData: "What is the output of 2*7 = ?", option_1: '10', option_2: '16', option_3: '14', option_4: '12', value: '7' },
            { valueData: "What is the output of 2*8 = ?", option_1: '16', option_2: '12', option_3: '10', option_4: '8', value: '8' },
            { valueData: "What is the output of 2*9 = ?", option_1: '14', option_2: '18', option_3: '8', option_4: '12', value: '9' },
            { valueData: "What is the output of 2*10 = ?", option_1: '16', option_2: '14', option_3: '10', option_4: '20', value: '10' }
        ];
    }
    DemoComponent.prototype.ngOnInit = function () {
        this.que = this.menu[0];
    };
    DemoComponent.prototype.submit = function (value) {
        this.duration = this.leftTime;
        console.log("Time left", this.duration);
        if (this.QuestionCount < (this.menu.length - 1)) {
            if (value != '') {
                // this.QuestionCount = 0;
                ++this.QuestionCount;
                this.que = this.menu[this.QuestionCount];
            }
            else {
                this.toastr.error("Please Choose the answer!");
            }
        }
        this.toastr.error("");
    };
    //   onNotify(event) {
    //     this.timer = event;
    //     if (this.timer == 1000) {
    //       var submitData = { candi_skillset: this.candi_skillset, given_answer: this.given_answer, question_content_id: this.question_content_id, status: 3, date: this.myFormattedDate };
    //       this.auth.postDetail('/mash_mcq_assessment_answer', this.token, submitData).then((data) =>
    //       {
    //         this.valueData = data.json().question;
    //         this.option_1 = data.json().option_1;
    //         this.option_2 = data.json().option_2;
    //         this.option_3 = data.json().option_3;
    //         this.option_4 = data.json().option_4;
    //         this.question_content_id = data.json().id;
    //         this.given_answer = '';
    //         this.route.navigateByUrl('/feedback');
    //     }
    //     if (this.timer == 300000) {
    //       this.toastr.error("You Have Only Five Minutes");
    //     }
    //   }
    // }
    //   cancel() {
    //   }
    DemoComponent.prototype.skip = function (value) {
        if (this.QuestionCount < (this.menu.length - 1)) {
            if (value != '') {
                // this.QuestionCount = 0;
                ++this.QuestionCount;
                this.que = this.menu[this.QuestionCount];
            }
            else {
                this.toastr.error("Please Choose the answer!");
            }
        }
        this.toastr.error("");
    };
    DemoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-demo',
            template: __webpack_require__(/*! ./demo.component.html */ "./src/app/Dashboard/demo/demo.component.html"),
            styles: [__webpack_require__(/*! ./demo.component.css */ "./src/app/Dashboard/demo/demo.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]])
    ], DemoComponent);
    return DemoComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/document-vault/document-vault.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/Dashboard/document-vault/document-vault.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Dashboard/document-vault/document-vault.component.html":
/*!************************************************************************!*\
  !*** ./src/app/Dashboard/document-vault/document-vault.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-full-layout></app-full-layout>\r\n<div class=\"container\">\r\n  <div class=\"row\" style=\"text-align: center\">\r\n    <div class=\"col-md-12\">\r\n      <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/webappComingSoon.png\">\r\n      <h3>\r\n        We are proud to serve this feature shortly\r\n      </h3>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n\r\n<footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n    &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/document-vault/document-vault.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/Dashboard/document-vault/document-vault.component.ts ***!
  \**********************************************************************/
/*! exports provided: DocumentVaultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentVaultComponent", function() { return DocumentVaultComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DocumentVaultComponent = /** @class */ (function () {
    function DocumentVaultComponent() {
    }
    DocumentVaultComponent.prototype.ngOnInit = function () {
    };
    DocumentVaultComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-document-vault',
            template: __webpack_require__(/*! ./document-vault.component.html */ "./src/app/Dashboard/document-vault/document-vault.component.html"),
            styles: [__webpack_require__(/*! ./document-vault.component.css */ "./src/app/Dashboard/document-vault/document-vault.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DocumentVaultComponent);
    return DocumentVaultComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/feedbackform/feedbackform.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/Dashboard/feedbackform/feedbackform.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:hover {\r\n\r\n    cursor:pointer;\r\n    color: red;\r\n}\r\ntextarea.ng-invalid {\r\n    background-color: pink;\r\n}\r\n/* The container */\r\n.container {\r\n    display: block;\r\n    position: relative;\r\n    padding-left: 35px;\r\n    margin-bottom: 12px;\r\n    cursor: pointer;\r\n    font-size: 18px;;\r\n    -webkit-user-select: none;\r\n    -moz-user-select: none;\r\n    -ms-user-select: none;\r\n    user-select: none;\r\n    font-family: sans-serif;\r\n}\r\n/* Hide the browser's default radio button */\r\n.container input {\r\n    position: absolute;\r\n    opacity: 0;\r\n    cursor: pointer;\r\n}\r\n/* Create a custom radio button */\r\n.checkmark {\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    height: 25px;\r\n    width: 25px;\r\n    background-color: #eee;\r\n    border-radius: 50%;\r\n}\r\n/* On mouse-over, add a grey background color */\r\n.container:hover input ~ .checkmark {\r\n    background-color: #ccc;\r\n}\r\n/* When the radio button is checked, add a blue background */\r\n.container input:checked ~ .checkmark {\r\n    background-color: #FF4F33;\r\n}\r\n/* Create the indicator (the dot/circle - hidden when not checked) */\r\n.checkmark:after {\r\n    content: \"\";\r\n    position: absolute;\r\n    display: none;\r\n}\r\n/* Show the indicator (dot/circle) when checked */\r\n.container input:checked ~ .checkmark:after {\r\n    display: block;\r\n}\r\n.checked {\r\n    color: #FF4F33;\r\n  }\r\n/* Style the indicator (dot/circle) */\r\n.container .checkmark:after {\r\n \ttop: 9px;\r\n\tleft: 9px;\r\n\twidth: 8px;\r\n\theight: 8px;\r\n\tborder-radius: 50%;\r\n\tbackground: white;\r\n}\r\n.btnsubmit{\r\n    background-color: #FF4F33;width: 179px;font-size: 18px;\r\n              height: 50px;color: #ffffff;border-radius: 25px;\r\n}"

/***/ }),

/***/ "./src/app/Dashboard/feedbackform/feedbackform.component.html":
/*!********************************************************************!*\
  !*** ./src/app/Dashboard/feedbackform/feedbackform.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\n<div class=\"container\" style=\"margin-left: 12%;\">\n  <div class=\"row\">\n    <div class=\"col-md-12\" style=\"text-align:center;\">\n      <h3 style=\"font-weight:bold;font-family: sans-serif \">Share your feedback</h3>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <div class=\"card bg-light\" style=\"background-color: #FFFFFF;\">\n        <div class=\"card-body\">\n          <p style=\"font-family: sans-serif;font-weight: bold;font-size: 20px;\">How would you like to rate this test</p>\n          <p style=\"font-family: sans-serif;font-size: 16px;\">Please select your rating choice</p>\n        </div>\n      </div>\n    </div>\n  </div>\n  <br>\n  <form [formGroup]=\"myform\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <div class=\"card bg-light\">\n          <div class=\"card-body\">\n            <label class=\"container\">\n              <p>Mind boggling</p>\n              <input type=\"radio\" checked=\"checked\" [(ngModel)]=\"give_star\" formControlName=\"give_star\" [value]=5 name=\"give_star\">\n              <span class=\"checkmark\"></span>\n              <div class=\"row\">\n                <div class=\"col-md-12 offset-6\" style=\"margin-top:-30px;\">\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                </div>\n              </div>\n            </label>\n          </div>\n        </div>\n        <div class=\"card bg-light\">\n          <div class=\"card-body\">\n            <label class=\"container\">\n              <p>Excellent</p>\n              <input type=\"radio\" checked=\"checked\" [(ngModel)]=\"give_star\" formControlName=\"give_star\" [value]=4 name=\"give_star\">\n              <span class=\"checkmark\"></span>\n              <div class=\"row\">\n                <div class=\"col-md-12 offset-6\" style=\"margin-top:-30px;\">\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                </div>\n              </div>\n            </label>\n          </div>\n        </div>\n        <div class=\"card bg-light\">\n          <div class=\"card-body\">\n            <label class=\"container\">\n              <p>Good</p>\n              <input type=\"radio\" checked=\"checked\" [(ngModel)]=\"give_star\" formControlName=\"give_star\" [value]=3 name=\"give_star\">\n              <span class=\"checkmark\"></span>\n              <div class=\"row\">\n                <div class=\"col-md-12 offset-6\" style=\"margin-top:-30px;\">\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                </div>\n              </div>\n            </label>\n          </div>\n        </div>\n        <div class=\"card bg-light\">\n          <div class=\"card-body\">\n            <label class=\"container\">\n              <p>Okay-ish</p>\n              <input type=\"radio\" checked=\"checked\" [(ngModel)]=\"give_star\" formControlName=\"give_star\" [value]=2 name=\"give_star\">\n              <span class=\"checkmark\"></span>\n              <div class=\"row\">\n                <div class=\"col-md-12 offset-6\" style=\"margin-top:-30px;\">\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span class=\"fa fa-star checked\"></span>\n                  <span class=\"fa fa-star checked\"></span>\n                </div>\n              </div>\n            </label>\n          </div>\n        </div>\n        <div class=\"card bg-light\">\n          <div class=\"card-body\">\n            <label class=\"container\">\n              <p>Need Improvement</p>\n              <input type=\"radio\" checked=\"checked\" [(ngModel)]=\"give_star\" formControlName=\"give_star\" [value]=1 name=\"give_star\">\n              <span class=\"checkmark\"></span>\n              <div class=\"row\">\n                <div class=\"col-md-12 offset-6\" style=\"margin-top:-30px;\">\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span>&nbsp;&nbsp;&nbsp;</span>\n                  <span class=\"fa fa-star checked \"></span>\n                </div>\n              </div>\n            </label>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row\" style=\"margin-top:5px;margin-left: 2px;\">\n      <div class=\"card\">\n        <div class=\"card-body\">\n          <div class=\"col-md-12 bg-light\">\n            <h4>Additional Comment (max 200 characters)</h4>\n          </div>\n          <div class=\"col-md-12\">\n            <textarea type=\"text\" class=\"form-group\" style=\"width:100%;\" formControlName=\"give_comment\" name=\"give_comment\" [(ngModel)]=\"give_comment\" maxlength=\"200\">\n\n\n\n          </textarea>\n          </div>\n        </div>\n      </div>\n\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-12\" style=\"margin-top:5px;text-align:center\">\n        <button id=\"btnDone\" class=\"btn btn-success\" type=\"button\"  (click)=\"done(candi_skillset)\">DONE</button>\n      </div>\n\n    </div>\n  </form><app-footer></app-footer>\n</div>\n\n<!-- <footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\n  &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer> -->\n"

/***/ }),

/***/ "./src/app/Dashboard/feedbackform/feedbackform.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/Dashboard/feedbackform/feedbackform.component.ts ***!
  \******************************************************************/
/*! exports provided: FeedbackformComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackformComponent", function() { return FeedbackformComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FeedbackformComponent = /** @class */ (function () {
    function FeedbackformComponent(share, auth, activatedRoute, router) {
        this.share = share;
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.candidateData = [];
        this.data = [];
    }
    FeedbackformComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.candi_skillset = option['id'];
            _this.token = localStorage.getItem('token');
        });
        this.auth.get('/candi_profile_details', this.token).then(function (list) {
            _this.candidateData = list.json().result;
            _this.candi_name = _this.candidateData.candi_fullname;
        }).catch(function (err) {
        });
        this.val = Math.floor(1000 + Math.random() * 9000);
        this.myform = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            give_star: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            give_comment: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
        });
    };
    FeedbackformComponent.prototype.done = function (id) {
        this.token = localStorage.getItem('token');
        this.data = [this.candi_name + "_" + this.val, this.candi_skillset, this.give_star, this.give_comment];
        this.share.setData(this.data);
        this.router.navigateByUrl('/result_test/' + id);
    };
    FeedbackformComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-feedbackform',
            template: __webpack_require__(/*! ./feedbackform.component.html */ "./src/app/Dashboard/feedbackform/feedbackform.component.html"),
            styles: [__webpack_require__(/*! ./feedbackform.component.css */ "./src/app/Dashboard/feedbackform/feedbackform.component.css")]
        }),
        __metadata("design:paramtypes", [_services_sharedata_service__WEBPACK_IMPORTED_MODULE_4__["SharedataService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], FeedbackformComponent);
    return FeedbackformComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/home/home.component.css":
/*!***************************************************!*\
  !*** ./src/app/Dashboard/home/home.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card_shadow{\r\n  box-shadow:        0 4px 6px -6px #000;\r\n}\r\n.container {\r\n  display: block;\r\n  position: relative;\r\n  padding-left: 35px;\r\n  margin-bottom: 12px;\r\n  cursor: pointer;\r\n  font-size: 18px;;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  font-family: sans-serif;\r\n}\r\n/* .container {\r\n    align-items: center;\r\n    display: flex;\r\n    justify-content: center;\r\n    min-height: 100vh;\r\n    width: 100vw;\r\n  } */\r\n/*\r\n  .content {\r\n    max-width: 60rem;\r\n    text-align: center;\r\n  }\r\n\r\n  .box {\r\n    background-color: #fff;\r\n    border-radius: 3px;\r\n    height: 48px;\r\n    width: 48px;\r\n    margin: 0 auto;\r\n    margin-top: 5rem;\r\n  }\r\n\r\n  .animations {\r\n    margin-top: 3rem;\r\n  } */\r\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 100px;\r\n  height: 34px;\r\n}\r\n.switch input {\r\n  opacity: 0;\r\n  width: 0;\r\n  height: 0;\r\n}\r\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: .4s;\r\n}\r\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 26px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: .4s;\r\n}\r\ninput:checked + .slider {\r\n  background-color: #FF3333;\r\n}\r\ninput:focus + .slider {\r\n  box-shadow: 0 0 1px #FF3333;\r\n}\r\ninput:checked + .slider:before {\r\n  -webkit-transform: translateX(26px);\r\n  transform: translateX(26px);\r\n}\r\n/* Rounded sliders */\r\n.slider.round {\r\n  border-radius: 34px;\r\n}\r\n.slider.round:before {\r\n  border-radius: 50%;\r\n}\r\nfieldset\r\n\t{\r\n\t\tborder: 1px solid #ddd !important;\r\n\t\tmargin: 0;\r\n\t\tmin-width: 0;\r\n\t\tpadding: 10px;\r\n\t\tposition: relative;\r\n\t\tborder-radius:4px;\r\n\t\tbackground-color:#f5f5f5;\r\n\t\tpadding-left:10px!important;\r\n\t}\r\nlegend\r\n\t\t{\r\n\t\t\tfont-size:18px;\r\n            font-weight:bold;\r\n            text-align: center;\r\n\t\t\tmargin-bottom: 0px;\r\n\t\t\tfont-family: sans-serif;\r\n\t\t\twidth: 90%;\r\n\t\t\theight: 60px;\r\n      background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\r\n\t\t\tborder: 1px solid #ddd;\r\n\t\t\tborder-radius: 4px;\r\n            padding: 20px 5px 5px 10px;\r\n            color: #f5f5f5;\r\n\t\t\t/* background-color: #ffffff; */\r\n\t\t}\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/Dashboard/home/home.component.html":
/*!****************************************************!*\
  !*** ./src/app/Dashboard/home/home.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\"  >\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\">\r\n      <div class=\"card bg-light pointer-cursor card_shadow\"  routerLink=\"/update_profile\"\r\n      id=\"Professional_Profile\">\r\n        <div class=\"card-body\" style=\"margin-top: 30px;\">\r\n          <div style=\"text-align:center;\">\r\n            <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/profile+(2).png\" />\r\n          </div>\r\n          <div style=\"text-align:center;\">\r\n            <label> <p>Professional Profile</p> </label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <div class=\"card bg-light pointer-cursor card_shadow\"   routerLink=\"/document\" id=\"Document_Vault\">\r\n        <div class=\"card-body\" style=\"margin-top: 30px;\">\r\n          <div style=\"text-align:center;\">\r\n            <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/document+vault.png\" />\r\n          </div>\r\n          <div style=\"text-align:center;\">\r\n            <label> <p>Document Vault</p> </label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <div\r\n        class=\"card bg-light pointer-cursor card_shadow\" routerLink=\"/rating\" id=\"Rate_Your_Skill\">\r\n        <div class=\"card-body\" style=\"margin-top: 30px;\">\r\n          <div style=\"text-align:center;\">\r\n            <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/rate+your+skill.png\" />\r\n          </div>\r\n          <div style=\"text-align:center;\">\r\n            <label> <p>Rate your skill</p> </label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\">\r\n      <div\r\n        class=\"card bg-light pointer-cursor card_shadow\" id=\"Job_Offers\" routerLink=\"/job\">\r\n        <div class=\"card-body\" style=\"margin-top: 30px;\">\r\n          <div style=\"text-align:center;\">\r\n            <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/job+offers.png\" />\r\n          </div>\r\n          <div style=\"text-align:center;\">\r\n            <label> <p>Job Offers</p> </label>\r\n            <div *ngIf=\"unseenData.length > 0\">\r\n              <a>{{ unseenData.offer_count }}</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <div\r\n        class=\"card bg-light pointer-cursor card_shadow\" routerLink=\"/interview\" id=\"Interviews\">\r\n        <div class=\"card-body\" style=\"margin-top: 30px;\">\r\n          <div style=\"text-align:center;\">\r\n            <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/interviews.png\" />\r\n          </div>\r\n          <div style=\"text-align:center;\">\r\n            <label> <p>Interviews</p> </label>\r\n            <div *ngIf=\"interviewscount.length > 0\">\r\n              <a>{{ interviewscount.interview_count }} Upcoming</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <div class=\"card bg-light pointer-cursor card_shadow\" routerLink=\"/mentor\" id=\"Mentorship\">\r\n        <div class=\"card-body\" style=\"margin-top: 30px;\">\r\n          <div style=\"text-align:center;\">\r\n            <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/mentorship.png\" />\r\n          </div>\r\n          <div style=\"text-align:center;\">\r\n            <label> <p>Mentorship</p> </label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n\r\n  <div class=\"row\" *ngIf=\"previousData\">\r\n    <div class=\"col-md-12\">\r\n      <div>\r\n        <div class=\"card card_shadow\">\r\n          <div class=\"card-body\">\r\n            <div class=\"span4\">\r\n              <img style=\"float: left;\" src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/job-alert-icon.svg\" />\r\n              <div class=\"content-heading\"><h3>Job offers</h3></div>\r\n            </div>\r\n           <p style=\"clear: both;\"></p>\r\n          <div class=\"row\">&nbsp;</div>\r\n            <div class=\"row\">\r\n              <div class=\"col-md-1\">\r\n                <div class=\"\" style=\"width:50px;height:50px;\">\r\n                  <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/avatar.png\"\r\n                    style=\"width:50px;height:50px;\" class=\"img-circle\" alt=\"Profile Photo\"/>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-8\" >\r\n                <p class=\"card-title\">{{ previousData.corp_post_job_position_name }} at\r\n                  {{ previousData.corp_name }}</p> \r\n                \r\n                <!-- <div>\r\n                  <button type=\"button\" class=\"btn btn-danger\" id=\"Respond\" >Respond</button>\r\n                </div> -->\r\n              </div>\r\n              <div class=\"col-md-3\">\r\n                <div class=\"card-text\" >\r\n                  <span class=\"pull-right\">\r\n                    <a style=\"color:#F24540;\"  class=\"pointer-cursor\" id=\"View_all\"  routerLink=\"/job\">View All</a>\r\n                </span>\r\n                  \r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n  <div class=\"row\" *ngIf=\"upcomingInterview\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"content-heading\"><h3 style=\"margin-left: 20px;\">Upcoming interviews</h3></div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-1\">\r\n              <div class=\"\" style=\"width:50px;height:50px;background-color: #FFCC66; \tborder: 1px solid #FF3333;\">\r\n                <p style=\"text-align:center;font-size: 14px; \">\r\n                  {{ upcomingInterview.sched_interview_date | date: \"d\" }}\r\n                </p>\r\n                  <p style=\"text-align:center;font-size: 14px;\">\r\n                  {{\r\n                    upcomingInterview.sched_interview_date\r\n                      | date: \"LLL\"\r\n                      | uppercase\r\n                  }}\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-8\" >\r\n              <p class=\"card-title\">\r\n                {{ upcomingInterview.corp_reg_corp_name }}\r\n              </p>\r\n              <p class=\"card-text\">\r\n                {{ upcomingInterview.sched_interview_time }} •\r\n                {{ upcomingInterview.sched_interview_date | date: \"d MMM y\" }}\r\n              </p>\r\n              <!-- <div >\r\n                <button class=\"btn btn-danger\" id=\"Respont_Interview\">Respond</button>\r\n              </div> -->\r\n            </div>\r\n            <div class=\"col-md-3\">\r\n                <div class=\"card-text\" >\r\n                  <span class=\"pull-right\">\r\n                    <a style=\"color:#F24540;\"  class=\"pointer-cursor\" id=\"View_all\"  routerLink=\"/interview\">View All</a>\r\n                </span>\r\n                  \r\n                </div>\r\n              </div>\r\n          </div> \r\n        </div>\r\n       \r\n       \r\n      </div>\r\n    </div>\r\n  </div>\r\n  <app-footer></app-footer>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/Dashboard/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(toast, auth) {
        this.toast = toast;
        this.auth = auth;
        this.interviewscount = { interview_count: "" };
        this.interviews = [];
        this.unseenData = [];
        this.unseen_data = { offer_count: "" };
        this.detailsDash = [];
        this.upcomingInterview = {
            sched_interview_date: "",
            sched_interview_time: "",
            corp_reg_corp_name: ""
        };
        this.previousData = {
            corp_post_job_position_name: "",
            job_offer_confirmation: ""
        };
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        function stopTab(e) {
            var evt = e || window.event;
            if (evt.keyCode === 9) {
                return false;
            }
        }
        this.token = localStorage.getItem("token");
        this.auth
            .get("/candi_dashboard", this.token)
            .then(function (list) {
            _this.candidateData = list.json();
            _this.upcomingInterview = list.json()["upcoming_interview"][0];
            _this.previousData = list.json()["previous_job_offer"][0];
            _this.interviewscount = list.json()["interviews"][0];
            _this.unseenData = list.json()["unseen_data"][0];
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])("document:keydown", ["$event"]),
        __metadata("design:type", Object)
    ], HomeComponent.prototype, "token", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-home",
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/Dashboard/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/Dashboard/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/home/splash-screen/splash-screen.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/Dashboard/home/splash-screen/splash-screen.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html,body,div,span,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,abbr,address,cite,code,del,dfn,em,img,ins,kbd,q,samp,small,strong,sub,sup,var,b,i,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,figcaption,figure,footer,header,hgroup,menu,nav,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent}\r\nbody{line-height:1}\r\narticle,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}\r\nnav ul{list-style:none}\r\nblockquote,q{quotes:none}\r\nblockquote:before,blockquote:after,q:before,q:after{content:none}\r\na{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent;text-decoration:none}\r\nmark{background-color:#ff9;color:#000;font-style:italic;font-weight:bold}\r\ndel{text-decoration:line-through}\r\nabbr[title],dfn[title]{border-bottom:1px dotted;cursor:help}\r\ntable{border-collapse:collapse;border-spacing:0}\r\nhr{display:block;height:1px;border:0;border-top:1px solid #ccc;margin:1em 0;padding:0}\r\ninput,select{vertical-align:middle}\r\nli{list-style:none}\r\n\r\n"

/***/ }),

/***/ "./src/app/Dashboard/home/splash-screen/splash-screen.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/Dashboard/home/splash-screen/splash-screen.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<particles [style]=\"myStyle\" [width]=\"width\" [height]=\"height\" [params]=\"myParams\"></particles>"

/***/ }),

/***/ "./src/app/Dashboard/home/splash-screen/splash-screen.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/Dashboard/home/splash-screen/splash-screen.component.ts ***!
  \*************************************************************************/
/*! exports provided: SplashScreenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashScreenComponent", function() { return SplashScreenComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SplashScreenComponent = /** @class */ (function () {
    function SplashScreenComponent() {
        this.myStyle = {};
        this.myParams = {};
        this.width = 100;
        this.height = 100;
    }
    SplashScreenComponent.prototype.ngOnInit = function () {
        this.myStyle = {
            'position': 'fixed',
            'width': '100%',
            'height': '100%',
            'z-index': -1,
            'top': 0,
            'left': 0,
            'right': 0,
            'bottom': 0,
        };
        this.myParams = {
            particles: {
                number: {
                    value: 200,
                },
                color: {
                    value: '#ff0000'
                },
                shape: {
                    type: 'circle',
                },
                events: {
                    onhover: {
                        enable: 'true',
                        mode: 'repulse',
                    }
                },
                polygon: {
                    nb_sides: 5
                },
                opacity: {
                    value: 0.5,
                    random: false,
                    anim: {
                        enable: false,
                        speed: 1,
                        opacity_min: 0.1,
                        sync: false
                    }
                },
                size: {
                    value: 5,
                    random: true,
                    anim: {
                        enable: false,
                        speed: 40,
                        size_min: 0.1,
                        sync: false
                    }
                },
                line_linked: {
                    enable: true,
                    distance: 150,
                    opacity: 0.4,
                    width: 1
                },
                move: {
                    enable: true,
                    speed: 6,
                    random: false,
                    straight: false,
                    attract: {
                        enable: false,
                        rotateX: 600,
                        rotateY: 1200
                    }
                },
                interactivity: {
                    events: {
                        onhover: {
                            enable: true,
                            mode: 'repulse'
                        },
                        onclick: {
                            enable: true,
                            mode: 'push'
                        },
                        resize: true
                    },
                    modes: {
                        grab: {
                            distance: 400,
                            line_linked: {
                                opacity: 1
                            }
                        },
                        bubble: {
                            distance: 400,
                            size: 40,
                            duration: 2,
                            opacity: 8,
                            speed: 3
                        },
                        repulse: {
                            distance: 200
                        },
                        push: {
                            particles_nb: 4
                        },
                        remove: {
                            particles_nb: 2
                        }
                    }
                },
                retina_detect: true,
                config_demo: {
                    hide_card: false,
                    background_color: '#b61924',
                    background_repeat: 'no-repeat',
                    background_size: 'cover'
                },
            },
        };
    };
    ;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('parent'),
        __metadata("design:type", Object)
    ], SplashScreenComponent.prototype, "name", void 0);
    SplashScreenComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-splash-screen',
            template: __webpack_require__(/*! ./splash-screen.component.html */ "./src/app/Dashboard/home/splash-screen/splash-screen.component.html"),
            styles: [__webpack_require__(/*! ./splash-screen.component.css */ "./src/app/Dashboard/home/splash-screen/splash-screen.component.css")],
        }),
        __metadata("design:paramtypes", [])
    ], SplashScreenComponent);
    return SplashScreenComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/interview-details/interview-details.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/Dashboard/interview-details/interview-details.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " /* card hr {\r\n    margin: 30px -20px 20px;\r\n    border: 0;\r\n    border-top: 1px solid #c9c7c7;\r\n} */\r\np{\r\n    font-family: sans-serif;\r\n    font-size: 16px;\r\n    margin-top: 20px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/Dashboard/interview-details/interview-details.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/Dashboard/interview-details/interview-details.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-sidenavbar></app-sidenavbar> -->\r\n<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left:12%\" *ngIf=\"data\">\r\n  <!-- <h2 style=\"font-family: sans-serif;font-size: 20px;\">Interview <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/if_angle-double-right-arrow-forward_3209389.jpg\"\r\n      style=\"width: 10px;height: 10px;\"> <span> Interview Details</span></h2> -->\r\n  <nav aria-label=\"breadcrumb\">\r\n    <ol class=\"breadcrumb\">\r\n      <li class=\"breadcrumb-item\"><a id=\"det_Interview\" href=\"#interview\">Interview</a></li>\r\n      <li class=\"breadcrumb-item active\" aria-current=\"page\">Interview Details</li>\r\n    </ol>\r\n  </nav>\r\n  <div class=\"card\">\r\n    <div class=\"card-body\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <p>{{data.profile_summary}}</p>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n\r\n          <p style=\"font-weight: bold\">Post</p>\r\n          <p>{{data.corp_position_name | titlecase}}</p>\r\n\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <p style=\"font-weight: bold\">Company</p>\r\n          <p>{{data.corp_reg_corp_name | titlecase}}</p>\r\n        </div>\r\n      </div>\r\n\r\n      <hr class=\"divider\">\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <p style=\"font-weight: bold\">Interview Mode</p>\r\n          <p>{{data.sched_interview_type}}</p>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <p style=\"font-weight: bold\">Interview Date & Time</p>\r\n          <p>{{data.sched_interview_date|date : 'd MMM y'}} . {{data.sched_interview_time }}</p>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <hr>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <p style=\"font-weight: bold\">\r\n            Don’t Forget To Carry Documents\r\n          </p>\r\n        </div>\r\n        <div class=\"col-md-12\" *ngFor=\"let doc of topic\">\r\n          <p>{{doc}}</p>\r\n\r\n        </div>\r\n\r\n      </div>\r\n      <hr class=\"divider\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <p style=\"font-weight: bold\">Note By Employer</p>\r\n\r\n          <p>{{data.sched_note}}</p>\r\n\r\n        </div>\r\n      </div>\r\n\r\n      <hr>\r\n\r\n      <div class=\"row\" style=\"margin-top: 50px;\">\r\n        <div class=\"col-md-9\">\r\n\r\n        </div>\r\n        <div class=\"col-md-3 pointer-cursor\">\r\n          <a id=\"View_job_details\" data-toggle=\"modal\" (click)=\"getView(data)\" data-target=\"#exampleModalLong\"\r\n            style=\"color: #F24540;font-family: sans-serif;font-size: 16px;\">View\r\n            Job Details</a>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row\" style=\"margin-top:30px;\"\r\n        [hidden]=\"data.sched_candi_confirmation === 'rejected' || data.sched_candi_confirmation === 'accepted'\">\r\n        <div class=\"col-md-12 offset-4\">\r\n          <p>Would Like to accept this interview request?</p>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row\" style=\"margin-top:30px;\"\r\n        [hidden]=\"data.sched_candi_confirmation === 'rejected' || data.sched_candi_confirmation === 'accepted'\">\r\n        <div class=\"col-md-4\" style=\"text-align: center;\r\n        justify-content: center;\">\r\n          <button class=\"btn btn-success btn_shadow\" value=\"accepted\"\r\n            (click)=\"acceptInterview($event.target.value)\">Accept</button>\r\n        </div>\r\n        <!-- (click)=\"rejectInterview($event.target.value)\" -->\r\n        <div class=\"col-md-4\" style=\"text-align: center;\r\n        justify-content: center;\">\r\n          <button class=\"btn btn-warning btn_shadow\" data-toggle=\"modal\" (click)=\"getView(data)\"\r\n            data-target=\"#exampleModal\">Reject</button>\r\n        </div>\r\n        <div class=\"col-md-4\" style=\"text-align: center;\r\n        justify-content: center;\">\r\n          <button class=\"btn btn-warning btn_shadow\" data-toggle=\"modal\" (click)=\"getView(data)\"\r\n            data-target=\"#exampleModal1\">Reschedule</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n\r\n\r\n\r\n<div class=\"modal fade\" id=\"exampleModalLong\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\" data-backdrop=\"static\" data-keyboard=\"false\">\r\n  <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-body\" *ngIf=\"data\">\r\n        <div>\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <form style=\"margin-left:40px;\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\" style=\"text-align: center;\">\r\n              <h4>View Job Details</h4>\r\n            </div>\r\n            <!-- <div class=\"col-md-12\">\r\n              <p><strong>Job Summary</strong></p>\r\n              <p style=\"text-align: justify;text-justify: inter-word;\">{{data.corp_job_description | titlecase}}</p>\r\n            </div> -->\r\n          </div>\r\n          <!-- <div class=\"col-md-12\">\r\n\r\n\r\n\r\n            </div> -->\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <p><strong>Company Name</strong></p>\r\n\r\n\r\n              <span style=\"\">{{data.corp_reg_corp_name}}</span>\r\n\r\n            </div>\r\n          </div>\r\n\r\n\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <p><strong>Job Location</strong></p>\r\n              <p>{{data.sched_interview_address}}</p>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <p><strong>Designation</strong></p>\r\n              <p>{{data.corp_position_name | titlecase}}</p>\r\n            </div>\r\n          </div>\r\n          <br>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <p><strong>Experienced Required</strong></p>\r\n              <p>{{data.min_work_ex_year}}-{{data.max_work_ex_year}} Years</p>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <p><strong>Required Skills</strong></p>\r\n              <span *ngFor=\"let skillcor of skillarrayinfo\">{{skillcor}} </span>\r\n              <span>,</span>\r\n            </div>\r\n          </div>\r\n\r\n        </form>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n  <app-footer></app-footer>\r\n</div>\r\n\r\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\" data-backdrop=\"static\" data-keyboard=\"false\">\r\n  <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-body\" *ngIf=\"data\">\r\n        <div>\r\n          <button type=\"button\" class=\"close\" style=\"font-size: 40px;\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <form style=\"margin-left:40px;\">\r\n          <div class=\"row\">\r\n            <div class=\"col-lg-12\" style=\"text-align:center;font-weight: bold\">\r\n              <h4>Reject Interview Reason</h4>\r\n            </div>\r\n\r\n          </div>\r\n          <div class=\"row\" style=\"margin-top:30px;\">\r\n            <div class=\"col-lg-12 form-check\" *ngFor=\"let cat of categories\">\r\n              <input class=\"form-check-input\" (change)=\"onChange(cat.name, $event.target.checked)\" name=\"{{ cat.name }}\"\r\n                type=\"checkbox\" id=\"{{ cat.name }}\" />\r\n              <label class=\"form-check-label\" for=\"{{ cat.name }}\" style=\"margin-left:20px;\">\r\n                {{ cat.name }}\r\n              </label>\r\n            </div>\r\n            <div class=\"col-lg-12\">\r\n              <input type=\"checkbox\" class=\"form-check-label\" (change)=\"toggleVisibility($event)\" id=\"Doc5\" />\r\n              Other\r\n            </div>\r\n            <div class=\"col-lg-4\">\r\n              <span *ngIf=\"marked\"><textarea [(ngModel)]=other [ngModelOptions]=\"{standalone: true}\"></textarea></span>\r\n              <span *ngIf=\"!marked\"></span>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-lg-12\" style=\"text-align:center\">\r\n              <button class=\"btn btn-primary\" value=\"rejected\" data-dismiss=\"modal\"\r\n                (click)=\"rejectInterview($event.target.value)\"> Submit</button>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"modal fade\" id=\"exampleModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\" data-backdrop=\"static\" data-keyboard=\"false\">\r\n  <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-body\" *ngIf=\"data\">\r\n        <div>\r\n          <button type=\"button\" class=\"close\" style=\"font-size: 40px;\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <form style=\"margin-left:40px;\">\r\n          <div class=\"row\">\r\n            <div class=\"col-lg-12\" style=\"text-align:center;font-weight: bold\">\r\n              <h4>Reschedule Interview </h4>\r\n            </div>\r\n\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-lg-6\">\r\n              <label>Interview date</label>\r\n              <input type=\"date\" class=\"form-control\"\r\n              [ngModelOptions]=\"{standalone: true}\"\r\n              [(ngModel)]=\"data.sched_interview_date\" value=\"{{data.sched_interview_date}}\">\r\n            </div>\r\n            <div class=\"col-lg-6\">\r\n              <label>Interview Time</label>\r\n              <input type=\"time\" class=\"form-control\"\r\n              [ngModelOptions]=\"{standalone: true}\"\r\n              [(ngModel)]=\"data.sched_interview_time\" value=\"{{data.sched_interview_time}}\">\r\n            </div>\r\n          </div>\r\n          <br><br>\r\n          <div class=\"row\" >\r\n            <div class=\"col-lg-12\" style=\"text-align:center\">\r\n              <button class=\"btn btn-primary\" value=\"rejected\" data-dismiss=\"modal\"\r\n                (click)=\"candiRescheduleInterview(data.sched_interview_date,data.sched_interview_time)\"> Submit</button>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/interview-details/interview-details.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/Dashboard/interview-details/interview-details.component.ts ***!
  \****************************************************************************/
/*! exports provided: InterviewDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterviewDetailsComponent", function() { return InterviewDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var InterviewDetailsComponent = /** @class */ (function () {
    function InterviewDetailsComponent(auth, toster, activatedRoute, share, router) {
        this.auth = auth;
        this.toster = toster;
        this.activatedRoute = activatedRoute;
        this.share = share;
        this.router = router;
        this.data = this.share.getData();
        this.topic = [];
        this.skillarray = [];
        this.candidateList = [];
        this.responseData = [];
        this.skillarrayinfo = [];
        this.corskill = [];
        this.rejectFormArray = [];
        this.otherreject = false;
        this.marked = false;
        this.categories = [
            { name: "Not interested in the job", id: 1 },
            { name: "Not well", id: 2 },
            { name: "Busy in Office work/meetings", id: 3 },
            { name: "Reschedule my interview", id: 4 },
        ];
        if (this.data) {
            this.data;
            console.log(this.data);
            this.skillarray = this.data.corp_skills_id.split(",");
            this.topic = this.data.sched_required_document.split(".");
        }
        else {
        }
    }
    InterviewDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth
            .get("/skills", this.token)
            .then(function (list) {
            _this.candidateList = list.json().result;
            var body = JSON.stringify(_this.candidateList);
            var stringify = JSON.parse(body);
            for (var i = 0; i < stringify.length; i++) {
                _this.responseData = stringify[i]["id"];
                for (var j = 0; j < _this.skillarray.length; j++) {
                    if (parseInt(_this.skillarray[j]) === _this.responseData) {
                        _this.skillarray[j] = {
                            id: stringify[i]["id"],
                            skill: stringify[i]["skill"]
                        };
                        _this.skillarrayinfo.push(stringify[i]["skill"]);
                        _this.corskill = stringify[i]["skill"];
                    }
                }
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    InterviewDetailsComponent.prototype.getView = function (data) {
        this.share.setData(data);
        console.log("data", data);
    };
    InterviewDetailsComponent.prototype.toggleVisibility = function (e) {
        this.marked = e.target.checked;
    };
    InterviewDetailsComponent.prototype.submit = function () {
        if (this.other != " ") {
            this.rejectFormArray.push(this.other);
        }
        else {
            this.rejectFormArray.push(this.other);
        }
        console.log(this.rejectFormArray);
    };
    InterviewDetailsComponent.prototype.onChange = function (reject, isChecked) {
        if (isChecked) {
            this.rejectFormArray.push(reject);
        }
        else {
            var index = this.rejectFormArray.indexOf(reject);
            this.rejectFormArray.splice(index, 1);
        }
        console.log("selected", this.rejectFormArray);
    };
    InterviewDetailsComponent.prototype.acceptInterview = function (e) {
        var _this = this;
        var DATE = new Date();
        // tslint:disable-next-line:prefer-const
        this.datePipe = new _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]("en-US");
        this.setdate = this.datePipe.transform(DATE, "dd-MM-yyyy");
        var id = this.data.candi_id;
        if (e == "accepted") {
            var accpect = confirm("Are you sure?");
            if (accpect == true) {
                this.auth
                    .putResult("/candi_interview_confirmation/" + id, {
                    candi_confirmation: e,
                    job_code: this.data.job_code,
                    corp_id: this.data.corp_reg_corp_id,
                    date: this.setdate
                }, this.token)
                    .then(function (data) {
                    _this.interviewList = data.json().message;
                    _this.toster.success("Accepted");
                });
                this.router.navigateByUrl("/interview");
            }
            else {
            }
        }
    };
    InterviewDetailsComponent.prototype.rejectInterview = function (e) {
        var _this = this;
        var reject;
        var id = this.data.candi_id;
        var DATE = new Date();
        // tslint:disable-next-line:prefer-const
        console.log(this.data);
        console.log(e);
        this.datePipe = new _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]("en-US");
        this.setdate = this.datePipe.transform(DATE, "dd-MM-yyyy");
        // if (this.other != '') {
        //   this.rejectFormArray.push(this.other);
        //   reject = {
        //     candi_confirmation: e,
        //     job_code: this.data.job_code,
        //     corp_id: this.data.corp_reg_corp_id,
        //     reject_date: this.setdate,
        //     reject_reason: this.rejectFormArray.toString()
        //   }
        //   console.log("If", reject);
        // } else {
        //   reject = {
        //     candi_confirmation: e,
        //     job_code: this.data.job_code,
        //     corp_id: this.data.corp_reg_corp_id,
        //     reject_date: this.setdate,
        //     reject_reason: this.rejectFormArray.toString()
        //   }
        //   console.log("else", reject);
        // }
        if (e == "rejected") {
            debugger;
            if (this.other != "") {
                this.rejectFormArray.push(this.other);
                this.auth
                    .putResult("/candi_interview_confirmation/" + id, {
                    candi_confirmation: e,
                    job_code: this.data.job_code,
                    corp_id: this.data.corp_reg_corp_id,
                    reject_date: this.setdate,
                    reject_reason: this.rejectFormArray.toString()
                }, this.token)
                    .then(function (data) {
                    _this.interviewList = data.json().message;
                    _this.toster.error("Rejected");
                });
                this.router.navigateByUrl("/interview");
            }
            else {
                this.auth
                    .putResult("/candi_interview_confirmation/" + id, {
                    candi_confirmation: e,
                    job_code: this.data.job_code,
                    corp_id: this.data.corp_reg_corp_id,
                    reject_date: this.setdate,
                    reject_reason: this.rejectFormArray.toString()
                }, this.token)
                    .then(function (data) {
                    _this.interviewList = data.json().message;
                    _this.toster.error("Rejected");
                });
                this.router.navigateByUrl("/interview");
            }
        }
    };
    InterviewDetailsComponent.prototype.candiRescheduleInterview = function (date, time) {
        var _this = this;
        console.log("date", date);
        console.log("time", time);
        this.auth.postDetail('/candiRescheuleInterview', this.token, {
            interview_date: date,
            interview_time: time,
            id: this.data.sched_candi_id, job_code: this.data.job_code
        }).then(function (data) {
            _this.toster.success(data.json().message);
        });
    };
    InterviewDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-interview-details",
            template: __webpack_require__(/*! ./interview-details.component.html */ "./src/app/Dashboard/interview-details/interview-details.component.html"),
            styles: [__webpack_require__(/*! ./interview-details.component.css */ "./src/app/Dashboard/interview-details/interview-details.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_sharedata_service__WEBPACK_IMPORTED_MODULE_2__["SharedataService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], InterviewDetailsComponent);
    return InterviewDetailsComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/interviews/interviews.component.css":
/*!***************************************************************!*\
  !*** ./src/app/Dashboard/interviews/interviews.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "label{\r\n    font-family: sans-serif;\r\n    font-size: 14px;\r\n}\r\na\r\n{\r\n    font-family: sans-serif;\r\n    font-size: 14px;\r\n}\r\ncard{\r\n    font-size: 14px;\r\n    font-family: sans-serif;\r\n}\r\n.card_shadow{\r\n    box-shadow:        0 4px 6px -6px #000;\r\n  }\r\np{padding: 0px;\r\n}\r\nbtn_shadow{\r\n    box-shadow:        0 4px 6px -6px #000; \r\n}\r\n.date_box{\r\n    width:50px;\r\n    height:50px;\r\n    background-color: #FFCC66; \r\n    border: 1px solid #FF3333;\r\n    box-shadow:        0 4px 6px -6px #000;\r\n  }\r\n.card-title{\r\n    font-size: 18px;\r\n    font-family: sans-serif;\r\n}\r\n.card-text{\r\n    font-size: 14px;\r\n    font-family: sans-serif;\r\n}\r\nbutton{\r\n    font-size: 14px;\r\n    font-family: sans-serif;\r\n    width: 179px;\r\n    box-shadow: 0 2px 16px 6px #CCCCCC;\r\n}\r\nli>a:hover{\r\n    background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%) !important;\r\n    color: #111;\r\n}\r\nli>a:active{\r\n    background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%) !important;\r\n    color: #111;\r\n}\r\nhref{\r\n    color: #111;\r\n    background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%) !important;\r\n}"

/***/ }),

/***/ "./src/app/Dashboard/interviews/interviews.component.html":
/*!****************************************************************!*\
  !*** ./src/app/Dashboard/interviews/interviews.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left:12%\">\r\n\r\n  <h3>Interviews</h3>\r\n  <ul class=\"nav nav-tabs\">\r\n    <li class=\"active\">\r\n      <a data-toggle=\"tab\" href=\"#home\" style=\"font-family: sans-serif;font-size: 18px;\">ALL</a>\r\n    </li>\r\n    <li><a data-toggle=\"tab\" href=\"#menu1\" (click)=\"getSchedule()\" style=\"font-family: sans-serif;font-size: 18px;\">Schedule</a></li>\r\n    <li><a data-toggle=\"tab\" href=\"#menu2\" (click)=\"getPast()\" style=\"font-family: sans-serif;font-size: 18px;\">Past</a></li>\r\n  </ul>\r\n\r\n  <div class=\"tab-content\">\r\n    <div id=\"home\" class=\"tab-pane fade in active\">\r\n      <div *ngIf=\"!pendingList || pendingList?.length === 0\" style=\"text-align:center;margin-top: 50px;\">\r\n        <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/ic-empty-box.png\" style=\"width:300px;height: 300px;\">\r\n        <div style=\"text-align:center\">\r\n          <h2>No Interview Right Now</h2>\r\n          <h4>You're up-to-date</h4>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"pendingList || pendingList?.length > 0\">\r\n        <div class=\"card card_shadow\" *ngFor=\"let data of pendingList\">\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-1\">\r\n                <div class=\"date_box\" >\r\n                  <p style=\"text-align:center;\">{{data.sched_interview_date|date:'d'}}</p>\r\n                  <p style=\"text-align:center;\">{{data.sched_interview_date|date:'LLL'|uppercase}}</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-8\" style=\"margin: 0px;\">\r\n                <p class=\"card-title\">{{data.corp_reg_corp_name}} </p>\r\n                <p class=\"card-text\">{{data.sched_interview_time}} &nbsp; {{data.sched_interview_date|date : 'd MMM y'}}</p>\r\n                <p class=\"card-text\">Status : {{data.sched_candi_confirmation | titlecase }}</p>\r\n\r\n              </div>\r\n              <div class=\"col-md-3\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div style=\"margin-left: 30px;padding: 10px;\">\r\n            <!-- <button class=\"btn btn-primary\" style=\"background-color: #308651;font-family: sans-serif;font-size:14px;border-radius: 25px;\">Respond</button> -->\r\n          </div>\r\n          <div class=\"pointer-cursor\" style=\"text-align:right;margin-top:-35px;margin-right: 30px;padding: 10px;\">\r\n            <a id=\"view_interview_det\" style=\"color: #F24540;\" (click)=\"somefunction(data)\">View Details</a>\r\n          </div>\r\n        </div>\r\n        <br>\r\n\r\n\r\n\r\n      </div>\r\n      <br>\r\n    </div>\r\n    <div id=\"menu1\" class=\"tab-pane fade\">\r\n      <div>\r\n\r\n        <div *ngIf=\"!scheduleList || scheduleList?.length === 0 \" style=\"text-align:center;margin-top: 50px;\">\r\n          <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/ic-empty-box.png\" style=\"width:300px;height: 300px;\">\r\n          <div style=\"text-align:center\">\r\n            <h2>No Schedule Interview Right Now</h2>\r\n            <h4>You're up-to-date</h4>\r\n          </div>\r\n\r\n        </div>\r\n        <div *ngIf=\"scheduleList || scheduleList?.length > 0 \">\r\n          <div class=\"card\" *ngFor=\"let data of scheduleList\">\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col-md-1\">\r\n                  <div class=\"\" style=\"width:50px;height:50px;background-color: #FFCC66; \tborder: 1px solid #FF3333;\">\r\n                    <p style=\"text-align:center;font-size: 14px;\">{{data.sched_interview_date|date:'d'}}</p>\r\n                    <p style=\"text-align:center;font-size: 14px;\">{{data.sched_interview_date|date:'LLL'|uppercase}}</p>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-md-8\" style=\"margin: 0px;\">\r\n                  <p class=\"card-title\">{{data.corp_reg_corp_name}}</p>\r\n                  <p class=\"card-text\">{{data.sched_interview_time}} &nbsp; {{data.sched_interview_date|date : 'd MMM\r\n                    y' }}</p>\r\n                  <p class=\"card-text\">Status : {{data.sched_candi_confirmation | titlecase }}</p>\r\n\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div style=\"margin-left: 30px;padding: 10px;\">\r\n              <!-- <button class=\"btn btn-primary\" style=\"background-color: #308651;border-radius: 25px;font-family: sans-serif;font-size:14px;\">Respond</button> -->\r\n            </div>\r\n            <div class=\"pointer-cursor\" style=\"text-align:right;margin-top:-35px;margin-right: 30px;padding: 10px;\">\r\n              <a style=\"color: #F24540;\" (click)=\"somefunction(data)\">View Details</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br>\r\n    </div>\r\n    <div id=\"menu2\" class=\"tab-pane fade\">\r\n      <div>\r\n        <div *ngIf=\"!pastList || pastList?.length === 0\" style=\"text-align:center;margin-top: 50px;\">\r\n          <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/ic-empty-box.png\" style=\"width:300px;height: 300px;\">\r\n          <div style=\"text-align:center\">\r\n            <h2>No Past Interview Right Now</h2>\r\n            <h4>You're up-to-date</h4>\r\n          </div>\r\n\r\n        </div>\r\n        <div *ngIf=\"pastList || pastList?.length > 0\">\r\n          <div class=\"card\" *ngFor=\"let data of pastList\">\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col-md-1\">\r\n                  <div class=\"\" style=\"width:50px;height:50px;background-color: #FFCC66; \tborder: 1px solid #FF3333;\">\r\n                    <p style=\"text-align:center;font-size: 14px; \">{{data.sched_interview_date|date:'d'}}</p>\r\n                    <p style=\"text-align:center;font-size: 14px;\">{{data.sched_interview_date|date:'LLL'|uppercase}}</p>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-md-8\" style=\"margin: 0px;\">\r\n                  <p class=\"card-title\">{{data.corp_reg_corp_name}}</p>\r\n                  <p class=\"card-text\">{{data.sched_interview_time}} &nbsp; {{data.sched_interview_date|date : 'd MMM\r\n                    y'}}</p>\r\n                  <p class=\"card-text\">Status : {{data.sched_candi_confirmation | titlecase }}</p>\r\n\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div style=\"margin-left: 30px;padding: 10px;\">\r\n              <!-- <button class=\"btn btn-primary\" style=\"background-color: #308651;border-radius: 25px;font-family: sans-serif;font-size:14px;\">Respond</button> -->\r\n            </div>\r\n            <div class=\"pointer-cursor\" style=\"text-align:right;margin-top:-35px;margin-right: 30px;padding: 10px;\">\r\n              <a style=\"color: #F24540;\" (click)=\"somefunction(data)\">View Details</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n      <br>\r\n    </div><app-footer></app-footer>\r\n  </div>\r\n  <!-- <footer class=\"app-footer\" style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n    &copy; <a  style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer> -->\r\n    \r\n"

/***/ }),

/***/ "./src/app/Dashboard/interviews/interviews.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/Dashboard/interviews/interviews.component.ts ***!
  \**************************************************************/
/*! exports provided: InterviewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterviewsComponent", function() { return InterviewsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InterviewsComponent = /** @class */ (function () {
    function InterviewsComponent(auth, share, router) {
        this.auth = auth;
        this.share = share;
        this.router = router;
    }
    InterviewsComponent.prototype.somefunction = function (data) {
        this.share.setData(data);
        this.router.navigateByUrl('/interview_details');
    };
    InterviewsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth.get('/candi_interviews_all', this.token)
            .then(function (data) {
            _this.pendingList = data.json().result;
            console.log(_this.pendingList);
        });
    };
    InterviewsComponent.prototype.getSchedule = function () {
        var _this = this;
        this.auth.get('/candi_interviews_schedule', this.token)
            .then(function (data) {
            _this.scheduleList = data.json().result;
        });
    };
    InterviewsComponent.prototype.getPast = function () {
        var _this = this;
        this.auth.get('/candi_interviews_past', this.token)
            .then(function (data) {
            _this.pastList = data.json().result;
        });
    };
    InterviewsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-interviews',
            template: __webpack_require__(/*! ./interviews.component.html */ "./src/app/Dashboard/interviews/interviews.component.html"),
            styles: [__webpack_require__(/*! ./interviews.component.css */ "./src/app/Dashboard/interviews/interviews.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _services_sharedata_service__WEBPACK_IMPORTED_MODULE_2__["SharedataService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], InterviewsComponent);
    return InterviewsComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/job-details/job-details.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/Dashboard/job-details/job-details.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Dashboard/job-details/job-details.component.html":
/*!******************************************************************!*\
  !*** ./src/app/Dashboard/job-details/job-details.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\n<div class=\"container\" *ngIf=\"data\" style=\"margin-left: 200px\">\n    <nav aria-label=\"breadcrumb\">\n        <ol class=\"breadcrumb\">\n          <li class=\"breadcrumb-item\"><a id=\"det_Job\" href=\"#job\">Job</a></li>\n          <li class=\"breadcrumb-item active\" aria-current=\"page\">Job Details</li>\n        </ol>\n      </nav>\n  <!-- <h2 style=\"font-family: sans-serif;font-size: 20px;\">Job <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/if_angle-double-right-arrow-forward_3209389.jpg\"\n      style=\"width: 10px;height: 10px;\"> <span> Job Details</span></h2> -->\n  <div class=\"card\">\n    <div class=\"card-body\" style=\"margin-left:30px;\">\n      <div class=\"row\">\n        <div class=\"col-md-12\">\n          <h4 style=\"text-align: center;\">View Job Details</h4>\n        </div>\n        <!-- <div class=\"col-md-12\">\n          <p><strong>Job Summary</strong></p>\n        </div>\n        <div class=\"col-md-12\">\n          <p style=\"text-align: justify;text-justify: inter-word;font-size: 16px;\">{{data.corp_post_job_description}}</p>\n        </div> -->\n        <div class=\"col-md-12\">\n          <p><strong>Company Name</strong></p>\n        </div>\n        <div class=\"col-md-12\">\n          <div>\n            <a style=\"width: 600px;font-size: 16px;\">{{data.corp_name}}</a>\n          </div>\n        </div>\n      </div>\n      <br>\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <p><strong>Job Location</strong></p>\n          <p style=\"font-size: 16px;\">{{data.sched_joining_location}}</p>\n        </div>\n        <div class=\"col-md-6\">\n          <p><strong>Designation</strong></p>\n          <p style=\"font-size: 16px;\">{{data.corp_post_job_position_name|titlecase}}</p>\n        </div>\n      </div>\n      <br>\n\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <p><strong>Joining Date</strong> </p>\n          <p style=\"font-size: 16px;\">{{data.sched_joining_date|date : 'd MMM y' }}</p>\n        </div>\n        <div class=\"col-md-6\">\n          <p><strong>Experienced Required</strong></p>\n          <p style=\"font-size: 16px;\">{{data.min_work_ex_year}} - {{data.max_work_ex_year}} years</p>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <p><strong>Professional Skills Required</strong></p>\n          <div class=\"row\">\n              <div style=\"font-size: 16px;margin-left: 20px;\" *ngFor=\"let data1 of skillarrayinfo\">{{data1}},</div>\n          </div>\n\n        </div>\n        <div class=\"col-md-6\">\n            <p><strong>CTC</strong></p>\n            <div class=\"row\">\n                <div style=\"font-size: 16px;margin-left: 20px;\">{{data.ctc}} LPA</div>\n            </div>\n        </div>\n      </div>\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <p><strong>Manager</strong> </p>\n              <p style=\"font-size: 16px;\">{{data.reporting_manager}}</p>\n            </div>\n      </div>\n      <div class=\"row\" style=\"margin-top:30px;\" [hidden]=\"data.job_offer_confirmation === 'rejected' || data.job_offer_confirmation === 'accepted'\">\n        <div class=\"col-md-6\" style=\"text-align: center;justify-content: center;\">\n          <button class=\"btn btn-primary\" id=\"offerAccept\" value=\"accepted\" style=\"width: 179px;\tbox-shadow: 0 1px 5px 3px #CCCCCC;background-color: #308651;border-radius: 25px;font-family: sans-serif;font-size:14px;\"\n            (click)=\"accept($event.target.value)\">Accept</button>\n        </div>\n        <div class=\"col-md-6\" style=\"text-align: center;justify-content: center;\">\n          <button class=\"btn btn-primary\" id=\"offerReject\" value=\"rejected\" style=\"width: 179px;\tbox-shadow: 0 1px 5px 3px #CCCCCC;background-color:  #F24540;border-radius: 25px;font-family: sans-serif;font-size:14px;\"\n            (click)=\"rejected($event.target.value)\">Reject</button>\n        </div>\n      </div>\n    </div>\n  </div><app-footer></app-footer>\n</div>\n\n<!-- <footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\n    &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer> -->\n"

/***/ }),

/***/ "./src/app/Dashboard/job-details/job-details.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/Dashboard/job-details/job-details.component.ts ***!
  \****************************************************************/
/*! exports provided: JobDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobDetailsComponent", function() { return JobDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobDetailsComponent = /** @class */ (function () {
    function JobDetailsComponent(share, toster, auth, router) {
        this.share = share;
        this.toster = toster;
        this.auth = auth;
        this.router = router;
        this.data = this.share.getData();
        this.skillarray = [];
        this.responseData = [];
        this.candidateList = [];
        this.candidateskill1 = localStorage.getItem('candi_skillset1');
        this.candidateskill2 = localStorage.getItem('candi_skillset2');
        this.candidateskill3 = localStorage.getItem('candi_skillset3');
        this.addskill = [parseInt(this.candidateskill1), parseInt(this.candidateskill2), parseInt(this.candidateskill3)];
        this.idnameskill = [];
        this.addallskill = [];
        this.skillarrayinfo = [];
        this.skill_id1 = [];
        this.topic = [];
        if (this.data) {
            this.data;
        }
        else {
        }
    }
    JobDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.skill_id1 = this.data.corp_skills_id;
        this.topic = this.skill_id1.split(",");
        this.auth.get('/skills', this.token).then(function (list) {
            _this.candidateList = list.json().result;
            var body = JSON.stringify(_this.candidateList);
            var stringify = JSON.parse(body);
            for (var i = 0; i < stringify.length; i++) {
                _this.responseData = stringify[i]['id'];
                for (var j = 0; j < _this.topic.length; j++) {
                    if (parseInt(_this.topic[j]) === _this.responseData) {
                        _this.skillarray[j] = { id: stringify[i]['id'], skill: stringify[i]['skill'] };
                        _this.skillarrayinfo.push(stringify[i]['skill']);
                    }
                }
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    JobDetailsComponent.prototype.accept = function (e) {
        var _this = this;
        this.token = localStorage.getItem('token');
        if (e === "accepted") {
            this.auth.postDetail('/candi_job_offer_confirmation', this.token, {
                job_offer_confirmation: e,
                candi_id: this.data.candi_id,
                job_code: this.data.corp_post_job_code,
            }).then(function (data) {
                _this.accpected = data.json().message;
                _this.toster.success(data.json().message);
            });
            this.router.navigateByUrl('/job');
        }
    };
    JobDetailsComponent.prototype.rejected = function (e) {
        var _this = this;
        this.token = localStorage.getItem('token');
        if (e === "rejected") {
            this.auth.postDetail('/candi_job_offer_confirmation', this.token, {
                job_offer_confirmation: e,
                candi_id: this.data.candi_id,
                job_code: this.data.corp_post_job_code
            }).then(function (data) {
                _this.accpected = data.json().message;
                _this.toster.error(data.json().message);
            });
        }
        this.router.navigateByUrl('/job');
    };
    JobDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-details',
            template: __webpack_require__(/*! ./job-details.component.html */ "./src/app/Dashboard/job-details/job-details.component.html"),
            styles: [__webpack_require__(/*! ./job-details.component.css */ "./src/app/Dashboard/job-details/job-details.component.css")]
        }),
        __metadata("design:paramtypes", [_services_sharedata_service__WEBPACK_IMPORTED_MODULE_1__["SharedataService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], JobDetailsComponent);
    return JobDetailsComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/job-offer/job-offer.component.css":
/*!*************************************************************!*\
  !*** ./src/app/Dashboard/job-offer/job-offer.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-title{\r\n    font-size: 18px;\r\n    font-family:  sans-serif;\r\n}\r\n.card-text{\r\n    font-size: 14px;\r\n    font-family: sans-serif;\r\n}\r\na{\r\n    font-family:  sans-serif;\r\n}\r\nbutton{\r\n    font-family:  sans-serif;\r\n    font-size: 14px;\r\n}\r\nul.a {\r\n    list-style-type: circle;\r\n}\r\n"

/***/ }),

/***/ "./src/app/Dashboard/job-offer/job-offer.component.html":
/*!**************************************************************!*\
  !*** ./src/app/Dashboard/job-offer/job-offer.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left:12%\">\r\n  <h3>Job Offers</h3>\r\n  <br>\r\n  <div *ngIf=\"!jobData || jobData?.length === 0\" style=\"text-align:center\">\r\n    <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/ic-empty-box.png\" style=\"width:300px;height: 300px;\">\r\n    <div style=\"text-align:center\">\r\n      <h2>No Job Offer Right Now</h2>\r\n      <h4>You're up-to-date</h4>\r\n    </div>\r\n\r\n  </div>\r\n\r\n  <div>\r\n    <div class=\"card\" *ngFor=\"let job of jobData;\">\r\n      <div class=\"card-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-1\">\r\n            <div class=\"\" style=\"width:50px;height:50px;\">\r\n              <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/avatar.png\" style=\"width:50px;height:50px;\" class=\"img-circle\" alt=\"Profile Photo\">\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-8\" style=\"margin: 0px;\">\r\n            <p class=\"card-title\">{{job.corp_post_job_position_name}}</p>\r\n            <p class=\"card-text\" style=\"color: #FF492E\">{{job.corp_name}}</p>\r\n            <p class=\"card-text\"><a>Status :</a> {{job.job_offer_confirmation | titlecase}}</p>\r\n          </div>\r\n          <div class=\"col-md-3\">\r\n            <div class=\"card-text\" style=\"margin-left:90px;\">\r\n              <a style=\"color:#F24540;\" class=\"pointer-cursor\" id=\"jobOfferDetails\" data-toggle=\"modal\" data-target=\"#exampleModalLong\"\r\n                (click)=\"somefunction(job)\">View Details</a>\r\n            </div> \r\n          </div>\r\n        </div>\r\n        <div class=\"modal fade\" id=\"exampleModalLong\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n          aria-hidden=\"true\" data-backdrop=\"static\" data-keyboard=\"false\">\r\n          <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n            <div class=\"modal-content\">\r\n              <div class=\"modal-body\">\r\n                <div>\r\n                  <button type=\"button\" class=\"close\" style=\"font-size: 40px;\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                  </button>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-12\" style=\"text-align: center;\">\r\n                    <p style=\"font-size: 24px;\">View Job Details</p>\r\n                  </div>\r\n                  <div class=\"col-md-12\">\r\n                    <p style=\"font-size: 22px;\">Job Summary</p>\r\n                  </div>\r\n                  <div class=\"col-md-12\">\r\n                    <p style=\"text-align: justify;text-justify: inter-word;\">{{job.corp_post_job_position_name}}</p>\r\n                  </div>\r\n                  <div class=\"col-md-12\">\r\n                    <p style=\"font-size: 22px;\">Company Type</p>\r\n                  </div>\r\n                  <div class=\"col-md-12\">\r\n                    <div style=\"height: 10px;\twidth: 10px;background-color: #FF4B33;\">\r\n                      <a style=\"margin-left: 20px;width: 600px\">Information Technology,MNC </a>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <br>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br>\r\n    </div>\r\n  </div><app-footer></app-footer>\r\n</div>\r\n<!-- <footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n  &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer> -->\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/Dashboard/job-offer/job-offer.component.ts":
/*!************************************************************!*\
  !*** ./src/app/Dashboard/job-offer/job-offer.component.ts ***!
  \************************************************************/
/*! exports provided: JobOfferComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobOfferComponent", function() { return JobOfferComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var JobOfferComponent = /** @class */ (function () {
    function JobOfferComponent(auth, router, share) {
        this.auth = auth;
        this.router = router;
        this.share = share;
        this.jobs = [{}];
        this.p = 1;
        this.checkedList = [];
        this.datas = [];
        this.options = [
            { name: 'OptionA', value: '1', checked: true },
            { name: 'OptionB', value: '2', checked: false },
            { name: 'OptionC', value: '3', checked: true }
        ];
        //this.index();
    }
    Object.defineProperty(JobOfferComponent.prototype, "selectedOptions", {
        get: function () {
            return this.options
                .filter(function (opt) { return opt.checked; })
                .map(function (opt) { return opt.value; });
        },
        enumerable: true,
        configurable: true
    });
    JobOfferComponent.prototype.ngOnInit = function () {
        this.view();
    };
    JobOfferComponent.prototype.onChange = function (city) {
        alert(city.name);
    };
    JobOfferComponent.prototype.view = function () {
        var _this = this;
        this.auth.get('/candiJobOffers', this.token).then(function (list) {
            _this.jobData = list.json().result;
            console.log(_this.jobData);
            // console.log("this.jobData",this.jobData;
            // this.datas = list.json()['datas'];
        }).catch(function (err) {
            console.log("Error", err);
        });
    };
    JobOfferComponent.prototype.onCheckboxChange = function (option, event) {
        if (event.target.checked) {
            this.checkedList.push(option.id);
        }
        else {
            for (var i = 0; i < this.xyzlist.length; i++) {
                if (this.checkedList[i] == option.id) {
                    this.checkedList.splice(i, 1);
                }
            }
        }
    };
    JobOfferComponent.prototype.somefunction = function (data) {
        this.share.setData(data);
        this.router.navigateByUrl('/job-detail');
    };
    JobOfferComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-offer',
            template: __webpack_require__(/*! ./job-offer.component.html */ "./src/app/Dashboard/job-offer/job-offer.component.html"),
            styles: [__webpack_require__(/*! ./job-offer.component.css */ "./src/app/Dashboard/job-offer/job-offer.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_sharedata_service__WEBPACK_IMPORTED_MODULE_3__["SharedataService"]])
    ], JobOfferComponent);
    return JobOfferComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/mentorship/mentorship.component.css":
/*!***************************************************************!*\
  !*** ./src/app/Dashboard/mentorship/mentorship.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Dashboard/mentorship/mentorship.component.html":
/*!****************************************************************!*\
  !*** ./src/app/Dashboard/mentorship/mentorship.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<app-full-layout></app-full-layout>\r\n<div class=\"container\">\r\n  <div class=\"row\" style=\"text-align: center\">\r\n    <div class=\"col-md-12\">\r\n      <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/webappComingSoon.png\">\r\n      <h3>\r\n        We are proud to serve this feature shortly\r\n      </h3>\r\n    </div>\r\n  </div>\r\n  <app-footer></app-footer>\r\n</div>\r\n\r\n  <!-- <footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n      &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer> -->\r\n"

/***/ }),

/***/ "./src/app/Dashboard/mentorship/mentorship.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/Dashboard/mentorship/mentorship.component.ts ***!
  \**************************************************************/
/*! exports provided: MentorshipComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentorshipComponent", function() { return MentorshipComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MentorshipComponent = /** @class */ (function () {
    function MentorshipComponent() {
        this.locations = 'meerut';
    }
    MentorshipComponent.prototype.ngOnInit = function () {
        this.locations = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"](function (observer) {
            var error = observer.error;
            var watchId;
            if ('geolocation' in navigator) {
            }
            else {
                error('Geolocation not available');
            }
            return { unsubscribe: function () { navigator.geolocation.clearWatch(watchId); } };
        });
        var locationsSubscription = this.locations.subscribe({
            next: function (position) { console.log('Current Position: ', position); },
            error: function (msg) { console.log('Error Getting Location: ', msg); }
        });
        setTimeout(function () { locationsSubscription.unsubscribe(); }, 10000);
        console.log(this.locations);
    };
    MentorshipComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-mentorship',
            template: __webpack_require__(/*! ./mentorship.component.html */ "./src/app/Dashboard/mentorship/mentorship.component.html"),
            styles: [__webpack_require__(/*! ./mentorship.component.css */ "./src/app/Dashboard/mentorship/mentorship.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MentorshipComponent);
    return MentorshipComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/navbar/navbar.component.css":
/*!*******************************************************!*\
  !*** ./src/app/Dashboard/navbar/navbar.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);\r\n\r\n@import url(https://fonts.googleapis.com/css?family=Titillium+Web:300);\r\n\r\nul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n    overflow: hidden;\r\n    background-color: #ff6600;\r\n}\r\n\r\nli {\r\n    float: right;\r\n}\r\n\r\nli a {\r\n    display: inline-block;\r\n    color: white;\r\n    text-align: center;\r\n    padding: 14px 16px;\r\n    text-decoration: none;\r\n}\r\n\r\nli a:hover {\r\n    background-color: #111;\r\n    cursor:pointer;\r\n}\r\n\r\nbody {\r\n    font-family: \"Lato\", sans-serif;\r\n}\r\n\r\n.sidenav {\r\n    height: 100%;\r\n    width: 0;\r\n    position: fixed;\r\n    z-index: 1;\r\n    top: 0;\r\n    left: 0;\r\n    background-color: white;\r\n    overflow-x: hidden;\r\n    transition: 0.5s;\r\n    padding-top: 60px;\r\n}\r\n\r\n.sidenav a {\r\n    padding: 8px 8px 8px 32px;\r\n    text-decoration: none;\r\n    font-size: 25px;\r\n    color: black;\r\n    display: block;\r\n    transition: 0.3s;\r\n}\r\n\r\n.sidenav a:hover {\r\n    color: #ff6600;\r\n}\r\n\r\n.sidenav .closebtn {\r\n    position: absolute;\r\n    top: 0;\r\n    right: 25px;\r\n    font-size: 36px;\r\n    margin-left: 50px;\r\n}\r\n\r\n.badge-notify{\r\n    background:red;\r\n    position:relative;\r\n    top: -20px;\r\n    left: -35px;\r\n}\r\n\r\n@media screen and (max-height: 450px) {\r\n    .sidenav {padding-top: 15px;}\r\n    .sidenav a {font-size: 18px;}\r\n}\r\n\r\n.img{\r\n    margin-left: 43%;\r\n    margin-top: 2%;\r\n}\r\n\r\n.footer {\r\n        position: absolute;\r\n        left: 0;\r\n        bottom: 0;\r\n        width: 100%;\r\n        background-color: red;\r\n        color: white;\r\n        text-align: center;\r\n    }\r\n\r\n.customClass {\r\n        background-color: #dd3;\r\n        border-radius: 5px;\r\n        margin:5px;\r\n        width: 500px;\r\n    }\r\n\r\n.customClass .img-ul-upload {\r\n        background-color: #000 !important;\r\n    }\r\n\r\n.customClass .img-ul-clear {\r\n        background-color: #B819BB !important;\r\n    }\r\n\r\n.customClass .img-ul-drag-box-msg {\r\n        color: purple !important;\r\n    }\r\n\r\n.customClass .img-ul-container {\r\n        background-color: #FF6CAD !important;\r\n    }\r\n\r\n/* ----------------------------------------------------------------------------*/\r\n\r\n/* nav{\r\n    background-color: #ff6600;\r\n} */\r\n\r\n/* @import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);\r\n\r\n@import url(https://fonts.googleapis.com/css?family=Titillium+Web:300); */\r\n\r\n.fa-2x {\r\nfont-size: 2em;\r\n}\r\n\r\n.fa {\r\nposition: relative;\r\ndisplay: table-cell;\r\nwidth: 60px;\r\nheight: 36px;\r\ntext-align: center;\r\nvertical-align: middle;\r\nfont-size:20px;\r\n}\r\n\r\n.main-menu:hover,nav.main-menu.expanded {\r\nwidth:250px;\r\noverflow:visible;\r\n}\r\n\r\n.main-menu {\r\nbackground:white;\r\nborder-right:1px solid #e5e5e5;\r\nposition:fixed;\r\ntop:60px;\r\nbottom:0;\r\nheight:100%;\r\nleft:0;\r\nwidth:60px;\r\noverflow:hidden;\r\ntransition:width .05s linear;\r\n-webkit-transform:translateZ(0) scale(1,1);\r\nz-index:1000;\r\n}\r\n\r\n.main-menu>ul {\r\nmargin:7px 0;\r\n}\r\n\r\n.main-menu li {\r\nposition:relative;\r\ndisplay:block;\r\nwidth:250px;\r\n}\r\n\r\n.main-menu li>a {\r\nposition:relative;\r\ndisplay:table;\r\nborder-collapse:collapse;\r\nborder-spacing:0;\r\ncolor:#999;\r\nfont-family: Calibri;\r\nfont-size: 14px;\r\ntext-decoration:none;\r\n-webkit-transform:translateZ(0) scale(1,1);\r\ntransition:all .1s linear;\r\n  \r\n}\r\n\r\n.main-menu .nav-icon {\r\nposition:relative;\r\ndisplay:table-cell;\r\nwidth:60px;\r\nheight:36px;\r\ntext-align:center;\r\nvertical-align:middle;\r\nfont-size:18px;\r\n}\r\n\r\n.main-menu .nav-text {\r\nposition:relative;\r\ndisplay:table-cell;\r\nvertical-align:middle;\r\nwidth:190px;\r\n  font-family: 'Titillium Web', sans-serif;\r\n}\r\n\r\n.no-touch .scrollable.hover {\r\noverflow-y:hidden;\r\n}\r\n\r\n.no-touch .scrollable.hover:hover {\r\noverflow-y:auto;\r\noverflow:visible;\r\n}\r\n\r\na:hover,a:focus {\r\ntext-decoration:none;\r\n}\r\n\r\nnav {\r\n-webkit-user-select:none;\r\n-moz-user-select:none;\r\n-ms-user-select:none;\r\n-o-user-select:none;\r\nuser-select:none;\r\n}\r\n\r\nnav ul,nav li {\r\noutline:0;\r\nmargin:0;\r\npadding:0;\r\n}\r\n\r\n.main-menu li:hover>a,nav.main-menu li.active>a,.dropdown-menu>li>a:hover,.dropdown-menu>li>a:focus,.dropdown-menu>.active>a,.dropdown-menu>.active>a:hover,.dropdown-menu>.active>a:focus,.no-touch .dashboard-page nav.dashboard-menu ul li:hover a,.dashboard-page nav.dashboard-menu ul li.active a {\r\ncolor:#fff;\r\nbackground-color:#5fa2db;\r\n}\r\n\r\n.area {\r\nfloat: left;\r\nbackground: #e2e2e2;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n\r\n@font-face {\r\n  font-family: 'Titillium Web';\r\n  font-style: normal;\r\n  font-weight: 300;\r\n  src: local('Titillium WebLight'), local('TitilliumWeb-Light'), url(http://themes.googleusercontent.com/static/fonts/titilliumweb/v2/anMUvcNT0H1YN4FII8wpr24bNCNEoFTpS2BTjF6FB5E.woff) format('woff');\r\n}\r\n\r\n.body {\r\n    margin: 0;\r\n    font-size: 28px;\r\n    font-family: Arial, Helvetica, sans-serif;\r\n    \r\n    \r\n  }\r\n\r\n.header {\r\n    background-color: #f1f1f1;\r\n    padding: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n#navbar {\r\n    overflow: hidden;\r\n    background-color: #ff6600;\r\n    height:60px;\r\n  }\r\n\r\n#navbar a {\r\n    float: left;\r\n    display: block;\r\n    color: #f2f2f2;\r\n    text-align: center;\r\n    padding: 14px 16px;\r\n    text-decoration: none;\r\n    font-size: 17px;\r\n  }\r\n\r\n#navbar a:hover {\r\n    background-color: #ddd;\r\n    color: black;\r\n  }\r\n\r\n#navbar a.active {\r\n    background-color: rgb(21, 17, 226);\r\n    color: white;\r\n  }\r\n\r\n.content {\r\n    padding: 16px;\r\n  }\r\n\r\n.sticky {\r\n    position: -webkit-sticky;\r\n    position: sticky;\r\n    top: 0;\r\n    width: 100%;\r\n  }\r\n\r\n.sticky + .content {\r\n    padding-top: 60px;\r\n  }\r\n\r\n.sidenav {\r\n    height: 100%;\r\n    width: 160px;\r\n    position: fixed;\r\n    z-index: 1;\r\n    top: -1px;\r\n    left: 0;\r\n    background-color: #111;\r\n    overflow-x: hidden;\r\n    padding-top: 20px;\r\n}\r\n\r\n.sidenav a {\r\n    padding: 6px 8px 6px 16px;\r\n    text-decoration: none;\r\n    font-size: 25px;\r\n    color: #818181;\r\n    display: block;\r\n}\r\n\r\n.sidenav a:hover {\r\n    color: #f1f1f1;\r\n}\r\n\r\n/* @media screen and (max-height: 450px) {\r\n  .sidenav {padding-top: 15px;}\r\n  .sidenav a {font-size: 18px;}\r\n} */\r\n\r\n.footer {\r\n  position: fixed;\r\n  left: 0;\r\n  bottom: 0;\r\n  width: 100%;\r\n  background-color: #ff6600;\r\n  color: white;\r\n  text-align: center;\r\n}\r\n\r\n#searchbox{\r\n  margin-top:2%;\r\n  border: 1px solid black;\r\n}\r\n\r\nh4{\r\n  color:white;\r\n}\r\n\r\n.search-container{\r\n \r\n  display: block;\r\n  margin: 0 auto;\r\n }\r\n\r\ninput#search-bar{\r\n  margin: 0 auto;\r\n  width: 100%;\r\n  height: 45px;\r\n  padding: 0 20px;\r\n  \r\n  border: 1px solid #D0CFCE;\r\n  outline: none;\r\n margin-top:10px;\r\n }\r\n\r\n.search-icon{\r\n  position: relative;\r\n  float: right;\r\n  width: 75px;\r\n  height: 75px;\r\n  top: -62px;\r\n  right: -45px;\r\n  \r\n  margin-left: 200px;\r\n  margin-top:-5px;\r\n }\r\n\r\n/* //////////////// */\r\n\r\n.sidenav {\r\n    height: 100%;\r\n    width: 200px;\r\n    position: fixed;\r\n    z-index: 1;\r\n    top:60px;;\r\n    left: 0;\r\n    background-color: white;\r\n    overflow-x: hidden;\r\n    padding-top: 20px;\r\n}\r\n\r\n/* Style the sidenav links and the dropdown button */\r\n\r\n.sidenav a, .dropdown-btn {\r\n    padding: 6px 8px 6px 16px;\r\n    text-decoration: none;\r\n    font-size: 20px;\r\n    color: #818181;\r\n    display: block;\r\n    border: none;\r\n    background: none;\r\n    width: 100%;\r\n    text-align: left;\r\n    cursor: pointer;\r\n    outline: none;\r\n    font-family:Calibri;\r\n    font-size:18px;\r\n}\r\n\r\n/* On mouse-over */\r\n\r\n.sidenav a:hover, .dropdown-btn:hover {\r\n    color:#111;\r\n\r\n}\r\n\r\n/* Main content */\r\n\r\n.main {\r\n    margin-left: 200px; /* Same as the width of the sidenav */\r\n    font-size: 20px; /* Increased text to enable scrolling */\r\n    padding: 0px 10px;\r\n}\r\n\r\n/* Add an active class to the active dropdown button */\r\n\r\n.active {\r\n    background-color: #ff6600;\r\n    color: #111;\r\n}\r\n\r\n/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */\r\n\r\n.dropdown-container {\r\n    display: none;\r\n    background-color: white;\r\n    padding-left: 8px;\r\n}\r\n\r\n/* Optional: Style the caret down icon */\r\n\r\n.fa-caret-down {\r\n    float: right;\r\n    padding-right: 8px;\r\n}\r\n\r\n/* Some media queries for responsiveness */\r\n\r\n@media screen and (max-height: 450px) {\r\n    .sidenav {padding-top: 15px;}\r\n    .sidenav a {font-size: 18px;}\r\n}\r\n\r\n.dropbtn {\r\n  background-color: #4CAF50;\r\n  color: white;\r\n  padding: 16px;\r\n  font-size: 16px;\r\n  border: none;\r\n}\r\n\r\n.dropdown {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\n\r\n.dropdown-content {\r\n  left:190px;\r\n  top:1px;\r\n  display: none;\r\n  position: absolute;\r\n  background-color: white;\r\n  min-width: 160px;\r\n  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n  z-index: 1;\r\n}\r\n\r\n.dropdown-content a {\r\n  color: black;\r\n  padding: 12px 16px;\r\n  text-decoration: none;\r\n  display: block;\r\n}\r\n\r\n.dropdown-content a:hover {background-color:#5fa2db;}\r\n\r\n.dropdown:hover .dropdown-content {display: block;}\r\n\r\n.dropdown:hover .dropbtn {background-color: #3e8e41;}\r\n\r\n.sidebar {\r\n  padding: 1rem;\r\n \r\n  margin-bottom: 1rem;\r\n \r\n  \r\n }\r\n\r\n.sidebar--left {\r\n  min-height: 120px;\r\n \r\n \r\n }"

/***/ }),

/***/ "./src/app/Dashboard/navbar/navbar.component.html":
/*!********************************************************!*\
  !*** ./src/app/Dashboard/navbar/navbar.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <img style=\"display:table-cell;margin: auto; vertical-align:middle; text-align:center\" src=\"../../assets/MineMark Logo-Orange.svg\" >\r\n      \r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n          <div class=\"col-md-12\" style=\"text-align: center\">\r\n              <h2>Minemark Solutions Private Limited </h2>\r\n            </div>\r\n      </div>\r\n<!-- <div class=\"col-md col-sm\">\r\n<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\r\n\r\n<div  style=\"background-color: #ff6600 ;height: 30px;\" >\r\n\r\n    <img class=\"img\" style=\"height: 30px;\" src=\"../../assets/light_mm_logo.png\" routerLink=\"/home\">\r\n\r\n    <br><br>\r\n</div> -->\r\n<!-- <image-upload class=\"customClass\"\r\n   [max]=\"1\"\r\n   buttonCaption=\"Upload\"\r\n   [extensions]=\"['jpeg','png']\"\r\n    url=\"https://httpbin.org/status/200\"\r\n   (removed)=\"onRemoved($event)\"\r\n   (uploadFinished)=\"onUploadFinished($event)\"\r\n   (uploadStateChanged)=\"onUploadStateChanged($event)\">\r\n</image-upload> -->\r\n\r\n<!-- <div id=\"mySidenav\" class=\"sidenav\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3\">\r\n                <div class=\"profile-sidebar\">\r\n                    <div class=\"profile-userpic\">\r\n                            <div *ngIf=\"!image\">\r\n                                    <img src=\"../../../assets/avatar.png\" class=\"img-circle\" width=\"150px\" alt=\"Profile Photo\" routerLink=\"/upload_photo\">\r\n                                  </div>\r\n                                  <div *ngIf=\"image\">\r\n                                        <img src=\"{{image}}\" class=\"img-circle\" width=\"150px\" routerLink=\"/upload_photo\">\r\n                                    </div>\r\n                    </div>\r\n                    <div class=\"profile-usertitle\">\r\n                        <div class=\"profile-usertitle-name\">\r\n                            Marcus Doe\r\n                        </div>\r\n                        <div class=\"profile-usertitle-job\">\r\n                            Developer\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"profile-userbuttons\">\r\n                        <button type=\"button\" class=\"btn btn-success btn-sm\">Follow</button>&nbsp;\r\n                        <button type=\"button\" class=\"btn btn-info btn-sm\">Message</button>\r\n                    </div>\r\n                </div></div></div></div>\r\n    <a href=\"javascript:void(0)\" class=\"closebtn\" (click)=\"closeNav()\">&times;</a>\r\n    <a *ngFor=\"let side of sideMenu\" routerLink=\"{{side.path}}\" [ngStyle]=\"{'font-size.px': 16, cursor:pointer}\" ><i class=\"{{side.class}}\"> {{side.name}}</i></a>\r\n\r\n    <p class=\"text-center\">\r\n        <button  (click)=\"logout()\" class=\"btn btn-danger\" [ngStyle]=\"{'font-size.px': 16}\">Logout</button>\r\n    </p>\r\n</div>\r\n -->\r\n\r\n<!-- <div class=\"col-md-12 \">\r\n\r\n\r\n    <ul>\r\n        <!-- <span style=\"font-size:30px;cursor:pointer;\" (click)=\"openNav()\">&#9776;</span>\r\n\r\n        <i class=\"glyphicon glyphicon-bell\" style=\"font-size:24px\"><button class=\"badge\" style=\"margin-top: -36px\" (click)=\"destroy()\">{{length}}</button></i> \r\n       <li *ngFor=\"let menus of menu\" [ngStyle]=\"{'font-size.px': 16}\"><a routerLink=\"{{menus.path}}\"><img src=\"{{menus.src}}\">{{menus.name}}</a></li>\r\n    </ul>\r\n    <hr>\r\n    </div>\r\n</div> -->\r\n<!-- <div class=\"sidenav sidebar sidebar-left\" style=\"position:fixed;\">\r\n        <a routerLink=\"/dashboard\" class=\"active\">Dashboard</a>\r\n        <button class=\"dropdown-btn\">Jobs \r\n          <i class=\"fa fa-caret-down\"></i>\r\n        </button>\r\n        <div class=\"dropdown-container\">\r\n          <a routerLink=\"/corp_post_job_create\">Post Job</a>\r\n          <a routerLink=\"/corp_post_job_list\">Posted Jobs</a>\r\n          <a routerLink=\"/job_status_approved\">Approved </a>\r\n          <a routerLink=\"/job_status_pending\">Pending</a>\r\n          <a routerLink=\"/job_status_rejected\">Rejected</a>\r\n          <!-- <a routerLink=\"#\">On Hold</a> \r\n        </div>\r\n    \r\n        <button class=\"dropdown-btn\">Candidate \r\n            <i class=\"fa fa-caret-down\"></i>\r\n          </button>\r\n          <div class=\"dropdown-container\">\r\n            <a routerLink=\"/recommended_candidates\">Recommended Candidate</a>\r\n            \r\n          </div>\r\n          <button class=\"dropdown-btn\">Interviews \r\n              <i class=\"fa fa-caret-down\"></i>\r\n            </button>\r\n            <div class=\"dropdown-container\">\r\n              <a routerLink=\"/interview_schedule\">Schedule Interview</a>\r\n              <a routerLink=\"/dashboard\">List of interviews(Accepted by candidates)</a>\r\n              <a routerLink=\"/dashboard\">Projectwise </a>\r\n              <a routerLink=\"/dashboard\">Feedback </a>\r\n            </div>\r\n            <button class=\"dropdown-btn\">Reports \r\n                <i class=\"fa fa-caret-down\"></i>\r\n              </button>\r\n              <div class=\"dropdown-container\">\r\n                <a href=\"#\"></a>\r\n                \r\n              </div>\r\n              \r\n      </div>\r\n\r\n -->\r\n"

/***/ }),

/***/ "./src/app/Dashboard/navbar/navbar.component.ts":
/*!******************************************************!*\
  !*** ./src/app/Dashboard/navbar/navbar.component.ts ***!
  \******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var ng_animate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng-animate */ "./node_modules/ng-animate/fesm5/ng-animate.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(router, auth, activate) {
        var _this = this;
        this.router = router;
        this.auth = auth;
        this.activate = activate;
        this.menu = {};
        this.params = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["URLSearchParams"]();
        this.jobData = { candi_profile_photo: '' };
        this.notification = 3;
        this.sideMenu = {};
        this.dropdown = document.getElementsByClassName("dropdown-btn");
        this.auth.get('/candi_data', this.token).then(function (list) {
            _this.jobData = list.json().result;
            _this.activate.params.subscribe(function (option) {
                option = _this.jobData.candi_profile_photo;
                if (option != null) {
                    var result = option.substring(15);
                    _this.auth.get('/image/' + result, _this.token).then(function (list) {
                        _this.image = list.url;
                        console.log(list);
                        console.log(_this.image);
                    }).catch(function (err) {
                    });
                }
            });
        }).catch(function (err) {
        });
        this.menu = [{ name: "Mentorship", path: '/mentor', src: '../../assets/mentorship-icon.png' },
            { name: "Interviews", path: '/interview', src: '../../assets/interviews-icon.png' },
            { name: "Job Offer", path: '/job', src: '../../assets/job-offers-icon.png' },
            { name: "Rate Your Skill", path: '/rating', src: '../../assets/rate-skills-icon.png' },
            { name: "Document Vault", path: '/document', src: '../../assets/document-vault-icon.png' },
            { name: "Professional Profile", path: '/professional', src: '../../assets/professional-profile-icon.png' },
        ];
        this.sideMenu = [{ name: "Home", class: 'glyphicon glyphicon-home', path: '/home' },
            { name: "Profile", class: 'glyphicon glyphicon-user', path: '/professional' },
            { name: "Share", class: 'glyphicon glyphicon-share', path: '/' },
            { name: "Settings", class: 'material-icon', path: '/' },
        ];
    }
    NavbarComponent.prototype.openNav = function () {
        document.getElementById("mySidenav").style.width = "250px";
    };
    NavbarComponent.prototype.closeNav = function () {
        document.getElementById("mySidenav").style.width = "0";
    };
    NavbarComponent.prototype.ngOnInit = function () {
        for (var i = 0; i < this.dropdown.length; i++) {
            this.dropdown[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var dropdownContent = this.nextElementSibling;
                if (dropdownContent.style.display === "block") {
                    dropdownContent.style.display = "none";
                }
                else {
                    dropdownContent.style.display = "block";
                }
            });
        }
    };
    NavbarComponent.prototype.logout = function () {
        localStorage.removeItem('token');
        this.router.navigateByUrl('/login');
    };
    NavbarComponent.prototype.destroy = function () {
        this.notification -= 1;
        this.router.navigateByUrl('/notification');
    };
    NavbarComponent.prototype.onUploadFinished = function (file) {
        console.log(file);
    };
    NavbarComponent.prototype.onRemoved = function (file) {
        console.log(file);
    };
    NavbarComponent.prototype.onUploadStateChanged = function (state) {
        console.log(state);
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/Dashboard/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/Dashboard/navbar/navbar.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('bounce', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* => *', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["useAnimation"])(ng_animate__WEBPACK_IMPORTED_MODULE_3__["bounce"]))])
            ],
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/newpassword/compare-validator.directive.ts":
/*!**********************************************************************!*\
  !*** ./src/app/Dashboard/newpassword/compare-validator.directive.ts ***!
  \**********************************************************************/
/*! exports provided: compareValidator, CompareValidatorDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "compareValidator", function() { return compareValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompareValidatorDirective", function() { return CompareValidatorDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


function compareValidator(controlNameToCompare) {
    return function (c) {
        if (c.value === null || c.value.length === 0) {
            return null;
        }
        var controlToCompare = c.root.get(controlNameToCompare);
        if (controlToCompare) {
            var subscription_1 = controlToCompare.valueChanges.subscribe(function () {
                c.updateValueAndValidity();
                subscription_1.unsubscribe();
            });
        }
        return controlToCompare && controlToCompare.value !== c.value ? { 'compare': true } : null;
    };
}
var CompareValidatorDirective = /** @class */ (function () {
    function CompareValidatorDirective() {
    }
    CompareValidatorDirective_1 = CompareValidatorDirective;
    CompareValidatorDirective.prototype.validate = function (c) {
        if (c.value === null || c.value.length === 0) {
            return null;
        }
        var controlToCompare = c.root.get(this.controlNameToCompare);
        if (controlToCompare) {
            var subscription_2 = controlToCompare.valueChanges.subscribe(function () {
                c.updateValueAndValidity();
                subscription_2.unsubscribe();
            });
        }
        return controlToCompare && controlToCompare.value !== c.value ? { 'compare': true } : null;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('compare'),
        __metadata("design:type", String)
    ], CompareValidatorDirective.prototype, "controlNameToCompare", void 0);
    CompareValidatorDirective = CompareValidatorDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[compare]',
            providers: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"], useExisting: CompareValidatorDirective_1, multi: true }]
        })
    ], CompareValidatorDirective);
    return CompareValidatorDirective;
    var CompareValidatorDirective_1;
}());



/***/ }),

/***/ "./src/app/Dashboard/newpassword/newpassword.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/Dashboard/newpassword/newpassword.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Dashboard/newpassword/newpassword.component.html":
/*!******************************************************************!*\
  !*** ./src/app/Dashboard/newpassword/newpassword.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\n\n<div style=\"background-color: #ff6600;height: 50px;\">\n\n  <img style=\"height:30px;margin-top: 1% !important;\" class=\"img\" src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/light_mm_logo.png\">\n<div><h3 style=\"color: white;margin-left: 50px;margin-top: -20px\">Candidate Portal</h3></div>\n</div>\n<form [formGroup]=\"myform\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-6\">\n        <h2> New Password </h2>\n      </div>\n      <div class=\"col-md-6\">\n        &nbsp;\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <label for=\"password\"> Password</label>\n          <input type=\"password\" name=\"password\" class=\"form-control\" formControlName=\"password\" [(ngModel)]=\"newPassword.password\">\n          <span class=\"help-block\" *ngIf=\"password.invalid &&(password.dirty || password.touched)\">\n            Please Enter password\n          </span>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <label for=\"password\">Confirm Password</label>\n          <input type=\"password\" name=\"cnfm_password\" class=\"form-control\" formControlName=\"confirm_password\"\n            [(ngModel)]=\"newPassword.confirm_password\">\n        </div>\n      </div>\n    </div>\n    <div>\n      <button class=\"btn btn-primary\" style=\"width:179px;height:40px;margin-left:13%;background-color: #F24540;border-radius: 25px;\" [disabled]=\"!myform.valid\" (click)=\"proceed()\">Proceed</button>\n    </div>\n  </div>\n</form>\n\n<footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\n    &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer>\n\n"

/***/ }),

/***/ "./src/app/Dashboard/newpassword/newpassword.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/Dashboard/newpassword/newpassword.component.ts ***!
  \****************************************************************/
/*! exports provided: NewpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewpasswordComponent", function() { return NewpasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _compare_validator_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./compare-validator.directive */ "./src/app/Dashboard/newpassword/compare-validator.directive.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NewpasswordComponent = /** @class */ (function () {
    function NewpasswordComponent(share, toastr, fb, router, auth) {
        this.share = share;
        this.toastr = toastr;
        this.fb = fb;
        this.router = router;
        this.auth = auth;
        this.newPassword = { password: '', confirm_password: '' };
        this.token = localStorage.getItem('token');
        if (this.data) {
            this.data;
        }
        else {
            console.log("hello");
        }
    }
    NewpasswordComponent.prototype.ngOnInit = function () {
        this.data = this.share.getData();
        var body = JSON.parse(this.data['_body']);
        this.candi_email = body;
        this.groupForm();
    };
    NewpasswordComponent.prototype.groupForm = function () {
        this.myform = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            confirm_password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, Object(_compare_validator_directive__WEBPACK_IMPORTED_MODULE_4__["compareValidator"])('password')]),
        });
    };
    Object.defineProperty(NewpasswordComponent.prototype, "password", {
        get: function () {
            return this.myform.get('password');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewpasswordComponent.prototype, "cnf_password", {
        get: function () {
            return this.myform.get('confirm_password');
        },
        enumerable: true,
        configurable: true
    });
    NewpasswordComponent.prototype.proceed = function () {
        var _this = this;
        this.auth.putResult('/new_password', { candi_email: this.candi_email.candi_email, new_password: this.newPassword.password }, this.token).then(function (data) {
            _this.candidateList = data.json();
            _this.toastr.remove(data.json().message);
            _this.router.navigateByUrl('/');
        });
    };
    NewpasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-newpassword',
            template: __webpack_require__(/*! ./newpassword.component.html */ "./src/app/Dashboard/newpassword/newpassword.component.html"),
            styles: [__webpack_require__(/*! ./newpassword.component.css */ "./src/app/Dashboard/newpassword/newpassword.component.css")]
        }),
        __metadata("design:paramtypes", [_services_sharedata_service__WEBPACK_IMPORTED_MODULE_1__["SharedataService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], NewpasswordComponent);
    return NewpasswordComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/objectivetest/objectivetest.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/Dashboard/objectivetest/objectivetest.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:hover {\r\n\r\n  cursor:pointer;\r\n  color: red;\r\n}\r\ntextarea.ng-invalid {\r\n  background-color: pink;\r\n}\r\n/* The container */\r\n.container {\r\n  display: block;\r\n  position: relative;\r\n  padding-left: 35px;\r\n  margin-bottom: 12px;\r\n  cursor: pointer;\r\n  font-size: 18px;;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  font-family: sans-serif;\r\n}\r\n/* Hide the browser's default radio button */\r\n.container input {\r\n  position: absolute;\r\n  opacity: 0;\r\n  cursor: pointer;\r\n}\r\n/* Create a custom radio button */\r\n.checkmark {\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  height: 25px;\r\n  width: 25px;\r\n  background-color: #eee;\r\n  border-radius: 50%;\r\n}\r\n/* On mouse-over, add a grey background color */\r\n.container:hover input ~ .checkmark {\r\n  background-color: #ccc;\r\n}\r\n/* When the radio button is checked, add a blue background */\r\n.container input:checked ~ .checkmark {\r\n  background-color: #FF4F33;\r\n}\r\n/* Create the indicator (the dot/circle - hidden when not checked) */\r\n.checkmark:after {\r\n  content: \"\";\r\n  position: absolute;\r\n  display: none;\r\n}\r\n/* Show the indicator (dot/circle) when checked */\r\n.container input:checked ~ .checkmark:after {\r\n  display: block;\r\n}\r\n/* Style the indicator (dot/circle) */\r\n.container .checkmark:after {\r\n top: 9px;\r\nleft: 9px;\r\nwidth: 8px;\r\nheight: 8px;\r\nborder-radius: 50%;\r\nbackground: white;\r\n}\r\n.btnsubmit{\r\n  background-color: #FF4F33;width: 179px;font-size: 18px;\r\n            height: 50px;color: #ffffff;border-radius: 25px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/Dashboard/objectivetest/objectivetest.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/Dashboard/objectivetest/objectivetest.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-full-layout></app-full-layout> -->\n<div>\n  <div class=\"container\" style=\"margin-top: 50px\">\n    <div class=\"row\">\n      <div\n        class=\"col-md-6\"\n        value=\"submit\"\n        (click)=\"cancel()\"\n        style=\"text-align: center; color: #FF4F33;font-size: 18px\"\n      >\n        Cancel Test\n      </div>\n      <div\n        class=\"col-md-6\"\n        style=\"text-align: center; color: #FF4F33;font-size: 18px\"\n      >\n        <img\n          src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/if_1-09_511570.jpg\"\n          style=\"width: 50px;height: 40px;\"\n        />\n        <!-- <countdown id=\"countdown\" [config]=\"{leftTime: 100 * 27, notify: [1,300]}\" *ngIf=\"counterTime\" (notify)=\"onNotify($event)\">$!m!:$!s!</countdown>  -->\n        <p (notify)=\"onNotify($event)\">{{ counterTime }}</p>\n      </div>\n    </div>\n    <div class=\"row\" style=\"margin-top: 30px\" >\n      <div class=\"col-md-12\">\n        <div class=\"card bg-light\">\n          <div class=\"card-body\">\n            <div class=\"row\" style=\"margin-top: 20px;\">\n              <div class=\"col-md-12\">\n                <p style=\"text-align: center;\">Question {{ QuestionCount }}</p>\n              </div>\n            </div>\n            <div class=\"row\" style=\"margin-top: 20px;\">\n              <div class=\"col-md-12\">\n                <p>{{ valueData }}</p>\n              </div>\n            </div>\n            <form>\n              <div class=\"row\" style=\"margin-top: 20px;\">\n                <div class=\"col-md-12 form-group\">\n                  <label class=\"container\">\n                    <p>{{ option_1 }}</p>\n                    <input\n                      type=\"radio\"\n                      checked=\"checked\"\n                      [(ngModel)]=\"given_answer\"\n                      [value]=\"1\"\n                      name=\"radio\"\n                    />\n                    <span class=\"checkmark\"></span>\n                  </label>\n                  <label class=\"container\">\n                    <p>{{ option_2 }}</p>\n                    <input\n                      type=\"radio\"\n                      [value]=\"2\"\n                      name=\"radio\"\n                      [(ngModel)]=\"given_answer\"\n                    />\n                    <span class=\"checkmark\"></span>\n                  </label>\n                  <label class=\"container\">\n                    <p>{{ option_3 }}</p>\n                    <input\n                      type=\"radio\"\n                      [value]=\"3\"\n                      name=\"radio\"\n                      [(ngModel)]=\"given_answer\"\n                    />\n                    <span class=\"checkmark\"></span>\n                  </label>\n                  <label class=\"container\">\n                    <p>{{ option_4 }}</p>\n                    <input\n                      type=\"radio\"\n                      [value]=\"4\"\n                      name=\"radio\"\n                      [(ngModel)]=\"given_answer\"\n                    />\n                    <span class=\"checkmark\"></span>\n                  </label>\n                </div>\n              </div>\n            </form>\n\n            <div class=\"row\" style=\"margin-top: 20px;\">\n              <div class=\"col-md-12 offset-4\">\n                <button\n                  class=\"btn btnsubmit\"\n                  value=\"submit\"\n                  (click)=\"submit($event.target.value)\"\n                >\n                  Submit\n                </button>\n              </div>\n            </div>\n            <div class=\"row\" style=\"margin-top: 20px;\">\n              <div class=\"col-md-12 offset-5\">\n                <button\n                  style=\"color:#FF4F33;font-size: 16px;\"\n                  value=\"skip\"\n                  (click)=\"skip($event.target.value)\"\n                >\n                  Skip the question\n                </button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <footer\n    class=\"app-footer\"\n    style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\"\n  >\n    &copy;\n    <a style=\"color:black;font-size: 16px;\"\n      >MineMark Solutions Private Limited</a\n    >\n    2018\n  </footer>\n</div>\n"

/***/ }),

/***/ "./src/app/Dashboard/objectivetest/objectivetest.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/Dashboard/objectivetest/objectivetest.component.ts ***!
  \********************************************************************/
/*! exports provided: ObjectivetestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ObjectivetestComponent", function() { return ObjectivetestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_talk_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/talk.service */ "./src/app/services/talk.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ObjectivetestComponent = /** @class */ (function () {
    function ObjectivetestComponent(auth, activatedRoute, speech, route, toastr) {
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.speech = speech;
        this.route = route;
        this.toastr = toastr;
        this.timeLeft = 100 * 27;
        this.windowObjectReference = null;
        this.getData = [];
        this.responseEvalData = [];
        this.responseNextData = [];
        this.started = false;
        this.QuestionCount = 1;
        this.question = { questions: "", question_id: "", skill_id: "" };
        this.questions = "";
        this.candi_skillset = localStorage.getItem("candi_skillset");
        this.given_answer = "";
        this.count = 0;
        this.datesystem = new Date();
        this.myFormattedDate = new _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]("en-US").transform(this.datesystem, "dd/MM/yyyy");
        this.testinfo = [];
        this.date = new Date().getMinutes();
        this.skipanswer = 1;
        this.visible = false;
    }
    ObjectivetestComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.candi_skillset = option["id"];
            _this.token = localStorage.getItem("token");
            console.log("Token of current page", _this.token);
            _this.auth
                .postDetail("/mash_mcq_assessment_start", _this.token, {
                candi_skillset: _this.candi_skillset
            })
                .then(function (data) {
                var plus = 0;
                _this.valueData = data.json().question;
                _this.option_1 = data.json().option_1;
                _this.option_2 = data.json().option_2;
                _this.option_3 = data.json().option_3;
                _this.option_4 = data.json().option_4;
                _this.question_content_id = data.json().id;
                if (_this.valueData) {
                    _this.count = ++plus;
                }
            });
        });
        window.onload = function () {
            document.onkeydown = function (e) {
                if (e.which == 17)
                    isCtrl = true;
                if ((e.which == 85 ||
                    e.which == 117 ||
                    e.which == 65 ||
                    e.which == 97 ||
                    e.which == 67 ||
                    e.which == 99) &&
                    isCtrl == true) {
                    // alert(‘Keyboard shortcuts are cool!’);
                    return false;
                }
                return (e.which || e.keyCode) != 116;
            };
            //////////F12 disable code////////////////////////
            document.onkeypress = function (e) {
                return (e.which || e.keyCode) != 123;
            };
            var isNS = navigator.appName == "Netscape" ? 1 : 0;
            // if (navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);
            function mischandler() {
                return false;
            }
            function mousehandler(e) {
                var myevent = isNS ? e : event;
                var eventbutton = isNS ? myevent.which : myevent.button;
                if (eventbutton == 2 || eventbutton == 3)
                    return false;
            }
            document.oncontextmenu = mischandler;
            document.onmousedown = mousehandler;
            document.onmouseup = mousehandler;
            var isCtrl = false;
            document.onkeyup = function (e) {
                if (e.which == 17)
                    isCtrl = false;
            };
        };
        history.pushState(null, null, document.URL);
        window.addEventListener("popstate", function () {
            history.pushState(null, null, document.URL);
        });
        this.startTimer();
        this.speech.started.subscribe(function (started) { return (_this.started = started); });
    };
    ObjectivetestComponent.prototype.startTimer = function () {
        var _this = this;
        if (!this.interval) {
            this.interval = setInterval(function () {
                if (_this.timeLeft === 0) {
                    var submitData = { candi_skillset: _this.candi_skillset, time_taken: _this.counterTime, given_answer: _this.given_answer, question_content_id: _this.question_content_id, status: 3, date: _this.myFormattedDate };
                    _this.auth.postDetail('/mash_mcq_assessment_answer', _this.token, submitData).then(function (data) {
                        _this.valueData = data.json().question;
                        _this.option_1 = data.json().option_1;
                        _this.option_2 = data.json().option_2;
                        _this.option_3 = data.json().option_3;
                        _this.option_4 = data.json().option_4;
                        _this.question_content_id = data.json().id;
                        _this.given_answer = '';
                        _this.route.navigateByUrl('/feedback/' + _this.candi_skillset);
                    });
                }
                if (_this.timeLeft >= 0) {
                    _this.startTimerHHMMSS(_this.timeLeft);
                    // this.FormatSecondsAsDurationString(this.timeLeft);
                    _this.timeLeft--;
                }
                else {
                    _this.timeLeft = 60;
                }
            }, 1000);
        }
    };
    ObjectivetestComponent.prototype.startTimerHHMMSS = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor((d % 3600) / 60);
        var s = Math.floor((d % 3600) % 60);
        var hDisplay = h > 0 ? h + (h === 1 ? "" : "") : "";
        var mDisplay = m > 0 ? m + (m === 1 ? "" : "") : "";
        var sDisplay = s > 0 ? s + (s === 1 ? "" : "") : "";
        this.counterTime =
            this.GetPaddedIntString(hDisplay, 2) +
                ":" +
                this.GetPaddedIntString(mDisplay, 2) +
                ":" +
                this.GetPaddedIntString(sDisplay, 2);
        return this.counterTime;
    };
    ObjectivetestComponent.prototype.pauseTimer = function () {
        clearInterval(this.interval);
    };
    ObjectivetestComponent.prototype.GetPaddedIntString = function (n, numDigits) {
        var nPadded = n;
        for (; nPadded.length < numDigits;) {
            nPadded = "0" + nPadded;
        }
        return nPadded;
    };
    ObjectivetestComponent.prototype.submit = function (value) {
        // this.duration=this.leftTime;
        var _this = this;
        this.counterTime;
        var submitData = {
            candi_skillset: this.candi_skillset,
            time_taken: this.counterTime,
            given_answer: this.given_answer,
            question_content_id: this.question_content_id,
            status: 1,
            date: this.myFormattedDate
        };
        if (this.given_answer != "") {
            this.submitButton = value;
            ++this.QuestionCount;
            if (this.QuestionCount > 20) {
                this.route.navigateByUrl("/feedback/" + this.candi_skillset);
            }
            this.auth
                .postDetail("/mash_mcq_assessment_answer", this.token, submitData)
                .then(function (data) {
                _this.valueData = data.json().question;
                _this.option_1 = data.json().option_1;
                _this.option_2 = data.json().option_2;
                _this.option_3 = data.json().option_3;
                _this.option_4 = data.json().option_4;
                _this.question_content_id = data.json().id;
                _this.given_answer = "";
            });
        }
        else {
            this.toastr.error("Please Choose the answer!");
        }
    };
    ObjectivetestComponent.prototype.onNotify = function (event) {
        var _this = this;
        this.timer = event;
        if (this.timer == 100) {
            var submitData = {
                candi_skillset: this.candi_skillset,
                time_taken: this.counterTime,
                given_answer: this.given_answer,
                question_content_id: this.question_content_id,
                status: 3,
                date: this.myFormattedDate
            };
            this.auth
                .postDetail("/mash_mcq_assessment_answer", this.token, submitData)
                .then(function (data) {
                _this.valueData = data.json().question;
                _this.option_1 = data.json().option_1;
                _this.option_2 = data.json().option_2;
                _this.option_3 = data.json().option_3;
                _this.option_4 = data.json().option_4;
                _this.question_content_id = data.json().id;
                _this.given_answer = "";
                _this.route.navigateByUrl("/feedback/" + _this.candi_skillset);
            });
        }
        if (this.timer == 100) {
            this.toastr.error("You Have Only Five Minutes");
        }
    };
    ObjectivetestComponent.prototype.cancel = function () {
        var _this = this;
        var submitData = {
            candi_skillset: this.candi_skillset,
            time_taken: this.counterTime,
            given_answer: this.given_answer,
            question_content_id: this.question_content_id,
            status: 2,
            date: this.myFormattedDate
        };
        this.auth
            .postDetail("/mash_mcq_assessment_answer", this.token, submitData)
            .then(function (data) {
            _this.valueData = data.json().question;
            _this.option_1 = data.json().option_1;
            _this.option_2 = data.json().option_2;
            _this.option_3 = data.json().option_3;
            _this.option_4 = data.json().option_4;
            _this.question_content_id = data.json().id;
            _this.given_answer = "";
            _this.route.navigateByUrl("/feedback/" + _this.candi_skillset);
        });
    };
    ObjectivetestComponent.prototype.skip = function (value) {
        var _this = this;
        this.skipButton = value;
        if (this.skipButton == "skip") {
            if (this.QuestionCount > 20) {
                this.route.navigateByUrl("/feedback/" + this.candi_skillset);
            }
            var submitData = {
                candi_skillset: this.candi_skillset,
                time_taken: this.counterTime,
                given_answer: 0,
                question_content_id: this.question_content_id,
                status: 2,
                date: this.myFormattedDate
            };
            var r = confirm("Are you sure you want too skip the Question?");
            if (r == true) {
                ++this.QuestionCount;
                this.auth
                    .postDetail("/mash_mcq_assessment_answer", this.token, submitData)
                    .then(function (datas) {
                    _this.valueData = datas.json().question;
                    _this.option_1 = datas.json().option_1;
                    _this.option_2 = datas.json().option_2;
                    _this.option_3 = datas.json().option_3;
                    _this.option_4 = datas.json().option_4;
                    _this.question_content_id = datas.json().id;
                    _this.given_answer = "";
                    _this.toastr.info("skipped!");
                });
            }
            else {
            }
        }
    };
    ObjectivetestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-objectivetest",
            template: __webpack_require__(/*! ./objectivetest.component.html */ "./src/app/Dashboard/objectivetest/objectivetest.component.html"),
            styles: [__webpack_require__(/*! ./objectivetest.component.css */ "./src/app/Dashboard/objectivetest/objectivetest.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_talk_service__WEBPACK_IMPORTED_MODULE_4__["TalkService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], ObjectivetestComponent);
    return ObjectivetestComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/onlinetest/onlinetest.component.css":
/*!***************************************************************!*\
  !*** ./src/app/Dashboard/onlinetest/onlinetest.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h2{\r\n    font-family: sans-serif;font-size: 20px;\r\n}\r\nfieldset \r\n{\r\n    border: 1px solid #ddd !important;\r\n    margin: 0;\r\n    xmin-width: 0;\r\n    padding: 10px;       \r\n    position: relative;\r\n    border-radius:4px;\r\n    background-color:#f5f5f5;\r\n    padding-left:10px!important;\r\n}\r\nlegend\r\n    {\r\n        font-size:20px;\r\n        font-weight:bold;\r\n        margin-bottom: 0px; \r\n        text-align: center;\r\n        width: 90%; \r\n        height: 70px;\r\n        border: 1px solid #ddd;\r\n        border-radius: 4px; \r\n        color: #FFFFFF;\r\n        font-family: sans-serif;\r\n\r\n        padding: 22px 5px 5px 10px; \r\n        background-color: #FF4F33;\r\n    }\r\nli{\r\n        font-size: 16px;\r\n        font-family: sans-serif;\r\n        color: #999999;\r\n    }\r\nbtnstart{\r\n        width: 179px;\r\n        font-size: 16px;\r\n        height: 40px;\r\n        color: #ffffff;\r\n        background-color: #FF4F33;\r\n        border-radius: 25px;\r\n        box-shadow: 0 2px 16px 6px #CCCCCC;\r\n    }"

/***/ }),

/***/ "./src/app/Dashboard/onlinetest/onlinetest.component.html":
/*!****************************************************************!*\
  !*** ./src/app/Dashboard/onlinetest/onlinetest.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left: 12%;\">\r\n\r\n  <h2>Rate Your Skills</h2>\r\n  <div class=\"card\">\r\n    <div class=\"card-body\" >\r\n      <form [formGroup]=\"myform\" >\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n            <p style=\"text-align: center;font-size: 20px;color: #000000;\">{{skilldisplay}} (Test)</p>\r\n          </div>\r\n          <div class=\"col-md-12\">\r\n            <p style=\"text-align: center;color: #FF4F33;\">Skill Rating Test</p>\r\n          </div>\r\n          <div class=\"col-md-12 alert \" style=\"background-color: #FFCCBC !important;\">\r\n            <p style=\"text-align: center;color: #ffffff;font-size: 16px;font-family: sans-serif\">In case you cancel the\r\n              test in between, you would not be able\r\n              to take new test within next one month</p>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6\">\r\n            <div class=\"card-body\" style=\"width: 70%;text-align: center;\">\r\n              <fieldset>\r\n                <legend style=\"text-align: center\"><a style=\"color:white;\">Demo Practice Test</a></legend>\r\n\r\n                <p style=\"color:#999999;text-align: center;\">Learn basics of taking a test</p>\r\n\r\n              </fieldset>\r\n\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-6\">\r\n            <div class=\"card-body\" style=\"width: 70%;text-align: center;\">\r\n              <fieldset>\r\n                <legend style=\"text-align: center\">Test Requirements</legend>\r\n\r\n                <p style=\"color:#999999;text-align: center;\">You should comply with the following requirements</p>\r\n\r\n              </fieldset>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <hr>\r\n\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\" style=\"margin-top: 30px;\" >\r\n            <p style=\"font-size: 16px;font-weight: bold;\">Topics</p>\r\n            <br>\r\n            <ul style=\"margin-left: 20px;\" *ngFor=\"let skill of skillinfo\"  >\r\n              <li>{{skill.topics}}</li>\r\n            </ul>\r\n          </div>\r\n          <hr>\r\n          <div class=\"col-md-12\">\r\n            <h4><a style=\"font-weight: bold;\" >Total Questions :</a> {{totalquestion}}</h4>\r\n          </div>\r\n          <hr>\r\n          <div class=\"col-md-12\" >\r\n            <h4><a style=\"font-weight: bold;\">Total Time Allocation :</a> {{totaltimetaken}} mins</h4>\r\n          </div>\r\n          <hr>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\" style=\"text-align: center;margin-top: 30px;\">\r\n            <label style=\"font-size:16px;\"><input type=\"checkbox\" value=\"\" formControlName=\"term\" *ngIf=\"!isActive\"\r\n                required>&nbsp; I agree and have read al the information mentioned above and I am ready\r\n              to take the test</label>\r\n          </div>\r\n          <div class=\"col-md-12\" style=\"text-align: center;margin-top: 20px;\">\r\n            <label><button [disabled]=\"!myform\" data-toggle=\"modal\" data-target=\"#myModal2\" style=\"width: 179px;font-size: 16px;height: 40px;color: #ffffff;;background-color: #FF4F33;border-radius: 25px;\">Start\r\n                Test</button></label>\r\n          </div>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n    &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer>\r\n\r\n\r\n\r\n  <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\r\n      <div class=\"modal-dialog modal-md\">\r\n        <div class=\"modal-content\">\r\n          <div class=\"modal-header\">\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-md-12 offset-4\">\r\n                <h4>\r\n                  <i class=\"glyphicon glyphicon-warning-sign\" style=\"color:red;\"></i><a style=\"font-size: 18px;font-family: sans-serif;\">\r\n                    &nbsp; Caution!</a>\r\n                </h4>\r\n              </div>\r\n              <div class=\"col-md-12\">\r\n                <p class=\"warn\">1) Please check your continuous power supply.</p>\r\n                <p class=\"warn\">2) Please check your internet connection.</p>\r\n                <p class=\"warn\">3) Please close other browsers running in background.</p>\r\n                <p class=\"warn\">4) Answers once skipped can not be revisited.</p>\r\n              </div>\r\n              <div class=\"col-md-12 offset-3\">\r\n                <button type=\"button\" class=\"btn btn-default btntest\"  data-dismiss=\"modal\" (click)=\"starttest(skillinfo[0].skill_id)\" >Start your test</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/Dashboard/onlinetest/onlinetest.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/Dashboard/onlinetest/onlinetest.component.ts ***!
  \**************************************************************/
/*! exports provided: OnlinetestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlinetestComponent", function() { return OnlinetestComponent; });
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OnlinetestComponent = /** @class */ (function () {
    function OnlinetestComponent(auth, activatedRoute, router, toster, share) {
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.toster = toster;
        this.share = share;
        this.isActive = false;
        this.responseData = [];
        this.candidateskill1 = localStorage.getItem("candi_skillset1");
        this.candidateskill2 = localStorage.getItem("candi_skillset2");
        this.candidateskill3 = localStorage.getItem("candi_skillset3");
        this.addskill = [
            parseInt(this.candidateskill1),
            parseInt(this.candidateskill2),
            parseInt(this.candidateskill3)
        ];
        this.skillarray = [];
        this.idnameskill = [];
        this.topiclist = [];
        this.topic = [];
        this.testinfo = [];
    }
    OnlinetestComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.assessment_no = localStorage.getItem("candi_skillset1");
        this.activatedRoute.params.subscribe(function (option) {
            _this.candi_skillset = option["id"];
            _this.token = localStorage.getItem("token");
            _this.auth
                .postDetail("/mash_candi_asses_info", _this.token, {
                candi_skillset: _this.candi_skillset
            })
                .then(function (data) {
                _this.skillinfo = data.json().result;
                if (_this.skillinfo === null || _this.skillinfo == "") {
                    _this.toster.info("Assessment not available");
                }
                else {
                    _this.totalquestion = _this.skillinfo[0].total_question;
                    _this.totaltimetaken = _this.skillinfo[0].total_time_allocate;
                }
            });
        });
        this.myform = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            term: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
        });
        this.auth
            .get("/skills", this.token)
            .then(function (list) {
            _this.candidateList = list.json().result;
            var body = JSON.stringify(_this.candidateList);
            var stringify = JSON.parse(body);
            for (var i = 0; i < stringify.length; i++) {
                _this.responseData = stringify[i]["id"];
                if (parseInt(_this.candi_skillset) === _this.responseData) {
                    _this.skilldisplay = stringify[i]["skill"];
                }
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    OnlinetestComponent.prototype.starttest = function (data1) {
        var _this = this;
        this.token = localStorage.getItem("token");
        this.assessment_no = localStorage.getItem("candi_skillset1");
        if (this.skillinfo.length === null) {
            this.toster.info("Assessment not available yet");
        }
        else {
            if (parseInt(this.assessment_no) === 0) {
                this.auth
                    .postDetail("/take_test", this.token, {})
                    .then(function (data) {
                    var temp = data.json().message;
                    if (temp === "success!") {
                        _this.share.setData(_this.skillinfo);
                        _this.router.navigateByUrl("/exam/" + data1);
                    }
                })
                    .catch(function (err) {
                    _this.taketest = err.json().message;
                    if (_this.taketest === "Assessment Running On Other Device") {
                        _this.toster.info("Assessment Running On Other Device");
                    }
                });
            }
            else {
                this.auth
                    .postDetail("/take_test", this.token, {})
                    .then(function (data) {
                    var temp = data.json().message;
                    if (temp === "success!") {
                        _this.share.setData(_this.skillinfo);
                        _this.router.navigateByUrl("/objective/" + data1);
                    }
                })
                    .catch(function (err) {
                    _this.taketest = err.json().message;
                    if (_this.taketest === "Assessment Running On Other Device") {
                        _this.toster.info("Assessment Running On Other Device");
                    }
                });
            }
        }
    };
    OnlinetestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-onlinetest",
            template: __webpack_require__(/*! ./onlinetest.component.html */ "./src/app/Dashboard/onlinetest/onlinetest.component.html"),
            styles: [__webpack_require__(/*! ./onlinetest.component.css */ "./src/app/Dashboard/onlinetest/onlinetest.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _services_sharedata_service__WEBPACK_IMPORTED_MODULE_0__["SharedataService"]])
    ], OnlinetestComponent);
    return OnlinetestComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/proffesional-profile/proffesional-profile.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/Dashboard/proffesional-profile/proffesional-profile.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Dashboard/proffesional-profile/proffesional-profile.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/Dashboard/proffesional-profile/proffesional-profile.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\r\n<div class=\"text-center\">\r\n  <img src=\"../../../assets/images.jpg\" class=\"img-circle\" height=\"100px\" width=\"150px\">\r\n  <div class=\"profile-usertitle\">\r\n    <div class=\"profile-usertitle-name\">\r\n      {{candidateData.CANDI_FULLNAME}}\r\n    </div>\r\n    <div class=\"profile-usertitle-job\">\r\n      {{candidateData.CANDI_FULLNAME}}\r\n    </div>\r\n  </div>\r\n  <hr>\r\n</div>\r\n<div class=\"container\">\r\n  <div class=\"text-center\">\r\n    <span style=\"margin-left: 8%\">{{candidateData.CANDI_SKILLSET}} {{candidateData.CANDI_SKILLSET}}\r\n      {{candidateData.CANDI_SKILLSET}}</span>\r\n  </div>\r\n  <hr>\r\n  <strong>Profile Summary</strong>\r\n  <p>\r\n    An enthusiastic fresher with highly motivated and leadership skills having bachelors of engineering degree in\r\n    Mechanical Engineering.\r\n    Expert in implementation of each step of project.\r\n    Eager to learn new technologies and methodologies.\r\n    Always willing to innovate the new things which can improve the existing technology.\r\n  </p>\r\n\r\n  <hr>\r\n  <strong>Ready To Locate</strong>\r\n  <p>\r\n\r\n  </p>\r\n  <hr>\r\n  <strong>Skills</strong>\r\n  <div class=\"container\">\r\n    <table class=\"table\">\r\n      <thead>\r\n        <tr>\r\n          <th>Skill</th>\r\n          <th>Level</th>\r\n          <th>MM Score</th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr>\r\n          <td>{{candidateData.CANDI_SKILLSET}}</td>\r\n          <td>{{candidateData.CANDI_SKILLSET}}</td>\r\n          <td>{{candidateData.CANDI_SKILLSET}}</td>\r\n        </tr>\r\n      </tbody>\r\n    </table>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/Dashboard/proffesional-profile/proffesional-profile.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/Dashboard/proffesional-profile/proffesional-profile.component.ts ***!
  \**********************************************************************************/
/*! exports provided: ProffesionalProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProffesionalProfileComponent", function() { return ProffesionalProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var URL = 'http://localhost:3002/api/upload';
var ProffesionalProfileComponent = /** @class */ (function () {
    function ProffesionalProfileComponent(auth) {
        var _this = this;
        this.auth = auth;
        this.uploader = new ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__["FileUploader"]({ url: URL, itemAlias: 'photo' });
        this.candidateData = { candidateData: '' };
        this.auth.get('/candidate_data', this.token).then(function (list) {
            _this.candidateData = list.json().result;
            console.log(_this.candidateData);
        }).catch(function (err) {
        });
    }
    ProffesionalProfileComponent.prototype.ngOnInit = function () {
        this.uploader.onAfterAddingFile = function (file) { file.withCredentials = false; };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            console.log('ImageUpload:uploaded:', item, status, response);
            alert('File uploaded successfully');
        };
    };
    ProffesionalProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-proffesional-profile',
            template: __webpack_require__(/*! ./proffesional-profile.component.html */ "./src/app/Dashboard/proffesional-profile/proffesional-profile.component.html"),
            styles: [__webpack_require__(/*! ./proffesional-profile.component.css */ "./src/app/Dashboard/proffesional-profile/proffesional-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], ProffesionalProfileComponent);
    return ProffesionalProfileComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/rate-your-skill/rate-your-skill.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/Dashboard/rate-your-skill/rate-your-skill.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h2{\r\n    font-family: sans-serif;\r\n    font-size: 18px;\r\n    font-weight: bold;\r\n}\r\nspan{\r\n    font-family: sans-serif;\r\n    font-size: 16px;\r\n    color: #999999;\r\n\r\n}\r\n.test{\r\n    width: 179px;\r\n    font-family: sans-serif;\r\n    font-size: 15px;\r\n    border: 1px solid #FF542E;\r\n    background-color: #FF4F33;\r\n    border-radius: 25px;\r\n\r\n}\r\n.btnfound{\r\n    font-family: sans-serif;\r\n    border-radius: 25px;\r\n    width: 179px;\r\n    font-size: 14px;\r\n    border: 1px solid #FF542E;\r\n    background-color: #CCCCCC;\r\n\r\n}\r\n.btnexpert{\r\n    font-family: sans-serif;\r\n    border-radius: 25px;\r\n    width: 179px;\r\n    font-size: 14px;\r\n    border: 1px solid #FF542E;\r\n    background-color: #FFFFFF;\r\n\r\n}\r\nth, td {\r\n    font-family: sans-serif;\r\n    font-size: 16px;\r\n    color: #999999;\r\n}\r\nli>a:hover{\r\n    background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%) !important;\r\n    color: #111;\r\n}\r\nli>a:active{\r\n    background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%) !important;\r\n    color: #111;\r\n}\r\np{\r\n    text-align: center;\r\n}\r\n.warn{\r\n    text-align: justify;\r\n    font-size: 18px;\r\n}\r\n.btntest{\r\n    color: #FFFFFF;\r\n    width: 130px;\r\n    border-radius: 25px;\r\n    background-color: #FF4F33;\r\n    border: 1px solid #000000;\r\n\r\n}\r\n"

/***/ }),

/***/ "./src/app/Dashboard/rate-your-skill/rate-your-skill.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/Dashboard/rate-your-skill/rate-your-skill.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left:12%\">\r\n  <h3>Rate Your Skills</h3>\r\n  <div class=\"card\">\r\n    <div class=\"card-body\">\r\n      <!-- <div class=\"tab-content\">\r\n        <div id=\"home\" class=\"tab-pane fade in active\"> -->\r\n      <div class=\"card\" *ngFor=\"let data of ratedskill; let i = index\">\r\n        <div>\r\n          <div>\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col-md-8\">\r\n                  <h2>{{ data.skill }}</h2>\r\n                  <span>Not assesment yet</span><br /><br />\r\n                  <!-- <button id=\"test\"\r\n                    class=\"btn btn-primary test\"\r\n                    (click)=\"navigate(data.id)\"\r\n                  >\r\n                    Take a test\r\n                  </button> -->\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"col-md-4\" style=\"margin-top: -80px;float: right;\">\r\n                <button id=\"test\" class=\"btn btn-primary test pull-right\" (click)=\"navigate(data.id)\">\r\n                  <!-- Foundation -->\r\n                  Take a test\r\n                </button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <br />\r\n\r\n      <div>\r\n        <div class=\"card\" *ngFor=\"let detailsresult of emtyskill\">\r\n          <div>\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col-md-8\">\r\n                  <h2>{{ detailsresult.skill_name }}</h2>\r\n                  <br />\r\n                  <span>Taken on {{ detailsresult.taken_date }}</span> <br />\r\n                  <table class=\"table\">\r\n                    <thead>\r\n                      <tr>\r\n                        <th>Score out of {{ detailsresult.total_marks }}</th>\r\n                        <th>Time taken</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr>\r\n                        <td>{{ detailsresult.obtain_score }}</td>\r\n                        <td>{{ detailsresult.time_taken }} min</td>\r\n                      </tr>\r\n                    </tbody>\r\n                    <tr>\r\n                      <td>\r\n                        <a><span style=\"color:#FF4F33\">Details</span></a>\r\n                      </td>\r\n                      <td>\r\n                        <a\r\n                          ><span style=\"color:#FF4F33\"\r\n                            >Re-test to improve the skill score</span\r\n                          ></a\r\n                        >\r\n                      </td>\r\n                    </tr>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"col-md-4\" style=\"margin-top: -160px;float: right;\">\r\n                <button class=\"btn btn-default btnexpert pull-right\">\r\n                  Expert\r\n                </button>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col-md-12\">\r\n                <p style=\"float: right;\">To view and download your report</p>\r\n                <a\r\n                class=\"pointer-cursor\"\r\n                  data-toggle=\"modal\"\r\n                  data-target=\"#myModal\"\r\n                  style=\"float: right;color:#FF4F33\"\r\n                  (click)=\"viewModel(detailsresult.id)\"\r\n                  >click here</a\r\n                >\r\n              </div>\r\n            </div>\r\n            <!-- Button trigger modal -->\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br />\r\n    </div>\r\n  </div>\r\n</div>\r\n<app-footer></app-footer>\r\n<div id=\"myModal\" class=\"modal fade\" role=\"dialog\" style=\"text-align:center;\">\r\n  <div class=\"modal-dialog\" *ngIf=\"modeldata1\">\r\n    <div class=\"modal-content modal-lg\">\r\n      <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">Assessment Details</h4>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">\r\n          &times;\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\" style=\"margin-top:5px;\">\r\n          <div class=\"col-md-4\">\r\n            <div class=\"card bg-light\" style=\"background-color: #FFFFFF;\">\r\n              <div class=\"card-body\">\r\n                <p\r\n                  style=\"text-align: center; font-weight: bold;font-family: sans-serif;color: #FF4F33;font-size: 20px;\"\r\n                >\r\n                  You have Scored\r\n                </p>\r\n                <p\r\n                  style=\"text-align: center;font-weight: bold;font-family: sans-serif;color: #FF4F33;font-size: 48px;\"\r\n                >\r\n                  {{ modeldata1.obtain_score }}\r\n                </p>\r\n                <p\r\n                  style=\"text-align: center;font-weight: bold;font-family: sans-serif;color: #999999;font-size: 14px;\"\r\n                >\r\n                  Out of {{ modeldata1.total_marks }}\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-8\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-12\">\r\n                <div class=\"card bg-light\" style=\"background-color: #FFFFFF;\">\r\n                  <div class=\"card-body\">\r\n                    <div class=\"col-md-8\">\r\n                      <p\r\n                        style=\"font-weight: bold;font-family: sans-serif;color: black;font-size: 20px;\"\r\n                      >\r\n                        Your profile has been upgraded to Foundation Level\r\n                      </p>\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                      <button class=\"btn btn-primary btntest\">Foundaion</button>\r\n                      <p style=\"color:#FF4F33;\">Learn more about this level</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\" style=\"margin-top: 5px;\">\r\n          <div class=\"col-md-12\">\r\n            <div class=\"card-body\">\r\n              <fieldset>\r\n                <legend style=\"text-align: center\">Test Summary</legend>\r\n\r\n                <div class=\"row\" style=\"margin-top: 10px;\">\r\n                  <div class=\"col-md-6\">\r\n                    <p\r\n                      style=\"font-size: 18px;font-family: sans-serif;float: left;\"\r\n                    >\r\n                      Test Name\r\n                    </p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 18px;color:#999999;\">\r\n                      {{ modeldata1.skill_name }}\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n                <hr />\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p\r\n                      style=\"font-size: 18px;font-family: sans-serif;float: left;\"\r\n                    >\r\n                      Taken On\r\n                    </p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 18px;color:#999999;\">\r\n                      {{ modeldata1.taken_date }}\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n                <hr />\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p\r\n                      style=\"font-size: 18px;font-family: sans-serif;float: left;\"\r\n                    >\r\n                      Time Taken\r\n                    </p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 18px;color:#999999;\">\r\n                      {{ modeldata1.time_taken }} Mins\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n                <hr />\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p\r\n                      style=\"font-size: 18px;font-family: sans-serif;float: left;\"\r\n                    >\r\n                      Test Status\r\n                    </p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 18px;color:#999999;\">\r\n                      {{ modeldata1.test_status }}\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </fieldset>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-md-6\">\r\n                <button\r\n                  class=\"btn btnbottom btntest pointer-cursor\"\r\n                  style=\"width: 250px;font-size: 18px;\"\r\n                >\r\n                  <a href=\"{{ modeldata1.summary_report_path }}\" style=\"color: white\">\r\n                    <i\r\n                    class=\"fa fa-download\"\r\n                    style=\"color:white;margin-right: 20px;\"\r\n                    aria-hidden=\"true\"\r\n                  ></i\r\n                >Summary PDF</a>\r\n                </button>\r\n              </div>\r\n              <div class=\"col-md-6\">\r\n                <button\r\n                  class=\"btn btnbottom btntest pointer-cursor\"\r\n                  style=\"width: 250px;font-size: 18px;\"\r\n                >\r\n                  <a href=\"{{ modeldata1.detail_report_path }}\" style=\"color: white\">\r\n                    <i\r\n                    class=\"fa fa-download\"\r\n                    style=\"color:white;margin-right: 20px;\"\r\n                    aria-hidden=\"true\"\r\n                  ></i\r\n                >Detail PDF</a>\r\n                </button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">\r\n          Close\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  \r\n</div>\r\n\r\n<!-- <footer\r\n  class=\"app-footer\"\r\n  style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\"\r\n>\r\n  &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019\r\n</footer> -->\r\n"

/***/ }),

/***/ "./src/app/Dashboard/rate-your-skill/rate-your-skill.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/Dashboard/rate-your-skill/rate-your-skill.component.ts ***!
  \************************************************************************/
/*! exports provided: RateYourSkillComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RateYourSkillComponent", function() { return RateYourSkillComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RateYourSkillComponent = /** @class */ (function () {
    function RateYourSkillComponent(router, auth, activatedRoute) {
        this.router = router;
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.candidateskill1 = localStorage.getItem("candi_skillset1");
        this.candidateskill2 = localStorage.getItem("candi_skillset2");
        this.candidateskill3 = localStorage.getItem("candi_skillset3");
        this.skillarray = [];
        this.responseData = [];
        this.addskill = [
            parseInt(this.candidateskill1),
            parseInt(this.candidateskill2),
            parseInt(this.candidateskill3)
        ];
        this.skillid = [];
        this.addallskill = [];
        this.arrayskill = [];
        this.resultskill = [];
        this.array1 = [];
        this.emtyskill = [];
        this.emtyresult = [];
        this.skillresult = [];
        this.compareresult = [];
        this.filteredX = [];
        this.yFilter = [];
        this.ResultSkill = [];
        this.EmptyResult = [];
        this.ratedskill = [];
    }
    RateYourSkillComponent.prototype.ngOnInit = function () {
        this.token = localStorage.getItem("token");
        this.getallSkill();
        this.candidate_rating();
    };
    RateYourSkillComponent.prototype.navigate = function (data) {
        this.token = localStorage.getItem("token");
        this.router.navigateByUrl("/test/" + data);
    };
    RateYourSkillComponent.prototype.candidate_rating = function () {
        var _this = this;
        this.auth
            .get("/candidate_skill_rating", this.token)
            .then(function (list) {
            _this.givenTest = list.json().givenTest;
            _this.resultData = list.json().resultData;
            var _loop_1 = function () {
                var array = _this.givenTest[i];
                var mapped = Object.keys(array).map(function (key) {
                    _this.array1 = array[key];
                    _this.emtyskill.push(_this.array1);
                });
            };
            for (var i = 0; i < _this.givenTest.length; i++) {
                _loop_1();
            }
            _this.ResultSkill = _this.emtyskill;
            var _loop_2 = function () {
                var array = _this.resultData[i];
                var mapped = Object.keys(array).map(function (key) {
                    _this.skillresult = array[key];
                    _this.emtyresult.push(_this.skillresult);
                });
            };
            for (var i = 0; i < _this.resultData.length; i++) {
                _loop_2();
            }
            _this.EmptyResult = _this.emtyresult;
            _this.yFilter = _this.ResultSkill.map(function (itemY) {
                return itemY.skill_id;
            });
            _this.ratedskill = _this.EmptyResult.filter(function (itemX) { return !_this.yFilter.includes(itemX.id); });
        })
            .catch(function (err) {
            console.log("Error", err);
        });
    };
    RateYourSkillComponent.prototype.getallSkill = function () {
        var _this = this;
        this.auth
            .get("/skills", this.token)
            .then(function (list) {
            _this.candidateList = list.json().result;
            var body = JSON.stringify(_this.candidateList);
            var stringify = JSON.parse(body);
            for (var i = 0; i < stringify.length; i++) {
                _this.responseData = stringify[i]["id"];
                for (var j = 0; j < _this.addskill.length; j++) {
                    if (_this.addskill[j] === _this.responseData) {
                        _this.skillid[j] = stringify[i]["id"];
                        _this.skillarray[j] = {
                            id: stringify[i]["id"],
                            skill: stringify[i]["skill"]
                        };
                    }
                }
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    RateYourSkillComponent.prototype.getsampleskill = function () {
        var _this = this;
        this.yFilter = this.emtyresult.map(function (itemY) {
            return itemY.id;
        });
        this.filteredX = this.compareresult.filter(function (itemX) { return !_this.yFilter.includes(itemX.skill_id); });
    };
    RateYourSkillComponent.prototype.viewModel = function (id) {
        for (var i = 0; i < this.ResultSkill.length; i++) {
            this.eachposted = this.ResultSkill[i];
            if (this.eachposted.id === id) {
                this.modeldata1 = this.eachposted;
            }
        }
    };
    RateYourSkillComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-rate-your-skill",
            template: __webpack_require__(/*! ./rate-your-skill.component.html */ "./src/app/Dashboard/rate-your-skill/rate-your-skill.component.html"),
            styles: [__webpack_require__(/*! ./rate-your-skill.component.css */ "./src/app/Dashboard/rate-your-skill/rate-your-skill.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], RateYourSkillComponent);
    return RateYourSkillComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/result-test/result-test.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/Dashboard/result-test/result-test.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h4{\r\n    color: #FF4F33;\r\n    font-weight: bold;\r\n    font-size: 24px;\r\n}\r\n.adone{\r\n    color: #FF4F33 !important;\r\n    font-size: 20px;\r\n    float:right;\r\n    margin-top:-30px;\r\n}\r\n.btnfoundation{\r\n    background-color: #FFFFFF;\r\n    border: 1px solid #FF4F33;\r\n    height: 30px;\r\n    color: black;\r\n    width: 179px;\r\n    border-radius: 25px;\r\n}\r\n.btnreq{\r\n    color: #ffffff;\r\n    background-color: #111111;\r\n    width: 179px;\r\n    border-radius: 25px;\r\n}\r\nfieldset \r\n{\r\n    border: 1px solid #ddd !important;\r\n    margin: 0;\r\n    width:500px;\r\n    padding: 10px; \r\n    height: 360px;      \r\n    position: relative;\r\n    border-radius:4px;\r\n    background-color:#f5f5f5;\r\n    padding-left:10px!important;\r\n}\r\nhr{\r\n    margin: 0px !important;\r\n}\r\nlegend\r\n    {\r\n        font-size:24px;\r\n        font-family: sans-serif;\r\n        margin-bottom: 0px; \r\n        text-align: center;\r\n        width: 90%; \r\n        height: 50px;\r\n        border: 1px solid #ddd;\r\n        border-radius: 4px; \r\n        color: #FFFFFF;\r\n        font-family: sans-serif;\r\n\r\n        padding: 10px 5px 5px 10px; \r\n        background: linear-gradient(109deg, #FF6633 0%, #FF3333 100%);\r\n    }\r\n.btnbottom{\r\n    height: 40px;\r\n    width: 150px;\r\n    font-size: 20px;\r\n    background: linear-gradient(109deg, #FF6633 0%, #FF3333 100%);\r\n    border-radius: 25px;\r\n    text-align: center;\r\n    color: #ffffff;\r\n}"

/***/ }),

/***/ "./src/app/Dashboard/result-test/result-test.component.html":
/*!******************************************************************!*\
  !*** ./src/app/Dashboard/result-test/result-test.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-top: 40px;\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\" style=\"text-align: center;\">\r\n      <h4>Result of the Test</h4>\r\n      <button class=\"adone pointer-cursor btnbottom\" (click)=\"done()\"><a style=\"color:white;text-align: center\" >Done</a></button>\r\n    </div>\r\n\r\n  </div>\r\n\r\n\r\n\r\n\r\n  <div class=\"row\" style=\"margin-top:5px;\" *ngIf=\"assessment_no === '1'\">\r\n    <div class=\"col-md-4\">\r\n      <div class=\"card bg-light\" style=\"background-color: #FFFFFF;\">\r\n        <div class=\"card-body\">\r\n          <p style=\"text-align: center; font-weight: bold;font-family: sans-serif;color: #FF4F33;font-size: 20px;\">You\r\n            have Scored</p>\r\n          <p style=\"text-align: center;font-weight: bold;font-family: sans-serif;color: #FF4F33;font-size: 48px;\">{{skillData.obtain_score}}</p>\r\n          <p style=\"text-align: center;font-weight: bold;font-family: sans-serif;color: #999999;font-size: 14px;\">Out\r\n            of {{skillData.total_marks}}</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-8\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <div class=\"card bg-light\" style=\"background-color: #FFFFFF;\">\r\n            <div class=\"card-body\">\r\n              <div class=\"col-md-8\">\r\n                <p style=\"font-weight: bold;font-family: sans-serif;color: black;font-size: 20px;\">Congratulations !!\r\n                  Your profile has been upgraded to Foundation Level</p>\r\n              </div>\r\n              <div class=\"col-md-4\">\r\n                <button class=\"btn btn-primary btnfoundation\">Foundaion</button>\r\n                <p style=\"color:#FF4F33;\">Learn more about this level</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12\" style=\"margin-top: 15px;\">\r\n          <div class=\"card bg-light\">\r\n            <div class=\"card-body\">\r\n              <p style=\"font-size: 16px;font-family: sans-serif;color:#111111;\">\r\n                Minemark Suggest\r\n              </p>\r\n              <p style=\"font-size: 14px;font-family: sans-serif;color: #111111;\">\r\n                A professinal mentor can help you learn quicker and help you in achieving a desired goal. Based on your\r\n                score, we request you to go for a mentorship program\r\n              </p>\r\n              <button class=\"btn btnreq\">Request Mentorship</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n      <div class=\"row\" style=\"margin-top: 5px;\">\r\n        <div class=\"col-md-8\">\r\n\r\n          <div class=\"card-body\" style=\"width: 70%;text-align: center;\">\r\n            <fieldset>\r\n              <legend style=\"text-align: center\">Test Summary</legend>\r\n\r\n              <div class=\"row\" style=\"margin-top: 10px;\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Test Name</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{skillData.skill_name}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Taken On</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{skillData.taken_date | date:'MMMM d, y' }}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Time Taken</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{skillData.time_taken}} Mins</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Total Questions Face</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{totalquestion}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Questions Attempted</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{attempted}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Questions skipped</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{skipped}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Test Status</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{skillData.test_status}}</p>\r\n                </div>\r\n              </div>\r\n            </fieldset>\r\n\r\n\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n          &nbsp;\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n\r\n\r\n  <div class=\"row\" style=\"margin-top:5px;\" *ngIf=\"assessment_no === '0'\">\r\n    <div class=\"col-md-4\">\r\n      <div class=\"card bg-light\" style=\"background-color: #FFFFFF;\">\r\n        <div class=\"card-body\">\r\n          <p style=\"text-align: center; font-weight: bold;font-family: sans-serif;color: #FF4F33;font-size: 20px;\">You\r\n            have Scored</p>\r\n          <p style=\"text-align: center;font-weight: bold;font-family: sans-serif;color: #FF4F33;font-size: 48px;\">{{createData.obtain_score | number:'4.2-2'}}</p>\r\n          <p style=\"text-align: center;font-weight: bold;font-family: sans-serif;color: #999999;font-size: 14px;\">Out\r\n            of {{createData.total_marks}}</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-8\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <div class=\"card bg-light\" style=\"background-color: #FFFFFF;\">\r\n            <div class=\"card-body\">\r\n              <div class=\"col-md-8\">\r\n                <p style=\"font-weight: bold;font-family: sans-serif;color: black;font-size: 20px;\">Congratulations !!\r\n                  Your profile has been upgraded to Foundation Level</p>\r\n              </div>\r\n              <div class=\"col-md-4\">\r\n                <button class=\"btn btn-primary btnfoundation\">Foundaion</button>\r\n                <p style=\"color:#FF4F33;\">Learn more about this level</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12\" style=\"margin-top: 15px;\">\r\n          <div class=\"card bg-light\">\r\n            <div class=\"card-body\">\r\n              <p style=\"font-size: 16px;font-family: sans-serif;color:#111111;\">\r\n                Minemark Suggest\r\n              </p>\r\n              <p style=\"font-size: 14px;font-family: sans-serif;color: #111111;\">\r\n                A professinal mentor can help you learn quicker and help you in achieving a desired goal. Based on your\r\n                score, we request you to go for a mentorship program\r\n              </p>\r\n              <button class=\"btn btnreq\">Request Mentorship</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n      <div class=\"row\" style=\"margin-top: 5px;\">\r\n        <div class=\"col-md-8\">\r\n\r\n          <div class=\"card-body\" style=\"width: 70%;text-align: center;\">\r\n            <fieldset>\r\n              <legend style=\"text-align: center\">Test Summary</legend>\r\n\r\n              <div class=\"row\" style=\"margin-top: 10px;\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Test Name</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{createData.skill_name}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Taken On</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{createData.taken_date}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Time Taken</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{createData.time_taken}} Mins</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Total Questions Face</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{totalassment}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Questions Attempted</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{attemptedassment}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Questions skipped</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{skipassment}}</p>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;font-family: sans-serif;float: left;\">Test Status</p>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <p style=\"font-size: 18px;color:#999999;\">{{statusmesssage}}</p>\r\n                </div>\r\n              </div>\r\n            </fieldset>\r\n\r\n\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n          &nbsp;\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n  <div class=\"row\" style=\"margin-top: 40px;\">\r\n    <div class=\"col-md-12\">\r\n      <footer class=\"app-footer\" style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n        &copy; <a style=\"color:black;font-size: 16px;\">Minemark Solutions Private Limited</a> 2019 </footer>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/result-test/result-test.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/Dashboard/result-test/result-test.component.ts ***!
  \****************************************************************/
/*! exports provided: ResultTestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultTestComponent", function() { return ResultTestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ResultTestComponent = /** @class */ (function () {
    function ResultTestComponent(auth, router, activatedRoute, share) {
        var _this = this;
        this.auth = auth;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.share = share;
        this.data = this.share.getData();
        this.skillData = ["obtain_score"];
        this.SetData = [];
        this.createData = [];
        if (this.data) {
            this.SetData = this.data[2];
            if (this.SetData === "undefined") {
                this.rating = "-1";
                this.auth
                    .postDetail("/mash_mcq_assessment_result", this.token, {
                    filename: this.data[0],
                    candi_skillset: this.data[1],
                    feedback: this.data[3],
                    rating: this.rating
                })
                    .then(function (data) {
                    _this.ratingData = data.json().result;
                })
                    .catch(function (err) {
                    console.log(err);
                });
            }
        }
        else {
        }
    }
    ResultTestComponent.prototype.ngOnInit = function () {
        this.perviousPageData();
    };
    ResultTestComponent.prototype.perviousPageData = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.candi_skillset = option["id"];
            _this.token = localStorage.getItem("token");
            _this.assessment_no = localStorage.getItem("candi_skillset1");
            debugger;
            if (parseInt(_this.assessment_no) === 1) {
                _this.auth
                    .postDetail("/mash_mcq_assessment_result", _this.token, {
                    filename: _this.data[0],
                    candi_skillset: _this.data[1],
                    feedback: _this.data[3],
                    rating: _this.data[2]
                })
                    .then(function (data) {
                    var sample = data.json();
                    _this.skillData = data.json()["skillData"];
                    _this.attempted = data.json()["attempted"];
                    _this.skipped = data.json()["skipped"];
                    _this.totaldatetime = data.json()["totaldatetime"];
                    _this.skill = data.json()["skill"];
                    _this.totalquestion = data.json()["totalquestion"];
                    _this.status = data.json()["status"];
                    _this.message = data.json()["message"];
                    _this.Skilldata(_this.skillData);
                })
                    .catch(function (err) {
                    console.log(err);
                });
            }
            else {
                _this.auth
                    .postDetail("/speech_assesment_marks", _this.token, {
                    filename: _this.data[0],
                    candi_skillset: _this.data[1],
                    feedback: _this.data[3],
                    rating: _this.data[2]
                })
                    .then(function (data) {
                    _this.sampleassesment = data.json();
                    console.log("sample", _this.sampleassesment);
                    _this.createData = data.json()["createData"];
                    console.log("this.createData", _this.createData);
                    _this.attemptedassment = data.json()["attempted"];
                    console.log(" this.attemptedassment", _this.attemptedassment);
                    _this.skipassment = data.json()["skip"];
                    console.log("this.skipassment", _this.skipassment);
                    _this.totalassment = _this.attemptedassment + _this.skipassment;
                    _this.statusmesssage = data.json()["statusmessage"];
                    console.log("this.statusmesssage", _this.statusmesssage);
                })
                    .catch(function (err) {
                    console.log(err);
                });
            }
        });
    };
    ResultTestComponent.prototype.Skilldata = function (skillID) {
        var _this = this;
        this.auth
            .get("/skills", this.token)
            .then(function (list) {
            _this.skills = list.json().result;
            var body = JSON.stringify(_this.skills);
            var stringify = JSON.parse(body);
            for (var i = 0; i < stringify.length; i++) {
                var responseData = stringify[i]["id"];
                if (parseInt(skillID.skill_id) === responseData) {
                    _this.skilltemp = stringify[i]["skill"];
                }
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    ResultTestComponent.prototype.done = function () {
        this.router.navigateByUrl("/rating");
    };
    ResultTestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-result-test",
            template: __webpack_require__(/*! ./result-test.component.html */ "./src/app/Dashboard/result-test/result-test.component.html"),
            styles: [__webpack_require__(/*! ./result-test.component.css */ "./src/app/Dashboard/result-test/result-test.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_sharedata_service__WEBPACK_IMPORTED_MODULE_3__["SharedataService"]])
    ], ResultTestComponent);
    return ResultTestComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/sidenavbar/sidenavbar.component.css":
/*!***************************************************************!*\
  !*** ./src/app/Dashboard/sidenavbar/sidenavbar.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p{\r\n    font-size: 14px;\r\n}\r\nbody {\r\n    font-size: .875rem;\r\n  }\r\n.feather {\r\n    width: 16px;\r\n    height: 16px;\r\n    vertical-align: text-bottom;\r\n  }\r\na{\r\n      font-family: sans-serif;\r\n      font-size: 14px;\r\n  }\r\nli{\r\n      float: right;\r\n  }\r\n/*\r\n   * Sidebar\r\n   */\r\n.sidebar {\r\n    position: fixed;\r\n    top: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    z-index: 100; /* Behind the navbar */\r\n    padding: 48px 0 0; /* Height of navbar */\r\n    box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);\r\n  }\r\n.sidebar-sticky {\r\n    position: relative;\r\n    top: 0;\r\n    height: calc(100vh - 48px);\r\n    padding-top: .5rem;\r\n    overflow-x: hidden;\r\n    overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */\r\n  }\r\n@supports ((position: -webkit-sticky) or (position: sticky)) {\r\n    .sidebar-sticky {\r\n      position: -webkit-sticky;\r\n      position: sticky;\r\n    }\r\n  }\r\n.sidebar .nav-link {\r\n    font-weight: 500;\r\n    color: #333;\r\n  }\r\n.sidebar .nav-link .feather {\r\n    margin-right: 4px;\r\n    color: #999;\r\n  }\r\n.sidebar .nav-link.active {\r\n    color: #007bff;\r\n  }\r\n.sidebar .nav-link:hover .feather,\r\n  .sidebar .nav-link.active .feather {\r\n    color: inherit;\r\n  }\r\n.sidebar-heading {\r\n    font-size: .75rem;\r\n    text-transform: uppercase;\r\n  }\r\n/*\r\n   * Content\r\n   */\r\n[role=\"main\"] {\r\n    padding-top: 48px; /* Space for fixed navbar */\r\n  }\r\n/*\r\n   * Navbar\r\n   */\r\n.navbar-brand {\r\n    padding-top: .75rem;\r\n    padding-bottom: .75rem;\r\n    font-size: 1rem;\r\n    background-color: rgba(0, 0, 0, .25);\r\n    box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);\r\n  }\r\n.navbar .form-control {\r\n    padding: .75rem 1rem;\r\n    border-width: 0;\r\n    border-radius: 0;\r\n  }\r\n.nav>li>a {\r\n    /* position: relative; */\r\n    display: block;\r\n    text-align: left;\r\n    padding: 10px 15px;\r\n}\r\n.form-control-dark {\r\n    color: #fff;\r\n    background-color: rgba(255, 255, 255, .1);\r\n    border-color: rgba(255, 255, 255, .1);\r\n  }\r\n.form-control-dark:focus {\r\n    border-color: transparent;\r\n    box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);\r\n  }\r\n  "

/***/ }),

/***/ "./src/app/Dashboard/sidenavbar/sidenavbar.component.html":
/*!****************************************************************!*\
  !*** ./src/app/Dashboard/sidenavbar/sidenavbar.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- \r\n<div class=\"sidebar-wrapper\" style=\"width:100px;\">\r\n  \r\n  <ul class=\"nav flex-column\">\r\n    <li class=\"active\">\r\n      <a href=\"dashboard.html\">\r\n        <i class=\"material-icons\">dashboard</i>\r\n        <p>Dashboard</p>\r\n      </a>\r\n    </li>\r\n    <li>\r\n      <a href=\"user.html\">\r\n        <i class=\"material-icons\">person</i>\r\n        <p>User Profile</p>\r\n      </a>\r\n    </li>\r\n    <li>\r\n      <a href=\"table.html\">\r\n        <i class=\"material-icons\">content_paste</i>\r\n        <p>Table List</p>\r\n      </a>\r\n    </li>\r\n    <li>\r\n\r\n    </li>\r\n    <li>\r\n      <a href=\"typography.html\">\r\n        <i class=\"material-icons\">library_books</i>\r\n        <p>Typography</p>\r\n      </a>\r\n    </li>\r\n    <li>\r\n      <a href=\"icons.html\">\r\n        <i class=\"material-icons\">bubble_chart</i>\r\n        <p>Icons</p>\r\n      </a>\r\n    </li>\r\n    <li>\r\n      <a href=\"maps.html\">\r\n        <i class=\"material-icons\">location_on</i>\r\n        <p>Maps</p>\r\n      </a>\r\n    </li>\r\n    <li>\r\n      <a href=\"notifications.html\">\r\n        <i class=\"material-icons text-gray\">notifications</i>\r\n        <p>Notifications</p>\r\n      </a>\r\n    </li>\r\n    <li class=\"active-pro\">\r\n      <a href=\"upgrade.html\">\r\n        <i class=\"material-icons\">unarchive</i>\r\n        <p>Upgrade to PRO</p>\r\n      </a>\r\n    </li>\r\n  </ul>\r\n</div> -->\r\n<!-- <div style=\"width:900px;margin-left:150px;margin-top: -500px;\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n    <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"card-title\">\r\n            <p>hellop</p>\r\n          </div>\r\n        </div>\r\n        </div>\r\n        <div class=\"card\">\r\n            <div class=\"card-body\">\r\n              <div class=\"card-title\">\r\n                <p>hellop</p>\r\n              </div>\r\n            </div>\r\n            </div>\r\n            <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"card-title\">\r\n                    <p>hellop</p>\r\n                  </div>\r\n                </div>\r\n                </div>\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                      <div class=\"card-title\">\r\n                        <p>hellop</p>\r\n                      </div>\r\n                    </div>\r\n                    </div>\r\n                    <div class=\"card\">\r\n                        <div class=\"card-body\">\r\n                          <div class=\"card-title\">\r\n                            <p>hellop</p>\r\n                          </div>\r\n                        </div>\r\n                        </div>\r\n                        <div class=\"card\">\r\n                            <div class=\"card-body\">\r\n                              <div class=\"card-title\">\r\n                                <p>hellop</p>\r\n                              </div>\r\n                            </div>\r\n                            </div>\r\n</div>\r\n</div>\r\n</div> -->\r\n\r\n<nav class=\"navbar fixed-top flex-md-nowrap p-0 shadow \"  style=\"\tbackground-color: #FF6600;\tbackground: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\">\r\n  <div ><a class=\"navbar-brand p-0 ml-0\" href=\"#\" style=\"background-color: white;\"><img style=\"margin-left:50px;align-content: center;padding:5px;\" src=\"../../../assets/MineMark Logo-Orange.svg\"></a></div>\r\n  <!-- <input class=\"form-control m-3\" type=\"text\" placeholder=\"Search\" aria-label=\"Search\"> -->\r\n  \r\n  <a href=\"#\" class=\"m-3 \" ><img src=\"../../../assets/notification.svg\" stye=\"width:20px;height20px;\" class=\"img-responsive\"></a>\r\n  \r\n  <ul class=\"navbar-nav px-3\">\r\n    <li class=\"nav-item text-nowrap\">\r\n      <a class=\"nav-link\" href=\"#\">Sign out</a>\r\n    </li>\r\n  </ul>\r\n  \r\n</nav>\r\n\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <nav class=\"col-md-2 d-none d-md-block\" style=\"margin-top:50px;\">\r\n      <div class=\"sidebar-sticky\">\r\n        <ul class=\"nav flex-column\">\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link active\" href=\"#\">\r\n              <span data-feather=\"home\"></span>\r\n              \r\n              Dashboard <span class=\"sr-only\">(current)</span>\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" routerLink=\"/update_profile\">\r\n              <span data-feather=\"file\"></span>\r\n               Profile\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" routerLink=\"/skill_update\">\r\n              <span data-feather=\"shopping-cart\"></span>\r\n              Skill Update\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" routerLink=\"/job\">\r\n              <img src=\"../../../assets/job-offers-icon.png\" style=\"width:20px;height:15px;\">\r\n              <span data-feather=\"users\"></span>\r\n              Job Offer\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" routerLink=\"/interview\">\r\n              <img src=\"../../../assets//interviews-icon.png\" style=\"width:20px;height:15px;\">\r\n              <span data-feather=\"bar-chart-2\"></span>\r\n              Interview\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"#\">\r\n              <span data-feather=\"layers\"></span>\r\n              Integrations\r\n            </a>\r\n          </li>\r\n        </ul>\r\n\r\n        <!-- <h6 class=\"sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted\">\r\n          <span>Saved reports</span>\r\n          <a class=\"d-flex align-items-center text-muted\" href=\"#\">\r\n            <span data-feather=\"plus-circle\"></span>\r\n          </a>\r\n        </h6>\r\n        <ul class=\"nav flex-column mb-2\">\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"#\">\r\n              <span data-feather=\"file-text\"></span>\r\n              Current month\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"#\">\r\n              <span data-feather=\"file-text\"></span>\r\n              Last quarter\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"#\">\r\n              <span data-feather=\"file-text\"></span>\r\n              Social engagement\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"#\">\r\n              <span data-feather=\"file-text\"></span>\r\n              Year-end sale\r\n            </a>\r\n          </li>\r\n        </ul> -->\r\n      </div>\r\n    </nav>\r\n  </div>\r\n</div>\r\n<!-- <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4\">\r\n          <router-outlet></router-outlet>\r\n      </main> -->\r\n\r\n\r\n\r\n\r\n\r\n\r\n      "

/***/ }),

/***/ "./src/app/Dashboard/sidenavbar/sidenavbar.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/Dashboard/sidenavbar/sidenavbar.component.ts ***!
  \**************************************************************/
/*! exports provided: SidenavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidenavbarComponent", function() { return SidenavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidenavbarComponent = /** @class */ (function () {
    function SidenavbarComponent() {
    }
    SidenavbarComponent.prototype.ngOnInit = function () {
    };
    SidenavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidenavbar',
            template: __webpack_require__(/*! ./sidenavbar.component.html */ "./src/app/Dashboard/sidenavbar/sidenavbar.component.html"),
            styles: [__webpack_require__(/*! ./sidenavbar.component.css */ "./src/app/Dashboard/sidenavbar/sidenavbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidenavbarComponent);
    return SidenavbarComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/skill-rate-test/skill-rate-test.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/Dashboard/skill-rate-test/skill-rate-test.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btnsubmit{\r\n    background: linear-gradient(270deg, #F47337 0%, #F23F42 100%);\r\n    width: 179px;\r\n    font-size: 18px;\r\n              height: 50px;\r\n              color: #ffffff;border-radius: 25px;\r\n}\r\n.textarea-container {\r\n    position: relative;\r\n  }\r\n.textarea-container textarea {\r\n    width: 100%;\r\n    height: 100px;\r\n    box-sizing: border-box;\r\n  }\r\n.textarea-container button {\r\n    position: absolute;\r\n    bottom: 10%;\r\n    left: 20px;\r\n    width: 150px;\r\n    color: #FF4F33;\r\n    background-color: #FFF5F5F5;\r\n    border-radius: 25px;\r\n    font-weight: bold;\r\n  }\r\n.circle-text {\r\n    width:20%;\r\n}\r\n.circle-text:after {\r\n    content: \"\";\r\n    display: block;\r\n    width: 100%;\r\n    height:0;\r\n    padding-bottom: 100%; \r\n    border-radius: 50%;\r\n    border: 3px solid #FF4F33;\r\n}\r\n.circle-text div {\r\n    float:left;\r\n    width:100%;\r\n    padding-top:2%;\r\n    line-height:1em;\r\n    margin-top:10px;\r\n    text-align:center;\r\n    color:white;\r\n}\r\n.button{\r\n    display: inline-block;\r\n    background-color: #FFFFFF;\r\n    border-radius: 25px;\r\n    font-family: sans-serif;\r\n    font-size: 14px;\r\n    color: #111111;\r\n    padding: 8px 12px;\r\n    cursor: pointer;\r\n    font-weight: bold;\r\n}"

/***/ }),

/***/ "./src/app/Dashboard/skill-rate-test/skill-rate-test.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/Dashboard/skill-rate-test/skill-rate-test.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"margin-top: 50px\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-6\" style=\"text-align: center; color: #FF4F33;font-size: 18px\">Cancel Test</div>\r\n    <div class=\"col-md-6\" style=\"text-align: center; color: #FF4F33;font-size: 18px\">&nbsp;</div>\r\n  </div>\r\n  <div class=\"row\" style=\"margin-top: 30px\">\r\n    <div class=\"col-md-6 offset-3\">\r\n      <div class=\"card bg-light\">\r\n        <div class=\"card-body\">\r\n          <div class=\"row\" style=\"margin-top: 20px;\">\r\n            <div class=\"col-md-12\">\r\n              <p style=\"text-align: center;font-family: sans-serif;font-size: 14px;font-weight: bold;\">Question 1 out\r\n                of 30</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\" style=\"margin-top: 20px;\">\r\n            <div class=\"col-md-12\">\r\n              <p style=\"text-align: center;font-family: sans-serif;font-size: 20px;font-weight: bold;\">Why do people\r\n                shop online?</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\" style=\"margin-top: 20px;\">\r\n            <div class=\"col-md-12\">\r\n              <div class=\"textarea-container\">\r\n                <textarea name=\"foo\" style=\"font-family: sans-serif;font-size: 16px;\" placeholder=\"Speak your answer\"></textarea>\r\n                <button class=\"btn btn-primary\">Clear Answer</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12 offset-4\">\r\n              <button class=\"btn\" style=\"width: 100px;height: 100px;border-radius: 50%;margin-left: 30px;\tbackground-color: #FFFFFF;\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                    <img src=\"../../../assets/mic.svg\">\r\n                  </div>\r\n                  <div class=\"col-md-12\">\r\n                    <a style=\"font-size: small;\">Tap to Speak</a>\r\n                  </div>\r\n                </div>\r\n              </button> </div>\r\n          </div>\r\n          <div class=\"row\" style=\"margin-top: 10px;\">\r\n            <div class=\"col-md-12 offset-4\">\r\n              <div>\r\n                <p class=\"button\" style=\"margin-left: 20px;\">Time left: <a style=\"color:#FF4F33;\">00:53</a></p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\" style=\"margin-top: 20px;\">\r\n            <div class=\"col-md-12 offset-4\">\r\n              <button class=\"btn btnsubmit\">Submit</button>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\" style=\"margin-top: 20px;\">\r\n            <div class=\"col-md-12 offset-5\">\r\n              <p style=\"color:#FF4F33;font-size: 16px;\">Skip the question</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\">&nbsp;&nbsp;&nbsp;</div>\r\n\r\n<footer class=\"app-footer\" style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n  &copy; <a  style=\"color:black;font-size: 16px;\">MineMark Solutions Private Limited</a> 2018 </footer>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/skill-rate-test/skill-rate-test.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/Dashboard/skill-rate-test/skill-rate-test.component.ts ***!
  \************************************************************************/
/*! exports provided: SkillRateTestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillRateTestComponent", function() { return SkillRateTestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SkillRateTestComponent = /** @class */ (function () {
    function SkillRateTestComponent() {
    }
    SkillRateTestComponent.prototype.ngOnInit = function () {
    };
    SkillRateTestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-skill-rate-test',
            template: __webpack_require__(/*! ./skill-rate-test.component.html */ "./src/app/Dashboard/skill-rate-test/skill-rate-test.component.html"),
            styles: [__webpack_require__(/*! ./skill-rate-test.component.css */ "./src/app/Dashboard/skill-rate-test/skill-rate-test.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SkillRateTestComponent);
    return SkillRateTestComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/skill-update/skill-update.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/Dashboard/skill-update/skill-update.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "fieldset \r\n\t{\r\n\t\tborder: 1px solid #ddd !important;\r\n\t\tmargin: 0;\r\n\t\tmin-width: 0;\r\n\t\tpadding: 10px;       \r\n\t\tposition: relative;\r\n\t\tborder-radius:4px;\r\n\t\tbackground-color:#f5f5f5;\r\n\t\tpadding-left:10px!important;\r\n\t}\t\r\n\t\r\n\t\tlegend\r\n\t\t{\r\n\t\t\tfont-size:18px;\r\n            font-weight:bold;\r\n            text-align: center;\r\n\t\t\tmargin-bottom: 0px; \r\n\t\t\tfont-family: sans-serif;\r\n\t\t\twidth: 90%;\r\n\t\t\theight: 60px; \r\n\t\t\tbackground: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\r\n\t\t\tborder: 1px solid #ddd;\r\n\t\t\tborder-radius: 4px; \r\n            padding: 20px 5px 5px 10px;\r\n            color: #f5f5f5; \r\n\t\t\t/* background-color: #ffffff; */\r\n\t\t}\t\r\n\t\r\n\t\tlabel{\r\n\tcolor: black;\r\n\tfont-size: 14px;\r\n}\t\r\n\t\r\n\t\t/* input{\r\n\tfont-size: 20px;\r\n} */\t\r\n\t\r\n\t\t.help-block{\r\n\tfont-family: sans-serif;\r\n\tfont-size: 14px;\r\n}\t\r\n\t\r\n\t\ttr{\r\n\theight: 45px;\r\n}\t\r\n\t\r\n\t\tbutton{\r\n\twidth: 179px;\r\n\tfont-size: 18px;\r\n\tfont-family: sans-serif;\r\n}\r\n"

/***/ }),

/***/ "./src/app/Dashboard/skill-update/skill-update.component.html":
/*!********************************************************************!*\
  !*** ./src/app/Dashboard/skill-update/skill-update.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <img\r\n        style=\"display:table-cell;margin: auto; vertical-align:middle; text-align:center\"\r\n        src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/MineMark+Logo-Orange.svg\"\r\n      />\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\" style=\"text-align: center\">\r\n      <h2 style=\"font-family:sans-serif;\">\r\n        Minemark Solutions Private Limited\r\n      </h2>\r\n    </div>\r\n  </div>\r\n\r\n  <form [formGroup]=\"myform\">\r\n    <fieldset>\r\n      <legend>Update Your Profile</legend>\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"firstname\"\r\n              >First Name<span class=\"asteriskField\" style=\"color:red\">\r\n                *\r\n              </span></label\r\n            >\r\n\r\n            <input\r\n              type=\"text\"\r\n              class=\"form-control\"\r\n              required\r\n              placeholder=\"Enter first name\"\r\n              formControlName=\"firstname\"\r\n              [(ngModel)]=\"userData.candi_firstname\"\r\n              name=\"firstname\"\r\n              pattern=\"[A-Za-z]+\"\r\n              [ngClass]=\"{ 'is-invalid': submitted && firstname.errors }\"\r\n            />\r\n            <div\r\n              class=\"help-block\"\r\n              *ngIf=\"\r\n                myform.get('firstname').touched &&\r\n                myform.get('firstname').invalid\r\n              \"\r\n            >\r\n              <p *ngIf=\"firstname.errors.required\">\r\n                Empty! Please fill the text box\r\n              </p>\r\n              <p *ngIf=\"firstname.errors?.pattern\">Please Enter a Valid Name</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"lastname\"\r\n              >Last Name<span class=\"asteriskField\" style=\"color:red\">\r\n                *\r\n              </span></label\r\n            >\r\n            <input\r\n              type=\"text\"\r\n              name=\"lastname\"\r\n              required\r\n              class=\"form-control\"\r\n              placeholder=\"Enter lastname\"\r\n              formControlName=\"lastname\"\r\n              [(ngModel)]=\"userData.candi_lastname\"\r\n              pattern=\"[A-Za-z]+\"\r\n              [ngClass]=\"{ 'is-invalid': submitted && lastname.errors }\"\r\n            />\r\n\r\n            <div\r\n              class=\"help-block\"\r\n              *ngIf=\"\r\n                myform.get('lastname').touched && myform.get('lastname').invalid\r\n              \"\r\n            >\r\n              <p *ngIf=\"lastname.errors.required\">\r\n                Empty! Please fill the text box\r\n              </p>\r\n              <p *ngIf=\"lastname.errors?.pattern\">Please Enter a Valid Name</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">&nbsp;</div>\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"Dob\"\r\n              >Date of Birth\r\n              <span class=\"asteriskField\" style=\"color:red\"> * </span>\r\n            </label>\r\n\r\n            <input\r\n              type=\"date\"\r\n              max=\"{{ newminAge | date: 'yyyy-MM-dd' }}\"\r\n              placeholder=\"DD-MM-YYYY\"\r\n              required\r\n              class=\"form-control\"\r\n              formControlName=\"dob\"\r\n              [(ngModel)]=\"userData.candi_dob\"\r\n            />\r\n            <span\r\n              class=\"help-block\"\r\n              *ngIf=\"myform.get('dob').touched && myform.get('dob').invalid\"\r\n              >DOB is Required</span\r\n            >\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"gender\">Gender</label>\r\n            <div>\r\n              <input\r\n                type=\"radio\"\r\n                name=\"radio\"\r\n                value=\"Male\"\r\n                [ngModelOptions]=\"{ standalone: true }\"\r\n                [(ngModel)]=\"userData.candi_gender\"\r\n              />\r\n              <span style=\"font-size:14px;margin-left: 10px;\">Male</span>\r\n              <input\r\n                type=\"radio\"\r\n                name=\"radio\"\r\n                value=\"Female\"\r\n                [ngModelOptions]=\"{ standalone: true }\"\r\n                style=\"margin-left:30px;\"\r\n                [(ngModel)]=\"userData.candi_gender\"\r\n              />\r\n              <span style=\"font-size:14px;margin-left: 10px;\">Female</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">&nbsp;</div>\r\n      <div class=\"row\">\r\n        <div class=\"container\"><label>Current Location</label></div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"Preferred location\"\r\n              >State<span class=\"asteriskField\" style=\"color:red\">\r\n                *\r\n              </span></label\r\n            >\r\n            <select\r\n              class=\"form-control selectpicker\"\r\n              formControlName=\"state\"\r\n              style=\"height: 40px;\"\r\n              [(ngModel)]=\"userData.candi_state\"\r\n              name=\"state\"\r\n              required\r\n              (change)=\"changeCountry($event.target.value)\"\r\n            >\r\n              <option\r\n                *ngFor=\"let count of statecity; let i = index\"\r\n                value=\"{{ i }}\"\r\n                >{{ count.name }}</option\r\n              >\r\n            </select>\r\n            <span\r\n              class=\"help-block\"\r\n              *ngIf=\"myform.get('state').touched && myform.get('state').invalid\"\r\n              >State is Required</span\r\n            >\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"Preferred location\"\r\n              >City<span class=\"asteriskField\" style=\"color:red\">\r\n                *\r\n              </span></label\r\n            >\r\n            <select\r\n              class=\"form-control selectpicker \"\r\n              style=\"height: 40px;\"\r\n              formControlName=\"city\"\r\n              required\r\n              [(ngModel)]=\"userData.candi_city\"\r\n            >\r\n              <option *ngFor=\"let city of cityselect\">{{ city.name }}</option>\r\n            </select>\r\n            <span\r\n              class=\"help-block\"\r\n              *ngIf=\"myform.get('city').touched && myform.get('city').invalid\"\r\n              >City is Required</span\r\n            >\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">&nbsp;</div>\r\n      <div class=\"row\">\r\n        <div class=\"container\"><label>Preferred Location</label></div>\r\n        <div class=\"card\" style=\"margin-left: 50px;\">\r\n          <div class=\"card-body\">\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Any\"\r\n                (change)=\"anyClick('Any', $event.target.checked)\"\r\n                [checked]=\"model.Any.checked\"\r\n                name=\"any\"\r\n              /><label>Any</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Hyderabad\"\r\n                (change)=\"anyClick('Hyderabad', $event.target.checked)\"\r\n                [checked]=\"model.Hyderabad.checked\"\r\n                [disabled]=\"model.Hyderabad.disable\"\r\n                name=\"hyderabad\"\r\n              /><label>Hyderabad</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Chennai\"\r\n                (change)=\"anyClick('Chennai', $event.target.checked)\"\r\n                [checked]=\"model.Chennai.checked\"\r\n                [disabled]=\"model.Chennai.disable\"\r\n                name=\"chennai\"\r\n              /><label>Chennai</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Pune\"\r\n                (change)=\"anyClick('Pune', $event.target.checked)\"\r\n                [checked]=\"model.Pune.checked\"\r\n                [disabled]=\"model.Pune.disable\"\r\n                name=\"pune\"\r\n              /><label>Pune</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Mumbai\"\r\n                (change)=\"anyClick('Mumbai', $event.target.checked)\"\r\n                [checked]=\"model.Mumbai.checked\"\r\n                [disabled]=\"model.Mumbai.disable\"\r\n                name=\"mumbai\"\r\n              /><label>Mumbai</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Bangalore\"\r\n                (change)=\"anyClick('Bangalore', $event.target.checked)\"\r\n                [checked]=\"model.Bangalore.checked\"\r\n                [disabled]=\"model.Bangalore.disable\"\r\n                name=\"bangalore\"\r\n              /><label>Bangalore</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Kolkata\"\r\n                (change)=\"anyClick('Kolkata', $event.target.checked)\"\r\n                [checked]=\"model.Kolkata.checked\"\r\n                [disabled]=\"model.Kolkata.disable\"\r\n                name=\"kolkata\"\r\n              /><label>Kolkata</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Delhi\"\r\n                (change)=\"anyClick('Delhi', $event.target.checked)\"\r\n                [checked]=\"model.Delhi.checked\"\r\n                [disabled]=\"model.Delhi.disable\"\r\n                name=\"delhi\"\r\n              /><label>Delhi</label>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <input\r\n                type=\"checkbox\"\r\n                value=\"Other\"\r\n                (change)=\"anyClick('Other', $event.target.checked)\"\r\n                [checked]=\"model.Other.checked\"\r\n                [disabled]=\"model.Other.disable\"\r\n                name=\"other\"\r\n              /><label>Other</label>\r\n              <span *ngIf=\"model.Other.checked\">\r\n                <div class=\"form-group\">\r\n                  <label for=\"Preferred location\"\r\n                    >State<span class=\"asteriskField\" style=\"color:red\">\r\n                      *\r\n                    </span></label\r\n                  >\r\n                  <select\r\n                    class=\"form-control selectpicker\"\r\n                    formControlName=\"state\"\r\n                    style=\"height: 40px;\"\r\n                    [(ngModel)]=\"other_state\"\r\n                    (change)=\"changeOtherCountry($event.target.value)\"\r\n                  >\r\n                    <option\r\n                      *ngFor=\"let count of statecity; let i = index\"\r\n                      value=\"{{ i }}\"\r\n                      >{{ count.name }}</option\r\n                    >\r\n                  </select>\r\n\r\n                  <span\r\n                    class=\"help-block\"\r\n                    *ngIf=\"\r\n                      myform.get('state').touched && myform.get('state').invalid\r\n                    \"\r\n                    >State is Required</span\r\n                  >\r\n                </div>\r\n                <div class=\"form-group\">\r\n                  <label for=\"Preferred location\"\r\n                    >City<span class=\"asteriskField\" style=\"color:red\">\r\n                      *\r\n                    </span></label\r\n                  >\r\n                  <select\r\n                    class=\"form-control selectpicker \"\r\n                    style=\"height: 40px;\"\r\n                    formControlName=\"city\"\r\n                    [(ngModel)]=\"other_city\"\r\n                  >\r\n                    <option *ngFor=\"let city of cityselect\">{{\r\n                      city.name\r\n                    }}</option>\r\n                  </select>\r\n                  <span\r\n                    class=\"help-block\"\r\n                    *ngIf=\"\r\n                      myform.get('city').touched && myform.get('city').invalid\r\n                    \"\r\n                    >City is Required</span\r\n                  >\r\n                </div>\r\n              </span>\r\n              <span *ngIf=\"!model.Other.checked\"></span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">&nbsp;</div>\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"skillset\"\r\n              >Select Skill Area<span class=\"asteriskField\" style=\"color:red\">\r\n                *\r\n              </span></label\r\n            >\r\n            <ng-multiselect-dropdown\r\n              [placeholder]=\"'choose skillset'\"\r\n              (onDeSelect)=\"onItemDeSelect($event)\"\r\n              [data]=\"selectskill\"\r\n              [(ngModel)]=\"userData.candi_skill_set\"\r\n              [settings]=\"dropdownSettings\"\r\n              (onSelect)=\"onItemSelect($event)\"\r\n              (onSelectAll)=\"onSelectAll($event)\"\r\n              formControlName=\"skill\"\r\n            >\r\n            </ng-multiselect-dropdown>\r\n\r\n            <span\r\n              class=\"help-block\"\r\n              *ngIf=\"myform.get('skill').touched && myform.get('skill').invalid\"\r\n              >Skill is Required</span\r\n            >\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"skillset\"\r\n              >Select Actual Skill<span class=\"asteriskField\" style=\"color:red\">\r\n                *\r\n              </span></label\r\n            >\r\n            <ng-multiselect-dropdown\r\n              [placeholder]=\"'choose actual skill'\"\r\n              [data]=\"categoryselectskill\"\r\n              [(ngModel)]=\"candi_skillset\"\r\n              [settings]=\"dropdownActualSettings\"\r\n              (onSelect)=\"onItemSelectActual($event)\"\r\n              (onDeSelect)=\"onItemDeSelectactual($event)\"\r\n              (onSelectAll)=\"onSelectAllActual($event)\"\r\n              formControlName=\"actualskill\"\r\n            >\r\n            </ng-multiselect-dropdown>\r\n            <span\r\n              class=\"help-block\"\r\n              *ngIf=\"\r\n                myform.get('actualskill').touched &&\r\n                myform.get('actualskill').invalid\r\n              \"\r\n              >Actual Skill is Required</span\r\n            >\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">&nbsp;</div>\r\n      <div class=\"form-group\">\r\n        <label for=\"IT work exp\"\r\n          >IT work experience<span class=\"asteriskField\" style=\"color:red\">\r\n            *\r\n          </span></label\r\n        >\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6\">\r\n            <label>Years</label>\r\n            <select\r\n              class=\"form-control\"\r\n              formControlName=\"year\"\r\n              required\r\n              style=\"height: 40px;\"\r\n              [(ngModel)]=\"userData.expYears\"\r\n            >\r\n              <option value=\"\" disabled=\"disabled\">Year</option>\r\n              <option [value]=\"year\" *ngFor=\"let year of years\">\r\n                {{ year }}\r\n              </option>\r\n            </select>\r\n            <span\r\n              class=\"help-block\"\r\n              *ngIf=\"myform.get('year').touched && myform.get('year').invalid\"\r\n              >Experience Year is Required</span\r\n            >\r\n          </div>\r\n          <div class=\"col-md-6\">\r\n            <label>Months</label>\r\n            <select\r\n              class=\"form-control\"\r\n              required\r\n              formControlName=\"month\"\r\n              style=\"height: 40px;\"\r\n              [(ngModel)]=\"userData.expMonths\"\r\n            >\r\n              <option value=\"\" disabled=\"disabled\">Months</option>\r\n              <option [value]=\"month\" *ngFor=\"let month of months\">\r\n                {{ month }}\r\n              </option>\r\n            </select>\r\n            <span\r\n              class=\"help-block\"\r\n              *ngIf=\"myform.get('month').touched && myform.get('month').invalid\"\r\n              >Experience Month is Required</span\r\n            >\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">&nbsp;</div>\r\n      <div class=\"row\">&nbsp;</div>\r\n      <div class=\"button\" style=\"text-align:center;\">\r\n        <button\r\n          class=\"btn btn-primary btn-sm\"\r\n          style=\"border-radius: 25px;background-color: #F24540;font-family: sans-serif;font-size:14px;\"\r\n          (click)=\"onUpdate()\"\r\n          [disabled]=\"!myform.valid\"\r\n        >\r\n          Update Profile\r\n        </button>\r\n      </div>\r\n    </fieldset>\r\n  </form>\r\n  <div class=\"row\">&nbsp;</div>\r\n  <div class=\"row\">&nbsp;</div>\r\n  <footer\r\n    class=\"app-footer\"\r\n    style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\"\r\n  >\r\n    &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019\r\n  </footer>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/skill-update/skill-update.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/Dashboard/skill-update/skill-update.component.ts ***!
  \******************************************************************/
/*! exports provided: SkillUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillUpdateComponent", function() { return SkillUpdateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_modal_StateCity__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/modal/StateCity */ "./src/app/modal/StateCity.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SkillUpdateComponent = /** @class */ (function () {
    function SkillUpdateComponent(fb, auth, router, activatedRoute) {
        this.fb = fb;
        this.auth = auth;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.params = URLSearchParams;
        this.userData = {
            candi_firstname: "",
            candi_skillset3: "",
            candi_skillset2: "",
            pushselectid: "",
            expYears: "",
            expMonths: "",
            cityvalue: "",
            lives_in: "",
            candi_fullname: "",
            checkarr: "",
            state: "",
            city: "",
            candi_lastname: "",
            candi_password: "",
            candi_phone: "",
            candi_actualskill: "",
            candi_gender: "",
            candi_dob: "",
            candi_skillset: "",
            candi_state: "",
            candi_city: "",
            prefercity: ""
        };
        this.candi_fullname = this.userData.candi_firstname + "" + this.userData.candi_lastname;
        this.lives_in = this.userData.city + "," + this.userData.state;
        this.isSelected = false;
        this.checked = true;
        this.fileToUpload = null;
        this.SelectfruitValue = [];
        this.prefercity = [];
        this.candi_actualskill = [];
        this.states = src_app_modal_StateCity__WEBPACK_IMPORTED_MODULE_4__["StateCity"];
        this.city = src_app_modal_StateCity__WEBPACK_IMPORTED_MODULE_4__["City"];
        this.years = [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25
        ];
        this.months = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        this.flag = false;
        this.cityvalue = [];
        this.selectskill = [];
        this.skillset = [];
        this.actualset = [];
        this.newarray = [];
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {};
        this.dropdownActualList = [];
        this.selectedActualItems = [];
        this.dropdownActualSettings = {};
        this.categoryselectskill = [];
        this.Responseselectedskill = [];
        this.completeSkill = [];
        this.statecity = [];
        this.cityselect = [];
        this.citymodel = [];
        this.model = {
            Any: {
                checked: false,
                disable: false
            },
            Mumbai: {
                checked: false,
                disable: false
            },
            Other: {
                checked: false,
                disable: false
            },
            Hyderabad: {
                checked: false,
                disable: false
            },
            Bangalore: {
                checked: false,
                disable: false
            },
            Kolkata: {
                checked: false,
                disable: false
            },
            Delhi: {
                checked: false,
                disable: false
            },
            Chennai: {
                checked: false,
                disable: false
            },
            Pune: {
                checked: false,
                disable: false
            }
        };
        this.submitted = false;
        this.itemArrayId = [];
        this.itemArraySkill = [];
        this.tempstate = [];
        this.tempskillarray = [];
        this.tempcatergory = [];
        this.tempItem = [];
        this.finalskill = [];
        this.pushselect = [];
        this.pushselectacutal = [];
        this.test = [];
        this.defaultItemCategories = {
            id: null,
            category: null
        };
        var today = new Date();
        var minAge = 18;
        this.newminAge = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());
    }
    SkillUpdateComponent.prototype.onsel = function () {
        this.slist = this.selectskill.filter(function (item) {
            var itemlist;
            itemlist = item.category;
            return itemlist;
        });
        return this.slist;
    };
    SkillUpdateComponent.prototype.anyClick = function (checkName, checked) {
        if (checkName === "Any") {
            if (checked) {
                this.model.Mumbai.checked = false;
                this.model.Other.checked = false;
                this.model.Chennai.checked = false;
                this.model.Hyderabad.checked = false;
                this.model.Bangalore.checked = false;
                this.model.Delhi.checked = false;
                this.model.Kolkata.checked = false;
                this.model.Pune.checked = false;
                this.citymodel = [];
                this.citymodel.push(checkName);
                this.model.Other.disable = true;
                this.model.Mumbai.disable = true;
                this.model.Chennai.disable = true;
                this.model.Hyderabad.disable = true;
                this.model.Bangalore.disable = true;
                this.model.Delhi.disable = true;
                this.model.Kolkata.disable = true;
                this.model.Pune.disable = true;
            }
            else {
                this.model.Other.disable = false;
                this.model.Mumbai.disable = false;
                this.model.Chennai.disable = false;
                this.model.Hyderabad.disable = false;
                this.model.Bangalore.disable = false;
                this.model.Delhi.disable = false;
                this.model.Kolkata.disable = false;
                this.model.Pune.disable = false;
                this.model.Any.checked = false;
                this.citymodel.splice(this.citymodel.indexOf(checkName), 1);
            }
        }
        else {
            if (checked) {
                this.model[checkName].checked = true;
                this.model[checkName].disable = false;
                this.citymodel.push(checkName);
            }
            else {
                this.model[checkName].checked = false;
                this.model[checkName].disable = false;
                this.citymodel.splice(this.citymodel.indexOf(checkName), 1);
            }
        }
    };
    SkillUpdateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.myCheck = this.fb.group({
            citycheck: this.fb.array([])
        });
        this.auth.get("/category_get", this.token).then(function (data) {
            _this.selectskill = data.json().result;
        });
        this.auth.get("/state", this.token).then(function (data) {
            _this.statecity = data.json().result;
        });
        this.myform = this.fb.group({
            firstname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[a-zA-Z]+")
            ]),
            lastname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[a-zA-Z]+")
            ]),
            dob: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            skillset: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            state: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            year: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            month: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            skill: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            radio: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            actualskill: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("")
        });
        this.dropdownSettings = {
            singleSelection: true,
            idField: "id",
            textField: "category",
            selectAllText: "Select All",
            unSelectAllText: "UnSelect All",
            itemsShowLimit: 3
        };
        this.dropdownActualSettings = {
            singleSelection: false,
            idField: "id",
            textField: "skill",
            // selectAllText: "Select All",
            unSelectAllText: "UnSelect All",
            itemsShowLimit: 3
        };
    };
    Object.defineProperty(SkillUpdateComponent.prototype, "firstname", {
        get: function () {
            return this.myform.get("firstname");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SkillUpdateComponent.prototype, "lastname", {
        get: function () {
            return this.myform.get("lastname");
        },
        enumerable: true,
        configurable: true
    });
    SkillUpdateComponent.prototype.onItemDeSelect = function (item) {
        var index = this.pushselect.indexOf(item.id);
        this.pushselect.splice(index, 1);
    };
    SkillUpdateComponent.prototype.onItemSelect = function (item) {
        var _this = this;
        this.selectedCategory = item;
        this.selectedProduct = undefined;
        this.selectedOrder = undefined;
        if (item.id == this.defaultItemCategories.id) {
        }
        else {
            this.pushselect = item.id;
            this.token = localStorage.getItem("token");
            this.auth
                .postDetail("/category_skill_get", this.token, { category_id: item.id })
                .then(function (data) {
                _this.categoryselectskill = data.json().result;
            });
        }
        this.userData.candi_skill_set = this.pushselect;
        this.tempcatergory = item.category;
    };
    SkillUpdateComponent.prototype.onSelectAll = function (items) {
        // console.log(items);
    };
    SkillUpdateComponent.prototype.onItemSelectActual = function (item) {
        this.tempItem.push(item);
        if (item.id) {
            this.itemArrayId.push(item.id);
            this.itemArraySkill.push(item.skill);
        }
    };
    SkillUpdateComponent.prototype.onItemDeSelectactual = function (item) {
        var val = item;
        var index = this.tempItem.findIndex(function (item, i) {
            return item.id === val.id;
        });
        this.tempItem.splice(index, 1);
        this.finalid = this.tempItem;
        var id = [];
        this.finalid.filter(function (item) {
            id.push(item.id);
        });
        this.itemArrayId = id;
        var skill = [];
        this.finalid.filter(function (item) {
            skill.push(item.skill);
        });
        this.itemArraySkill = skill;
    };
    SkillUpdateComponent.prototype.onSelectAllActual = function (items) {
        // console.log(items);
    };
    SkillUpdateComponent.prototype.onUpdate = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.token = localStorage.getItem("token");
            _this.auth
                .putResult('/candi_profile_update/' + option["id"], {
                candi_fullname: _this.userData.candi_fullname =
                    _this.userData.candi_firstname +
                        ' ' +
                        _this.userData.candi_lastname,
                candi_gender: _this.userData.candi_gender,
                candi_dob: _this.userData.candi_dob,
                prefer_location: _this.citymodel.toString(),
                category_name: _this.tempcatergory,
                skill_name: _this.itemArraySkill.toString(),
                candi_skillset: _this.itemArrayId.toString(),
                candi_exp_years: _this.userData.expYears,
                candi_exp_months: _this.userData.expMonths,
                lives_in: _this.userData.lives_in =
                    _this.userData.candi_city + ', ' + _this.tempstate,
                lives_in_id: _this.lives_in_id,
                category: _this.pushselect
            }, _this.token)
                .then(function (data) {
                _this.token = localStorage.getItem("token");
                _this.candidateList = data.json().result;
            });
            _this.router.navigateByUrl("/home");
        });
    };
    SkillUpdateComponent.prototype.countcheck = function (data) {
        throw new Error("Method not implemented.");
    };
    SkillUpdateComponent.prototype.e = function (e, event) {
        throw new Error("Method not implemented.");
    };
    SkillUpdateComponent.prototype.getValue = function (e, event) {
        if (e) {
        }
    };
    SkillUpdateComponent.prototype.selectUsers = function (event, user) {
        user.flag = !user.flag;
    };
    SkillUpdateComponent.prototype.sendInvitation = function () {
        // if (this.flag != false) {
        //   console.log("not send");
        // } else {
        //   console.log(" send!");
        // }
    };
    SkillUpdateComponent.prototype.changeCountry = function (event) {
        var _this = this;
        var temp = parseInt(event);
        this.lives_in_id = parseInt(event);
        console.log(parseInt(event));
        var temp1 = this.statecity[event].id;
        if (temp === temp1) {
            this.statecity[event].name;
        }
        this.tempstate = this.statecity[event].name;
        this.auth.postDetail("/city", this.token, { id: temp1 }).then(function (data) {
            _this.cityselect = data.json().result;
        });
    };
    SkillUpdateComponent.prototype.changeOtherCountry = function (event) {
        var _this = this;
        var temp = parseInt(event);
        var temp1 = this.statecity[event].id;
        if (temp === temp1) {
            this.statecity[event].name;
        }
        this.tempstate = this.statecity[event].name;
        this.auth.postDetail("/city", this.token, { id: temp1 }).then(function (data) {
            _this.cityselect = data.json().result;
        });
    };
    SkillUpdateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-skill-update",
            template: __webpack_require__(/*! ./skill-update.component.html */ "./src/app/Dashboard/skill-update/skill-update.component.html"),
            styles: [__webpack_require__(/*! ./skill-update.component.css */ "./src/app/Dashboard/skill-update/skill-update.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], SkillUpdateComponent);
    return SkillUpdateComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/update-profile/update-profile.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/Dashboard/update-profile/update-profile.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "card{\r\n    /* font-size: 14px; */\r\n    font-family: sans-serif;\r\n}\r\n.card_shadow{\r\n    box-shadow:        0 4px 6px -6px #000;\r\n}\r\n.card-title{\r\n    font-size: 18px;\r\n    font-family: sans-serif;\r\n}\r\n.card-text{\r\n    font-size: 14px;\r\n    font-family: sans-serif;\r\n}\r\nlabel{\r\n    font-family: sans-serif;\r\n    font-size: 14px;\r\n}\r\np{\r\n    font-family: sans-serif;\r\n    font-size: 16px;\r\n}\r\nh4{\r\n    font-family: sans-serif;\r\n}\r\na{\r\n    font-size: 18px;\r\n}\r\nimg {\r\n    border-radius: 50%;\r\n  }\r\n"

/***/ }),

/***/ "./src/app/Dashboard/update-profile/update-profile.component.html":
/*!************************************************************************!*\
  !*** ./src/app/Dashboard/update-profile/update-profile.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left:12%\">\r\n  <h3>Update Profile</h3>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-3\" style=\"text-align: center\">\r\n      <div class=\"card card_shadow\" style=\"width:265px;\">\r\n        <div class=\"card-body \">\r\n          <div>\r\n            <img src=\"{{ candidateData.candi_profile_photo }}\" height=\"150\" />\r\n            <br />\r\n          </div>\r\n          <div class=\"pointer-cursor\">\r\n            <img id=\"photo_upload\" src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/photo-upload.svg\"\r\n              data-toggle=\"modal\" data-target=\"#exampleModal1\"\r\n              style=\"width:60px;height:30px;float:right;font-family: sans-serif;font-size: 10px;\" />\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- Modal -->\r\n    <div class=\"modal fade\" id=\"exampleModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\r\n      aria-hidden=\"true\">\r\n      <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n          <div class=\"modal-header\">\r\n            <h5 class=\"modal-title\" id=\"exampleModalLabel\">\r\n              Profile Picture Upload\r\n            </h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n              <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n            <div class=\"\">\r\n              <img *ngIf=\"url\" [src]=\"url\" height=\"150\" /> <br />\r\n              <img *ngIf=\"!url\" src=\"{{ candidateData.candi_profile_photo }}\" height=\"150\" />\r\n              <br />\r\n            </div>\r\n            <input type=\"file\" name=\"photo\" ng2FileSelect [uploader]=\"uploader\" style=\"margin-left:40%\"\r\n              (change)=\"fileUpload($event)\" />\r\n            <button type=\"button\" style=\"margin-top:3%;margin-right:8%\" class=\"btn btn-success btn-s \"\r\n              (click)=\"uploader.uploadAll()\" [disabled]=\"!uploader.getNotUploadedItems().length\" data-dismiss=\"modal\">\r\n              Upload an Image\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-9\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\" id=\"user_details\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-11 text-center\">\r\n              <h5>Reg.ID : {{candidateData.registration_id}}</h5>\r\n              <h4>\r\n                <strong>{{ candidateData.candi_fullname | uppercase }}</strong>\r\n              </h4>\r\n              <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                  <h4 class=\"card-title\">\r\n                    <span style=\"font-weight: bold\">Technical Area: </span>\r\n                    <span>{{ candidateData.category_name }}</span>\r\n                  </h4>\r\n                </div>\r\n\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-1\">\r\n              <a class=\"pointer-cursor\"><img id=\"edit_user_data\"\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n                  data-toggle=\"modal\" data-target=\"#exampleModalLong\" (click)=\"openModal('exampleModalLong')\"\r\n                  style=\"float: right;width: 20px;height: 20px;\" /></a>\r\n            </div>\r\n          </div>\r\n          <!-- <hr> -->\r\n          <div>&nbsp;</div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <h4 class=\"card-title\">\r\n                <span style=\"font-weight: bold\">Gender :</span>\r\n                <span>{{ candidateData.candi_gender }}</span>\r\n              </h4>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <h4 class=\"card-title\">\r\n                <span style=\"font-weight: bold\">Age :</span>\r\n                <span> {{ totalage }} yrs</span>\r\n              </h4>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n\r\n              <h4 class=\"card-title\">\r\n                <span style=\"font-weight: bold\">Experience :</span>\r\n                <span>{{ candidateData.candi_exp_years }} Year(s) {{\r\n                      candidateData.candi_exp_months\r\n                    }}\r\n                  Month(s)</span>\r\n              </h4>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n\r\n\r\n              <h4 class=\"card-title\">\r\n                <span style=\"font-weight: bold\">Current Location :</span>\r\n                <span>{{ candidateData.lives_in\r\n                }}\r\n                </span>\r\n              </h4>\r\n            </div>\r\n\r\n          </div>\r\n          <!--rowclose-->\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\" id=\"profile_summary\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-10\">\r\n              <h4 class=\"card-title\">\r\n                <strong>Profile Summary</strong>\r\n              </h4>\r\n            </div>\r\n            <div class=\"col-md-2\">\r\n              <a class=\"pointer-cursor\"><img id=\"edit_profile_summary\"\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n                  style=\"float:right;width: 20px;height: 20px;\" data-toggle=\"modal\" data-target=\"#exampleModalLong6\"\r\n                  (click)=\"openModal('exampleModalLong6')\" /></a>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <br />\r\n              <span class=\"card-text\" style=\"font-size:16px;\" id=\"summary_detail\">\r\n                {{ candidateData.profile_summary }}\r\n              </span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\" id=\"assessed_skills\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-10\">\r\n              <h4 class=\"card-title\"><strong>Assessed skills</strong></h4>\r\n            </div>\r\n            <div class=\"col-md-2\">&nbsp;</div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <table class=\"table\">\r\n                <thead>\r\n                  <tr>\r\n                    <th>Skill</th>\r\n                    <th>Level</th>\r\n                    <th>Minemark score</th>\r\n                  </tr>\r\n                </thead>\r\n                <tbody>\r\n                  <tr *ngFor=\"let dataresult of sampledata\">\r\n                    <td style=\"font-size:16px;\">{{ dataresult.skill_name }}</td>\r\n                    <td style=\"font-size:16px;\">Foundation</td>\r\n                    <td style=\"font-size:16px;\">\r\n                      {{ dataresult.obtain_score }}/{{ dataresult.total_marks }}\r\n                    </td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\" id=\"prefer_location\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-10\">\r\n              <h4 class=\"card-title\">\r\n                <strong>Preferred Locations</strong>\r\n              </h4>\r\n            </div>\r\n\r\n            <div class=\"col-md-2 \">\r\n              <a class=\"pointer-cursor\"><img id=\"pre_location\"\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n                  style=\"float:right;width: 20px;height: 20px;\" data-toggle=\"modal\" data-target=\"#exampleModal9\" /></a>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <br />\r\n              <span class=\"card-text\" style=\"font-size:16px;\">\r\n                {{ candidateData.prefer_location }}\r\n              </span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\" id=\"experience\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-10\">\r\n              <h4 class=\"card-title\">\r\n                <strong> Experience</strong>\r\n              </h4>\r\n            </div>\r\n            <div class=\"col-md-2\">\r\n              <a class=\"pointer-cursor\"><img id=\"edit_exp\"\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-add-24px.svg\"\r\n                  style=\"float:right;width: 20px;height: 20px;\" data-toggle=\"modal\"\r\n                  data-target=\"#exampleModalLong4\" /></a>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\" *ngFor=\"let exp of candiemployeedeatils; let i = index\">\r\n            <div class=\"col-md-2\">\r\n              <br />\r\n              <div class=\"\">\r\n                <img id=\"edit_education\"\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/twotone-work-24px.svg\"\r\n                  style=\"width: 50px;height: 50px;\" />\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <br />\r\n              <p style=\"font-weight: bold\">{{ exp.designation | uppercase }}</p>\r\n              <p>{{ exp.company | titlecase }}</p>\r\n              <p>{{ exp.location | titlecase }}</p>\r\n              <p>\r\n                {{ exp.from | date: \"d MMM y\" }} to\r\n                {{ exp.to | date: \"d MMM y\" }}\r\n              </p>\r\n            </div>\r\n            <div class=\"col-md-2\" style=\"margin-top: 60px;\">\r\n              <a class=\"pointer-cursor\"><img\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n                  data-toggle=\"modal\" data-target=\"#exampleModalLong3\" (click)=\"openModalExperiences(i)\"\r\n                  style=\"width:20px;height: 20px;float: right;\" /></a>\r\n            </div>\r\n            <hr />\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\" id=\"education\">\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-md-10\">\r\n              <h4 class=\"card-title\">\r\n                <strong>Education</strong>\r\n              </h4>\r\n            </div>\r\n            <div class=\"col-md-2\">\r\n              <a class=\"pointer-cursor\"><img\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-add-24px.svg\"\r\n                  data-toggle=\"modal\" data-target=\"#exampleModalLong5\"\r\n                  style=\"float: right;width: 20px;height: 20px;\" /></a>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\" *ngFor=\"let edu of educationData; let i = index\">\r\n            <div class=\"col-md-2\">\r\n              <br />\r\n              <div class=\"\">\r\n                <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/twotone-school-24px.svg\"\r\n                  style=\"width: 50px;height: 50px;\" />\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <br />\r\n              <p>{{ edu.degree | uppercase }}</p>\r\n              <p>{{ edu.school | titlecase }}</p>\r\n              <p>\r\n                {{ edu.from | date: \"d MMM y\" }} to\r\n                {{ edu.to | date: \"d MMM y\" }}\r\n              </p>\r\n            </div>\r\n            <div class=\"col-md-2\" style=\"margin-top: 60px;\">\r\n              <a class=\"pointer-cursor\"><img\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n                  (click)=\"openModalEducation(i)\" data-toggle=\"modal\" data-target=\"#exampleModalLong2\"\r\n                  style=\"float:right;width: 20px;height: 20px;\" /></a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body card_shadow\" id=\"ctc\">\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-md-5\">\r\n              <h4 class=\"card-title\">\r\n                <strong>Current CTC (INR) :</strong>\r\n                <span>\r\n                  {{ candidateData.current_ctc }} LPA\r\n                </span>\r\n              </h4>\r\n            </div>\r\n            <div class=\"col-md-5\">\r\n              <h4 class=\"card-title\">\r\n                <strong>Expected CTC (INR) :</strong>\r\n                <span>\r\n                  {{ candidateData.expected_ctc }} LPA\r\n                </span></h4>\r\n            </div>\r\n            <div class=\"col-md-2\">\r\n              <a class=\"pointer-cursor\"><img id=\"edit_CTC\"\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n                  style=\"float:right;width: 20px;height: 20px;\" data-toggle=\"modal\" data-target=\"#exampleModal8\"\r\n                  (click)=\"openModal('exampleModal8')\" /></a>\r\n            </div>\r\n          </div>\r\n          <!--row close-->\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\" id=\"skill\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-10\">\r\n              <h4 class=\"card-title\" style=\"font-weight: bold\">Skill</h4>\r\n            </div>\r\n            <div class=\"col-md-2\">\r\n              <a class=\"pointer-cursor\"><img id=\"edit_Skill\"\r\n                  src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n                  style=\"float:right;width: 20px;height: 20px;\" data-toggle=\"modal\"\r\n                  data-target=\"#exampleModalLong7\" /></a>\r\n            </div>\r\n          </div>\r\n          <br />\r\n          <div class=\"row\">\r\n            <div class=\"col-md-8\">\r\n              <p id=\"candi_skill\">{{ candidateData.skill_name }}</p>\r\n            </div>\r\n            <div class=\"col-md-4\">&nbsp;</div>\r\n            <div class=\"col-md-12\">\r\n              <p id=\"can_join\">\r\n                <span style=\"font-weight: bold\">\r\n                  Can Join :\r\n                </span>\r\n                {{ candidateData.can_join }}\r\n              </p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br />\r\n\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-5\">\r\n              <div class=\"card-body\" id=\"actively_looking\">\r\n                <h4 class=\"card-title\"><strong>Actively Looking</strong></h4>\r\n                <!-- <div class=\"row\">\r\n                  <div class=\"col-md-10\"> -->\r\n                <input type=\"checkbox\" id=\"looking_job\" (change)=\"activelyLooking($event)\"\r\n                  [checked]=\"candidateData.looking_job == 'Yes' ? true :false\">\r\n                <label>Are you actively looking for the job :</label><span> {{ candidateData.looking_job }}</span>\r\n                <!-- </div> -->\r\n                <!-- </div> -->\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-5\">\r\n              <!-- <div class=\"card-body\" id=\"differently_able\"> -->\r\n              <h4 class=\"card-title\"><strong>Differently able</strong></h4>\r\n              <!-- <div class=\"row\"> -->\r\n              <!-- <div class=\"col-md-10\"> -->\r\n              <input type=\"checkbox\" id=\"differently_able\" (change)=\"disability($event)\"\r\n                [checked]=\"candidateData.disability == 'Yes' ? true :false\">\r\n              <label>Any kind of Differently able factor :</label><span> {{ candidateData.disability }}</span>\r\n              <!-- </div> -->\r\n              <!-- <div class=\"col-md-8\"> -->\r\n\r\n              <!-- </div> -->\r\n              <!-- </div> -->\r\n              <!-- </div>     -->\r\n            </div>\r\n            <!-- <div class=\"col-md-2\">\r\n            <a class=\"pointer-cursor\"\r\n            ><img id=\"edit_diffable\"\r\n            src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/outline-create-24px+(1).png\"\r\n              style=\"float:right;width: 20px;height: 20px;\"\r\n              data-toggle=\"modal\"\r\n              data-target=\"#exampleModalLong12\"\r\n              (click)=\"openModal('exampleModalLong12')\"\r\n          /></a>\r\n        </div> -->\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <!-- <div class=\"col-md-5\"> -->\r\n\r\n        <!-- </div> -->\r\n        <!-- </div> -->\r\n        <!-- <div class=\"col-md-5\"> -->\r\n        <!-- <div class=\"card card_shadow\"> -->\r\n\r\n        <!-- </div> -->\r\n        <!-- </div> -->\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card card_shadow\">\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <h4 class=\"card-title\">\r\n                <strong>Upload your resume</strong>\r\n              </h4>\r\n              <div *ngIf=\"candidateData.resume_path\">\r\n                <span class=\"card-title\">{{ lastPart }}</span>\r\n                <a href=\"{{ candidateData.resume_path }}\" style=\"margin-left:200px\"><i class=\"fa fa-download\"\r\n                    style=\"color:#F24540\" aria-hidden=\"true\"></i></a>\r\n              </div>\r\n              <button id=\"add_resume\" class=\"btn\"\r\n                style=\"float: right;margin-top:-23px;background-color:#F24540;color:#ffffff\" data-toggle=\"modal\"\r\n                data-target=\"#exampleModal10\">\r\n                Add Resume\r\n              </button>\r\n              <div style=\"margin-top: 20px;\">\r\n                <p>\r\n                  <a style=\"font-weight: bold\">Note</a> : Only .doc file allow<a style=\"color:red\">*</a>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- Modal -->\r\n  <div class=\"modal fade\" id=\"exampleModal10\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Resume Upload</h5>\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <div class=\"well\" style=\"float: right;\">\r\n            <label for=\"Upload Resume\">Upload Resume</label>\r\n            <!-- <input\r\n              type=\"file\"\r\n              name=\"resume\"\r\n              (change)=\"onFileSelected($event)\"\r\n              class=\"form-control\"\r\n            /><br />\r\n            <input\r\n              (click)=\"onUpload()\"\r\n              type=\"submit\"\r\n              value=\"Upload Resume\"\r\n              class=\"btn btn-success\"\r\n            /> -->\r\n            <input type=\"file\" name=\"resume\" ng2FileSelect [uploader]=\"uploaderresume\" style=\"margin-left:40%\"\r\n              (change)=\"fileUploadResume($event)\" />\r\n            <button type=\"button\" style=\"margin-top:3%;margin-right:8%\" class=\"btn btn-success btn-s\"\r\n              (click)=\"uploaderresume.uploadAll()\">\r\n              Upload Resume\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <app-footer></app-footer>\r\n</div>\r\n\r\n<!-- Modal -->\r\n<div class=\"modal fade\" id=\"exampleModalLong\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\" data-backdrop=\"static\" data-keyboard=\"false\">\r\n  <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-body\">\r\n        <button type=\"button\" style=\"font-size: 30px;\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\" style=\"width: 50px;height: 50px;\">&times;</span>\r\n        </button>\r\n        <form [formGroup]=\"myform\" novalidate>\r\n          <div class=\"row\" style=\"margin-top:30px;\">\r\n            <div class=\"col-md-12\">\r\n              <label>Full Name</label>\r\n              <input type=\"text\" name=\"fullname\" formControlName=\"fullname\" placeholder=\"\" class=\"form-control\"\r\n                [(ngModel)]=\"candidateData.candi_fullname\" />\r\n            </div>\r\n          </div>\r\n          <br />\r\n          <br>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"gender\">Gender</label>\r\n                <div>\r\n                  <input type=\"radio\" name=\"radio\" value=\"Male\" [ngModelOptions]=\"{ standalone: true }\"\r\n                    [(ngModel)]=\"candidateData.candi_gender\" />\r\n                  <span style=\"font-size:14px;margin-left: 10px;\">Male</span>\r\n                  <input type=\"radio\" name=\"radio\" value=\"Female\" [ngModelOptions]=\"{ standalone: true }\"\r\n                    style=\"margin-left:30px;\" [(ngModel)]=\"candidateData.candi_gender\" />\r\n                  <span style=\"font-size:14px;margin-left: 10px;\">Female</span>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"Dob\">Date of Birth\r\n                  <span class=\"asteriskField\" style=\"color:red\"> * </span>\r\n                </label>\r\n\r\n                <input type=\"date\" max=\"{{ newminAge | date: 'yyyy-MM-dd' }}\" class=\"form-control\" formControlName=\"dob\"\r\n                  [(ngModel)]=\"candidateData.candi_dob\" placeholder=\"DD-MM-YYYY\" />\r\n                <span class=\"help-block\" *ngIf=\"myform.get('dob').touched && myform.get('dob').invalid\">DOB is\r\n                  Required</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <br />\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <label>State </label>\r\n              <select class=\"form-control selectpicker\" formControlName=\"state\" style=\"height: 40px;\"\r\n                [(ngModel)]=\"stateindex\" (change)=\"changeCountry($event.target.value)\"\r\n                 name=\"state\">\r\n                 \r\n                <option *ngFor=\"let count of statecity;let i=index\"  value={{count.id}}\r\n                >{{\r\n                    count.name\r\n                  }}</option>\r\n\r\n              </select>\r\n              <span class=\"help-block\" *ngIf=\"\r\n                  myform.get('state').touched && myform.get('state').invalid\r\n                \">State is Required</span>\r\n            </div>\r\n\r\n            <div class=\"col-md-6\">\r\n              <label>City</label>\r\n              <select class=\"form-control selectpicker \" style=\"height: 40px;\" formControlName=\"city\"\r\n                [(ngModel)]=\"cityindex\">\r\n                <!-- <option value=\"0\" disabled [ngValue]=\"cityindex\" selected>{{\r\n                  cityindex\r\n                }}</option> -->\r\n                <option *ngFor=\"let city of cityselect\">{{ city.name }}</option>\r\n              </select>\r\n              <span class=\"help-block\" *ngIf=\"myform.get('city').touched && myform.get('city').invalid\">City is\r\n                Required</span>\r\n            </div>\r\n          </div>\r\n          <br />\r\n\r\n          <div class=\"form-group\">\r\n            <label for=\"IT work exp\">IT work experience<span class=\"asteriskField\" style=\"color:red\">*</span></label>\r\n            <div class=\"row\">\r\n              <div class=\"col-md-6\">\r\n                <label>Years</label>\r\n                <select class=\"form-control\" formControlName=\"year\" style=\"height: 40px;\"\r\n                  [(ngModel)]=\"candidateData.candi_exp_years\">\r\n                  <option [value]=\"year\" *ngFor=\"let year of years\">\r\n                    {{ year }}\r\n                  </option>\r\n                </select>\r\n                <span class=\"help-block\" *ngIf=\"\r\n                    myform.get('year').touched && myform.get('year').invalid\r\n                  \">Experience is Required</span>\r\n              </div>\r\n              <div class=\"col-md-6\">\r\n                <label>Months</label>\r\n                <select class=\"form-control\" formControlName=\"month\" style=\"height: 40px;\"\r\n                  [(ngModel)]=\"candidateData.candi_exp_months\">\r\n                  <option [value]=\"month\" *ngFor=\"let month of months\">\r\n                    {{ month }}\r\n                  </option>\r\n                </select>\r\n                <span class=\"help-block\" *ngIf=\"\r\n                    myform.get('month').touched && myform.get('month').invalid\r\n                  \">Experience is Required</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\" style=\"text-align: center;\">\r\n              <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-primary\" [disabled]=\"myform.valid\"\r\n                (click)=\"onUpdate()\" data-dismiss=\"modal\"\r\n                style=\"background-color: #F24540;border-radius: 15px;width: 179px;\tbox-shadow: 0 1px 5px 3px #CCCCCC;\">\r\n                Update\r\n              </button>\r\n            </div>\r\n            <div class=\"col-md-6\" style=\"text-align: center;\">\r\n              <button type=\"button\" class=\"btn\" data-dismiss=\"modal\"\r\n                style=\"background-color:#ffffff;border-radius: 15px;width: 179px;color: black;\tbox-shadow: 0 1px 5px 3px #CCCCCC;\">\r\n                Close\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- Education Modal -->\r\n<div class=\"modal fade\" id=\"exampleModalLong2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">\r\n          Edit Education History\r\n        </h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form [formGroup]=\"form\" novalidate>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <label>School</label>\r\n              <input type=\"text\" class=\"form-control\" formControlName=\"school\" pattern=\"[a-zA-Z]+\"\r\n                [(ngModel)]=\"edu_details.school\" name=\"school\" />\r\n              <div class=\"help-block\" *ngIf=\"form.get('school').touched && form.get('school').invalid\">\r\n                <p *ngIf=\"school.errors.required\">School is required</p>\r\n                <p *ngIf=\"school.errors.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Degree</label>\r\n              <input type=\"text\" class=\"form-control\" formControlName=\"degree\" pattern=\"[a-zA-Z]+\"\r\n                [(ngModel)]=\"edu_details.degree\" name=\"degree\" />\r\n              <div class=\"help-block\" *ngIf=\"form.get('degree').touched && form.get('degree').invalid\">\r\n                <p *ngIf=\"degree.errors.required\">Degree is required</p>\r\n                <p *ngIf=\"degree.errors.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Field of Study</label>\r\n              <input type=\"text\" class=\"form-control\" formControlName=\"field_of_study\" pattern=\"[a-zA-Z]+\"\r\n                [(ngModel)]=\"edu_details.field_of_study\" name=\"field_of_study\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  form.get('field_of_study').touched &&\r\n                  form.get('field_of_study').invalid\r\n                \">\r\n                <p *ngIf=\"field_of_study.errors.required\">\r\n                  >Field of Study is required\r\n                </p>\r\n                <p *ngIf=\"field_of_study.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Grade</label>\r\n              <input type=\"text\" class=\"form-control\" formControlName=\"grade\" [(ngModel)]=\"edu_details.grade\"\r\n                name=\"grade\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  form.get('field_of_study').touched &&\r\n                  form.get('field_of_study').invalid\r\n                \">\r\n                <p *ngIf=\"field_of_study.errors.required\">Grade is required</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <label>From</label>\r\n              <input type=\"date\" class=\"form-control\" formControlName=\"dateFrom\" useValueAsDate\r\n                [(ngModel)]=\"edu_details.from\" name=\"from\" />\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <label>To</label>\r\n              <input type=\"date\" class=\"form-control\" formControlName=\"dateTo\" useValueAsDate\r\n                [(ngModel)]=\"edu_details.to\" name=\"to\" />\r\n              <label *ngIf=\"form.errors\" style=\"color:red\">{{\r\n                form.errors?.dates | json\r\n              }}</label>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Description</label>\r\n              <input type=\"text\" class=\"form-control\" formControlName=\"description\"\r\n                [(ngModel)]=\"edu_details.description\" name=\"description\" />\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!form.valid\" (click)=\"UpdateEducation()\"\r\n          data-dismiss=\"modal\">\r\n          Update changes\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" id=\"exampleModalLong5\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">\r\n          Education History\r\n        </h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form [formGroup]=\"form\" novalidate>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <label>School/College</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"school\" formControlName=\"school\" pattern=\"[a-zA-Z]+\"\r\n                [(ngModel)]=\"educationData.school\" />\r\n              <div class=\"help-block\" *ngIf=\"form.get('school').touched && form.get('school').invalid\">\r\n                <p *ngIf=\"school.errors.required\">School is required</p>\r\n                <p *ngIf=\"school.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Degree</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"degree\" formControlName=\"degree\" pattern=\"[a-zA-Z]+\"\r\n                [(ngModel)]=\"educationData.degree\" />\r\n              <div class=\"help-block\" *ngIf=\"form.get('degree').touched && form.get('degree').invalid\">\r\n                <p *ngIf=\"degree.errors.required\">Degree is required</p>\r\n                <p *ngIf=\"degree.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Field of Study</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"field_of_study\" pattern=\"[a-zA-Z]+\"\r\n                formControlName=\"field_of_study\" [(ngModel)]=\"educationData.field_of_study\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  form.get('field_of_study').touched &&\r\n                  form.get('field_of_study').invalid\r\n                \">\r\n                <p *ngIf=\"field_of_study.errors.required\">\r\n                  Field of study is required\r\n                </p>\r\n                <p *ngIf=\"field_of_study.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Grade</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"grade\" formControlName=\"grade\"\r\n                [(ngModel)]=\"educationData.grade\" />\r\n              <div class=\"help-block\" *ngIf=\"form.get('grade').touched && form.get('grade').invalid\">\r\n                <p *ngIf=\"grade.errors.required\">Grade is required</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <label>From</label>\r\n              <input type=\"date\" class=\"form-control\" name=\"from\" formControlName=\"dateFrom\" useValueAsDate\r\n                [(ngModel)]=\"educationData.from\" />\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <label>To</label>\r\n              <input type=\"date\" class=\"form-control\" name=\"to\" formControlName=\"dateTo\" useValueAsDate\r\n                [(ngModel)]=\"educationData.to\" />\r\n              <label *ngIf=\"form.errors\" style=\"color:red\">{{\r\n                form.errors?.dates | json\r\n              }}</label>\r\n            </div>\r\n            <div class=\"col-md-12\">\r\n              <label>Description</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"description\" formControlName=\"description\"\r\n                [(ngModel)]=\"educationData.description\" />\r\n            </div>\r\n          </div>\r\n          <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">\r\n              Close\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#F24540;color:#ffffff\"\r\n              [disabled]=\"!form.valid\" (click)=\"SaveEducation()\">\r\n              Save changes\r\n            </button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- Experiences Modal -->\r\n<div class=\"modal fade\" id=\"exampleModalLong3\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Edit Experience</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form [formGroup]=\"formexp\" novalidate>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Company</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"company\" pattern=\"[a-zA-Z]+\" formControlName=\"company\"\r\n                [(ngModel)]=\"emp_details.company\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  formexp.get('company').touched &&\r\n                  formexp.get('company').invalid\r\n                \">\r\n                <p *ngIf=\"company.errors.required\">Company is required</p>\r\n                <p *ngIf=\"company.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Designation</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"designation\" pattern=\"[a-zA-Z]+\"\r\n                formControlName=\"designation\" [(ngModel)]=\"emp_details.designation\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  formexp.get('designation').touched &&\r\n                  formexp.get('designation').invalid\r\n                \">\r\n                <p *ngIf=\"designation.errors.required\">\r\n                  Designation is required\r\n                </p>\r\n                <p *ngIf=\"designation.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Location</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"location\" formControlName=\"location\" pattern=\"[a-zA-Z]+\"\r\n                [(ngModel)]=\"emp_details.location\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  formexp.get('location').touched &&\r\n                  formexp.get('location').invalid\r\n                \">\r\n                <p *ngIf=\"location.errors.required\">Location is required</p>\r\n                <p *ngIf=\"location.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6 form-group\">\r\n              <label>From</label>\r\n              <input type=\"date\" placeholder=\"Datepicker\" class=\"form-control\" name=\"from\" useValueAsDate\r\n                formControlName=\"dateFrom\" [(ngModel)]=\"emp_details.from\" />\r\n            </div>\r\n            <div class=\"col-md-6 form-group\">\r\n              <label>To</label>\r\n              <input type=\"date\" placeholder=\"Datepicker\" class=\"form-control\" name=\"to\" useValueAsDate\r\n                formControlName=\"dateTo\" [(ngModel)]=\"emp_details.to\" />\r\n              <label *ngIf=\"form.errors\" style=\"color:red\">{{\r\n                formexp.errors?.dates | json\r\n              }}</label>\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <input type=\"checkbox\" class=\"form-check-input\" formControlName=\"currently_working\"\r\n                name=\"currently_working\" id=\"exampleCheck1\" [(ngModel)]=\"emp_details.currently_working\" />\r\n              <label class=\"form-check-label\" for=\"exampleCheck1\">&nbsp;&nbsp;&nbsp;&nbsp; I currently work here\r\n                description</label>\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Description</label>\r\n              <input type=\"text\" class=\"form-control\" formControlName=\"description\"\r\n                [(ngModel)]=\"emp_details.description\" name=\"description\" />\r\n            </div>\r\n          </div>\r\n          <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">\r\n              Close\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#F24540;color:#ffffff\"\r\n              (click)=\"UpdateExperience()\" data-dismiss=\"modal\">\r\n              Update\r\n            </button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" id=\"exampleModalLong4\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Add Experience</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form [formGroup]=\"formexp\" novalidate>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Company</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"company\" formControlName=\"company\" pattern=\"[a-zA-Z]+\"\r\n                [(ngModel)]=\"candiemployeedeatils.company\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  formexp.get('company').touched &&\r\n                  formexp.get('company').invalid\r\n                \">\r\n                <p *ngIf=\"company.errors.required\">Company is required</p>\r\n                <p *ngIf=\"company.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Designation</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"designation\" formControlName=\"designation\"\r\n                [(ngModel)]=\"candiemployeedeatils.designation\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  formexp.get('designation').touched &&\r\n                  formexp.get('designation').invalid\r\n                \">\r\n                <p *ngIf=\"designation.errors.required\">\r\n                  Designation is required\r\n                </p>\r\n                <p *ngIf=\"designation.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Location</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"location\" formControlName=\"location\"\r\n                [(ngModel)]=\"candiemployeedeatils.location\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  formexp.get('location').touched &&\r\n                  formexp.get('location').invalid\r\n                \">\r\n                <p *ngIf=\"location.errors.required\">Location is required</p>\r\n                <p *ngIf=\"location.errors?.pattern\">\r\n                  only alphabet charater allow\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6 form-group\">\r\n              <label>From</label>\r\n              <input type=\"date\" class=\"form-control\" name=\"from\" formControlName=\"dateFrom\" useValueAsDate\r\n                [(ngModel)]=\"candiemployeedeatils.from\" />\r\n            </div>\r\n            <div class=\"col-md-6 form-group\">\r\n              <label>To</label>\r\n              <input type=\"date\" class=\"form-control\" *ngIf=\"!candiemployeedeatils.currently_working\" name=\"to\"\r\n                formControlName=\"dateTo\" useValueAsDate [(ngModel)]=\"candiemployeedeatils.to\" />\r\n              <!-- <label *ngIf=\"formexp.errors\" style=\"color:red\">{{ formexp.errors?.dates | json }}</label> -->\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <input type=\"checkbox\" class=\"form-check-input\" name=\"currently_working\"\r\n                formControlName=\"currently_working\" id=\"exampleCheck1\"\r\n                [(ngModel)]=\"candiemployeedeatils.currently_working\" />\r\n              <label class=\"form-check-label\" for=\"exampleCheck1\">&nbsp;&nbsp;&nbsp;&nbsp; I currently work here\r\n                description</label>\r\n            </div>\r\n            <div class=\"col-md-12 form-group\">\r\n              <label>Description</label>\r\n              <input type=\"text\" class=\"form-control\" name=\"description\" formControlName=\"description\"\r\n                [(ngModel)]=\"candiemployeedeatils.description\" />\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"myModal\"\r\n          style=\"background-color:#F24540;color:#ffffff\" [disabled]=\"!formexp.valid\" (click)=\"SaveExperience()\">\r\n          Save\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--Profile Summary Modal -->\r\n<div class=\"modal fade\" id=\"exampleModalLong6\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">\r\n          Add Profile Summary\r\n        </h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form [formGroup]=\"summaryForm\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <label>Add Profile Summary</label>\r\n              <textarea type=\"text\" placeholder=\"\" name=\"profile_summary\" formControlName=\"profile_summary\"\r\n                class=\"form-control\" [(ngModel)]=\"candidateData.profile_summary\"></textarea>\r\n              <span class=\"help-block\" *ngIf=\"\r\n                  summaryForm.get('profile_summary').touched &&\r\n                  summaryForm.get('profile_summary').invalid\r\n                \">Profile Summary is Required</span>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#F24540;color:#ffffff\"\r\n          (click)=\"profilesummary()\">\r\n          Save\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--Skill And Canjoin Modal -->\r\n<div class=\"modal fade\" id=\"exampleModalLong7\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Edit Skills</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form [formGroup]=\"skillForm\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"skillset\">Select Skill Area</label>\r\n                <ng-multiselect-dropdown [placeholder]=\"'choose skillset'\" (onDeSelect)=\"onItemDeSelect($event)\"\r\n                  [data]=\"ratedskill\" [(ngModel)]=\"skillupdate\" [settings]=\"dropdownSettings\"\r\n                  (onSelect)=\"onItemSelect($event)\" (onSelectAll)=\"onSelectAll($event)\" formControlName=\"skill_name\">\r\n                </ng-multiselect-dropdown>\r\n\r\n                <span class=\"help-block\" *ngIf=\"\r\n                    skillForm.get('skill_name').touched &&\r\n                    skillForm.get('skill_name').invalid\r\n                  \">Skill is Required</span>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <label>Can Join</label>\r\n              <select class=\"form-control\" formControlName=\"day\" placeholder=\"Immediately\" style=\"height: 40px;\"\r\n                [(ngModel)]=\"candidateData.can_join\">\r\n                <option [value]=\"day\" *ngFor=\"let day of days\">\r\n                  {{ day }}\r\n                </option>\r\n              </select>\r\n              <span class=\"help-block\" *ngIf=\"\r\n                  skillForm.get('day').touched && skillForm.get('day').invalid\r\n                \">Can join is Required</span>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn \" data-dismiss=\"modal\">Close</button>\r\n        <button type=\"button\" style=\"background-color:#F24540;color:#ffffff\" class=\"btn btn-primary\"\r\n          (click)=\"skilljoinadd()\" data-dismiss=\"modal\">\r\n          Update\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--cctc and ectc Modal -->\r\n<div class=\"modal fade\" id=\"exampleModal8\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">CTC ADD</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form [formGroup]=\"ctcForm\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <label>Current CTC (LPA) </label>\r\n              <input type=\"text\" class=\"form-control\" required formControlName=\"ctc\" pattern=\"[0-9]+\"\r\n                [(ngModel)]=\"candidateData.current_ctc\" />\r\n              <div class=\"help-block\" *ngIf=\"ctcForm.get('ctc').touched && ctcForm.get('ctc').invalid\">\r\n                <div *ngIf=\"ectc.errors?.pattern\">Ctc only number enter</div>\r\n                <div *ngIf=\"ectc.errors?.required\">Ctc required</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"col-md-6\">\r\n              <label>Expected CTC (LPA) </label>\r\n              <input type=\"text\" class=\"form-control\" required formControlName=\"ectc\" pattern=\"[0-9]+\"\r\n                [(ngModel)]=\"candidateData.expected_ctc\" />\r\n              <div class=\"help-block\" *ngIf=\"\r\n                  ctcForm.get('ectc').touched && ctcForm.get('ectc').invalid\r\n                \">\r\n                <div *ngIf=\"ectc.errors?.pattern\">Ctc only number enter</div>\r\n                <div *ngIf=\"ectc.errors?.required\">ECTC required</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">\r\n              Close\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#F24540;color:#ffffff\"\r\n              [disabled]=\"!ctcForm.valid\" (click)=\"ctcadd()\" data-dismiss=\"modal\">\r\n              Update\r\n            </button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- Modal -->\r\n<div class=\"modal fade\" id=\"exampleModal9\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Preferred Location</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n\r\n        <div class=\"row\">\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Any\" *ngIf=\"\r\n                finalcity[0] == 'Any' ||\r\n                  finalcity[1] == 'Any' ||\r\n                  finalcity[2] == 'Any' ||\r\n                  finalcity[3] == 'Any' ||\r\n                  finalcity[4] == 'Any' ||\r\n                  finalcity[5] == 'Any' ||\r\n                  finalcity[6] == 'Any' ||\r\n                  finalcity[7] == 'Any' ||\r\n                  finalcity[8] == 'Any';\r\n                else anyBlock\r\n              \" [checked]=\"true\" name=\"any\" />\r\n\r\n            <ng-template #anyBlock>\r\n              <input type=\"checkbox\" value=\"Any\" [checked]=\"false\" name=\"any\" /><label>Any</label>\r\n            </ng-template>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Hyderabad\" *ngIf=\"\r\n                finalcity[0] == 'Hyderabad' ||\r\n                  finalcity[1] == 'Hyderabad' ||\r\n                  finalcity[2] == 'Hyderabad' ||\r\n                  finalcity[3] == 'Hyderabad' ||\r\n                  finalcity[4] == 'Hyderabad' ||\r\n                  finalcity[5] == 'Hyderabad' ||\r\n                  finalcity[6] == 'Hyderabad' ||\r\n                  finalcity[7] == 'Hyderabad' ||\r\n                  finalcity[8] == 'Hyderabad';\r\n                else hydBlock\r\n              \" [checked]=\"true\" [disabled]=\"model.Hyderabad.disable\" name=\"hyderabad\" /><label>Hyderabad</label>\r\n\r\n            <ng-template #hydBlock>\r\n              <input type=\"checkbox\" value=\"Hyderabad\" [checked]=\"false\" [disabled]=\"model.Hyderabad.disable\"\r\n                name=\"hyderabad\" />\r\n            </ng-template>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Chennai\" *ngIf=\"\r\n                finalcity[0] == 'Chennai' ||\r\n                  finalcity[1] == 'Chennai' ||\r\n                  finalcity[2] == 'Chennai' ||\r\n                  finalcity[3] == 'Chennai' ||\r\n                  finalcity[4] == 'Chennai' ||\r\n                  finalcity[5] == 'Chennai' ||\r\n                  finalcity[6] == 'Chennai' ||\r\n                  finalcity[7] == 'Chennai' ||\r\n                  finalcity[8] == 'Chennai';\r\n                else cheBlock\r\n              \" [checked]=\"model.Chennai.checked\" [disabled]=\"model.Chennai.disable\"\r\n              name=\"chennai\" /><label>Chennai</label>\r\n\r\n            <ng-template #cheBlock>\r\n              <input type=\"checkbox\" value=\"Chennai\" [checked]=\"false\" [disabled]=\"model.Chennai.disable\"\r\n                name=\"chennai\" />\r\n            </ng-template>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Pune\" *ngIf=\"\r\n                finalcity[0] == 'Pune' ||\r\n                  finalcity[1] == 'Pune' ||\r\n                  finalcity[2] == 'Pune' ||\r\n                  finalcity[3] == 'Pune' ||\r\n                  finalcity[4] == 'Pune' ||\r\n                  finalcity[5] == 'Pune' ||\r\n                  finalcity[6] == 'Pune' ||\r\n                  finalcity[7] == 'Pune' ||\r\n                  finalcity[8] == 'Pune';\r\n                else punBlock\r\n              \" (change)=\"anyClick('Pune', $event.target.checked)\" [checked]=\"model.Pune.checked\"\r\n              [disabled]=\"model.Pune.disable\" name=\"pune\" /><label>Pune</label>\r\n\r\n            <ng-template #punBlock>\r\n              <input type=\"checkbox\" value=\"Pune\" [checked]=\"false\" [disabled]=\"model.Pune.disable\" name=\"pune\" />\r\n            </ng-template>\r\n          </div>\r\n\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Mumbai\" *ngIf=\"\r\n                finalcity[0] == 'Mumbai' ||\r\n                  finalcity[1] == 'Mumbai' ||\r\n                  finalcity[2] == 'Mumbai' ||\r\n                  finalcity[3] == 'Mumbai' ||\r\n                  finalcity[4] == 'Mumbai' ||\r\n                  finalcity[5] == 'Mumbai' ||\r\n                  finalcity[6] == 'Mumbai' ||\r\n                  finalcity[7] == 'Mumbai' ||\r\n                  finalcity[8] == 'Mumbai';\r\n                else mumbaiBlock\r\n              \" [checked]=\"true\" [disabled]=\"model.Mumbai.disable\" name=\"mumbai\" /><label>Mumbai</label>\r\n\r\n            <ng-template #mumbaiBlock>\r\n              <input type=\"checkbox\" value=\"Mumbai\" [checked]=\"false\" [disabled]=\"model.Mumbai.disable\" name=\"mumbai\" />\r\n            </ng-template>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Bangalore\" *ngIf=\"\r\n                finalcity[0] == 'Bangalore' ||\r\n                  finalcity[1] == 'Bangalore' ||\r\n                  finalcity[2] == 'Bangalore' ||\r\n                  finalcity[3] == 'Bangalore' ||\r\n                  finalcity[4] == 'Bangalore' ||\r\n                  finalcity[5] == 'Bangalore' ||\r\n                  finalcity[6] == 'Bangalore' ||\r\n                  finalcity[7] == 'Bangalore' ||\r\n                  finalcity[8] == 'Bangalore';\r\n                else bangBlock\r\n              \" [checked]=\"model.Bangalore.checked\" [disabled]=\"model.Bangalore.disable\"\r\n              name=\"bangalore\" /><label>Bangalore</label>\r\n\r\n            <ng-template #bangBlock>\r\n              <input type=\"checkbox\" value=\"Bangalore\" [checked]=\"false\" [disabled]=\"model.Bangalore.disable\"\r\n                name=\"bangalore\" />\r\n            </ng-template>\r\n          </div>\r\n\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Kolkata\" *ngIf=\"\r\n                finalcity[0] == 'Kolkata' ||\r\n                  finalcity[1] == 'Kolkata' ||\r\n                  finalcity[2] == 'Kolkata' ||\r\n                  finalcity[3] == 'Kolkata' ||\r\n                  finalcity[4] == 'Kolkata' ||\r\n                  finalcity[5] == 'Kolkata' ||\r\n                  finalcity[6] == 'Kolkata' ||\r\n                  finalcity[7] == 'Kolkata' ||\r\n                  finalcity[8] == 'Kolkata';\r\n                else kolBlock\r\n              \" [checked]=\"true\" [disabled]=\"model.Kolkata.disable\" name=\"kolkata\" /><label>Kolkata</label>\r\n\r\n            <ng-template #kolBlock>\r\n              <input type=\"checkbox\" value=\"Kolkata\" [checked]=\"false\" [disabled]=\"model.Delhi.disable\"\r\n                name=\"kolkata\" />\r\n            </ng-template>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Delhi\" *ngIf=\"\r\n                finalcity[0] == 'Delhi' ||\r\n                  finalcity[1] == 'Delhi' ||\r\n                  finalcity[2] == 'Delhi' ||\r\n                  finalcity[3] == 'Delhi' ||\r\n                  finalcity[4] == 'Delhi' ||\r\n                  finalcity[5] == 'Delhi' ||\r\n                  finalcity[6] == 'Delhi' ||\r\n                  finalcity[7] == 'Delhi' ||\r\n                  finalcity[8] == 'Delhi';\r\n                else delBlock\r\n              \" (change)=\"anyClick('Delhi', $event.target.checked)\" [checked]=\"true\" [disabled]=\"model.Delhi.disable\"\r\n              name=\"delhi\" /><label>Delhi</label>\r\n\r\n            <ng-template #delBlock>\r\n              <input type=\"checkbox\" value=\"Delhi\" (change)=\"anyClick('Delhi', $event.target.checked)\" [checked]=\"false\"\r\n                name=\"delhi\" />\r\n            </ng-template>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <input type=\"checkbox\" value=\"Other\" (change)=\"anyClick('Other', $event.target.checked)\"\r\n              [checked]=\"model.Other.checked\" [disabled]=\"model.Other.disable\" name=\"other\" /><label>Other</label>\r\n            <span *ngIf=\"model.Other.checked\">\r\n              <div class=\"form-group\">\r\n                <label for=\"Preferred location\">State<span class=\"asteriskField\" style=\"color:red\">\r\n                    *\r\n                  </span></label>\r\n                <select class=\"form-control selectpicker\" formControlName=\"state\" style=\"height: 40px;\" name=\"state\"\r\n                  [(ngModel)]=\"userData.candi_state\" (change)=\"changeCountry($event.target.value)\">\r\n                  <option *ngFor=\"let count of statecity\" value=\"{{ count.id }}\">{{ count.name }}</option>\r\n                </select>\r\n\r\n                <span class=\"help-block\" *ngIf=\"\r\n                    myform.get('state').touched && myform.get('state').invalid\r\n                  \">State is Required</span>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"Preferred location\">City<span class=\"asteriskField\" style=\"color:red\">\r\n                    *\r\n                  </span></label>\r\n                <select class=\"form-control selectpicker \" style=\"height: 40px;\" formControlName=\"city\"\r\n                  [(ngModel)]=\"userData.candi_city\">\r\n                  <option *ngFor=\"let city of cityselect\">{{\r\n                    city.name\r\n                  }}</option>\r\n                </select>\r\n                <span class=\"help-block\" *ngIf=\"\r\n                    myform.get('city').touched && myform.get('city').invalid\r\n                  \">City is Required</span>\r\n              </div>\r\n            </span>\r\n            <span *ngIf=\"!model.Other.checked\"></span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\"\r\n          style=\"background-color:#F24540;color:#ffffff\" (click)=\"preferlocation()\" data-dismiss=\"modal\">\r\n          Update\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--Actively looking Modal -->\r\n<div class=\"modal fade\" id=\"exampleModalLong12\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitle\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Actively looking</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n\r\n              <label name=\"actively_looking\">Actively Looking</label>\r\n              <input type=\"checkbox\" id=\"looking_job\" [(ngModel)]=\"candidateData.looking_job\" #ref\r\n                [checked]=\"candidateData.looking_job == 'Yes' ? true :false\">\r\n              <!-- <ui-switch\r\n                [checked]=\"candidateData.looking_job == 'Yes' ? true :false\"\r\n                [ngModelOptions]=\"{standalone: true}\"\r\n                name=\"looking_job\"\r\n                (change)=\"onSubscriptionChange($event)\"\r\n                [(ngModel)]=\"candidateData.looking_job\"\r\n                >{{ candidateData.looking_job }}</ui-switch\r\n              > -->\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n\r\n              <label name=\"differently_able\">Differently able?</label>\r\n              <input type=\"checkbox\" id=\"differently_able\" [(ngModel)]=\"candidateData.disability\"\r\n                [checked]=\"candidateData.disability == 'Yes' ? true :false\">\r\n              <!-- <ui-switch\r\n                *ngIf=\"candidateData.disability == 'No'; else uiSwitchBlock\"\r\n                [checked]=\"false\"\r\n                [ngModelOptions]=\"{standalone: true}\"\r\n                (change)=\"toggleaction($event)\"\r\n                [(ngModel)]=\"candidateData.disability\"\r\n                >{{ candidateData.disability }}</ui-switch\r\n              >\r\n              <ng-template #uiSwitchBlock>\r\n                <ui-switch\r\n                  [checked]=\"true\"\r\n                  defaultBoColor=\"black\"\r\n                  (change)=\"toggleaction($event)\"\r\n                  [(ngModel)]=\"candidateData.disability\"\r\n                  [ngModelOptions]=\"{standalone: true}\"\r\n                  >{{ candidateData.disability }}</ui-switch\r\n                > </ng-template\r\n              > -->\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn\" data-dismiss=\"modal\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#F24540;color:#ffffff\"\r\n          (click)=\"Activelylook(ref)\" data-dismiss=\"modal\">\r\n          Update\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Dashboard/update-profile/update-profile.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/Dashboard/update-profile/update-profile.component.ts ***!
  \**********************************************************************/
/*! exports provided: UpdateProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfileComponent", function() { return UpdateProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { parse } from 'url';





// const URL = "http://18.223.249.160:3002/api/candi_profile_image_update";
var URL = "http://localhost:3002/api/candi_profile_image_update";
var UrlResume = "http://18.223.249.160:3002/api/candidate_resume_upload";
var authHeader = [];
var UpdateProfileComponent = /** @class */ (function () {
    function UpdateProfileComponent(toast, httpc, http, el, auth, fb, activatedRoute, share, router) {
        this.toast = toast;
        this.httpc = httpc;
        this.http = http;
        this.el = el;
        this.auth = auth;
        this.fb = fb;
        this.activatedRoute = activatedRoute;
        this.share = share;
        this.router = router;
        this.candidateData = {
            disability: "",
            candi_profile_photo: "",
            candi_fullname: "",
            cityvalue: "",
            candi_exp_years: "",
            candi_exp_months: "",
            profile_summary: "",
            drpstateindex: "",
            cityindex: "",
            lives_in: "",
            can_join: ""
        };
        this.cityvalue = [];
        this.prefercity = [];
        this.userData = {
            candi_firstname: "",
            expYears: "",
            expMonths: "",
            cityvalue: "",
            lives_in: "",
            candi_fullname: "",
            checkarr: "",
            state: "",
            city: "",
            candi_lastname: "",
            candi_password: "",
            candi_phone: "",
            candi_actualskill: "",
            candi_gender: "",
            candi_dob: "",
            candi_skillset: "",
            candi_state: "",
            candi_city: "",
            prefercity: ""
        };
        this.years = [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25
        ];
        this.months = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        this.mySplits = [];
        this.candi_skillset = [];
        this.days = ["Immediately", 15, 30, 45, 90];
        this.params = URLSearchParams;
        this.category = localStorage.getItem("category");
        this.selectskill = [];
        this.category1 = [];
        this.statecity = [];
        this.rateskill = [];
        this.cityselect = [];
        this.skillarray = [];
        this.resultskill = [{ obtain_score: "" }];
        this.finalresultskill = [];
        // finalValue:any;
        this.selectedFile = null;
        this.candiemployeedeatils = {
            company: "",
            location: "",
            designation: "",
            currently_working: "",
            description: "",
            from: "",
            to: ""
        };
        this.educationData = {
            school: "",
            degree: "",
            field_of_study: "",
            grade: "",
            from: "",
            to: "",
            description: ""
        };
        this.candi_edu_details_edit = {};
        this.candi_emp_details = [];
        this.selectedexp = {};
        this.candi_education_edit = {};
        this.candi_emp_details_update = [];
        this.candi_edu_details_update = [];
        // citymodel: any = [];
        this.candi_emp_details_edit = { company: "" };
        this.emp_details = {
            company: "",
            location: "",
            designation: "",
            currently_working: "",
            description: "",
            from: "",
            to: ""
        };
        this.edu_details = {
            school: "",
            degree: "",
            field_of_study: "",
            grade: "",
            from: "",
            to: "",
            description: ""
        };
        // public uploader: FileUploader = new FileUploader({ url: '/candi_profile_image_update', itemAlias: 'image' });
        this.model = {
            Any: {
                checked: false,
                disable: false
            },
            Mumbai: {
                checked: false,
                disable: false
            },
            Other: {
                checked: false,
                disable: false
            },
            Hyderabad: {
                checked: false,
                disable: false
            },
            Bangalore: {
                checked: false,
                disable: false
            },
            Kolkata: {
                checked: false,
                disable: false
            },
            Delhi: {
                checked: false,
                disable: false
            },
            Chennai: {
                checked: false,
                disable: false
            },
            Pune: {
                checked: false,
                disable: false
            }
        };
        this.fileToUpload = null;
        this.abc = [];
        this.notification = [];
        this.SkillData = [];
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {};
        this.allowedMimeType = ["image/jpeg", "image/jpg", "image/png"];
        this.allowedMimeResumeType = [
            "application/pdf",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        ];
        this.sampledata = { skill_name: "", obtain_score: "", total_marks: "" };
        this.tempcatergory = [];
        this.categoryselectskill = [];
        this.skillemty = [];
        this.xyz = [];
        this.datesystem = new Date();
        this.tempdata = [];
        this.sampleskill = [];
        this.yFilter = [];
        this.ResultSkill = [];
        this.ratedskill = [];
        this.EmptyResult = [];
        this.selectedcity = "Mumbai";
        this.breakcity = [];
        this.breakcitypush = [];
        this.stateshow = [];
        this.finalcity = [];
        this.getcheck = [];
        this.lastPart = [];
        this.myFormattedDate = new _angular_common__WEBPACK_IMPORTED_MODULE_10__["DatePipe"]("en-US").transform(this.datesystem, "ddMMyyyy");
        this.defaultItemCategories = {
            id: null,
            category: null
        };
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_8__["Headers"]({
            "Content-Type": "application/json"
        });
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__["FileUploader"]({
            url: URL,
            itemAlias: "image",
            allowedFileType: ["image"],
            maxFileSize: 500 * 1024
        });
        this.uploaderresume = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__["FileUploader"]({
            url: UrlResume,
            itemAlias: "resume",
            allowedFileType: ["doc"],
            maxFileSize: 500 * 1024 * 1024
        });
        this.sample = true;
        this.Objectarray = [];
        this.Objectskill = [];
        this.tempItem = [];
        this.finalid = [];
        var currentUser = localStorage.getItem("token");
        var today = new Date();
        var minAge = 18;
        this.newminAge = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());
        if (currentUser) {
            authHeader.push({ name: "x-access-token", value: currentUser });
        }
        var uploadOptions = { headers: authHeader };
        this.uploader.setOptions(uploadOptions);
        this.uploaderresume.setOptions(uploadOptions);
    }
    UpdateProfileComponent.prototype.changeCountry = function (id) {
        var _this = this;
        debugger;
        this.auth.postDetail("/city", this.token, { id: id }).then(function (data) {
            _this.cityselect = data.json().result;
        });
    };
    UpdateProfileComponent.prototype.onItemDeSelect = function (item) {
        var val = item;
        var index = this.tempItem.findIndex(function (item, i) {
            return item.id === val.id;
        });
        this.tempItem.splice(index, 1);
        this.finalid = this.tempItem;
        var id = [];
        this.finalid.filter(function (item) {
            id.push(item.id);
        });
        this.Objectskill = id;
        var skill = [];
        this.finalid.filter(function (item) {
            skill.push(item.skill);
        });
        this.Objectarray = skill;
    };
    UpdateProfileComponent.prototype.onItemSelect = function (item) {
        this.tempItem.push(item);
        if (item.id) {
            this.Objectskill.push(item.id);
            console.log(this.Objectskill);
            this.Objectarray.push(item.skill);
            console.log(this.Objectarray);
        }
        else {
        }
        this.candi_skillset = this.Objectskill.toString();
    };
    UpdateProfileComponent.prototype.onSelectAll = function (items) { };
    Object.defineProperty(UpdateProfileComponent.prototype, "selectedOptions", {
        get: function () {
            return this.options.filter(function (opt) { return opt.checked; }).map(function (opt) { return opt.value; });
        },
        enumerable: true,
        configurable: true
    });
    UpdateProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.createFormExp();
        this.getSkill();
        this.getCandidateDetails();
        this.dropdownSettings = {
            singleSelection: false,
            idField: "id",
            textField: "skill",
            selectAllText: "Select All",
            unSelectAllText: "UnSelect All",
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
        this.uploaderresume.onWhenAddingFileFailed = function (item, filter, options) {
            switch (filter.name) {
                case "fileSize":
                    _this.errorMessage = "Maximum upload size 500kb allowed.";
                    _this.toast.error(_this.errorMessage);
                    break;
                case "mimeType":
                    var allowedTypes = _this.allowedMimeResumeType.join();
                    _this.errorMessage = "Type \"$ {item.type} is not allowed. Allowed types: \"" + allowedTypes + "\".";
                    _this.toast.error(_this.errorMessage);
                    break;
                default:
                    _this.errorMessage = "Unknown error (filter is $ {filter.name}).";
            }
        };
        this.uploaderresume.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploaderresume.onCompleteItem = function (item, response, status, headers) {
            alert("File uploaded successfully");
            _this.onUpdate();
            // window.location.reload();
        };
        this.uploader.onWhenAddingFileFailed = function (item, filter, options) {
            switch (filter.name) {
                case "fileSize":
                    _this.errorMessage = "Maximum upload size 500kb allowed.";
                    _this.toast.error(_this.errorMessage);
                    break;
                case "mimeType":
                    var allowedTypes = _this.allowedMimeType.join();
                    _this.errorMessage = "Type \"$ {item.type} is not allowed. Allowed types: \"" + allowedTypes + "\".";
                    _this.toast.error(_this.errorMessage);
                    break;
                default:
                    _this.errorMessage = "Unknown error (filter is $ {filter.name}).";
            }
        };
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            alert("File uploaded successfully");
            _this.onUpdate();
            // window.location.reload();
        };
        this.auth.get("/state", this.token).then(function (data) {
            _this.statecity = data.json().result;
            console.log("this.statecity", _this.statecity);
            for (var i = 0; i < _this.statecity.length; i++) {
                if (_this.statecity[i].id === _this.stateindex) {
                    _this.stateindex = _this.statecity[i].name;
                }
                _this.stateshow = _this.stateindex;
            }
        });
        this.myCheck = this.fb.group({
            citycheck: this.fb.array([])
        });
        this.prefercity = [
            { name: "Any", checked: false },
            { name: "Hyderabad", checked: false },
            { name: "Chennai", checked: false },
            { name: "Pune", checked: false },
            { name: "Mumbai", checked: false },
            { name: "Bangalore", checked: false },
            { name: "Kolkata", checked: false },
            { name: "Delhi", checked: false },
            { name: "Other", checked: false }
        ];
        this.myform = this.fb.group({
            fullname: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dob: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            gender: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            state: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            city: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            year: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            month: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.summaryForm = this.fb.group({
            profile_summary: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.skillForm = this.fb.group({
            skill_name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            day: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.ctcForm = this.fb.group({
            ctc: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            ectc: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    Object.defineProperty(UpdateProfileComponent.prototype, "dob", {
        get: function () {
            return this.myform.get("dob");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "year", {
        get: function () {
            return this.myform.get("year");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "month", {
        get: function () {
            return this.myform.get("month");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "ctc", {
        get: function () {
            return this.ctcForm.get("ctc");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "ectc", {
        get: function () {
            return this.ctcForm.get("ectc");
        },
        enumerable: true,
        configurable: true
    });
    UpdateProfileComponent.prototype.resetform = function () {
        this.form.reset();
    };
    UpdateProfileComponent.prototype.resetformexp = function () {
        this.formexp.reset();
    };
    UpdateProfileComponent.prototype.createForm = function () {
        this.form = this.fb.group({
            school: [
                "",
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("")])
            ],
            degree: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            field_of_study: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            grade: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dateTo: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dateFrom: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        }, { validator: this.dateLessThan("dateFrom", "dateTo") });
    };
    UpdateProfileComponent.prototype.createUpdateForm = function () {
        this.formupdate = this.fb.group({
            school: [
                "",
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("")])
            ],
            degree: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            field_of_study: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            grade: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dateTo: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dateFrom: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        }, { validator: this.dateLessThan("dateFrom", "dateTo") });
    };
    Object.defineProperty(UpdateProfileComponent.prototype, "schooll", {
        get: function () {
            return this.formupdate.get("school");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "degreee", {
        get: function () {
            return this.formupdate.get("degree");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "field_of_studyy", {
        get: function () {
            return this.formupdate.get("field_of_study");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "gradee", {
        get: function () {
            return this.formupdate.get("grade");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "school", {
        get: function () {
            return this.form.get("school");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "degree", {
        get: function () {
            return this.form.get("degree");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "field_of_study", {
        get: function () {
            return this.form.get("field_of_study");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "grade", {
        get: function () {
            return this.form.get("grade");
        },
        enumerable: true,
        configurable: true
    });
    UpdateProfileComponent.prototype.anyClick = function (checkName, checked) {
        if (checkName === "Any") {
            if (checked) {
                this.model.Mumbai.checked = false;
                this.model.Other.checked = false;
                this.model.Chennai.checked = false;
                this.model.Hyderabad.checked = false;
                this.model.Bangalore.checked = false;
                this.model.Delhi.checked = false;
                this.model.Kolkata.checked = false;
                this.model.Pune.checked = false;
                this.finalcity = [];
                this.finalcity.push(checkName);
                this.model.Other.disable = true;
                this.model.Mumbai.disable = true;
                this.model.Chennai.disable = true;
                this.model.Hyderabad.disable = true;
                this.model.Bangalore.disable = true;
                this.model.Delhi.disable = true;
                this.model.Kolkata.disable = true;
                this.model.Pune.disable = true;
            }
            else {
                this.model.Other.disable = false;
                this.model.Mumbai.disable = false;
                this.model.Chennai.disable = false;
                this.model.Hyderabad.disable = false;
                this.model.Bangalore.disable = false;
                this.model.Delhi.disable = false;
                this.model.Kolkata.disable = false;
                this.model.Pune.disable = false;
                this.model.Any.checked = false;
                this.finalcity.splice(this.finalcity.indexOf(checkName), 1);
            }
        }
        else {
            if (checked) {
                this.model[checkName].checked = true;
                this.model[checkName].disable = false;
                this.finalcity.push(checkName);
            }
            else {
                this.model[checkName].checked = false;
                this.model[checkName].disable = false;
                this.finalcity.splice(this.finalcity.indexOf(checkName), 1);
            }
        }
    };
    UpdateProfileComponent.prototype.createFormExp = function () {
        this.formexp = this.fb.group({
            company: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            designation: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            location: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            currently_working: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dateTo: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dateFrom: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        }, { validator: this.dateLessThan("dateFrom", "dateTo") });
    };
    Object.defineProperty(UpdateProfileComponent.prototype, "company", {
        get: function () {
            return this.formexp.get("company");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "designation", {
        get: function () {
            return this.formexp.get("designation");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpdateProfileComponent.prototype, "location", {
        get: function () {
            return this.formexp.get("location");
        },
        enumerable: true,
        configurable: true
    });
    UpdateProfileComponent.prototype.dateLessThan = function (from, to) {
        return function (group) {
            var f = group.controls[from];
            var t = group.controls[to];
            if (f.value > t.value) {
                return {
                    dates: "Date from should be less than Date to"
                };
            }
            return {};
        };
    };
    UpdateProfileComponent.prototype.fileUpload = function (event) {
        var _this = this;
        // debugger;
        console.log("event", event);
        if (this.errorMessage != "Maximum upload size 500kb allowed.") {
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(event.target.files[0]); // read file as data url
                reader.onload = function (event) {
                    _this.url = event.target.result;
                };
            }
        }
    };
    UpdateProfileComponent.prototype.fileUploadResume = function (event) {
        var _this = this;
        if (this.errorMessage != "Maximum upload size 500kb allowed.") {
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(event.target.files[0]); // read file as data url
                reader.onload = function (event) {
                    _this.url = event.target.result;
                };
            }
        }
    };
    UpdateProfileComponent.prototype.toggleaction = function (event) {
        if (event === true) {
            this.togglevalue = event;
        }
        else {
            this.togglevalue = event;
        }
    };
    UpdateProfileComponent.prototype.onSubscriptionChange = function (event) {
        // debugger;
        if (event === true) {
            this.candidateData.disability = event;
        }
        else {
            this.activelylooking = event;
        }
    };
    UpdateProfileComponent.prototype.getCandidateDetails = function () {
        // let looking_job: string;
        // let disability: string;
        var _this = this;
        this.auth
            .get("/candi_profile_details", this.token)
            .then(function (list) {
            _this.sampledata = list.json().data;
            _this.candidateData = list.json().result;
            console.table("candidateData", _this.candidateData);
            console.table("sampledata", _this.sampledata);
            _this.calculate_age(new Date(_this.candidateData.candi_dob));
            _this.educationData = list.json().education_details;
            _this.candiemployeedeatils = list.json().candi_emp_details;
            for (var i = 0; i < _this.candidateData.prefer_location.length; i++) {
                _this.breakcity = _this.candidateData.prefer_location;
                _this.breakcitypush = _this.breakcity.split(",");
                if (_this.breakcitypush[i] != null) {
                    _this.finalcity.push(_this.breakcitypush[i]);
                    console.log("finalcity", _this.finalcity);
                }
            }
            console.log("finalcity", _this.finalcity);
            _this.getcheck = _this.finalcity;
            // let exp_required = this.totalage - 18;
            // console.log("exp_required",exp_required);
            //         debugger;
            //         for (let i = 0; i < this.years.length; i++) {
            //           if (exp_required < i) {
            //             console.log("required",exp_required);
            // console.log(this.candidateData.years);
            //             this.yFilter = this.candidateData.years.map(itemY => {
            //               console.log("itemY",itemY.years);
            //               return itemY.years;
            //             });
            //             console.log(this.years);
            //             this.ratedskill = this.years.filter(
            //               itemX => !this.yFilter.includes(itemX)
            //             );
            //           }
            //         }
            for (var i = 0; i < _this.candidateData.candi_skillset.length; i++) {
                _this.xyz = _this.candidateData.candi_skillset;
                _this.abc = _this.xyz.split(",");
                if (_this.abc[i] != null) {
                    _this.sampleskill.push(parseInt(_this.abc[i]));
                }
            }
            // this.lastPart = this.candidateData.resume_path.split("/").pop();
            // console.log("lastpart", this.lastPart);
            _this.sampleskill.forEach(function (element) {
                var data = { id: element };
                _this.tempdata.push(data);
            });
            _this.yFilter = _this.tempdata.map(function (itemY) {
                return itemY.id;
            });
            // debugger;
            _this.ratedskill = _this.SkillData.filter(function (itemX) { return !_this.yFilter.includes(itemX.id); });
            console.log("dgfdgf", _this.ratedskill);
            _this.candidateData.looking_job == true ? "Yes" : "No";
            // if (this.candidateData.looking_job == "Yes") {
            //   return (this.candidateData.looking_job = 1);
            // }
            // if (this.candidateData.looking_job == "No") {
            //   return (this.candidateData.looking_job = 0);
            // }
            _this.candidateData.disability == true ? "Yes" : "No";
            // if (this.candidateData.disability == "Yes") {
            //   return (this.candidateData.disability = 1);
            // }
            // if (this.candidateData.disability == "No") {
            //   return (this.candidateData.disability = 0);
            // }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    UpdateProfileComponent.prototype.calculate_age = function (dob) {
        var diff_ms = Date.now() - dob.getTime();
        var age_dt = new Date(diff_ms);
        this.totalage = Math.abs(age_dt.getUTCFullYear() - 1970);
        return this.totalage;
    };
    UpdateProfileComponent.prototype.countAge = function (dob) {
        var timeDiff = Math.abs(Date.now() - dob.getTime());
        this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365);
    };
    UpdateProfileComponent.prototype.getSkill = function () {
        var _this = this;
        this.auth
            .get("/skills", this.token)
            .then(function (list) {
            _this.SkillData = list.json().result;
            console.log(_this.SkillData);
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    UpdateProfileComponent.prototype.onSelectFile = function (event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
        }
    };
    //multiselect preferred city
    UpdateProfileComponent.prototype.onChange = function (city, isChecked, index) {
        var _this = this;
        this.cityFormArray = this.myCheck.controls.citycheck;
        if (isChecked) {
            this.cityFormArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.prefercity[index].name));
        }
        else {
            var index_1 = this.cityFormArray.controls.findIndex(function (x) { return x.value == _this.prefercity.name; });
            this.cityFormArray.removeAt(index_1);
        }
        this.cityvalue = this.cityFormArray.value.join(",");
    };
    //open model
    UpdateProfileComponent.prototype.openModal = function (id) {
        var myString = this.candidateData.lives_in;
        this.mySplits = myString.split(",");
        this.cityindex = this.mySplits[0];
        console.log(this.cityindex);
        this.stateindex = this.mySplits[1].trim();
        console.log(this.stateindex);
        this.drpstateindex = this.stateindex;
        this.share.open(id);
    };
    UpdateProfileComponent.prototype.openModalExperiences = function (i) {
        var _this = this;
        this.resetformexp();
        this.token = localStorage.getItem("token");
        this.auth
            .postDetail("/candi_emp_details_edit", this.token, {
            id: this.candiemployeedeatils[i].id
        })
            .then(function (data) {
            _this.candi_emp_details_edit = data.json().result;
            _this.emp_details = _this.candi_emp_details_edit[0];
        });
    };
    UpdateProfileComponent.prototype.openModalEducation = function (i) {
        var _this = this;
        this.token = localStorage.getItem("token");
        this.auth
            .postDetail("/candi_education_edit", this.token, {
            id: this.educationData[i].id
        })
            .then(function (data) {
            _this.candi_edu_details_edit = data.json().result;
            _this.edu_details = _this.candi_edu_details_edit[0];
        });
    };
    UpdateProfileComponent.prototype.UpdateEducation = function () {
        var _this = this;
        this.auth
            .postDetail("/candi_education_update", this.token, {
            id: this.edu_details.id,
            school: this.edu_details.school,
            degree: this.edu_details.degree,
            field_of_study: this.edu_details.field_of_study,
            from: this.edu_details.from,
            to: this.edu_details.to,
            grade: this.emp_details.grade,
            description: this.emp_details.description
        })
            .then(function (data) {
            _this.candi_edu_details_update = data.json();
        });
    };
    UpdateProfileComponent.prototype.UpdateExperience = function () {
        var _this = this;
        this.token = localStorage.getItem("token");
        this.auth
            .postDetail("/candi_emp_details_update", this.token, {
            id: this.emp_details.id,
            company: this.emp_details.company,
            location: this.emp_details.location,
            designation: this.emp_details.designation,
            from: this.emp_details.from,
            to: this.emp_details.to,
            currently_working: this.emp_details.currently_working,
            description: this.emp_details.description
        })
            .then(function (data) {
            _this.candi_emp_details_update = data.json();
        });
    };
    UpdateProfileComponent.prototype.SaveEducation = function () {
        var _this = this;
        this.token = localStorage.getItem("token");
        this.auth
            .postDetail("/candi_education", this.token, {
            school: this.educationData.school,
            degree: this.educationData.degree,
            field_of_study: this.educationData.field_of_study,
            from: this.educationData.from,
            to: this.educationData.to,
            grade: this.educationData.grade,
            description: this.educationData.description
        })
            .then(function (data) {
            _this.candi_emp_details = data.json().result;
        });
        this.form.reset();
    };
    UpdateProfileComponent.prototype.SaveExperience = function () {
        var _this = this;
        this.token = localStorage.getItem("token");
        this.auth
            .postDetail("/candi_emp_details", this.token, {
            company: this.candiemployeedeatils.company,
            location: this.candiemployeedeatils.location,
            designation: this.candiemployeedeatils.designation,
            from: this.candiemployeedeatils.from,
            to: this.candiemployeedeatils.to,
            currently_working: this.candiemployeedeatils.currently_working,
            description: this.candiemployeedeatils.description
        })
            .then(function (data) {
            _this.candi_emp_details = data.json().result;
        });
        this.formexp.reset();
    };
    //update button click
    UpdateProfileComponent.prototype.onUpdate = function () {
        for (var i = 0; i < this.statecity.length; i++) {
            if (this.statecity[i].id == this.stateindex) {
                this.stateindex = this.statecity[i].name;
            }
        }
        // this.activatedRoute.params.subscribe((option: Params) => {
        //   this.token = localStorage.getItem("token");
        //   this.auth
        //     .putResult(
        //       "/candi_profile_update/" + this.candidateData.id,
        //       {
        //         candi_fullname: this.candidateData.candi_fullname,
        //         candi_dob: this.candidateData.candi_dob,
        //         candi_exp_years: this.candidateData.candi_exp_years,
        //         candi_exp_months: this.candidateData.candi_exp_months,
        //         lives_in: this.candidateData.lives_in =
        //           this.cityindex + ", " + this.stateindex
        //       },
        //       this.token
        //     )
        //     .then(data => {
        //       this.candidateList = data.json().result;
        //     });
        //   // window.location.reload();
        // });
    };
    UpdateProfileComponent.prototype.skilljoinadd = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.token = localStorage.getItem("token");
            _this.auth
                .putResult("/candi_profile_update/" + _this.candidateData.id, {
                skill_name: _this.candidateData.skill_name + "," + _this.Objectarray.toString(),
                candi_skillset: _this.candidateData.candi_skillset +
                    "," +
                    _this.Objectskill.toString(),
                can_join: _this.candidateData.can_join
            }, _this.token)
                .then(function (data) {
                _this.candidateList1 = data.json().result;
            });
            _this.onUpdate();
            // window.location.reload();
        });
    };
    UpdateProfileComponent.prototype.onUpload = function () {
        var fd = new FormData();
        fd.append("resume", this.selectedFile, this.selectedFile.name);
        this.token = localStorage.getItem("token");
        this.auth.uploadfile("/candi_profile_update/" + this.candidateData.id, this.token, { resume_path: fd });
    };
    UpdateProfileComponent.prototype.preferlocation = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.token = localStorage.getItem("token");
            _this.auth
                .putResult("/candi_profile_update/" + _this.candidateData.id, {
                prefer_location: _this.finalcity.toString()
            }, _this.token)
                .then(function (data) {
                _this.candidateList1 = data.json().result;
            });
            _this.getCandidateDetails();
            // window.location.reload();
        });
    };
    UpdateProfileComponent.prototype.profilesummary = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.token = localStorage.getItem("token");
            _this.auth
                .putResult("/candi_profile_update/" + _this.candidateData.id, {
                profile_summary: _this.candidateData.profile_summary
            }, _this.token)
                .then(function (data) {
                _this.candidateList1 = data.json().result;
            });
            _this.onUpdate();
            // window.location.reload();
        });
    };
    UpdateProfileComponent.prototype.ctcadd = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (option) {
            _this.token = localStorage.getItem("token");
            _this.auth
                .putResult("/candi_profile_update/" + _this.candidateData.id, {
                current_ctc: _this.candidateData.current_ctc,
                expected_ctc: _this.candidateData.expected_ctc
            }, _this.token)
                .then(function (data) {
                _this.candidateList1 = data.json().result;
            });
            _this.onUpdate();
            // window.location.reload();
        });
    };
    UpdateProfileComponent.prototype.DocUpload = function (event) {
        // console.log("uploadeddd", event);
    };
    UpdateProfileComponent.prototype.Activelylook = function () {
        var _this = this;
        // debugger;
        // this.activatedRoute.params.subscribe((option: Params) => {
        this.token = localStorage.getItem("token");
        var looking = this.candidateData.looking_job === "Yes" ? "Yes" : "No";
        var disability = this.candidateData.disability === "Yes" ? "Yes" : "No";
        this.auth
            .putResult("/candi_profile_update/" + this.candidateData.id, {
            looking_job: looking,
            disability: disability
        }, this.token)
            .then(function (data) {
            _this.candidateList1 = data.json().result;
        });
        // this.onUpdate();
        // window.location.reload();
        //  / });
    };
    UpdateProfileComponent.prototype.activelyLooking = function (e) {
        // debugger;
        var looking;
        if (this.candidateData.looking_job == "Yes") {
            looking = "No";
            this.auth
                .putResult("/candi_profile_update/" + this.candidateData.id, {
                looking_job: looking
            }, this.token)
                .then(function (data) {
                // console.log(data.json());
            });
            this.getCandidateDetails();
        }
        else {
            looking = "Yes";
            this.auth
                .putResult("/candi_profile_update/" + this.candidateData.id, {
                looking_job: looking
            }, this.token)
                .then(function (data) { });
        }
        this.getCandidateDetails();
    };
    UpdateProfileComponent.prototype.disability = function (e) {
        var disability;
        //  debugger;
        if (this.candidateData.disability == "No") {
            disability = "Yes";
            this.auth
                .putResult("/candi_profile_update/" + this.candidateData.id, {
                disability: disability
            }, this.token)
                .then(function (data) {
                // console.log(data.json());
            });
            this.getCandidateDetails();
        }
        else {
            disability = "No";
            this.auth
                .putResult("/candi_profile_update/" + this.candidateData.id, {
                disability: disability
            }, this.token)
                .then(function (data) { });
        }
        this.getCandidateDetails();
    };
    UpdateProfileComponent.prototype.prefferCity = function (e) {
        alert();
        console.log(this.finalcity);
        console.log("==", e);
    };
    UpdateProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-update-profile",
            template: __webpack_require__(/*! ./update-profile.component.html */ "./src/app/Dashboard/update-profile/update-profile.component.html"),
            styles: [__webpack_require__(/*! ./update-profile.component.css */ "./src/app/Dashboard/update-profile/update-profile.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"],
            _angular_http__WEBPACK_IMPORTED_MODULE_8__["Http"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _services_sharedata_service__WEBPACK_IMPORTED_MODULE_3__["SharedataService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], UpdateProfileComponent);
    return UpdateProfileComponent;
}());



/***/ }),

/***/ "./src/app/Dashboard/user-interface-design/user-interface-design.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/Dashboard/user-interface-design/user-interface-design.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".circle-text {\r\n    width:50%;\r\n}\r\n.circle-text:after {\r\n    content: \"\";\r\n    display: block;\r\n    width: 100%;\r\n    height:0;\r\n    padding-bottom: 100%; \r\n    border-radius: 50%;\r\n    border: 3px solid #FF4F33;\r\n}\r\n.circle-text div {\r\n    float:left;\r\n    width:100%;\r\n    padding-top:2%;\r\n    line-height:1em;\r\n    margin-top:10px;\r\n    text-align:center;\r\n    color:white;\r\n}\r\n.btnreq{\r\n    color: #ffffff;\r\n    background-color: #111111;\r\n    width: 179px;\r\n    border-radius: 25px;\r\n}\r\nhr{\r\n    margin-top: 4px;\r\n    margin-bottom: 4px;\r\n}\r\n.cardtitle{\r\n    background-color: #FF3333;width: 200px;margin-top: -12px;margin-left: 3px;height: 20px; font-size: 14px;color: aliceblue;\r\n}"

/***/ }),

/***/ "./src/app/Dashboard/user-interface-design/user-interface-design.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/Dashboard/user-interface-design/user-interface-design.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-navbar></app-navbar> -->\r\n<!-- <app-sidenavbar></app-sidenavbar> -->\r\n<app-full-layout></app-full-layout>\r\n<div class=\"container\" style=\"margin-left: 12%\">\r\n\r\n  <h2 style=\"font-family: sans-serif;font-size: 20px;\">Interviews</h2>\r\n  <ul class=\"nav nav-tabs\">\r\n    <li class=\"active\"><a data-toggle=\"tab\" href=\"#home\" style=\"font-family: sans-serif;font-size: 18px;\">Summary</a></li>\r\n    <li><a data-toggle=\"tab\" href=\"#menu1\" style=\"font-family: sans-serif;font-size: 18px;\">Detail Report</a></li>\r\n  </ul>\r\n\r\n  <div class=\"tab-content\">\r\n    <div id=\"home\" class=\"tab-pane fade in active\">\r\n      <div>\r\n        <div class=\"card\">\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-4\">\r\n                <div class=\"circle-text\">\r\n                  <div style=\"font-size: 24px;margin-top: 30px;color: black\">Scored</div>\r\n                  <div style=\"font-size: 40px;color: #FF4F33\">435</div>\r\n                  <div style=\"font-size: 16px;color:black;\">Out of 500</div>\r\n                </div>\r\n                <div id='page' style='margin-top: -20px;margin-left: 30px;'>\r\n                  <a style=\"height: 100px;\"><span style='border:1px black solid;border-radius: 25px; font-size:18px;width: 900px;background-color: bisque;'>Expert\r\n                      Level</span></a>\r\n                </div>\r\n                <!-- <a style=\"width: 150px;border-radius: 25px;height: 40px;\">Expert Level</a> -->\r\n              </div>\r\n              <div class=\"col-md-8\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;font-family: sans-serif;float: left;\">Test Name</p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;color:#999999;\">Java Core Test</p>\r\n                  </div>\r\n                </div>\r\n                <hr>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;font-family: sans-serif;float: left;\">Taken On</p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;color:#999999;\">20 October 2018</p>\r\n                  </div>\r\n                </div>\r\n                <hr>\r\n\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;font-family: sans-serif;float: left;\">Time Taken</p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;color:#999999;\">32 Mins</p>\r\n                  </div>\r\n                </div>\r\n                <hr>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;font-family: sans-serif;float: left;\">Total Questions</p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;color:#999999;\">20</p>\r\n                  </div>\r\n                </div>\r\n                <hr>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;font-family: sans-serif;float: left;\">Questions Skipped</p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;color:#999999;\">2</p>\r\n                  </div>\r\n                </div>\r\n                <hr>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;font-family: sans-serif;float: left;\">Right Replies</p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;color:#999999;\">12</p>\r\n                  </div>\r\n                </div>\r\n                <hr>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;font-family: sans-serif;float: left;\">Wrong Replies</p>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <p style=\"font-size: 14px;color:#999999;\">6</p>\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br>\r\n      <div>\r\n        <div class=\"card\">\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div>\r\n                <p class=\"cardtitle\">MINEMARK\r\n                  SUGGESTION</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" style=\"margin-left: 8px;\">\r\n              <div>\r\n                <h4>Become a mentor</h4>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <p style=\"margin-left: 21px\">Congratulations! Your score is elegible for our mentorship program. Join the\r\n                esteemed group of\r\n                Mentors and help others in their career path ahead</p>\r\n            </div>\r\n            <div>\r\n              <button class=\"btn btnreq\">Request Mentorship</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br>\r\n      <div class=\"row\" style=\"text-align: center;\">\r\n        <div class=\"col-md-12\">\r\n          <button class=\"btn\" style=\"width: 200px; height: 50px;background-color: #FF4F33;color: #ffffff;font-size: 16px; border-radius: 25px;\">Take\r\n            test again</button>\r\n        </div>\r\n      </div>\r\n      <br>\r\n    </div>\r\n    <div id=\"menu1\" class=\"tab-pane fade\">\r\n      <div>\r\n        <div class=\"card\">\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-1\">\r\n                <div class=\"\" style=\"width:50px;height:50px;background-color: #FFCC66; \tborder: 1px solid #FF3333;\">\r\n                  <p style=\"text-align:center;font-size: 14px;\">23</p>\r\n                  <p style=\"text-align:center;font-size: 14px;\">Nov</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-8\" style=\"margin: 0px;\">\r\n                <p class=\"card-title\">Informatica Group Pvt Ltd </p>\r\n                <p class=\"card-text\">11pm &nbsp; 23 Nov</p>\r\n                <p class=\"card-text\">Lead Angular Js Developer</p>\r\n\r\n              </div>\r\n              <div class=\"col-md-3\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div style=\"margin-left: 30px;padding: 10px;\">\r\n            <button class=\"btn btn-primary\" style=\"background-color: #308651;border-radius: 25px;font-family: sans-serif;font-size:14px;\">Respond</button>\r\n          </div>\r\n          <div style=\"text-align:right;margin-top:-35px;margin-right: 30px;padding: 10px;\">\r\n            <a href=\"#\" style=\"color: #F24540;\">View Details</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br>\r\n\r\n      <div>\r\n        <div class=\"card\">\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-1\">\r\n                <div class=\"\" style=\"width:50px;height:50px;background-color: #FFCC66; \tborder: 1px solid #FF3333;\">\r\n                  <p style=\"text-align:center;font-size: 14px;\">23</p>\r\n                  <p style=\"text-align:center;font-size: 14px;\">Nov</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-8\" style=\"margin: 0px;\">\r\n                <p class=\"card-title\">Informatica Group Pvt Ltd </p>\r\n                <p class=\"card-text\">11pm &nbsp; 23 Nov</p>\r\n                <p class=\"card-text\">Lead Angular Js Developer</p>\r\n\r\n              </div>\r\n              <div class=\"col-md-3\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div style=\"margin-left: 30px;padding: 10px;\">\r\n            <button class=\"btn btn-primary\" style=\"background-color:#308651;border-radius: 25px;font-family: sans-serif;font-size:14px;\">Respond</button>\r\n          </div>\r\n          <div style=\"text-align:right;margin-top:-35px;margin-right: 30px;padding: 10px;\">\r\n            <a href=\"#\" style=\"color: #F24540;\">View Details</a>\r\n          </div>\r\n        </div>\r\n\r\n\r\n\r\n        <!-- <h2>Hover Rows</h2>\r\n            <p>The .table-hover class enables a hover state on table rows:</p>\r\n            <table class=\"table table-hover\">\r\n              <thead>\r\n                <tr>\r\n                  <th>Firstname</th>\r\n                  <th>Lastname</th>\r\n                  <th>Email</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td>John</td>\r\n                  <td>Doe</td>\r\n                  <td>john@example.com</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>Mary</td>\r\n                  <td>Moe</td>\r\n                  <td>mary@example.com</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>July</td>\r\n                  <td>Dooley</td>\r\n                  <td>july@example.com</td>\r\n                </tr>\r\n              </tbody>\r\n            </table>-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<footer class=\"app-footer\" style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n  &copy; <a href=\"#\" style=\"color:black\">MineMark Solutions Private Limited</a> 2018 </footer>"

/***/ }),

/***/ "./src/app/Dashboard/user-interface-design/user-interface-design.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/Dashboard/user-interface-design/user-interface-design.component.ts ***!
  \************************************************************************************/
/*! exports provided: UserInterfaceDesignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserInterfaceDesignComponent", function() { return UserInterfaceDesignComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserInterfaceDesignComponent = /** @class */ (function () {
    function UserInterfaceDesignComponent() {
    }
    UserInterfaceDesignComponent.prototype.ngOnInit = function () {
    };
    UserInterfaceDesignComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-interface-design',
            template: __webpack_require__(/*! ./user-interface-design.component.html */ "./src/app/Dashboard/user-interface-design/user-interface-design.component.html"),
            styles: [__webpack_require__(/*! ./user-interface-design.component.css */ "./src/app/Dashboard/user-interface-design/user-interface-design.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UserInterfaceDesignComponent);
    return UserInterfaceDesignComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* ul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n    overflow: hidden;\r\n    background-color: #ffffff;\r\n    \r\n}\r\n\r\nli {\r\n    float: right;\r\n}\r\n\r\nli a {\r\n    display: inline-block;\r\n    color: black;\r\n    text-align: center;\r\n    font-family: sans-serif;\r\n    padding: 14px 16px;\r\n    text-decoration: none;\r\n}\r\n\r\nli a:hover {\r\n    background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\r\n    color: #111;\r\n}\r\n\r\nbody {\r\n    font-family: \"Lato\", sans-serif;\r\n}\r\n\r\n.sidenav {\r\n    height: 100%;\r\n    width: 0;\r\n    position: fixed;\r\n    z-index: 1;\r\n    top: 0;\r\n    left: 0;\r\n    color: #111;\r\n    background-color: white;\r\n    overflow-x: hidden;\r\n    transition: 0.5s;\r\n    padding-top: 60px;\r\n}\r\n\r\n.sidenav a {\r\n    padding: 8px 8px 8px 32px;\r\n    text-decoration: none;\r\n    font-size: 25px;\r\n    color: black;\r\n    display: block;\r\n    transition: 0.3s;\r\n}\r\n\r\n.sidenav a:hover {\r\n    color: #ff6600;\r\n}\r\n\r\n.sidenav .closebtn {\r\n    position: absolute;\r\n    top: 0;\r\n    right: 25px;\r\n    font-size: 36px;\r\n    margin-left: 50px;\r\n}\r\n.badge-notify{\r\n    background:red;\r\n    position:relative;\r\n    top: -20px;\r\n    left: -35px;\r\n}\r\n@media screen and (max-height: 450px) {\r\n    .sidenav {padding-top: 15px;}\r\n    .sidenav a {font-size: 18px;}\r\n}\r\n\r\n.img{\r\n    margin-left: 43%;\r\n    margin-top: 2%;\r\n} */\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <img class=\"img\" src=\"../../assets/light_mm_logo.png\"  [@bounceInLeft]=\"{ value: bounceInLeft }\"> -->\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var ng_animate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng-animate */ "./node_modules/ng-animate/fesm5/ng-animate.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.menu = {};
        this.sideMenu = {};
        this.menu = [{ name: "Mentorship", path: 'mentor' },
            { name: "Interviews", path: 'interview' },
            { name: "Job Offer", path: 'job' },
            { name: "Rate Your Skill", path: 'rating' },
            { name: "Document Vault", path: 'document' },
            { name: "Professional Profile", path: 'professional' },
        ];
        this.sideMenu = [{ name: "Home", class: 'glyphicon glyphicon-home' },
            { name: "Profile", class: 'glyphicon glyphicon-user' },
            { name: "Share", class: 'glyphicon glyphicon-share' },
            { name: "Settings", class: 'material-icon' },
        ];
    }
    AppComponent.prototype.openNav = function () {
        document.getElementById("mySidenav").style.width = "250px";
    };
    AppComponent.prototype.closeNav = function () {
        document.getElementById("mySidenav").style.width = "0";
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('bounceInLeft', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('*=> *', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["useAnimation"])(ng_animate__WEBPACK_IMPORTED_MODULE_2__["bounceInLeft"], {}))])
            ],
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: tokenGetter, getAuthServiceConfigs, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tokenGetter", function() { return tokenGetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAuthServiceConfigs", function() { return getAuthServiceConfigs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_file_size__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-size */ "./node_modules/ng2-file-size/index.js");
/* harmony import */ var ng2_file_size__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_file_size__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var angular_particle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-particle */ "./node_modules/angular-particle/index.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var angular2_notifications__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! angular2-notifications */ "./node_modules/angular2-notifications/angular2-notifications.umd.js");
/* harmony import */ var angular2_notifications__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(angular2_notifications__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _Dashboard_home_home_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./Dashboard/home/home.component */ "./src/app/Dashboard/home/home.component.ts");
/* harmony import */ var _Dashboard_proffesional_profile_proffesional_profile_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./Dashboard/proffesional-profile/proffesional-profile.component */ "./src/app/Dashboard/proffesional-profile/proffesional-profile.component.ts");
/* harmony import */ var _Dashboard_document_vault_document_vault_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./Dashboard/document-vault/document-vault.component */ "./src/app/Dashboard/document-vault/document-vault.component.ts");
/* harmony import */ var _Dashboard_rate_your_skill_rate_your_skill_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./Dashboard/rate-your-skill/rate-your-skill.component */ "./src/app/Dashboard/rate-your-skill/rate-your-skill.component.ts");
/* harmony import */ var _Dashboard_job_offer_job_offer_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./Dashboard/job-offer/job-offer.component */ "./src/app/Dashboard/job-offer/job-offer.component.ts");
/* harmony import */ var _Dashboard_interviews_interviews_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./Dashboard/interviews/interviews.component */ "./src/app/Dashboard/interviews/interviews.component.ts");
/* harmony import */ var _Dashboard_mentorship_mentorship_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./Dashboard/mentorship/mentorship.component */ "./src/app/Dashboard/mentorship/mentorship.component.ts");
/* harmony import */ var angular_tabs_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! angular-tabs-component */ "./node_modules/angular-tabs-component/dist/index.js");
/* harmony import */ var ngx_speech__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ngx-speech */ "./node_modules/ngx-speech/ngx-speech.es5.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _services_talk_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./services/talk.service */ "./src/app/services/talk.service.ts");
/* harmony import */ var ng_circle_progress__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ng-circle-progress */ "./node_modules/ng-circle-progress/index.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _validations_no_whitespace_directive__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./validations/no-whitespace.directive */ "./src/app/validations/no-whitespace.directive.ts");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/esm5/countdown.js");
/* harmony import */ var angular_confirmation_popover__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! angular-confirmation-popover */ "./node_modules/angular-confirmation-popover/fesm5/angular-confirmation-popover.js");
/* harmony import */ var ngx_bar_rating__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ngx-bar-rating */ "./node_modules/ngx-bar-rating/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_34___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_34__);
/* harmony import */ var ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ngx-show-hide-password */ "./node_modules/ngx-show-hide-password/ngx-show-hide-password.umd.js");
/* harmony import */ var ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_35___default = /*#__PURE__*/__webpack_require__.n(ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_35__);
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_36___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__);
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/index.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _Dashboard_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./Dashboard/navbar/navbar.component */ "./src/app/Dashboard/navbar/navbar.component.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/forgot-password/forgot-password.component.ts");
/* harmony import */ var _exam_exam_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./exam/exam.component */ "./src/app/exam/exam.component.ts");
/* harmony import */ var _exam_question_answer_question_answer_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./exam/question-answer/question-answer.component */ "./src/app/exam/question-answer/question-answer.component.ts");
/* harmony import */ var _services_deactivate_guard_service__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./services/deactivate-guard.service */ "./src/app/services/deactivate-guard.service.ts");
/* harmony import */ var ngx_popover__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ngx-popover */ "./node_modules/ngx-popover/index.js");
/* harmony import */ var ngx_popover__WEBPACK_IMPORTED_MODULE_45___default = /*#__PURE__*/__webpack_require__.n(ngx_popover__WEBPACK_IMPORTED_MODULE_45__);
/* harmony import */ var angular_file_uploader__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! angular-file-uploader */ "./node_modules/angular-file-uploader/fesm5/angular-file-uploader.js");
/* harmony import */ var _src_app_Dashboard_home_splash_screen_splash_screen_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ../../src/app/Dashboard/home/splash-screen/splash-screen.component */ "./src/app/Dashboard/home/splash-screen/splash-screen.component.ts");
/* harmony import */ var _notification_notification_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./notification/notification.component */ "./src/app/notification/notification.component.ts");
/* harmony import */ var _signup_upload_photo_upload_photo_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./signup/upload-photo/upload-photo.component */ "./src/app/signup/upload-photo/upload-photo.component.ts");
/* harmony import */ var _Dashboard_skill_update_skill_update_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./Dashboard/skill-update/skill-update.component */ "./src/app/Dashboard/skill-update/skill-update.component.ts");
/* harmony import */ var angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! angular2-multiselect-dropdown */ "./node_modules/angular2-multiselect-dropdown/fesm5/angular2-multiselect-dropdown.js");
/* harmony import */ var _Dashboard_update_profile_update_profile_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./Dashboard/update-profile/update-profile.component */ "./src/app/Dashboard/update-profile/update-profile.component.ts");
/* harmony import */ var _Dashboard_sidenavbar_sidenavbar_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./Dashboard/sidenavbar/sidenavbar.component */ "./src/app/Dashboard/sidenavbar/sidenavbar.component.ts");
/* harmony import */ var _Dashboard_demo_demo_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./Dashboard/demo/demo.component */ "./src/app/Dashboard/demo/demo.component.ts");
/* harmony import */ var _Dashboard_interview_details_interview_details_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./Dashboard/interview-details/interview-details.component */ "./src/app/Dashboard/interview-details/interview-details.component.ts");
/* harmony import */ var _layout_full_layout_full_layout_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./layout/full-layout/full-layout.component */ "./src/app/layout/full-layout/full-layout.component.ts");
/* harmony import */ var _Dashboard_onlinetest_onlinetest_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./Dashboard/onlinetest/onlinetest.component */ "./src/app/Dashboard/onlinetest/onlinetest.component.ts");
/* harmony import */ var _Dashboard_result_test_result_test_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./Dashboard/result-test/result-test.component */ "./src/app/Dashboard/result-test/result-test.component.ts");
/* harmony import */ var _Dashboard_user_interface_design_user_interface_design_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./Dashboard/user-interface-design/user-interface-design.component */ "./src/app/Dashboard/user-interface-design/user-interface-design.component.ts");
/* harmony import */ var _Dashboard_skill_rate_test_skill_rate_test_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./Dashboard/skill-rate-test/skill-rate-test.component */ "./src/app/Dashboard/skill-rate-test/skill-rate-test.component.ts");
/* harmony import */ var _Dashboard_aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./Dashboard/aboutus/aboutus.component */ "./src/app/Dashboard/aboutus/aboutus.component.ts");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _Dashboard_newpassword_newpassword_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./Dashboard/newpassword/newpassword.component */ "./src/app/Dashboard/newpassword/newpassword.component.ts");
/* harmony import */ var _Dashboard_newpassword_compare_validator_directive__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./Dashboard/newpassword/compare-validator.directive */ "./src/app/Dashboard/newpassword/compare-validator.directive.ts");
/* harmony import */ var _Dashboard_job_details_job_details_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./Dashboard/job-details/job-details.component */ "./src/app/Dashboard/job-details/job-details.component.ts");
/* harmony import */ var _Dashboard_feedbackform_feedbackform_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./Dashboard/feedbackform/feedbackform.component */ "./src/app/Dashboard/feedbackform/feedbackform.component.ts");
/* harmony import */ var ngx_toggle_switch__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ngx-toggle-switch */ "./node_modules/ngx-toggle-switch/ui-switch.es5.js");
/* harmony import */ var _Dashboard_objectivetest_objectivetest_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./Dashboard/objectivetest/objectivetest.component */ "./src/app/Dashboard/objectivetest/objectivetest.component.ts");
/* harmony import */ var _validations_backcopypaste_directive__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./validations/backcopypaste.directive */ "./src/app/validations/backcopypaste.directive.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







































































function tokenGetter() {
    return localStorage.getItem("access_token");
}
function getAuthServiceConfigs() {
    var config = new angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["AuthServiceConfig"]([
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["FacebookLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["FacebookLoginProvider"]("329982914208524")
        },
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["GoogleLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["GoogleLoginProvider"]("Your-Google-Client-Id")
        },
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["LinkedinLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["LinkedinLoginProvider"]("Your-Google-Client-Id")
        }
    ]);
    return config;
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _validations_no_whitespace_directive__WEBPACK_IMPORTED_MODULE_30__["NoWhitespaceDirective"],
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__["SignupComponent"],
                _Dashboard_home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"],
                _Dashboard_proffesional_profile_proffesional_profile_component__WEBPACK_IMPORTED_MODULE_18__["ProffesionalProfileComponent"],
                _Dashboard_document_vault_document_vault_component__WEBPACK_IMPORTED_MODULE_19__["DocumentVaultComponent"],
                _Dashboard_rate_your_skill_rate_your_skill_component__WEBPACK_IMPORTED_MODULE_20__["RateYourSkillComponent"],
                _Dashboard_job_offer_job_offer_component__WEBPACK_IMPORTED_MODULE_21__["JobOfferComponent"],
                _Dashboard_interviews_interviews_component__WEBPACK_IMPORTED_MODULE_22__["InterviewsComponent"],
                _Dashboard_mentorship_mentorship_component__WEBPACK_IMPORTED_MODULE_23__["MentorshipComponent"],
                _Dashboard_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_40__["NavbarComponent"],
                _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_41__["ForgotPasswordComponent"],
                _exam_exam_component__WEBPACK_IMPORTED_MODULE_42__["ExamComponent"],
                _exam_question_answer_question_answer_component__WEBPACK_IMPORTED_MODULE_43__["QuestionAnswerComponent"],
                _src_app_Dashboard_home_splash_screen_splash_screen_component__WEBPACK_IMPORTED_MODULE_47__["SplashScreenComponent"],
                _notification_notification_component__WEBPACK_IMPORTED_MODULE_48__["NotificationComponent"],
                _signup_upload_photo_upload_photo_component__WEBPACK_IMPORTED_MODULE_49__["UploadPhotoComponent"],
                _Dashboard_skill_update_skill_update_component__WEBPACK_IMPORTED_MODULE_50__["SkillUpdateComponent"],
                _Dashboard_update_profile_update_profile_component__WEBPACK_IMPORTED_MODULE_52__["UpdateProfileComponent"],
                _Dashboard_sidenavbar_sidenavbar_component__WEBPACK_IMPORTED_MODULE_53__["SidenavbarComponent"],
                _Dashboard_demo_demo_component__WEBPACK_IMPORTED_MODULE_54__["DemoComponent"],
                _Dashboard_interview_details_interview_details_component__WEBPACK_IMPORTED_MODULE_55__["InterviewDetailsComponent"],
                _layout_full_layout_full_layout_component__WEBPACK_IMPORTED_MODULE_56__["FullLayoutComponent"],
                _Dashboard_onlinetest_onlinetest_component__WEBPACK_IMPORTED_MODULE_57__["OnlinetestComponent"],
                _Dashboard_result_test_result_test_component__WEBPACK_IMPORTED_MODULE_58__["ResultTestComponent"],
                _Dashboard_user_interface_design_user_interface_design_component__WEBPACK_IMPORTED_MODULE_59__["UserInterfaceDesignComponent"],
                _Dashboard_skill_rate_test_skill_rate_test_component__WEBPACK_IMPORTED_MODULE_60__["SkillRateTestComponent"],
                _Dashboard_aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_61__["AboutusComponent"],
                _Dashboard_newpassword_newpassword_component__WEBPACK_IMPORTED_MODULE_63__["NewpasswordComponent"],
                _Dashboard_newpassword_compare_validator_directive__WEBPACK_IMPORTED_MODULE_64__["CompareValidatorDirective"],
                _Dashboard_job_details_job_details_component__WEBPACK_IMPORTED_MODULE_65__["JobDetailsComponent"],
                _Dashboard_feedbackform_feedbackform_component__WEBPACK_IMPORTED_MODULE_66__["FeedbackformComponent"],
                _Dashboard_objectivetest_objectivetest_component__WEBPACK_IMPORTED_MODULE_68__["ObjectivetestComponent"],
                _validations_backcopypaste_directive__WEBPACK_IMPORTED_MODULE_69__["BackcopypasteDirective"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_70__["FooterComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_14__["CommonModule"],
                ngx_bar_rating__WEBPACK_IMPORTED_MODULE_33__["BarRatingModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_10__["HttpModule"],
                ngx_toggle_switch__WEBPACK_IMPORTED_MODULE_67__["UiSwitchModule"],
                angular_file_uploader__WEBPACK_IMPORTED_MODULE_46__["AngularFileUploaderModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_34__["FileUploadModule"],
                angular_tabs_component__WEBPACK_IMPORTED_MODULE_24__["TabModule"],
                ng2_file_size__WEBPACK_IMPORTED_MODULE_2__["Ng2FileSizeModule"],
                angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["SocialLoginModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClientModule"],
                ngx_speech__WEBPACK_IMPORTED_MODULE_25__["SpeechModule"],
                angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_51__["AngularMultiSelectModule"],
                ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_13__["GooglePlaceModule"],
                ngx_popover__WEBPACK_IMPORTED_MODULE_45__["PopoverModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                angular2_notifications__WEBPACK_IMPORTED_MODULE_11__["SimpleNotificationsModule"].forRoot(),
                ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_35__["ShowHidePasswordModule"].forRoot(),
                angular_confirmation_popover__WEBPACK_IMPORTED_MODULE_32__["ConfirmationPopoverModule"].forRoot({
                    confirmButtonType: "danger" // set defaults here
                }),
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_26__["BsDatepickerModule"].forRoot(),
                ng_circle_progress__WEBPACK_IMPORTED_MODULE_28__["NgCircleProgressModule"].forRoot({
                    radius: 60,
                    space: -5,
                    outerStrokeWidth: 5,
                    outerStrokeColor: "#76C2AF",
                    innerStrokeColor: "#ffffff",
                    innerStrokeWidth: 5,
                    imageSrc: "assets/Profile (1).png",
                    imageHeight: 70,
                    imageWidth: 70,
                    showImage: true,
                    showBackground: false
                }),
                ngx_pagination__WEBPACK_IMPORTED_MODULE_29__["NgxPaginationModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__["BrowserAnimationsModule"],
                ngx_countdown__WEBPACK_IMPORTED_MODULE_31__["CountdownModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_15__["ToastrModule"].forRoot({
                    timeOut: 5000,
                    positionClass: "toast-bottom-right",
                    preventDuplicates: true
                }),
                ngx_toastr__WEBPACK_IMPORTED_MODULE_15__["ToastContainerModule"],
                angular_particle__WEBPACK_IMPORTED_MODULE_4__["ParticlesModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_39__["NgMultiSelectDropDownModule"],
                _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_38__["JwtModule"].forRoot({
                    config: {
                        tokenGetter: tokenGetter,
                        whitelistedDomains: ["localhost:4200"],
                        blacklistedRoutes: ["localhost:4200/auth/"]
                    }
                }),
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forRoot([
                    {
                        path: "",
                        component: _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"]
                    },
                    {
                        path: "login",
                        component: _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"]
                    },
                    {
                        path: "full_layout",
                        component: _layout_full_layout_full_layout_component__WEBPACK_IMPORTED_MODULE_56__["FullLayoutComponent"]
                    },
                    {
                        path: "footer",
                        component: _footer_footer_component__WEBPACK_IMPORTED_MODULE_70__["FooterComponent"]
                    },
                    {
                        path: "demo",
                        component: _Dashboard_demo_demo_component__WEBPACK_IMPORTED_MODULE_54__["DemoComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "newpassword",
                        component: _Dashboard_newpassword_newpassword_component__WEBPACK_IMPORTED_MODULE_63__["NewpasswordComponent"]
                    },
                    {
                        path: "result_test/:id",
                        component: _Dashboard_result_test_result_test_component__WEBPACK_IMPORTED_MODULE_58__["ResultTestComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "feedback/:id",
                        component: _Dashboard_feedbackform_feedbackform_component__WEBPACK_IMPORTED_MODULE_66__["FeedbackformComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "aboutus",
                        component: _Dashboard_aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_61__["AboutusComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "test/:id",
                        component: _Dashboard_onlinetest_onlinetest_component__WEBPACK_IMPORTED_MODULE_57__["OnlinetestComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "interview_details",
                        component: _Dashboard_interview_details_interview_details_component__WEBPACK_IMPORTED_MODULE_55__["InterviewDetailsComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "home",
                        component: _Dashboard_home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "job",
                        component: _Dashboard_job_offer_job_offer_component__WEBPACK_IMPORTED_MODULE_21__["JobOfferComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "professional",
                        component: _Dashboard_proffesional_profile_proffesional_profile_component__WEBPACK_IMPORTED_MODULE_18__["ProffesionalProfileComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "document",
                        component: _Dashboard_document_vault_document_vault_component__WEBPACK_IMPORTED_MODULE_19__["DocumentVaultComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "mentor",
                        component: _Dashboard_mentorship_mentorship_component__WEBPACK_IMPORTED_MODULE_23__["MentorshipComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "rating",
                        component: _Dashboard_rate_your_skill_rate_your_skill_component__WEBPACK_IMPORTED_MODULE_20__["RateYourSkillComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                        //canDeactivate:[CanDeactivateGuard]
                    },
                    {
                        path: "interview",
                        component: _Dashboard_interviews_interviews_component__WEBPACK_IMPORTED_MODULE_22__["InterviewsComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "forgot",
                        component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_41__["ForgotPasswordComponent"]
                    },
                    {
                        path: "update_profile",
                        component: _Dashboard_update_profile_update_profile_component__WEBPACK_IMPORTED_MODULE_52__["UpdateProfileComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "exam/:id",
                        component: _exam_exam_component__WEBPACK_IMPORTED_MODULE_42__["ExamComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "objective/:id",
                        component: _Dashboard_objectivetest_objectivetest_component__WEBPACK_IMPORTED_MODULE_68__["ObjectivetestComponent"],
                        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"]]
                    },
                    {
                        path: "skill-update/:id",
                        component: _Dashboard_skill_update_skill_update_component__WEBPACK_IMPORTED_MODULE_50__["SkillUpdateComponent"]
                    },
                    {
                        path: "job-detail",
                        component: _Dashboard_job_details_job_details_component__WEBPACK_IMPORTED_MODULE_65__["JobDetailsComponent"]
                    }
                ], { useHash: true })
            ],
            providers: [
                _services_auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"],
                _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_37__["AuthGuardService"],
                _services_talk_service__WEBPACK_IMPORTED_MODULE_27__["TalkService"],
                _services_deactivate_guard_service__WEBPACK_IMPORTED_MODULE_44__["CanDeactivateGuard"],
                _services_sharedata_service__WEBPACK_IMPORTED_MODULE_62__["SharedataService"],
                {
                    provide: angular_6_social_login__WEBPACK_IMPORTED_MODULE_36__["AuthServiceConfig"],
                    useFactory: getAuthServiceConfigs
                },
                { provide: "SPEECH_LANG", useValue: "en-US" }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/exam/exam.component.css":
/*!*****************************************!*\
  !*** ./src/app/exam/exam.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:hover {\r\n\r\n    cursor:pointer;\r\n    color: red;\r\n}\r\ntextarea.ng-invalid {\r\n    background-color: pink;\r\n}\r\n/* The container */\r\n.container {\r\n    display: block;\r\n    position: relative;\r\n    padding-left: 35px;\r\n    margin-bottom: 12px;\r\n    cursor: pointer;\r\n    font-size: 18px;;\r\n    -webkit-user-select: none;\r\n    -moz-user-select: none;\r\n    -ms-user-select: none;\r\n    user-select: none;\r\n    font-family: sans-serif;\r\n}\r\n/* Hide the browser's default radio button */\r\n.container input {\r\n    position: absolute;\r\n    opacity: 0;\r\n    cursor: pointer;\r\n}\r\n/* Create a custom radio button */\r\n.checkmark {\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    height: 25px;\r\n    width: 25px;\r\n    background-color: #eee;\r\n    border-radius: 50%;\r\n}\r\n/* On mouse-over, add a grey background color */\r\n.container:hover input ~ .checkmark {\r\n    background-color: #ccc;\r\n}\r\n/* When the radio button is checked, add a blue background */\r\n.container input:checked ~ .checkmark {\r\n    background-color: #FF4F33;\r\n}\r\n/* Create the indicator (the dot/circle - hidden when not checked) */\r\n.checkmark:after {\r\n    content: \"\";\r\n    position: absolute;\r\n    display: none;\r\n}\r\n/* Show the indicator (dot/circle) when checked */\r\n.container input:checked ~ .checkmark:after {\r\n    display: block;\r\n}\r\n/* Style the indicator (dot/circle) */\r\n.container .checkmark:after {\r\n \ttop: 9px;\r\n\tleft: 9px;\r\n\twidth: 8px;\r\n\theight: 8px;\r\n\tborder-radius: 50%;\r\n\tbackground: white;\r\n}\r\n.btnsubmit{\r\n    background-color: #FF4F33;width: 179px;font-size: 18px;\r\n              height: 50px;color: #ffffff;border-radius: 25px;\r\n}"

/***/ }),

/***/ "./src/app/exam/exam.component.html":
/*!******************************************!*\
  !*** ./src/app/exam/exam.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-full-layout></app-full-layout> -->\r\n<div>\r\n   <!-- oncontextmenu=\"return false\"\r\n  onkeypress=\"return disableCtrlKeyCombination(event);\"\r\n  onkeydown=\"return disableCtrlKeyCombination(event);\"> -->\r\n\r\n  <div class=\"container\" style=\"margin-top: 50px\">\r\n    <div class=\"row\">\r\n      <div id=\"test_cancel\"\r\n        class=\"col-md-6\"\r\n        value=\"submit\"\r\n        (click)=\"cancel()\"\r\n        style=\"text-align: center; color: #FF4F33;font-size: 18px\"\r\n      >\r\n        Cancel Test\r\n      </div>\r\n      <div\r\n        class=\"col-md-6\"\r\n        style=\"text-align: center; color: #FF4F33;font-size: 18px\"\r\n      >\r\n        <img\r\n          src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/if_1-09_511570.jpg\"\r\n          style=\"width: 50px;height: 40px;\"\r\n        />\r\n        <!-- <countdown id=\"countdown\" [config]=\"{leftTime: 100 * 27, notify: [1,300]}\" *ngIf=\"counterTime\" (notify)=\"onNotify($event)\">$!m!:$!s!</countdown>  -->\r\n        <p>{{ counterTime }}</p>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <div class=\"card bg-light\">\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-12\">\r\n              <p style=\"text-align: center;\">Question {{QuestionCount}} / 25 </p>\r\n            </div>\r\n            </div>\r\n            <div class=\"row\" style=\"margin-top: 40px;\">\r\n              <div\r\n                class=\"col-md-12\"\r\n                style=\"background-color:lightblue;color:black;padding:20px;\"\r\n                *ngIf=\"!responseNextData.question_text\"\r\n              >\r\n                <p>Question {{QuestionCount}} : {{ speechresponseNext.question_text }}</p>\r\n              </div>\r\n              <div\r\n                class=\"col-md-12\"\r\n                style=\"background-color:lightblue;color:black;padding:20px;\"\r\n                *ngIf=\"responseNextData.question_text\"\r\n              >\r\n                <p>Question {{QuestionCount}} : {{ responseNextData.question_text }}</p>\r\n              </div>\r\n            </div>\r\n\r\n           <form style=\"margin-top:50px;\">\r\n\r\n\r\n              <div class=\"text-center\" *ngIf=\"visible\">\r\n\r\n                <textarea [(ngModel)]=\"msg\" [ngModelOptions]=\"{standalone: true}\" appBackcopypaste [ngStyle]=\"{'width.%': 60}\" rows=\"6\"\r\n                  maxlength=\"400\" placeholder=\"Write here?\"></textarea>\r\n\r\n              </div>\r\n\r\n\r\n              <div class=\"text-center\" *ngIf=\"!visible\">\r\n\r\n                <textarea [(ngModel)]=\"msg\" [ngModelOptions]=\"{standalone: true}\" appBackcopypaste disabled [ngStyle]=\"{'width.%': 60}\"\r\n                  rows=\"6\" placeholder=\"Speak here?\"></textarea>\r\n                <div class=\"pull-right\"></div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-md-12 \">\r\n                  <div class=\"col-md-6\">\r\n                    <div class=\"text-center\">\r\n                      <button id=\"btnSpeak\" [className]=\"started ? 'btn btn-danger' : 'btn btn-success'\" (click)=\"toggleVoiceRecognition($event.target.value)\"\r\n                        value=\"Speak\">{{started ? 'Stop' : 'Speak' }} <i class=\"fa fa-microphone\"></i></button>\r\n\r\n                      <div style=\"margin-left:50%;margin-top:-32px;\">\r\n                      <div *ngIf=\"keystart==false;else falseBlock\">\r\n                        <button class=\"btn btn-success\" (click)=\"toggleVoiceRecognition2($event.target.value)\" value=\"Keyboard\" id=\"btnKeyboard\">Keyboard<i\r\n                          class=\"fa fa-keyboard-o\" style=\"margin-left: 10px;\"></i></button>\r\n                          </div>\r\n\r\n                      <ng-template #falseBlock>\r\n\r\n                        <button class=\"btn btn-danger\" (click)=\"toggleVoiceRecognition2($event.target.value)\" value=\"Stop\">Stop<i\r\n                            class=\"fa fa-microphone\"></i></button>\r\n\r\n                      </ng-template>\r\n\r\n                    </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </form>\r\n\r\n            <div class=\"row\" style=\"margin-top: 20px;\">\r\n              <div class=\"col-md-12 offset-4\">\r\n                <button id=\"btnSubmit\"\r\n                  class=\"btn btnsubmit\"\r\n                  style=\"margin-left:7%;\"\r\n                  value=\"submit\"\r\n                  (click)=\"submitspeech($event.target.value)\"\r\n                >\r\n                  Submit\r\n                </button>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" style=\"margin-top:10px;\">\r\n              <div class=\"col-md-12 offset-5\">\r\n                <a id=\"skipTheQuestion\"\r\n                  style=\"color:#FF4F33;font-size: 16px;\"\r\n                  value=\"skip\"\r\n                  (click)=\"skipspeech($event.target.value)\"\r\n                  >Skip the question</a\r\n                >\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <br /><br />\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n<app-footer></app-footer>\r\n  <!-- <footer\r\n    class=\"app-footer\"\r\n    style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\"\r\n  >\r\n    &copy;\r\n    <a  style=\"color:black;font-size: 16px;\"\r\n      >Minemark Solutions Private Limited</a\r\n    >\r\n    2019\r\n  </footer> -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/exam/exam.component.ts":
/*!****************************************!*\
  !*** ./src/app/exam/exam.component.ts ***!
  \****************************************/
/*! exports provided: ExamComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExamComponent", function() { return ExamComponent; });
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_talk_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/talk.service */ "./src/app/services/talk.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ExamComponent = /** @class */ (function () {
    function ExamComponent(http, auth, activatedRoute, speech, route, toastr, share) {
        this.http = http;
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.speech = speech;
        this.route = route;
        this.toastr = toastr;
        this.share = share;
        this.started = false;
        this.QuestionCount = 1;
        this.question = { questions: "", question_id: "", skill_id: "" };
        this.questions = "";
        this.candi_skillset = localStorage.getItem("candi_skillset");
        this.given_answer = "";
        this.count = 0;
        this.speechresponseNext = [];
        this.datesystem = new Date();
        this.myFormattedDate = new _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]("en-US").transform(this.datesystem, "dd/MM/yyyy");
        this.testinfo = [];
        this.date = new Date().getMinutes();
        this.skipanswer = 1;
        this.visible = false;
        /* clock variables */
        this.timeLeft = 100 * 27;
        this.windowObjectReference = null;
        this.getData = [];
        this.keystart = false;
        this.responseEvalData = [];
        this.responseNextData = [];
        this.datainfo = this.share.getData();
        this.skillarray = [];
        this.skill = [];
        this.test = [];
    }
    ExamComponent.prototype.ngOnInit = function () {
        var _this = this;
        window.onload = function () {
            document.onkeydown = function (e) {
                if (e.which == 17)
                    isCtrl = true;
                if ((e.which == 85 ||
                    e.which == 117 ||
                    e.which == 65 ||
                    e.which == 97 ||
                    e.which == 67 ||
                    e.which == 99) &&
                    isCtrl == true) {
                    // alert(‘Keyboard shortcuts are cool!’);
                    return false;
                }
                return (e.which || e.keyCode) != 116;
            };
            //////////F12 disable code////////////////////////
            document.onkeypress = function (e) {
                return (e.which || e.keyCode) != 123;
            };
            var isNS = navigator.appName == "Netscape" ? 1 : 0;
            // if (navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);
            function mischandler() {
                return false;
            }
            function mousehandler(e) {
                var myevent = isNS ? e : event;
                var eventbutton = isNS ? myevent.which : myevent.button;
                if (eventbutton == 2 || eventbutton == 3)
                    return false;
            }
            document.oncontextmenu = mischandler;
            document.onmousedown = mousehandler;
            document.onmouseup = mousehandler;
            var isCtrl = false;
            document.onkeyup = function (e) {
                if (e.which == 17)
                    isCtrl = false;
            };
        };
        history.pushState(null, null, document.URL);
        window.addEventListener("popstate", function () {
            history.pushState(null, null, document.URL);
        });
        this.startTimer();
        if (this.datainfo) {
            this.datainfo;
            this.skillarray = this.datainfo;
            this.skill = this.skillarray[0].question_set;
            this.test = this.skillarray[0].per_question_sec;
        }
        else {
        }
        this.speech.started.subscribe(function (started) { return (_this.started = started); });
        this.activatedRoute.params.subscribe(function (option) {
            _this.candi_skillset = option["id"];
            _this.auth
                .postspeech({
                Question_Set_id: _this.skillarray[0].question_set,
                skill_id: _this.candi_skillset
            })
                .then(function (data) {
                _this.speechresponseNext = data.json();
            });
        });
    };
    ExamComponent.prototype.startTimer = function () {
        var _this = this;
        if (!this.interval) {
            this.interval = setInterval(function () {
                if (_this.timeLeft === 0) {
                    var submitData = {
                        question_content_id: _this.responseEvalData.Next_Question_id,
                        question_bank_id: _this.QuestionCount,
                        marks: 0,
                        topic_id: _this.responseNextData.topic_id,
                        given_answer: " ",
                        skill_id: _this.candi_skillset,
                        date: _this.myFormattedDate,
                        time_taken: _this.counterTime,
                        status: 3,
                        window_status: 0,
                        edit_count: 0,
                        speak_count: 0,
                        extra_time: 0,
                        skip: 1
                    };
                    _this.auth
                        .postDetail("/mash_speech_assessment_answer", _this.token, submitData)
                        .then(function (data) {
                        _this.valueData = data.json().question;
                        _this.msg = "";
                        _this.route.navigateByUrl("/feedback/" + _this.candi_skillset);
                    });
                }
                if (_this.timeLeft >= 0) {
                    _this.startTimerHHMMSS(_this.timeLeft);
                    _this.timeLeft--;
                }
                else {
                    _this.timeLeft = 60;
                }
            }, 1000);
        }
    };
    ExamComponent.prototype.startTimerHHMMSS = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor((d % 3600) / 60);
        var s = Math.floor((d % 3600) % 60);
        var hDisplay = h > 0 ? h + (h === 1 ? "" : "") : "";
        var mDisplay = m > 0 ? m + (m === 1 ? "" : "") : "";
        var sDisplay = s > 0 ? s + (s === 1 ? "" : "") : "";
        this.counterTime =
            this.GetPaddedIntString(hDisplay, 2) +
                ":" +
                this.GetPaddedIntString(mDisplay, 2) +
                ":" +
                this.GetPaddedIntString(sDisplay, 2);
        return this.counterTime;
    };
    ExamComponent.prototype.pauseTimer = function () {
        clearInterval(this.interval);
    };
    ExamComponent.prototype.GetPaddedIntString = function (n, numDigits) {
        var nPadded = n;
        for (; nPadded.length < numDigits;) {
            nPadded = "0" + nPadded;
        }
        return nPadded;
    };
    ExamComponent.prototype.submitspeech = function (value) {
        var _this = this;
        this.counterTime;
        if (this.msg === "") {
            this.toastr.info("Please give the answer");
        }
        if (this.msg != "") {
            this.submitButton = value;
            if (this.QuestionCount > 25) {
                this.route.navigateByUrl("/feedback/" + this.candi_skillset);
            }
            if (this.QuestionCount === 1) {
                this.auth
                    .postspeechEvaluate({
                    question_id: this.speechresponseNext.question_id,
                    skill_id: this.candi_skillset,
                    Question_Set_id: this.skillarray[0].question_set,
                    Answer: this.msg
                })
                    .then(function (data) {
                    _this.responseEvalData = data.json();
                    _this.token = localStorage.getItem("token");
                    _this.auth
                        .postDetail("/mash_speech_assessment_answer", _this.token, {
                        question_content_id: _this.speechresponseNext.question_id,
                        question_bank_id: _this.QuestionCount,
                        marks: _this.responseEvalData.Marks,
                        topic_id: _this.speechresponseNext.topic_id,
                        given_answer: _this.msg,
                        skill_id: _this.candi_skillset,
                        date: _this.myFormattedDate,
                        time_taken: _this.counterTime,
                        status: 1,
                        window_status: 0,
                        edit_count: 0,
                        speak_count: 0,
                        extra_time: 0,
                        skip: 0
                    })
                        .then(function (data) {
                        _this.getData = data.json();
                        _this.toastr.success(data.json().message);
                    });
                    _this.auth
                        .postspeechNext({
                        Next_question_id: _this.responseEvalData.Next_Question_id,
                        Next_question_Set_id: _this.responseEvalData
                            .next_question_Set_id,
                        Next_skill_id: _this.responseEvalData.next_question_Skill_id
                    })
                        .then(function (data) {
                        _this.responseNextData = data.json();
                    });
                });
                ++this.QuestionCount;
            }
            else {
                this.auth
                    .postspeechEvaluate({
                    question_id: this.responseEvalData.Next_Question_id,
                    skill_id: this.candi_skillset,
                    Question_Set_id: this.skillarray[0].question_set,
                    Answer: this.msg
                })
                    .then(function (data) {
                    _this.responseEvalData = data.json();
                    _this.token = localStorage.getItem("token");
                    _this.auth
                        .postDetail("/mash_speech_assessment_answer", _this.token, {
                        question_content_id: _this.responseEvalData.Next_Question_id,
                        question_bank_id: _this.QuestionCount,
                        marks: _this.responseEvalData.Marks,
                        topic_id: _this.responseNextData.topic_id,
                        given_answer: _this.msg,
                        skill_id: _this.candi_skillset,
                        date: _this.myFormattedDate,
                        time_taken: _this.counterTime,
                        status: 1,
                        window_status: 0,
                        edit_count: 0,
                        speak_count: 0,
                        extra_time: 0,
                        skip: 0
                    })
                        .then(function (data) {
                        _this.getData = data.json();
                        _this.toastr.success(data.json().message);
                    });
                    _this.auth
                        .postspeechNext({
                        Next_question_id: _this.responseEvalData.Next_Question_id,
                        Next_question_Set_id: _this.responseEvalData
                            .next_question_Set_id,
                        Next_skill_id: _this.responseEvalData.next_question_Skill_id
                    })
                        .then(function (data) {
                        _this.responseNextData = data.json();
                    });
                });
                ++this.QuestionCount;
                if (this.QuestionCount > 25) {
                    this.route.navigateByUrl("/feedback/" + this.candi_skillset);
                }
            }
            this.msg = "";
        }
    };
    ExamComponent.prototype.toggleVoiceRecognition = function (data) {
        var _this = this;
        if (data === "Speak") {
            if (this.started) {
                this.visible = false;
                this.speech.stop();
                this.subscription.unsubscribe();
            }
            else {
                this.visible = false;
                this.speech.start();
                this.subscription = this.speech.message.subscribe(function (data) {
                    _this.msg += data.message + " ";
                });
            }
        }
    };
    ExamComponent.prototype.toggleVoiceRecognition2 = function (data) {
        var _this = this;
        if (data === "Keyboard") {
            this.keystart = true;
            this.visible = true;
        }
        if (data === "Stop") {
            this.visible = false;
            this.keystart = false;
            this.subscription = this.speech.message.subscribe(function (data) {
                _this.msg += data.message + " ";
            });
        }
    };
    ExamComponent.prototype.skipspeech = function (value) {
        var _this = this;
        this.skipButton = value;
        if (this.QuestionCount > 25) {
            this.route.navigateByUrl("/feedback/" + this.candi_skillset);
        }
        var r = confirm("Are you sure you want too skip the Question?");
        if (r == true) {
            if (this.QuestionCount === 1) {
                this.auth
                    .postspeechEvaluate({
                    question_id: this.speechresponseNext.question_id,
                    skill_id: this.candi_skillset,
                    Question_Set_id: this.skillarray[0].question_set,
                    Answer: ""
                })
                    .then(function (data) {
                    _this.responseEvalData = data.json();
                    _this.token = localStorage.getItem("token");
                    var submitData = {
                        question_content_id: _this.speechresponseNext.question_id,
                        question_bank_id: _this.QuestionCount,
                        marks: _this.responseEvalData.Marks,
                        topic_id: _this.speechresponseNext.topic_id,
                        given_answer: _this.msg,
                        skill_id: _this.candi_skillset,
                        date: _this.myFormattedDate,
                        time_taken: _this.counterTime,
                        status: 1,
                        window_status: 0,
                        edit_count: 0,
                        speak_count: 0,
                        extra_time: 0,
                        skip: 1
                    };
                    _this.auth
                        .postDetail("/mash_speech_assessment_answer", _this.token, submitData)
                        .then(function (datas) {
                        _this.valueData = datas.json();
                        _this.msg = "";
                        _this.toastr.info("skipped!");
                    });
                    _this.auth
                        .postspeechNext({
                        Next_question_id: _this.responseEvalData.Next_Question_id,
                        Next_question_Set_id: _this.responseEvalData
                            .next_question_Set_id,
                        Next_skill_id: _this.responseEvalData.next_question_Skill_id
                    })
                        .then(function (data) {
                        _this.responseNextData = data.json();
                    });
                });
                ++this.QuestionCount;
            }
            else {
                this.auth
                    .postspeechEvaluate({
                    question_id: this.responseEvalData.Next_Question_id,
                    skill_id: this.candi_skillset,
                    Question_Set_id: this.skillarray[0].question_set,
                    Answer: " "
                })
                    .then(function (data) {
                    _this.responseEvalData = data.json();
                    _this.token = localStorage.getItem("token");
                    _this.auth
                        .postDetail("/mash_speech_assessment_answer", _this.token, {
                        question_content_id: _this.responseEvalData.Next_Question_id,
                        question_bank_id: _this.QuestionCount,
                        marks: _this.responseEvalData.Marks,
                        topic_id: _this.responseNextData.topic_id,
                        given_answer: _this.msg,
                        skill_id: _this.candi_skillset,
                        date: _this.myFormattedDate,
                        time_taken: _this.counterTime,
                        status: 1,
                        window_status: 0,
                        edit_count: 0,
                        speak_count: 0,
                        extra_time: 0,
                        skip: 1
                    })
                        .then(function (data) {
                        _this.getData = data.json();
                        _this.msg = "";
                        _this.toastr.info("skipped!");
                    });
                    _this.auth
                        .postspeechNext({
                        Next_question_id: _this.responseEvalData.Next_Question_id,
                        Next_question_Set_id: _this.responseEvalData
                            .next_question_Set_id,
                        Next_skill_id: _this.responseEvalData.next_question_Skill_id
                    })
                        .then(function (data) {
                        _this.responseNextData = data.json();
                    });
                });
                ++this.QuestionCount;
                if (this.QuestionCount > 25) {
                    this.route.navigateByUrl("/feedback/" + this.candi_skillset);
                }
            }
        }
        else {
        }
    };
    ExamComponent.prototype.cancel = function () {
        var _this = this;
        var submitData = {
            question_content_id: this.speechresponseNext.question_id,
            question_bank_id: this.QuestionCount,
            marks: 0,
            topic_id: this.speechresponseNext.topic_id,
            given_answer: "",
            skill_id: this.candi_skillset,
            date: this.myFormattedDate,
            time_taken: this.counterTime,
            status: 2,
            window_status: 0,
            edit_count: 0,
            speak_count: 0,
            extra_time: 0,
            skip: 1
        };
        var submit = {
            question_content_id: this.responseEvalData.Next_Question_id,
            question_bank_id: this.QuestionCount,
            marks: this.responseEvalData.Marks,
            topic_id: this.responseNextData.topic_id,
            given_answer: this.msg,
            skill_id: this.candi_skillset,
            date: this.myFormattedDate,
            time_taken: this.counterTime,
            status: 2,
            window_status: 0,
            edit_count: 0,
            speak_count: 0,
            extra_time: 0,
            skip: 1
        };
        this.token = localStorage.getItem("token");
        if (this.QuestionCount === 1) {
            this.auth
                .postDetail("/mash_speech_assessment_answer", this.token, submitData)
                .then(function (data) {
                _this.valueData = data.json().question;
                _this.msg = "";
                _this.route.navigateByUrl("/feedback/" + _this.candi_skillset);
            });
        }
        else {
            this.auth
                .postDetail("/mash_speech_assessment_answer", this.token, submit)
                .then(function (data) {
                _this.valueData = data.json().question;
                _this.msg = "";
                _this.route.navigateByUrl("/feedback/" + _this.candi_skillset);
            });
        }
    };
    ExamComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-exam",
            template: __webpack_require__(/*! ./exam.component.html */ "./src/app/exam/exam.component.html"),
            styles: [__webpack_require__(/*! ./exam.component.css */ "./src/app/exam/exam.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_6__["Http"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_talk_service__WEBPACK_IMPORTED_MODULE_2__["TalkService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"],
            _services_sharedata_service__WEBPACK_IMPORTED_MODULE_0__["SharedataService"]])
    ], ExamComponent);
    return ExamComponent;
}());



/***/ }),

/***/ "./src/app/exam/question-answer/question-answer.component.css":
/*!********************************************************************!*\
  !*** ./src/app/exam/question-answer/question-answer.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/exam/question-answer/question-answer.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/exam/question-answer/question-answer.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center\" style=\"background-color: #ff6600\">\r\n\r\n    <img class=\"img\" src=\"../../assets/light_mm_logo.png\" routerLink=\"/home\">\r\n    </div>\r\n    <hr>\r\n\r\n<div class=\"container\">\r\n<div *ngFor=\"let menus of menu\">{{menus.name}}<br>\r\n{{menus.value}}\r\n</div>\r\n</div>\r\n<circle-progress\r\n  [percent]=\"85\"\r\n  [radius]=\"100\"\r\n  [outerStrokeWidth]=\"16\"\r\n  [innerStrokeWidth]=\"8\"\r\n  [outerStrokeColor]=\"'#78C000'\"\r\n  [innerStrokeColor]=\"'#C7E596'\"\r\n  [animation]=\"true\"\r\n  [animationDuration]=\"300\"\r\n></circle-progress> "

/***/ }),

/***/ "./src/app/exam/question-answer/question-answer.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/exam/question-answer/question-answer.component.ts ***!
  \*******************************************************************/
/*! exports provided: QuestionAnswerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionAnswerComponent", function() { return QuestionAnswerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var QuestionAnswerComponent = /** @class */ (function () {
    function QuestionAnswerComponent() {
        this.menu = [
            { name: "What is java and its benefits?", value: '1' },
            { name: "What is jvm and how it works?", value: '2' },
            { name: "What is Polymorphism.", value: '3' },
            { name: "What is Inheritance", value: '4' },
            { name: "What is thread in java.", value: '5' },
            { name: "What is String Buffer", value: '6' },
            { name: "What are the benefits of OOPs", value: '7' },
            { name: "What is byte code.", value: '8' },
        ];
    }
    QuestionAnswerComponent.prototype.ngOnInit = function () {
    };
    QuestionAnswerComponent.prototype.canDeactivate = function () {
        console.log('works');
    };
    QuestionAnswerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-question-answer',
            template: __webpack_require__(/*! ./question-answer.component.html */ "./src/app/exam/question-answer/question-answer.component.html"),
            styles: [__webpack_require__(/*! ./question-answer.component.css */ "./src/app/exam/question-answer/question-answer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], QuestionAnswerComponent);
    return QuestionAnswerComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".copyfooter{\r\n    margin-top:40px;\r\n    font-size:14px;\r\n    color:black;\r\n    font-family: sans-serif;\r\n    text-align: center;\r\n}"

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"app-footer copyfooter\">\n    <span > &copy; Minemark Solutions Private Limited 2019</span> \n</footer>"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.css":
/*!***************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img{\r\n    margin-left: 43%;\r\n    margin-top: 2%;\r\n    margin-bottom:2%;\r\n}\r\ninput.ng-invalid.ng-touched{\r\n    border: 1px solid red;\r\n}\r\n.help-block\r\n{\r\n    color: red;\r\n}"

/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.html":
/*!****************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\r\n\r\n<div style=\"background-color: #ff6600;height: 50px;\">\r\n    <div style=\"margin-left:0px\" ><img style=\"height:30px;margin-top: 1% !important;\" class=\"img\" src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/light_mm_logo.png\"\r\n            routerLink=\"/home\">\r\n    </div>\r\n    <div>\r\n        <h3 style=\"color:white;margin-top:-60px;margin-left: 800px\">Candidate Portal</h3>\r\n    </div>\r\n</div>\r\n<form [formGroup]=\"myform\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n                <h2> Forgot Password </h2>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                &nbsp;\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <!-- <h2>First, let's find your account</h2>-->\r\n                    <h4>Please enter email address</h4>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-8\">\r\n                            <!-- <span>Email </span> -->\r\n                            <label for=\"email\" class=\"control-label\">Email</label>\r\n                            <span style=\"color: red\">*</span><br>\r\n                            <input type=\"text\" placeholder=\"Enter Email Address \" class=\"form-control\" [(ngModel)]=\"forgotPassword.to\"\r\n                                formControlName=\"email\">\r\n                            <span class=\"help-block\" *ngIf=\"myform.get('email').touched && myform.get('email').invalid\">Email\r\n                                is Required!</span>\r\n                        </div>\r\n                        <div class=\"col-md-4\" style=\"margin-top:40px;\">\r\n                            <label><a class=\"pointer-cursor\" style=\"color:#F24540\" (click)=\"otp()\">click here</a> get\r\n                                OTP</label>\r\n                        </div>\r\n                    </div>\r\n\r\n\r\n                    <!-- <button class=\"btn btn-default\">Cancel</button> -->\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-8\">\r\n                        <div class=\"form-group\">\r\n                            <label for=\"otp\" class=\"control-label\">Enter OTP</label>\r\n                            <input type=\"number\" class=\"form-control\" id=\"otp\" placeholder=\"Enter 4 digit OTP\"\r\n                                [(ngModel)]=\"forgotPassword.otp\" name=\"otp\" formControlName=\"otp\">\r\n                            <span class=\"help-block\" *ngIf=\"myform.get('otp').touched && myform.get('otp').invalid\">Enter\r\n                                OTP\r\n                                is Required!</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-12\">\r\n                        <button class=\"btn btn-primary\" [disabled]=\"!myform.valid\" style=\"width:179px;height:40px;margin-left:13%;background-color: #F24540;border-radius: 25px;\"\r\n                            (click)=\"proceed()\">Proceed</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                &nbsp;\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n\r\n\r\n<footer class=\"app-footer\" style=\"margin-top:40px;font-size:16px;color:black;font-family: sans-serif;text-align: center;\">\r\n        &copy; <a style=\"color:black\">Minemark Solutions Private Limited</a> 2019 </footer>\r\n"

/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.ts ***!
  \**************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _services_sharedata_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/sharedata.service */ "./src/app/services/sharedata.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(auth, toastr, share, router) {
        this.auth = auth;
        this.toastr = toastr;
        this.share = share;
        this.router = router;
        this.forgotPassword = { to: "", otp: "" };
        this.token = localStorage.getItem('token');
        this.remove = 'otp is not correct';
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
        this.myform = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
            otp: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[0-9]*$"),]),
        });
    };
    ForgotPasswordComponent.prototype.proceed = function () {
        var _this = this;
        this.auth.postDetail('/password_verify', this.token, { candi_email: this.forgotPassword.to, email_otp: this.forgotPassword.otp })
            .then(function (data) {
            _this.otpVerify = data.json().message;
            if (_this.otpVerify == 'OTP matched successfuly') {
                _this.share.setData(data);
                _this.router.navigateByUrl('/newpassword');
            }
            else {
                _this.toastr.remove(_this.remove);
            }
        });
    };
    ForgotPasswordComponent.prototype.otp = function () {
        var _this = this;
        this.auth.postDetail('/forgot_password', " ", { candi_email: this.forgotPassword.to }).then(function (data) {
            _this.accesstoken = data.json().token;
            localStorage.setItem("token", _this.accesstoken);
            _this.otpData = data.json().message;
            _this.toastr.success(_this.otpData);
        }).catch(function (err) {
            console.log(err);
        });
    };
    ForgotPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-forgot-password',
            template: __webpack_require__(/*! ./forgot-password.component.html */ "./src/app/forgot-password/forgot-password.component.html"),
            styles: [__webpack_require__(/*! ./forgot-password.component.css */ "./src/app/forgot-password/forgot-password.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _services_sharedata_service__WEBPACK_IMPORTED_MODULE_4__["SharedataService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/layout/full-layout/full-layout.component.css":
/*!**************************************************************!*\
  !*** ./src/app/layout/full-layout/full-layout.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media(min-width:768px) {\r\n  body {\r\n      margin-top: 50px;\r\n  }\r\n  \r\n}\r\n\r\n#wrapper {\r\n  padding-left: 0;    \r\n}\r\n\r\n#page-wrapper {\r\n  width: 100%;        \r\n  padding: 0;\r\n  background-color: #fff;\r\n}\r\n\r\n@media(min-width:768px) {\r\n  #wrapper {\r\n      padding-left: 170px;\r\n  }\r\n\r\n  #page-wrapper {\r\n      padding: 22px 10px;\r\n  }\r\n}\r\n\r\n/* Top Navigation */\r\n\r\n.top-nav {\r\n  padding: 0 10px;\r\n}\r\n\r\n.top-nav>li {\r\n  display: inline-block;\r\n  float: left;\r\n}\r\n\r\n.top-nav>li>a {\r\n  padding-top: 10px;\r\n  padding-bottom: 10px;\r\n  line-height: 10px;\r\n  color: #fff;\r\n}\r\n\r\n.top-nav>li>a:hover,\r\n.top-nav>li>a:focus,\r\n.top-nav>.open>a,\r\n.top-nav>.open>a:hover,\r\n.top-nav>.open>a:focus {\r\n  color: #fff;\r\n  background-color: #FF3333;\r\n}\r\n\r\n.active{\r\n  background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\r\n  color: #fff;\r\n}\r\n\r\n.top-nav>.open>.dropdown-menu {\r\n  float: left;\r\n  position: absolute;\r\n  margin-top: 0;\r\n  /*border: 1px solid rgba(0,0,0,.15);*/\r\n  border-top-left-radius: 0;\r\n  border-top-right-radius: 0;\r\n  background-color: #fff;\r\n  box-shadow: 0 6px 12px rgba(0,0,0,.175);\r\n}\r\n\r\n.top-nav>.open>.dropdown-menu>li>a {\r\n  white-space: normal;\r\n}\r\n\r\n/* Side Navigation */\r\n\r\n@media only screen and (min-width:340px) and (max-width:786px){\r\n  .side-nav {\r\n      position: fixed;\r\n      top: 50px;\r\n      left: 225px;\r\n      width: 160px;\r\n      margin-left: -225px;\r\n      border: 1px solid #FF6600;\r\n      border-radius: 0;\r\n      border-top: 1px #FF6600 solid; \r\n      overflow-y: auto;\r\n      background-color: #fff;\r\n      /*background-color: #5A6B7D;*/\r\n      bottom: 0;\r\n      overflow-x: hidden;\r\n      padding-bottom: 40px;\r\n      box-shadow: 0 2px 15px 6px #CCCCCC;\r\n  }\r\n\r\n  .side-nav>li>a {\r\n      width: 160px;\r\n      border-bottom: 1px rgba(0,0,0,.3) solid;\r\n      \r\n  }\r\n\r\n  .side-nav li a:hover,\r\n  .side-nav li a:focus {\r\n      outline: none;\r\n      background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\r\n    \r\n  }\r\n}\r\n\r\n.side-nav {\r\n  position: fixed;\r\n  top: 48px;\r\n  left: 225px;\r\n  width: 160px;\r\n  margin-left: -225px;\r\n  border: 1px solid #FF6600;\r\n  border-radius: 0;\r\n  border-top: 1px #FF6600 solid; \r\n  overflow-y: auto;\r\n  background-color: #fff;\r\n  /*background-color: #5A6B7D;*/\r\n  bottom: 0;\r\n  overflow-x: hidden;\r\n  padding-bottom: 40px;\r\n  \r\n}\r\n\r\n.side-nav>li>a {\r\n  width: 163px;\r\n  border-bottom: 1px rgba(0,0,0,.3) solid;\r\n  color: #111;\r\n  font-size: 16px;\r\n}\r\n\r\n.side-nav li a:hover,\r\n.side-nav li a:focus {\r\n  outline: none;\r\n  background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\r\ncolor: #fff;\r\n}\r\n\r\n.side-nav>li>ul {\r\n  padding: 0;\r\n  border-bottom: 1px rgba(0,0,0,.3) solid;\r\n  background-color: #ffffff;\r\n}\r\n\r\n.side-nav>li>ul>li>a {\r\n  display: block;\r\n  padding: 10px 15px 10px 38px;\r\n  text-decoration: none;\r\n  /*color: #999;*/\r\n  \r\n}\r\n\r\n.side-nav>li>ul>li>a:hover {\r\n  color: #fff;\r\n}\r\n\r\n.navbar .nav > li > a > .label {\r\nborder-radius: 50%;\r\nposition: absolute;\r\ntop: 10px;\r\nright: -4px;\r\nfont-size: 24px;\r\nfont-weight: normal;\r\nmin-width: 17px;\r\nmin-height: 17px;\r\nline-height: 1.0em;\r\ntext-align: center;\r\npadding: 2px;\r\n}\r\n\r\n.navbar .nav > li > a:hover > .label {\r\ntop: 5px;\r\n}\r\n\r\n.navbar-inverse {\r\n  background: linear-gradient(90deg, #FF6633 0%, #FF3333 100%);\r\n border-color: #fff;\r\n  height: 50px;;\r\n}\r\n\r\n.navbar-brand {\r\n  padding: 2px 10px;\r\n}\r\n\r\n.navbar-toggle {\r\n  position: relative;\r\n  float: right;\r\n  padding: 9px 10px;\r\n  margin-top: 8px;\r\n  margin-right: 70px;\r\n  margin-bottom: 8px;\r\n  background-color: transparent;\r\n  background-image: none;\r\n  border: 1px solid transparent;\r\n  border-radius: 4px;\r\n}\r\n\r\n.navbar-nav>.notifications-menu>.dropdown-menu, .navbar-nav>.messages-menu>.dropdown-menu, .navbar-nav>.tasks-menu>.dropdown-menu {\r\n  width: 280px;\r\n  padding: 0 0 0 0;\r\n  margin: 0;\r\n  top: 100%;\r\n}\r\n\r\n.navbar-nav>.messages-menu>.dropdown-menu li .menu>li>a>div>img {\r\n  margin: auto 10px auto auto;\r\n  width: 40px;\r\n  height: 40px;\r\n}\r\n\r\n.navbar-nav>.messages-menu>.dropdown-menu li .menu>li>a ,.navbar-nav>.notifications-menu>.dropdown-menu li .menu>li>a{\r\n  margin: 0;\r\n  padding: 10px 10px;\r\n      display: block;\r\n  white-space: nowrap;\r\n  border-bottom: 1px solid #f4f4f4;\r\n}\r\n\r\n.navbar-nav>.messages-menu>.dropdown-menu li .menu>li>a>h4 {\r\n  padding: 0;\r\n  margin: 0 0 0 45px;\r\n  color: #333;\r\n  font-size: 15px;\r\n  position: relative;\r\n}\r\n\r\n.navbar-nav>.messages-menu>.dropdown-menu li .menu>li>a>p {\r\n  margin: 0 0 0 45px;\r\n  font-size: 12px;\r\n  color: #888888;\r\n}\r\n\r\n.notifycount{\r\n  background-color: #00BCD4;\r\n}"

/***/ }),

/***/ "./src/app/layout/full-layout/full-layout.component.html":
/*!***************************************************************!*\
  !*** ./src/app/layout/full-layout/full-layout.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div id=\"throbber\" style=\"display:none; min-height:120px;\"></div>\r\n<div id=\"noty-holder\"></div>\r\n<div id=\"wrapper\">\r\n  <!-- Navigation -->\r\n  <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">\r\n    <!-- Brand and toggle get grouped for better mobile display -->\r\n\r\n    <div class=\"navbar-header\">\r\n\r\n      <a class=\"navbar-brand\">\r\n        <img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/MineMark+Logo-Orange.svg\" alt=\"LOGO\">\r\n        <h3 style=\"margin-left:50px;margin-top:-20px;color:white;font-family: sans-serif;font-weight: bold\">Candidate\r\n          Portal</h3>\r\n        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n\r\n      </a>\r\n      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">\r\n        <span class=\"sr-only\">Toggle navigation</span>\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n      </button>\r\n    </div>\r\n    <!-- Top Menu Items -->\r\n    <ul class=\"nav navbar-right top-nav\">\r\n      <li class=\"dropdown\">\r\n        <a class=\"dropdown-toggle pointer-cursor\" data-toggle=\"dropdown\" id=\"candi_name\" style=\"font-size:16px;\" >{{candidateData.candi_fullname| uppercase}}</a>\r\n        <ul class=\"dropdown-menu\">\r\n          <!-- <li><a href=\"#\"><i class=\"fa fa-fw fa-user\"></i> Edit Profile</a></li>\r\n          <li><a href=\"#\"><i class=\"fa fa-fw fa-cog\"></i> Change Password</a></li>\r\n          <li class=\"divider\"></li> -->\r\n          <li class=\"pointer-cursor\"><a (click)=\"logout()\" id=\"logout\"><i class=\"fa fa-fw fa-power-off\"></i> Logout</a></li>\r\n        </ul>\r\n      </li>\r\n    </ul>\r\n    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->\r\n    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">\r\n      <ul class=\"nav navbar-nav side-nav\">\r\n        <li>\r\n          <a routerLink=\"/home\" id=\"dashboard\" routerLinkActive=\"active\"><img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/professional-profile-icon.png\">&nbsp;\r\n            Dashboard</a>\r\n        </li>\r\n        <li>\r\n          <a routerLink=\"/update_profile\" id=\"update\" routerLinkActive=\"active\"><img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/professional-profile-icon.png\">&nbsp;\r\n            Update Profile</a>\r\n        </li>\r\n        <li>\r\n          <a routerLink=\"/interview\" id=\"interview\" routerLinkActive=\"active\"><img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/interviews-icon.png\">&nbsp;\r\n            Interview</a>\r\n        </li>\r\n        <li>\r\n          <a routerLink=\"/job\" id=\"job\" routerLinkActive=\"active\"><img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/job-offers-icon.png\">&nbsp; Job\r\n            Offer</a>\r\n        </li>\r\n\r\n        <li>\r\n          <a routerLink=\"/rating\" id=\"rating\"routerLinkActive=\"active\"><img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/rate-skills-icon.svg\">&nbsp;\r\n            Rating</a>\r\n        </li>\r\n        <li>\r\n          <a routerLink=\"/mentor\" id=\"mentor\" routerLinkActive=\"active\"><img src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/mentorshop-icon.svg\">&nbsp;\r\n            MentorShip</a>\r\n        </li>\r\n        <!-- <li>\r\n          <a routerLink=\"/result_test\" routerLinkActive=\"active\"><img src=\"/assets/interviews-icon.png\">&nbsp;\r\n            Result Test</a>\r\n        </li> -->\r\n        <!-- <li>\r\n          <a routerLink=\"/aboutus\" routerLinkActive=\"active\"><img src=\"/assets/interviews-icon.png\">&nbsp;\r\n            About Us</a>\r\n        </li> -->\r\n\r\n\r\n      </ul>\r\n    </div>\r\n    <!-- /.navbar-collapse -->\r\n  </nav>\r\n\r\n\r\n  <div class=\"row\" id=\"main\">\r\n    <div class=\"container-fluid\">\r\n      <div class=\"col-sm-12 col-md-12 well\" id=\"content\">\r\n        <router-outlet></router-outlet>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n  <!-- /.row -->\r\n\r\n\r\n</div><!-- /#wrapper -->\r\n"

/***/ }),

/***/ "./src/app/layout/full-layout/full-layout.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layout/full-layout/full-layout.component.ts ***!
  \*************************************************************/
/*! exports provided: FullLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullLayoutComponent", function() { return FullLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FullLayoutComponent = /** @class */ (function () {
    function FullLayoutComponent(auth, router) {
        this.auth = auth;
        this.router = router;
        this.candidateData = [];
        this.categoryselectskill = [];
        this.headers = new Headers({
            "Content-Type": "application/json"
        });
    }
    FullLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth
            .get("/candi_profile_details", this.token)
            .then(function (list) {
            _this.candidateData = list.json().result;
            localStorage.setItem("candi_skillset1", _this.candidateData.assessment_type);
        })
            .catch(function (err) { });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $(".side-nav .collapse").on("hide.bs.collapse", function () {
                $(this)
                    .prev()
                    .find(".fa")
                    .eq(1)
                    .removeClass("fa-angle-right")
                    .addClass("fa-angle-down");
            });
            $(".side-nav .collapse").on("show.bs.collapse", function () {
                $(this)
                    .prev()
                    .find(".fa")
                    .eq(1)
                    .removeClass("fa-angle-down")
                    .addClass("fa-angle-right");
            });
        });
    };
    FullLayoutComponent.prototype.logout = function () {
        this.auth.get("/candi_logout", this.token);
        localStorage.removeItem("candi_skillset1");
        localStorage.removeItem("token");
        this.router.navigateByUrl("/");
    };
    FullLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-full-layout",
            template: __webpack_require__(/*! ./full-layout.component.html */ "./src/app/layout/full-layout/full-layout.component.html"),
            styles: [__webpack_require__(/*! ./full-layout.component.css */ "./src/app/layout/full-layout/full-layout.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], FullLayoutComponent);
    return FullLayoutComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body,\r\nhtml {\r\n  height: 100%;\r\n  background-color: red !important;\r\n  overflow-x: hidden;\r\n  font-family: \"Dosis\", sans-serif;\r\n}\r\n\r\n.btn {\r\n  border-radius: 10px;\r\n}\r\n\r\n.btn:focus,\r\n.btn:active,\r\n.btn.active,\r\n.btn:active:focus {\r\n  outline: 0;\r\n  border-radius: 0;\r\n}\r\n\r\n.help-block {\r\n  font-family: sans-serif;\r\n}\r\n\r\n.btn-larger {\r\n  padding: 15px 40px !important;\r\n  /* border:2px solid #F7CA18 !important;; */\r\n  border-radius: 25px;\r\n\r\n  font-family: \"Dosis\", sans-serif;\r\n  font-size: 18px;\r\n  font-weight: 300;\r\n  color: #f7ca18;\r\n  background-color: transparent;\r\n  transition: all 0.6s;\r\n}\r\n\r\n.btn-larger:hover,\r\n.btn-larger:focus,\r\n.btn-larger:active,\r\n.btn-larger.active,\r\n.open .dropdown-toggle.btn-larger {\r\n  border-color: #f24540;\r\n  color: #fff;\r\n  background-color: #f24540;\r\n  border-radius: 25px;\r\n}\r\n\r\n.btn-larger:active,\r\n.btn-larger.active,\r\n.open .dropdown-toggle.btn-larger {\r\n  background-image: none;\r\n}\r\n\r\n.btn-larger.disabled,\r\n.btn-larger[disabled],\r\nfieldset[disabled] .btn-larger,\r\n.btn-larger.disabled:hover,\r\n.btn-larger[disabled]:hover,\r\nfieldset[disabled] .btn-larger:hover,\r\n.btn-larger.disabled:focus,\r\n.btn-larger[disabled]:focus,\r\nfieldset[disabled] .btn-larger:focus,\r\n.btn-larger.disabled:active,\r\n.btn-larger[disabled]:active,\r\nfieldset[disabled] .btn-larger:active,\r\n.btn-larger.disabled.active,\r\n.btn-larger[disabled].active,\r\nfieldset[disabled] .btn-larger.active {\r\n  border-color: #f24540;\r\n  background-color: #f24540;\r\n}\r\n\r\n.btn-larger .badge {\r\n  color: #aea8d3;\r\n  background-color: #fff;\r\n}\r\n\r\nul {\r\n  list-style-type: none;\r\n  margin: 0;\r\n  padding: 0;\r\n  overflow: hidden;\r\n  background-color: #ff6600;\r\n}\r\n\r\ndiv#form {\r\n  color: #fff;\r\n\r\n  background-attachment: scroll;\r\n\r\n  background-position: center center;\r\n  background-repeat: none;\r\n  background-size: cover;\r\n  -o-background-size: cover;\r\n  min-height: 100%;\r\n}\r\n\r\n#userform p {\r\n  font-size: 14px;\r\n  margin-bottom: 5px;\r\n}\r\n\r\n#userform span{\r\n  font-size: 14px;\r\n  margin-bottom: 5px;\r\n}\r\n\r\n#userform ul {\r\n  list-style-type: none;\r\n  padding: 0;\r\n\r\n  margin-bottom: 0px;\r\n}\r\n\r\n#userform {\r\n  background: rgba(0, 0, 0, 0.8);\r\n  margin: 20px 0 20px 0;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  #userform {\r\n    /* background: rgba(0,0,0,0.8); */\r\n    /* margin: 50px 0 20px 0 */\r\n  }\r\n}\r\n\r\n#userform .nav-tabs.nav-justified > li > a {\r\n  font-size: 20px;\r\n  color: #f7ca18;\r\n  background-color: #fff;\r\n}\r\n\r\n#userform .nav-tabs.nav-justified > .active > a,\r\n#userform .nav-tabs.nav-justified > .active > a:hover,\r\n#userform .nav-tabs.nav-justified > .active > a:focus {\r\n  border: 0;\r\n  /* background: #F7CA18; */\r\n  color: white;\r\n  border-radius: 0;\r\n}\r\n\r\n#userform .nav-justified > li > a {\r\n  margin-bottom: 0;\r\n  transition: all 0.6s;\r\n}\r\n\r\n#userform .nav-justified > li > a:hover {\r\n  background: #aea8d3;\r\n  color: #fff;\r\n}\r\n\r\n#userform .nav-tabs > li > a {\r\n  border: 0px solid transparent;\r\n  border-radius: 0;\r\n}\r\n\r\n#userform .nav-tabs.nav-justified > li > a:hover {\r\n  background: #f7ca18;\r\n  color: #fff;\r\n  border-radius: 0;\r\n  border: 0;\r\n  transition: all 0.6s;\r\n}\r\n\r\n#userform .nav-tabs > li.active > a,\r\n#userform .nav-tabs > li.active > a:hover,\r\n#userform .nav-tabs > li.active > a:focus {\r\n  color: #f24540;\r\n  cursor: default;\r\n  background-color: transparent;\r\n  border: 0;\r\n  transition: all 0.6s;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  #userform .nav-tabs.nav-justified > li > a {\r\n    border: 0;\r\n    transition: all 0.6s;\r\n  }\r\n  #userform .nav-tabs.nav-justified > li > a:hover {\r\n    background-color: #f24540;\r\n    border-color: transparent;\r\n    border: 0;\r\n    transition: all 0.6s;\r\n  }\r\n}\r\n\r\n@media (max-width: 768px) {\r\n  .nav-justified > li {\r\n    display: table-cell !important;\r\n    width: 1% !important;\r\n  }\r\n}\r\n\r\n#userform .nav-tabs {\r\n  border-bottom: 0px solid #ddd;\r\n}\r\n\r\n#userform .tab-pane h2 {\r\n  margin: 10px 0;\r\n  color: #fff;\r\n}\r\n\r\n#userform .tab-pane p.lead {\r\n  margin-top: 20px;\r\n}\r\n\r\n#userform .tab-content {\r\n  padding: 30px;\r\n  background-color: #fff;\r\n  border: 1px solid lightgray;\r\n  box-shadow: #ddd;\r\n}\r\n\r\n#userform .form-group {\r\n  margin-bottom: 0px;\r\n  color: #fff;\r\n}\r\n\r\n@media (min-width: 400px) {\r\n  #userform .nav-tabs {\r\n    font-size: 12px;\r\n  }\r\n}\r\n\r\n.icon {\r\n  background-color: linear-gradient(90deg, #ff6633 0%, #ff3333 100%) !important;\r\n\r\n  border-color: #007bff;\r\n}\r\n\r\n/* #userform .form-group input, #userform .form-group textarea {\r\n      padding: 10px;\r\n  } */\r\n\r\n/* #userform .form-group input.form-control {\r\n      height: auto;\r\n      background-color: rgba(120, 110, 184, 0.932);\r\n      color: #FFF;\r\n  } */\r\n\r\n/* #userform .form-control {\r\n      border-radius: 0;\r\n      border: 1px solid #fff;\r\n  }\r\n  #userform .form-control:focus {\r\n      border-color: #F7CA18;\r\n      box-shadow: none;\r\n  } */\r\n\r\n/* #userform::-webkit-input-placeholder {\r\n\r\n  font-family: 'Dosis', sans-serif;\r\n  font-weight: 700;\r\n   color: #bbb;\r\n  } */\r\n\r\n/* #userform #signup {\r\n      position: relative;\r\n      -webkit-transform: translateY(35px);\r\n      -ms-transform: translateY(35px);\r\n      transform: translateY(35px);\r\n      left: 10px;\r\n      top: 0px;\r\n      color: rgba(255, 255, 255, 0.5);\r\n      -webkit-transition: all 0.25s ease;\r\n      transition: all 0.25s ease;\r\n      -webkit-backface-visibility: hidden;\r\n      pointer-events: none;\r\n      font-size: 12px;\r\n      font-weight: 300\r\n  }\r\n  #userform #signup  .req {\r\n      margin: 2px;\r\n      color: #F7CA18;\r\n  }\r\n  #userform #signup.active {\r\n      -webkit-transform: translateY(0px);\r\n      -ms-transform: translateY(0px);\r\n      transform: translateY(0px);\r\n      left: 2px;\r\n      font-size: 12px;\r\n  }\r\n  #userform #signup  .req {\r\n      opacity: 0;\r\n  }\r\n  #userform label.highlight {\r\n      color: #ffffff;\r\n  }\r\n  #userform #login {\r\n      position: relative;\r\n      -webkit-transform: translateY(35px);\r\n      -ms-transform: translateY(35px);\r\n      transform: translateY(35px);\r\n      left: 10px;\r\n      top: 0px;\r\n      color: rgba(255, 255, 255, 0.5);\r\n      -webkit-transition: all 0.25s ease;\r\n      transition: all 0.25s ease;\r\n      -webkit-backface-visibility: hidden;\r\n      pointer-events: none;\r\n      font-size: 12px;\r\n      font-weight: 300\r\n  }\r\n  #userform #login .req {\r\n      margin: 2px;\r\n      color: #F7CA18;\r\n  }\r\n  #userform #login.active {\r\n      -webkit-transform: translateY(0px);\r\n      -ms-transform: translateY(0px);\r\n      transform: translateY(0px);\r\n      left: 2px;\r\n      font-size: 12px;\r\n  }\r\n  #userform #login.req {\r\n      opacity: 0;\r\n  }\r\n   */\r\n\r\n.mrgn-30-top {\r\n  margin-top: 30px;\r\n}\r\n\r\nlabel {\r\n  color: #000000;\r\n  font-family: sans-serif;\r\n}\r\n\r\ninput {\r\n  font-family: sans-serif;\r\n}\r\n\r\nh2 {\r\n  font-family: sans-serif;\r\n}\r\n"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-12\" style=\"margin-top:10px;\">\r\n    <img\r\n      style=\"display:table-cell;margin: auto; vertical-align:middle; text-align:center\"\r\n      src=\"https://s3.us-east-2.amazonaws.com/mmsdevrepo/candi_web_assets/MineMark+Logo-Orange.svg\"\r\n    />\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-md-12\" style=\"text-align: center\">\r\n    <h2>Minemark Solutions Private Limited</h2>\r\n  </div>\r\n</div>\r\n\r\n<div id=\"form\">\r\n  <div class=\"container\">\r\n    <div class=\"col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-md-8 col-md-offset-2\">\r\n      <div id=\"userform\">\r\n        <ul class=\"nav nav-tabs nav-justified\" role=\"tablist\">\r\n          <li class=\"active\" style=\"width:266px\">\r\n            <a href=\"#login\" role=\"tab\" data-toggle=\"tab\" (click)=\"resetlogin()\">Login</a>\r\n          </li>\r\n          <li>\r\n            <a href=\"#signup\" style=\"width:300px\" role=\"tab\" data-toggle=\"tab\" (click)=\"reset()\">Sign up</a>\r\n          </li>\r\n        </ul>\r\n        <div class=\"tab-content\">\r\n          <div class=\"tab-pane fade active in\" id=\"login\">\r\n            <div class=\"conatainer\">\r\n              <form class=\"form-horizontal\" [formGroup]=\"myform\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                    <div class=\"form-group\">\r\n                      <label for=\"email\" class=\"control-label\">Email ID</label>\r\n                      <input type=\"email\" class=\"form-control\" [(ngModel)]=\"user.candi_email\"              \r\n                       placeholder=\"Enter Email\"\r\n                        formControlName=\"email\"\r\n                        id=\"email\"\r\n                        [ngClass]=\"{ 'is-invalid': submitted && email.errors }\"\r\n                        pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\"/>\r\n                      <div class=\"help-block\" *ngIf=\" myform.get('email').touched && myform.get('email').invalid\">\r\n                        <span *ngIf=\"email.errors.required\" id=\"loginemail_err1\">Please enter email id</span>\r\n                        <span *ngIf=\"email.errors?.pattern\" id=\"loginemail_err2\">Please enter valid email address </span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-11\">\r\n                    <div class=\"form-group\">\r\n                      <label for=\"password\" class=\"control-label\">Password</label>\r\n                      <show-hide-password icon=\"fontawesome\" btnStyle=\"primary\" [btnOutline]=\"false\">\r\n                        <input type=\"password\" class=\"form-control\"  id=\"password\" placeholder=\"Enter password\"\r\n                          [(ngModel)]=\"user.candi_password\"\r\n                          name=\"password\"\r\n                          formControlName=\"password\"\r\n                          minlength=\"8\"/>\r\n                      </show-hide-password>\r\n                      <div class=\"help-block\"  *ngIf=\" myform.get('password').touched && myform.get('password').invalid\">\r\n                        <span *ngIf=\"password.errors.required\" id=\"loginpass_err\">\r\n                          Please enter password\r\n                        </span>\r\n                        <span *ngIf=\"password.errors?.minlength\">\r\n                          Password length must have at least 8 characters\r\n                        </span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <br />\r\n                <div class=\"form-group\">\r\n                  <div class=\"col-md-12\">\r\n                    <div class=\"checkbox\" style=\"float:right\">\r\n                      <a class=\"pointer-cursor\" routerLink=\"/forgot\" id=\"forgotpassword\">Forgot Password</a>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                  <div class=\"col-md-8 col-md-offset-4\">\r\n                    <button type=\"submit\" id=\"submit\" class=\"btn btn-larger btn-primary\" [disabled]=\"!myform.valid\" (click)=\"onLogin()\">\r\n                      <i class=\"fa fa-btn fa-sign-in\"></i> Login\r\n                    </button>\r\n                    <!-- New User? <a routerLink=\"/signup\">Register Here</a> -->\r\n                  </div>\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n          <div class=\"tab-pane fade in\" id=\"signup\">\r\n            <div class=\"conatainer\">\r\n              <form class=\"form-horizontal\" [formGroup]=\"myformsignup\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-12\">\r\n                    <div class=\"form-group\">\r\n                      <label for=\"email\" class=\"control-label\">Email ID</label>\r\n                      <input type=\"email\" class=\"form-control\" [(ngModel)]=\"signup.email\" placeholder=\"Enter Email\"formControlName=\"email\"\r\n                        id=\"email\"\r\n                        [ngClass]=\"{ 'is-invalid': submitted && email1.errors }\"\r\n                        pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\"\r\n                        (change)=\"emailvalidate()\"\r\n                      />\r\n                      <div class=\"help-block\" *ngIf=\" myformsignup.get('email').touched && myformsignup.get('email').invalid\">\r\n                        <span *ngIf=\"email1.errors.required\" id=\"signupemail_err1\" >\r\n                         Please enter email id\r\n                        </span>\r\n                        <span *ngIf=\"email1.errors?.pattern\" id=\"signupemail_err2\">\r\n                          Please provide a valid email address\r\n                        </span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\" *ngIf=\"emailvalidatation.message === 'Email already exist!'\">\r\n                  <div class=\"col-md-12\">\r\n                    <div class=\"form-group\">\r\n                      <span style=\"color:red;font-weight: bold;\">\r\n                        Email id already exist\r\n                      </span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\" [hidden]=\" emailvalidatation.message === 'Email already exist!'\">\r\n                  <div class=\"col-md-8\">\r\n                    <div class=\"form-group\">\r\n                      <label for=\"mobileno\" class=\" control-label\">Mobile No</label>\r\n                      <input type=\"text\"\r\n                        class=\"form-control\"\r\n                        placeholder=\"Enter Mobile No\"\r\n                        [(ngModel)]=\"signup.mobileno\"\r\n                        name=\"mobileno\"\r\n                        id=\"mobileno\"\r\n                        formControlName=\"mobileno\"\r\n                        autocomplete=\"false\"\r\n                        maxlength=\"10\"\r\n                        pattern=\"^[6-9]\\d{9}$\"\r\n                      />\r\n                      <span class=\"help-block\" id=\"mobile_err\" \r\n                      *ngIf=\" myformsignup.get('mobileno').touched && myformsignup.get('mobileno').invalid\">\r\n                        Mobile number is required</span>\r\n                      <!-- <div *ngIf=\"mobileno.errors?.maxlength\">Username should be maxlength 10</div> -->\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-md-4\" style=\"margin-top:40px;\">\r\n                    <label\r\n                      ><a id=\"getOTP\"\r\n                        class=\"pointer-cursor\"\r\n                        style=\"color:#F24540\"\r\n                        (click)=\"otp()\"\r\n                        >click here</a\r\n                      >\r\n                      get OTP</label\r\n                    >\r\n                  </div>\r\n                </div>\r\n                <div\r\n                  class=\"row\"\r\n                  [hidden]=\"\r\n                    emailvalidatation.message === 'Email already exist!'\r\n                  \"\r\n                >\r\n                  <div class=\"col-md-8\">\r\n                    <div class=\"form-group\">\r\n                      <label for=\"otp\" class=\"control-label\">OTP</label>\r\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Enter 4 digit OTP\" [(ngModel)]=\"signup.otp\"\r\n                        name=\"otp\" id=\"otp\" formControlName=\"otp\" autocomplete=\"false\" maxlength=\"4\" />\r\n                      <span class=\"help-block\" *ngIf=\" myformsignup.get('otp').touched && myformsignup.get('otp').invalid \"\r\n                      id=\"otp_err\">OTP is required</span\r\n                      >\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\" [hidden]=\" emailvalidatation.message === 'Email already exist!'\">\r\n                  <div class=\"col-md-12\">\r\n                    <div class=\"form-group\">\r\n                      <label for=\"password\" class=\"control-label\">Password</label>\r\n                      <input\r\n                        type=\"password\"\r\n                        class=\"form-control\"\r\n                        id=\"password1\"\r\n                        placeholder=\"Enter password\"\r\n                        [(ngModel)]=\"signup.password\"\r\n                        name=\"password\"\r\n                        formControlName=\"password\"\r\n                        minlength=\"8\"\r\n                        autocomplete=\"false\"\r\n                      />\r\n\r\n                      <div\r\n                        class=\"help-block\"\r\n                        *ngIf=\"\r\n                          myformsignup.get('password').touched &&\r\n                          myformsignup.get('password').invalid\r\n                        \"\r\n                      >\r\n                        <span *ngIf=\"password1.errors.required\" id=\"signuppass_err1\">Please enter password\r\n                        </span>\r\n                        <span *ngIf=\"password1.errors?.minlength\" id=\"signuppass_err2\">\r\n                          Password length must have at least 8 characters\r\n                        </span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <br />\r\n                <div class=\"row\" \r\n                  [hidden]=\"\r\n                    emailvalidatation.message === 'Email already exist!'\r\n                  \"\r\n                >\r\n                  <div class=\"checkbox\">\r\n                    <label\r\n                      ><input\r\n                        type=\"checkbox\"\r\n                        value=\"\"\r\n                        *ngIf=\"!isActive\"\r\n                        required\r\n                        id=\"term\"\r\n                        [(ngModel)]=\"signup.term\"\r\n                        formControlName=\"term\"\r\n                      /><span style=\"font-size:14px;font-family: sans-serif\"\r\n                        >By selecting with Minemark, you agree to our\r\n                        <a\r\n                          style=\"color:#F24540\"\r\n                          routerLink=\"http://www.minemarksolutions.com/privacy.php\"\r\n                          >Terms</a\r\n                        >\r\n                        and\r\n                        <a\r\n                          style=\"color:#F24540\"\r\n                          routerLink=\"http://www.minemarksolutions.com/privacy.php\"\r\n                          >Privacy Policy</a\r\n                        ></span\r\n                      ></label\r\n                    >\r\n                  </div>\r\n                </div>\r\n                <br />\r\n                <div\r\n                  class=\"form-group\"\r\n                  [hidden]=\"\r\n                    emailvalidatation.message === 'Email already exist!'\r\n                  \"\r\n                >\r\n                  <div class=\"col-md-8 col-md-offset-4\">\r\n                    <button\r\n                      type=\"submit\"\r\n                      [disabled]=\"!myformsignup.valid\"\r\n                      class=\"btn btn-larger btn-primary\"\r\n                      (click)=\"Signup()\"\r\n                      id=\"btnSignup\"\r\n                    >\r\n                      <i class=\"fa fa-btn fa-sign-in\"></i> Signup\r\n                    </button>\r\n                    <!-- <a routerLink=\"/skill-update\" style=\"margin-left:10px;\">Register Here</a> -->\r\n                  </div>\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <!-- <footer\r\n    class=\"app-footer\"\r\n    style=\"font-size:16px;color:black;font-family: sans-serif;text-align: center;\"\r\n  >\r\n    &copy;\r\n    <a style=\"color:black;font-size: 16px;\"\r\n      >Minemark Solutions Private Limited</a\r\n    >\r\n    2019\r\n  </footer> --><app-footer></app-footer>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, activatedRoute, toastr, router, http, auth) {
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.toastr = toastr;
        this.router = router;
        this.http = http;
        this.auth = auth;
        this.submitted = false;
        this.emailvalidatation = [];
        this.isActive = false;
        this.parentMessage = "message from parent";
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({
            "Content-Type": "application/json"
        });
        this.user = { candi_email: "", candi_password: "" };
        this.signup = { email: "", password: "", mobileno: "", otp: "", term: "" };
    }
    LoginComponent.prototype.ngOnInit = function () {
        $(document).ready(function () {
            $(".tab a").on("click", function (e) {
                e.preventDefault();
                $(this)
                    .parent()
                    .addClass("active");
                $(this)
                    .parent()
                    .siblings()
                    .removeClass("active");
                var href = $(this).attr("href");
                $(".forms > form").hide();
                $(href).fadeIn(500);
            });
        });
        this.myform = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
            ]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
        this.myformsignup = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
            ]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            mobileno: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(12)
            ]),
            otp: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            term: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
    };
    Object.defineProperty(LoginComponent.prototype, "email", {
        get: function () {
            return this.myform.get("email");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "password", {
        get: function () {
            return this.myform.get("password");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "email1", {
        get: function () {
            return this.myformsignup.get("email");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "password1", {
        get: function () {
            return this.myformsignup.get("password");
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.emailvalidate = function () {
        var _this = this;
        this.auth
            .postDetail("/email_check_candidate", "", { email: this.signup.email })
            .then(function (data) {
            _this.emailvalidatation = data.json();
        })
            .catch(function (error) { });
    };
    // Login Api binding function
    LoginComponent.prototype.onLogin = function () {
        var _this = this;
        this.auth
            .login(this.user)
            .then(function (result) {
            if (result.json().message === "Authentication Successfull!!") {
                _this.user = result.json();
                localStorage.setItem("token", result.json().token);
                _this.token = localStorage.getItem("token");
                _this.userId = localStorage.getItem(_this.user.id);
                if (_this.user.candi_fullname === null) {
                    _this.activatedRoute.params.subscribe(function (option) {
                        _this.router.navigateByUrl("/skill-update/" + _this.user.id);
                    });
                }
                else {
                    _this.router.navigateByUrl("/home");
                }
            }
        })
            .catch(function (err) {
            _this.toastr.error(err.json().message);
        });
    };
    //signup Api binding function
    LoginComponent.prototype.Signup = function () {
        var _this = this;
        this.auth
            .postDetail('/candi_sendotp_verfiy', this.token, {
            mobile_no: this.signup.mobileno,
            otp: this.signup.otp
        })
            .then(function (data) {
            _this.otpVerify = data.json().data;
            if (_this.otpVerify.message == "otp_verified") {
                _this.auth
                    .SignuppostDetail('/candi_signup', {
                    candi_email: _this.signup.email,
                    candi_password: _this.signup.password,
                    candi_phone: _this.signup.mobileno
                })
                    .then(function (data) {
                    if (data) {
                        _this.signup = data.json().message;
                        _this.toastr.success(_this.signup);
                        _this.reset();
                    }
                    else {
                        _this.toastr.error("Please enter valid OTP");
                    }
                })
                    .catch(function (err) {
                    console.log(err);
                });
            }
            else {
                _this.toastr.error("Please enter valid OTP");
            }
        });
    };
    LoginComponent.prototype.otp = function () {
        var _this = this;
        if (this.signup.mobileno != '') {
            this.auth
                .postDetail('/candi_sendotp', this.token, {
                mobile_no: this.signup.mobileno,
                candi_email: this.signup.email
            })
                .then(function (data) {
                _this.data = data.json();
                _this.otpData = data.json().message;
                _this.Id = _this.data.id;
                _this.toastr.success('The OTP is sent to your registered Mobile Number');
            });
        }
        else {
            this.toastr.error('Please Enter Mobile Number');
        }
    };
    LoginComponent.prototype.reset = function () {
        this.myformsignup.reset();
    };
    LoginComponent.prototype.resetlogin = function () {
        this.myform.reset();
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-login",
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/modal/StateCity.ts":
/*!************************************!*\
  !*** ./src/app/modal/StateCity.ts ***!
  \************************************/
/*! exports provided: StateCity, City, StateCityJson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StateCity", function() { return StateCity; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "City", function() { return City; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StateCityJson", function() { return StateCityJson; });
var StateCity = [
    { "state": "Andhra Pradesh" },
    { "state": "Arunachal Pradesh" },
    { "state": "Assam" },
    { "state": "Bihar" },
    { "state": "Chhattisgarh" },
    { "state": "Chandigarh" },
    { "state": "Dadra and Nagar Haveli" },
    { "state": "Daman and Diu" },
    { "state": "Delhi" },
    { "state": "Goa" },
    { "state": "Gujarat" },
    { "state": "Haryana" },
    { "state": "Himachal Pradesh" },
    { "state": "Jammu and Kashmir" },
    { "state": "Jharkhand" },
    { "state": "Karnataka" },
    { "state": "Kerala" },
    { "state": "Madhya Pradesh" },
    { "state": "Maharashtra" },
    { "state": "Manipur" },
    { "state": "Meghalaya" },
    { "state": "Mizoram" },
    { "state": "Nagaland" },
    { "state": "Orissa" },
    { "state": "Punjab" },
    { "state": "Pondicherry" },
    { "state": "Rajasthan" },
    { "state": "Sikkim" },
    { "state": "Tamil Nadu" },
    { "state": "Tripura" },
    { "state": "Uttar Pradesh" },
    { "state": "Uttarakhand" },
    { "state": "West Bengal" }
];
var City = [
    { "city": "Mumbai" },
    { "city": "Pune" },
    { "city": "Delhi" },
    { "city": "Meerut" },
    { "city": "Agra" },
    { "city": "Goa" },
    { "city": "Betul" },
    { "city": "Bhind" },
    { "city": "Bhopal" },
    { "city": "Burhanpur" },
    { "city": "Chhatarpur" },
    { "city": "Chhindwara" },
    { "city": "Damoh" },
    { "city": "Datia" },
    { "city": "Anantapur" },
    { "city": "Chittoor" },
    { "city": "East Godavari" },
    { "city": "Guntur" },
    { "city": "Krishna" },
    { "city": "Kurnool" },
    { "city": "Nellore" },
    { "city": "Prakasam" },
    { "city": "Srikakulam" },
    { "city": "Visakhapatnam" },
    { "city": "Vizianagaram" },
    { "city": "West Godavari" },
];
var StateCityJson = [{
        name: "Andaman and Nicobar Islands", cities: [
            "Port Blair*"
        ]
    }, {
        "Andhra Pradesh": [
            "Adoni",
            "Amalapuram",
            "Anakapalle",
            "Anantapur",
            "Bapatla",
            "Bheemunipatnam",
            "Bhimavaram",
            "Bobbili",
            "Chilakaluripet",
            "Chirala",
            "Chittoor",
            "Dharmavaram",
            "Eluru",
            "Gooty",
            "Gudivada",
            "Gudur",
            "Guntakal",
            "Guntur",
            "Hindupur",
            "Jaggaiahpet",
            "Jammalamadugu",
            "Kadapa",
            "Kadiri",
            "Kakinada",
            "Kandukur",
            "Kavali",
            "Kovvur",
            "Kurnool",
            "Macherla",
            "Machilipatnam",
            "Madanapalle",
            "Mandapeta",
            "Markapur",
            "Nagari",
            "Naidupet",
            "Nandyal",
            "Narasapuram",
            "Narasaraopet",
            "Narsipatnam",
            "Nellore",
            "Nidadavole",
            "Nuzvid",
            "Ongole",
            "Palacole",
            "Palasa Kasibugga",
            "Parvathipuram",
            "Pedana",
            "Peddapuram",
            "Pithapuram",
            "Ponnur",
            "Proddatur",
            "Punganur",
            "Puttur",
            "Rajahmundry",
            "Rajam",
            "Rajampet",
            "Ramachandrapuram",
            "Rayachoti",
            "Rayadurg",
            "Renigunta",
            "Repalle",
            "Salur",
            "Samalkot",
            "Sattenapalle",
            "Srikakulam",
            "Srikalahasti",
            "Srisailam Project (Right Flank Colony) Township",
            "Sullurpeta",
            "Tadepalligudem",
            "Tadpatri",
            "Tanuku",
            "Tenali",
            "Tirupati",
            "Tiruvuru",
            "Tuni",
            "Uravakonda",
            "Venkatagiri",
            "Vijayawada",
            "Vinukonda",
            "Visakhapatnam",
            "Vizianagaram",
            "Yemmiganur",
            "Yerraguntla"
        ],
        "Arunachal Pradesh": [
            "Naharlagun",
            "Pasighat"
        ],
        "Assam": [
            "Barpeta",
            "Bongaigaon City",
            "Dhubri",
            "Dibrugarh",
            "Diphu",
            "Goalpara",
            "Guwahati",
            "Jorhat",
            "Karimganj",
            "Lanka",
            "Lumding",
            "Mangaldoi",
            "Mankachar",
            "Margherita",
            "Mariani",
            "Marigaon",
            "Nagaon",
            "Nalbari",
            "North Lakhimpur",
            "Rangia",
            "Sibsagar",
            "Silapathar",
            "Silchar",
            "Tezpur",
            "Tinsukia"
        ],
        "Bihar": [
            "Araria",
            "Arrah",
            "Arwal",
            "Asarganj",
            "Aurangabad",
            "Bagaha",
            "Barh",
            "Begusarai",
            "Bettiah",
            "Bhabua",
            "Bhagalpur",
            "Buxar",
            "Chhapra",
            "Darbhanga",
            "Dehri-on-Sone",
            "Dumraon",
            "Forbesganj",
            "Gaya",
            "Gopalganj",
            "Hajipur",
            "Jamalpur",
            "Jamui",
            "Jehanabad",
            "Katihar",
            "Kishanganj",
            "Lakhisarai",
            "Lalganj",
            "Madhepura",
            "Madhubani",
            "Maharajganj",
            "Mahnar Bazar",
            "Makhdumpur",
            "Maner",
            "Manihari",
            "Marhaura",
            "Masaurhi",
            "Mirganj",
            "Mokameh",
            "Motihari",
            "Motipur",
            "Munger",
            "Murliganj",
            "Muzaffarpur",
            "Narkatiaganj",
            "Naugachhia",
            "Nawada",
            "Nokha",
            "Patna*",
            "Piro",
            "Purnia",
            "Rafiganj",
            "Rajgir",
            "Ramnagar",
            "Raxaul Bazar",
            "Revelganj",
            "Rosera",
            "Saharsa",
            "Samastipur",
            "Sasaram",
            "Sheikhpura",
            "Sheohar",
            "Sherghati",
            "Silao",
            "Sitamarhi",
            "Siwan",
            "Sonepur",
            "Sugauli",
            "Sultanganj",
            "Supaul",
            "Warisaliganj"
        ],
        "Chandigarh": [
            "Chandigarh*"
        ],
        "Chhattisgarh": [
            "Ambikapur",
            "Bhatapara",
            "Bhilai Nagar",
            "Bilaspur",
            "Chirmiri",
            "Dalli-Rajhara",
            "Dhamtari",
            "Durg",
            "Jagdalpur",
            "Korba",
            "Mahasamund",
            "Manendragarh",
            "Mungeli",
            "Naila Janjgir",
            "Raigarh",
            "Raipur*",
            "Rajnandgaon",
            "Sakti",
            "Tilda Newra"
        ],
        "Dadra and Nagar Haveli": [
            "Silvassa*"
        ],
        "Delhi": [
            "Delhi",
            "New Delhi*"
        ],
        "Goa": [
            "Mapusa",
            "Margao",
            "Marmagao",
            "Panaji*"
        ],
        "Gujarat": [
            "Adalaj",
            "Ahmedabad",
            "Amreli",
            "Anand",
            "Anjar",
            "Ankleshwar",
            "Bharuch",
            "Bhavnagar",
            "Bhuj",
            "Chhapra",
            "Deesa",
            "Dhoraji",
            "Godhra",
            "Jamnagar",
            "Kadi",
            "Kapadvanj",
            "Keshod",
            "Khambhat",
            "Lathi",
            "Limbdi",
            "Lunawada",
            "Mahesana",
            "Mahuva",
            "Manavadar",
            "Mandvi",
            "Mangrol",
            "Mansa",
            "Mahemdabad",
            "Modasa",
            "Morvi",
            "Nadiad",
            "Navsari",
            "Padra",
            "Palanpur",
            "Palitana",
            "Pardi",
            "Patan",
            "Petlad",
            "Porbandar",
            "Radhanpur",
            "Rajkot",
            "Rajpipla",
            "Rajula",
            "Ranavav",
            "Rapar",
            "Salaya",
            "Sanand",
            "Savarkundla",
            "Sidhpur",
            "Sihor",
            "Songadh",
            "Surat",
            "Talaja",
            "Thangadh",
            "Tharad",
            "Umbergaon",
            "Umreth",
            "Una",
            "Unjha",
            "Upleta",
            "Vadnagar",
            "Vadodara",
            "Valsad",
            "Vapi",
            "Vapi",
            "Veraval",
            "Vijapur",
            "Viramgam",
            "Visnagar",
            "Vyara",
            "Wadhwan",
            "Wankaner"
        ],
        "Haryana": [
            "Bahadurgarh",
            "Bhiwani",
            "Charkhi Dadri",
            "Faridabad",
            "Fatehabad",
            "Gohana",
            "Gurgaon",
            "Hansi",
            "Hisar",
            "Jind",
            "Kaithal",
            "Karnal",
            "Ladwa",
            "Mahendragarh",
            "Mandi Dabwali",
            "Narnaul",
            "Narwana",
            "Palwal",
            "Panchkula",
            "Panipat",
            "Pehowa",
            "Pinjore",
            "Rania",
            "Ratia",
            "Rewari",
            "Rohtak",
            "Safidon",
            "Samalkha",
            "Sarsod",
            "Shahbad",
            "Sirsa",
            "Sohna",
            "Sonipat",
            "Taraori",
            "Thanesar",
            "Tohana",
            "Yamunanagar"
        ],
        "Himachal Pradesh": [
            "Mandi",
            "Nahan",
            "Palampur",
            "Shimla*",
            "Solan",
            "Sundarnagar"
        ],
        "Jammu and Kashmir": [
            "Anantnag",
            "Baramula",
            "Jammu",
            "Kathua",
            "Punch",
            "Rajauri",
            "Sopore",
            "Srinagar*",
            "Udhampur"
        ],
        "Jharkhand": [
            "Adityapur",
            "Bokaro Steel City",
            "Chaibasa",
            "Chatra",
            "Chirkunda",
            "Medininagar (Daltonganj)",
            "Deoghar",
            "Dhanbad",
            "Dumka",
            "Giridih",
            "Gumia",
            "Hazaribag",
            "Jamshedpur",
            "Jhumri Tilaiya",
            "Lohardaga",
            "Madhupur",
            "Mihijam",
            "Musabani",
            "Pakaur",
            "Patratu",
            "Phusro",
            "Ramgarh",
            "Ranchi*",
            "Sahibganj",
            "Saunda",
            "Simdega",
            "Tenu dam-cum-Kathhara"
        ],
        "Karnataka": [
            "Adyar",
            "Afzalpur",
            "Arsikere",
            "Athni",
            "Bengaluru",
            "Belagavi",
            "Ballari",
            "Chikkamagaluru",
            "Davanagere",
            "Gokak",
            "Hubli-Dharwad",
            "Karwar",
            "Kolar",
            "Lakshmeshwar",
            "Lingsugur",
            "Maddur",
            "Madhugiri",
            "Madikeri",
            "Magadi",
            "Mahalingapura",
            "Malavalli",
            "Malur",
            "Mandya",
            "Mangaluru",
            "Manvi",
            "Mudalagi",
            "Mudabidri",
            "Muddebihal",
            "Mudhol",
            "Mulbagal",
            "Mundargi",
            "Nanjangud",
            "Nargund",
            "Navalgund",
            "Nelamangala",
            "Pavagada",
            "Piriyapatna",
            "Puttur",
            "Rabkavi Banhatti",
            "Raayachuru",
            "Ranebennuru",
            "Ramanagaram",
            "Ramdurg",
            "Ranibennur",
            "Robertson Pet",
            "Ron",
            "Sadalagi",
            "Sagara",
            "Sakaleshapura",
            "Sindagi",
            "Sanduru",
            "Sankeshwara",
            "Saundatti-Yellamma",
            "Savanur",
            "Sedam",
            "Shahabad",
            "Shahpur",
            "Shiggaon",
            "Shikaripur",
            "Shivamogga",
            "Surapura",
            "Shrirangapattana",
            "Sidlaghatta",
            "Sindhagi",
            "Sindhnur",
            "Sira",
            "Sirsi",
            "Siruguppa",
            "Srinivaspur",
            "Tarikere",
            "Tekkalakote",
            "Terdal",
            "Talikota",
            "Tiptur",
            "Tumkur",
            "Udupi",
            "Vijayapura",
            "Wadi",
            "Yadgir"
        ],
        "Karnatka": [
            "Mysore"
        ],
        "Kerala": [
            "Adoor",
            "Alappuzha",
            "Attingal",
            "Chalakudy",
            "Changanassery",
            "Cherthala",
            "Chittur-Thathamangalam",
            "Guruvayoor",
            "Kanhangad",
            "Kannur",
            "Kasaragod",
            "Kayamkulam",
            "Kochi",
            "Kodungallur",
            "Kollam",
            "Kottayam",
            "Kozhikode",
            "Kunnamkulam",
            "Malappuram",
            "Mattannur",
            "Mavelikkara",
            "Mavoor",
            "Muvattupuzha",
            "Nedumangad",
            "Neyyattinkara",
            "Nilambur",
            "Ottappalam",
            "Palai",
            "Palakkad",
            "Panamattom",
            "Panniyannur",
            "Pappinisseri",
            "Paravoor",
            "Pathanamthitta",
            "Peringathur",
            "Perinthalmanna",
            "Perumbavoor",
            "Ponnani",
            "Punalur",
            "Puthuppally",
            "Koyilandy",
            "Shoranur",
            "Taliparamba",
            "Thiruvalla",
            "Thiruvananthapuram",
            "Thodupuzha",
            "Thrissur",
            "Tirur",
            "Vaikom",
            "Varkala",
            "Vatakara"
        ],
        "Madhya Pradesh": [
            "Alirajpur",
            "Ashok Nagar",
            "Balaghat",
            "Bhopal",
            "Ganjbasoda",
            "Gwalior",
            "Indore",
            "Itarsi",
            "Jabalpur",
            "Lahar",
            "Maharajpur",
            "Mahidpur",
            "Maihar",
            "Malaj Khand",
            "Manasa",
            "Manawar",
            "Mandideep",
            "Mandla",
            "Mandsaur",
            "Mauganj",
            "Mhow Cantonment",
            "Mhowgaon",
            "Morena",
            "Multai",
            "Mundi",
            "Murwara (Katni)",
            "Nagda",
            "Nainpur",
            "Narsinghgarh",
            "Narsinghgarh",
            "Neemuch",
            "Nepanagar",
            "Niwari",
            "Nowgong",
            "Nowrozabad (Khodargama)",
            "Pachore",
            "Pali",
            "Panagar",
            "Pandhurna",
            "Panna",
            "Pasan",
            "Pipariya",
            "Pithampur",
            "Porsa",
            "Prithvipur",
            "Raghogarh-Vijaypur",
            "Rahatgarh",
            "Raisen",
            "Rajgarh",
            "Ratlam",
            "Rau",
            "Rehli",
            "Rewa",
            "Sabalgarh",
            "Sagar",
            "Sanawad",
            "Sarangpur",
            "Sarni",
            "Satna",
            "Sausar",
            "Sehore",
            "Sendhwa",
            "Seoni",
            "Seoni-Malwa",
            "Shahdol",
            "Shajapur",
            "Shamgarh",
            "Sheopur",
            "Shivpuri",
            "Shujalpur",
            "Sidhi",
            "Sihora",
            "Singrauli",
            "Sironj",
            "Sohagpur",
            "Tarana",
            "Tikamgarh",
            "Ujjain",
            "Umaria",
            "Vidisha",
            "Vijaypur",
            "Wara Seoni"
        ],
        "Maharashtra": [
            "[[]]",
            "Ahmednagar",
            "Akola",
            "Akot",
            "Amalner",
            "Ambejogai",
            "Amravati",
            "Anjangaon",
            "Arvi",
            "Aurangabad",
            "Bhiwandi",
            "Dhule",
            "Kalyan-Dombivali",
            "Ichalkaranji",
            "Kalyan-Dombivali",
            "Karjat",
            "Latur",
            "Loha",
            "Lonar",
            "Lonavla",
            "Mahad",
            "Malegaon",
            "Malkapur",
            "Mangalvedhe",
            "Mangrulpir",
            "Manjlegaon",
            "Manmad",
            "Manwath",
            "Mehkar",
            "Mhaswad",
            "Mira-Bhayandar",
            "Morshi",
            "Mukhed",
            "Mul",
            "Greater Mumbai*",
            "Murtijapur",
            "Nagpur",
            "Nanded-Waghala",
            "Nandgaon",
            "Nandura",
            "Nandurbar",
            "Narkhed",
            "Nashik",
            "Navi Mumbai",
            "Nawapur",
            "Nilanga",
            "Osmanabad",
            "Ozar",
            "Pachora",
            "Paithan",
            "Palghar",
            "Pandharkaoda",
            "Pandharpur",
            "Panvel",
            "Parbhani",
            "Parli",
            "Partur",
            "Pathardi",
            "Pathri",
            "Patur",
            "Pauni",
            "Pen",
            "Phaltan",
            "Pulgaon",
            "Pune",
            "Purna",
            "Pusad",
            "Rahuri",
            "Rajura",
            "Ramtek",
            "Ratnagiri",
            "Raver",
            "Risod",
            "Sailu",
            "Sangamner",
            "Sangli",
            "Sangole",
            "Sasvad",
            "Satana",
            "Satara",
            "Savner",
            "Sawantwadi",
            "Shahade",
            "Shegaon",
            "Shendurjana",
            "Shirdi",
            "Shirpur-Warwade",
            "Shirur",
            "Shrigonda",
            "Shrirampur",
            "Sillod",
            "Sinnar",
            "Solapur",
            "Soyagaon",
            "Talegaon Dabhade",
            "Talode",
            "Tasgaon",
            "Thane",
            "Tirora",
            "Tuljapur",
            "Tumsar",
            "Uchgaon",
            "Udgir",
            "Umarga",
            "Umarkhed",
            "Umred",
            "Uran",
            "Uran Islampur",
            "Vadgaon Kasba",
            "Vaijapur",
            "Vasai-Virar",
            "Vita",
            "Wadgaon Road",
            "Wai",
            "Wani",
            "Wardha",
            "Warora",
            "Warud",
            "Washim",
            "Yavatmal",
            "Yawal",
            "Yevla"
        ],
        "Manipur": [
            "Imphal*",
            "Lilong",
            "Mayang Imphal",
            "Thoubal"
        ],
        "Meghalaya": [
            "Nongstoin",
            "Shillong*",
            "Tura"
        ],
        "Mizoram": [
            "Aizawl",
            "Lunglei",
            "Saiha"
        ],
        "Nagaland": [
            "Dimapur",
            "Kohima*",
            "Mokokchung",
            "Tuensang",
            "Wokha",
            "Zunheboto"
        ],
        "Odisha": [
            "Balangir",
            "Baleshwar Town",
            "Barbil",
            "Bargarh",
            "Baripada Town",
            "Bhadrak",
            "Bhawanipatna",
            "Bhubaneswar*",
            "Brahmapur",
            "Byasanagar",
            "Cuttack",
            "Dhenkanal",
            "Jatani",
            "Jharsuguda",
            "Kendrapara",
            "Kendujhar",
            "Malkangiri",
            "Nabarangapur",
            "Paradip",
            "Parlakhemundi",
            "Pattamundai",
            "Phulabani",
            "Puri",
            "Rairangpur",
            "Rajagangapur",
            "Raurkela",
            "Rayagada",
            "Sambalpur",
            "Soro",
            "Sunabeda",
            "Sundargarh",
            "Talcher",
            "Tarbha",
            "Titlagarh"
        ],
        "Puducherry": [
            "Karaikal",
            "Mahe",
            "Pondicherry*",
            "Yanam"
        ],
        "Punjab": [
            "Amritsar",
            "Barnala",
            "Batala",
            "Bathinda",
            "Dhuri",
            "Faridkot",
            "Fazilka",
            "Firozpur",
            "Firozpur Cantt.",
            "Gobindgarh",
            "Gurdaspur",
            "Hoshiarpur",
            "Jagraon",
            "Jalandhar Cantt.",
            "Jalandhar",
            "Kapurthala",
            "Khanna",
            "Kharar",
            "Kot Kapura",
            "Longowal",
            "Ludhiana",
            "Malerkotla",
            "Malout",
            "Mansa",
            "Moga",
            "Mohali",
            "Morinda, India",
            "Mukerian",
            "Muktsar",
            "Nabha",
            "Nakodar",
            "Nangal",
            "Nawanshahr",
            "Pathankot",
            "Patiala",
            "Pattran",
            "Patti",
            "Phagwara",
            "Phillaur",
            "Qadian",
            "Raikot",
            "Rajpura",
            "Rampura Phul",
            "Rupnagar",
            "Samana",
            "Sangrur",
            "Sirhind Fatehgarh Sahib",
            "Sujanpur",
            "Sunam",
            "Talwara",
            "Tarn Taran",
            "Urmar Tanda",
            "Zira",
            "Zirakpur"
        ],
        "Rajasthan": [
            "Ajmer",
            "Alwar",
            "Bikaner",
            "Bharatpur",
            "Bhilwara",
            "Jaipur*",
            "Jodhpur",
            "Lachhmangarh",
            "Ladnu",
            "Lakheri",
            "Lalsot",
            "Losal",
            "Makrana",
            "Malpura",
            "Mandalgarh",
            "Mandawa",
            "Mangrol",
            "Merta City",
            "Mount Abu",
            "Nadbai",
            "Nagar",
            "Nagaur",
            "Nasirabad",
            "Nathdwara",
            "Neem-Ka-Thana",
            "Nimbahera",
            "Nohar",
            "Nokha",
            "Pali",
            "Phalodi",
            "Phulera",
            "Pilani",
            "Pilibanga",
            "Pindwara",
            "Pipar City",
            "Prantij",
            "Pratapgarh",
            "Raisinghnagar",
            "Rajakhera",
            "Rajaldesar",
            "Rajgarh (Alwar)",
            "Rajgarh (Churu)",
            "Rajsamand",
            "Ramganj Mandi",
            "Ramngarh",
            "Ratangarh",
            "Rawatbhata",
            "Rawatsar",
            "Reengus",
            "Sadri",
            "Sadulshahar",
            "Sadulpur",
            "Sagwara",
            "Sambhar",
            "Sanchore",
            "Sangaria",
            "Sardarshahar",
            "Sawai Madhopur",
            "Shahpura",
            "Shahpura",
            "Sheoganj",
            "Sikar",
            "Sirohi",
            "Sojat",
            "Sri Madhopur",
            "Sujangarh",
            "Sumerpur",
            "Suratgarh",
            "Taranagar",
            "Todabhim",
            "Todaraisingh",
            "Tonk",
            "Udaipur",
            "Udaipurwati",
            "Vijainagar, Ajmer"
        ],
        "Tamil Nadu": [
            "Arakkonam",
            "Aruppukkottai",
            "Chennai*",
            "Coimbatore",
            "Erode",
            "Gobichettipalayam",
            "Kancheepuram",
            "Karur",
            "Lalgudi",
            "Madurai",
            "Manachanallur",
            "Nagapattinam",
            "Nagercoil",
            "Namagiripettai",
            "Namakkal",
            "Nandivaram-Guduvancheri",
            "Nanjikottai",
            "Natham",
            "Nellikuppam",
            "Neyveli (TS)",
            "O' Valley",
            "Oddanchatram",
            "P.N.Patti",
            "Pacode",
            "Padmanabhapuram",
            "Palani",
            "Palladam",
            "Pallapatti",
            "Pallikonda",
            "Panagudi",
            "Panruti",
            "Paramakudi",
            "Parangipettai",
            "Pattukkottai",
            "Perambalur",
            "Peravurani",
            "Periyakulam",
            "Periyasemur",
            "Pernampattu",
            "Pollachi",
            "Polur",
            "Ponneri",
            "Pudukkottai",
            "Pudupattinam",
            "Puliyankudi",
            "Punjaipugalur",
            "Ranipet",
            "Rajapalayam",
            "Ramanathapuram",
            "Rameshwaram",
            "Rasipuram",
            "Salem",
            "Sankarankoil",
            "Sankari",
            "Sathyamangalam",
            "Sattur",
            "Shenkottai",
            "Sholavandan",
            "Sholingur",
            "Sirkali",
            "Sivaganga",
            "Sivagiri",
            "Sivakasi",
            "Srivilliputhur",
            "Surandai",
            "Suriyampalayam",
            "Tenkasi",
            "Thammampatti",
            "Thanjavur",
            "Tharamangalam",
            "Tharangambadi",
            "Theni Allinagaram",
            "Thirumangalam",
            "Thirupuvanam",
            "Thiruthuraipoondi",
            "Thiruvallur",
            "Thiruvarur",
            "Thuraiyur",
            "Tindivanam",
            "Tiruchendur",
            "Tiruchengode",
            "Tiruchirappalli",
            "Tirukalukundram",
            "Tirukkoyilur",
            "Tirunelveli",
            "Tirupathur",
            "Tirupathur",
            "Tiruppur",
            "Tiruttani",
            "Tiruvannamalai",
            "Tiruvethipuram",
            "Tittakudi",
            "Udhagamandalam",
            "Udumalaipettai",
            "Unnamalaikadai",
            "Usilampatti",
            "Uthamapalayam",
            "Uthiramerur",
            "Vadakkuvalliyur",
            "Vadalur",
            "Vadipatti",
            "Valparai",
            "Vandavasi",
            "Vaniyambadi",
            "Vedaranyam",
            "Vellakoil",
            "Vellore",
            "Vikramasingapuram",
            "Viluppuram",
            "Virudhachalam",
            "Virudhunagar",
            "Viswanatham"
        ],
        "Telangana": [
            "Adilabad",
            "Bellampalle",
            "Bhadrachalam",
            "Bhainsa",
            "Bhongir",
            "Bodhan",
            "Farooqnagar",
            "Gadwal",
            "Hyderabad*",
            "Jagtial",
            "Jangaon",
            "Kagaznagar",
            "Kamareddy",
            "Karimnagar",
            "Khammam",
            "Koratla",
            "Kothagudem",
            "Kyathampalle",
            "Mahbubnagar",
            "Mancherial",
            "Mandamarri",
            "Manuguru",
            "Medak",
            "Miryalaguda",
            "Nagarkurnool",
            "Narayanpet",
            "Nirmal",
            "Nizamabad",
            "Palwancha",
            "Ramagundam",
            "Sadasivpet",
            "Sangareddy",
            "Siddipet",
            "Sircilla",
            "Suryapet",
            "Tandur",
            "Vikarabad",
            "Wanaparthy",
            "Warangal",
            "Yellandu"
        ],
        "Tripura": [
            "Agartala*",
            "Belonia",
            "Dharmanagar",
            "Kailasahar",
            "Khowai",
            "Pratapgarh",
            "Udaipur"
        ],
        "Uttar Pradesh": [
            "Achhnera",
            "Agra",
            "Aligarh",
            "Allahabad",
            "Amroha",
            "Azamgarh",
            "Bahraich",
            "Chandausi",
            "Etawah",
            "Firozabad",
            "Fatehpur Sikri",
            "Hapur",
            "Hardoi *",
            "Jhansi",
            "Kalpi",
            "Kanpur",
            "Khair",
            "Laharpur",
            "Lakhimpur",
            "Lal Gopalganj Nindaura",
            "Lalitpur",
            "Lalganj",
            "Lar",
            "Loni",
            "Lucknow*",
            "Mathura",
            "Meerut",
            "Modinagar",
            "Moradabad",
            "Nagina",
            "Najibabad",
            "Nakur",
            "Nanpara",
            "Naraura",
            "Naugawan Sadat",
            "Nautanwa",
            "Nawabganj",
            "Nehtaur",
            "Niwai",
            "Noida",
            "Noorpur",
            "Obra",
            "Orai",
            "Padrauna",
            "Palia Kalan",
            "Parasi",
            "Phulpur",
            "Pihani",
            "Pilibhit",
            "Pilkhuwa",
            "Powayan",
            "Pukhrayan",
            "Puranpur",
            "Purquazi",
            "Purwa",
            "Rae Bareli",
            "Rampur",
            "Rampur Maniharan",
            "Rampur Maniharan",
            "Rasra",
            "Rath",
            "Renukoot",
            "Reoti",
            "Robertsganj",
            "Rudauli",
            "Rudrapur",
            "Sadabad",
            "Safipur",
            "Saharanpur",
            "Sahaspur",
            "Sahaswan",
            "Sahawar",
            "Sahjanwa",
            "Saidpur",
            "Sambhal",
            "Samdhan",
            "Samthar",
            "Sandi",
            "Sandila",
            "Sardhana",
            "Seohara",
            "Shahabad, Hardoi",
            "Shahabad, Rampur",
            "Shahganj",
            "Shahjahanpur",
            "Shamli",
            "Shamsabad, Agra",
            "Shamsabad, Farrukhabad",
            "Sherkot",
            "Shikarpur, Bulandshahr",
            "Shikohabad",
            "Shishgarh",
            "Siana",
            "Sikanderpur",
            "Sikandra Rao",
            "Sikandrabad",
            "Sirsaganj",
            "Sirsi",
            "Sitapur",
            "Soron",
            "Suar",
            "Sultanpur",
            "Sumerpur",
            "Tanda",
            "Thakurdwara",
            "Thana Bhawan",
            "Tilhar",
            "Tirwaganj",
            "Tulsipur",
            "Tundla",
            "Ujhani",
            "Unnao",
            "Utraula",
            "Varanasi",
            "Vrindavan",
            "Warhapur",
            "Zaidpur",
            "Zamania"
        ],
        "Uttarakhand": [
            "Bageshwar",
            "Dehradun",
            "Haldwani-cum-Kathgodam",
            "Hardwar",
            "Kashipur",
            "Manglaur",
            "Mussoorie",
            "Nagla",
            "Nainital",
            "Pauri",
            "Pithoragarh",
            "Ramnagar",
            "Rishikesh",
            "Roorkee",
            "Rudrapur",
            "Sitarganj",
            "Srinagar",
            "Tehri"
        ],
        "West Bengal": [
            "Adra",
            "Alipurduar",
            "Arambagh",
            "Asansol",
            "Baharampur",
            "Balurghat",
            "Bankura",
            "Darjiling",
            "English Bazar",
            "Gangarampur",
            "Habra",
            "Hugli-Chinsurah",
            "Jalpaiguri",
            "Jhargram",
            "Kalimpong",
            "Kharagpur",
            "Kolkata",
            "Mainaguri",
            "Malda",
            "Mathabhanga",
            "Medinipur",
            "Memari",
            "Monoharpur",
            "Murshidabad",
            "Nabadwip",
            "Naihati",
            "Panchla",
            "Pandua",
            "Paschim Punropara",
            "Purulia",
            "Raghunathpur",
            "Raghunathganj",
            "Raiganj",
            "Rampurhat",
            "Ranaghat",
            "Sainthia",
            "Santipur",
            "Siliguri",
            "Sonamukhi",
            "Srirampore",
            "Suri",
            "Taki",
            "Tamluk",
            "Tarakeswar"
        ]
    }
];


/***/ }),

/***/ "./src/app/notification/notification.component.css":
/*!*********************************************************!*\
  !*** ./src/app/notification/notification.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/notification/notification.component.html":
/*!**********************************************************!*\
  !*** ./src/app/notification/notification.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  notification works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/notification/notification.component.ts":
/*!********************************************************!*\
  !*** ./src/app/notification/notification.component.ts ***!
  \********************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotificationComponent = /** @class */ (function () {
    function NotificationComponent() {
    }
    NotificationComponent.prototype.ngOnInit = function () {
    };
    NotificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__(/*! ./notification.component.html */ "./src/app/notification/notification.component.html"),
            styles: [__webpack_require__(/*! ./notification.component.css */ "./src/app/notification/notification.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/services/auth-guard.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/auth-guard.service.ts ***!
  \************************************************/
/*! exports provided: AuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardService", function() { return AuthGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuardService.prototype.canActivate = function () {
        if (!this.auth.isAuthenticated()) {
            this.router.navigateByUrl('/login');
            return false;
        }
        return true;
    };
    AuthGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var AuthService = /** @class */ (function () {
    // private selectedFile: File = null;
    // private fb: any = new FormData();
    function AuthService(jwtHelper, httpClient, http, router) {
        this.jwtHelper = jwtHelper;
        this.httpClient = httpClient;
        this.http = http;
        this.router = router;
        // private BASE_URL: string = 'http://192.168.1.109:3002/api';   // api url
        this.BASE_URL = "http://18.223.249.160:3002/api"; // api url
        this.Speech = " http://13.233.39.240:3002/getfirstquestion"; // speech api url
        this.evaluate = " http://13.233.39.240:3002"; // speech api url
        this.NextSpeech = "http://13.233.39.240:3002/getnextquestion"; // speech api url
        this.URL = "http://localhost:1997/api/candiusers/forgot"; //Forgot Passwword
        //token :any;
        this.Authid = localStorage.getItem("Authid");
        this.token = localStorage.getItem("token");
        this.loggedIn = false;
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json"
        }); // For CORS HTTP
        if (localStorage.getItem("token")) {
            this.headers.set("x-access-token", "" + localStorage.getItem("token")); // Set The Token And Authorization
        }
    }
    // Forgot Password
    AuthService.prototype.forgot = function (user) {
        var url = this.URL + "/password";
        return this.http.post(url, user, { headers: this.headers }).toPromise();
    };
    AuthService.prototype.menuList = function (p1, Authid) {
        var url = "" + this.BASE_URL + p1;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json",
            Authid: "" + localStorage.getItem("Authid")
        });
        return this.http.post(url, { headers: headers }).toPromise();
    };
    // For Login And Authenticate
    AuthService.prototype.login = function (user) {
        var url = this.BASE_URL + "/candi_signin";
        return this.http.post(url, user, { headers: this.headers }).toPromise();
    };
    AuthService.prototype.uploadfile = function (p1, token, fd) {
        var url = "" + this.BASE_URL + p1;
        // let headers: Headers = new Headers({
        //     'Content-Type': 'application/json',
        //     // 'Content-Type': 'multipart/form-data',
        //     // 'Content-Type': 'form-data',
        //     // 'Authid': `${localStorage.getItem('Authid')}`,
        // });
        // return this.http.post(url, { headers: headers }).toPromise();
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json",
            "x-access-token": "" + localStorage.getItem("token")
        });
        this.httpClient.post(url, { headers: headers }, fd).subscribe(function (res) {
            return res;
        });
    };
    // check the authentication and token
    AuthService.prototype.isAuthenticated = function () {
        var token = localStorage.getItem("token");
        return !this.jwtHelper.isTokenExpired(token);
    };
    // get and Post Data (Both)
    AuthService.prototype.getList = function (p1, method, data) {
        if (method === void 0) { method = "get"; }
        if (data === void 0) { data = ""; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var url, headers;
            return __generator(this, function (_a) {
                url = "" + this.BASE_URL + p1;
                headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
                    " Content-Type": "application/json"
                    // 'x-access-token': `${token}`
                });
                if (method == "get") {
                    return [2 /*return*/, this.http
                            .get(url, { headers: headers })
                            .toPromise()
                            .then(function (res) {
                            return res;
                        })
                            .catch(function (error) { return _this.handleError(error); })];
                }
                else {
                    return [2 /*return*/, this.http
                            .post(url, data, { headers: headers })
                            .toPromise()
                            .then(function (res) {
                            return res;
                        })
                            .catch(function (error) { return _this.handleError(error); })];
                }
                return [2 /*return*/];
            });
        });
    };
    // store the data
    AuthService.prototype.get = function (p1, token) {
        var url = "" + this.BASE_URL + p1;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json",
            "x-access-token": "" + localStorage.getItem("token")
        });
        return this.http.get(url, { headers: headers }).toPromise();
    };
    // update for update
    AuthService.prototype.update = function (p1, token, data) {
        if (data === void 0) { data = ""; }
        var url = "" + this.BASE_URL + p1;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json",
            "x-access-token": "" + token
        });
        return this.http.post(url, data, { headers: headers }).toPromise();
    };
    // for get the data
    AuthService.prototype.postDetail = function (p1, token, data) {
        if (data === void 0) { data = ""; }
        var url = "" + this.BASE_URL + p1;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json",
            "x-access-token": "" + token
        });
        return this.http.post(url, data, { headers: headers }).toPromise();
    };
    // For delete operation
    AuthService.prototype.delete = function (p1, token) {
        var url = "" + this.BASE_URL + p1;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        });
        return this.http.delete(url, { headers: headers }).toPromise();
    };
    AuthService.prototype.handleError = function (error) {
        if (error.status === 401) {
        }
    };
    AuthService.prototype.putResult = function (p1, data, token) {
        if (data === void 0) { data = ""; }
        var url = "" + this.BASE_URL + p1;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json",
            "x-access-token": "" + localStorage.getItem("token")
        });
        return this.http.put(url, data, { headers: headers }).toPromise();
    };
    // for get the data
    AuthService.prototype.SignuppostDetail = function (p1, data) {
        if (data === void 0) { data = ""; }
        var url = "" + this.BASE_URL + p1;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json"
        });
        return this.http.post(url, data, { headers: headers }).toPromise();
    };
    AuthService.prototype.dataShare = function (data) {
        if (data === void 0) { data = ""; }
        return data.toPromise();
    };
    AuthService.prototype.Logout = function () {
        localStorage.removeItem("token");
        localStorage.removeItem("Authid");
        this.router.navigateByUrl("/");
    };
    //speech post data
    AuthService.prototype.postspeech = function (data) {
        if (data === void 0) { data = ""; }
        var url = "" + this.Speech;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json"
        });
        return this.http.post(url, data, { headers: headers }).toPromise();
    };
    //speech post data
    AuthService.prototype.postspeechEvaluate = function (data) {
        if (data === void 0) { data = ""; }
        var url = "" + this.evaluate;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json"
        });
        return this.http.post(url, data, { headers: headers }).toPromise();
    };
    //speech post data Next
    AuthService.prototype.postspeechNext = function (data) {
        if (data === void 0) { data = ""; }
        var url = "" + this.NextSpeech;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            "Content-Type": "application/json"
        });
        return this.http.post(url, data, { headers: headers }).toPromise();
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_3__["JwtHelperService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/deactivate-guard.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/deactivate-guard.service.ts ***!
  \******************************************************/
/*! exports provided: CanDeactivateGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanDeactivateGuard", function() { return CanDeactivateGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CanDeactivateGuard = /** @class */ (function () {
    function CanDeactivateGuard() {
    }
    CanDeactivateGuard.prototype.canDeactivate = function (component, route, state) {
        var url = state.url;
        console.log('Url: ' + url);
        return component.canDeactivate ? component.canDeactivate() : true;
    };
    CanDeactivateGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], CanDeactivateGuard);
    return CanDeactivateGuard;
}());



/***/ }),

/***/ "./src/app/services/sharedata.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/sharedata.service.ts ***!
  \***********************************************/
/*! exports provided: SharedataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedataService", function() { return SharedataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

//import {Observable}  from 'rxjs;'
var SharedataService = /** @class */ (function () {
    function SharedataService() {
        this.modals = [];
    }
    SharedataService.prototype.setData = function (data) {
        this.data = data;
    };
    SharedataService.prototype.getData = function () {
        var temp = this.data;
        this.clearData();
        return temp;
    };
    SharedataService.prototype.clearData = function () {
        this.data = undefined;
    };
    SharedataService.prototype.open = function (id) {
        // open modal specified by id
        var modal = this.modals.filter(function (x) { return x.id === id; })[0];
        // modal.open();
    };
    SharedataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        })
    ], SharedataService);
    return SharedataService;
}());



/***/ }),

/***/ "./src/app/services/talk.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/talk.service.ts ***!
  \******************************************/
/*! exports provided: TalkService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TalkService", function() { return TalkService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var DEFAULT_GRAMMAR = "#JSGF V1.0; grammar Digits;\npublic <Digits> = ( <digit> ) + ;\n<digit> = ( zero | one | two | three | four | five | six | seven | eight | nine );";
var TalkService = /** @class */ (function () {
    function TalkService(zone, lang) {
        var _this = this;
        this.zone = zone;
        this.lang = lang;
        this.message = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.command = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.commands = {};
        this.context = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
        this.refreshGrammar = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
        this.started = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
        var webkitSpeechRecognition = window.webkitSpeechRecognition;
        this.recognition = new webkitSpeechRecognition();
        if (this.recognition == 'undefined') {
            alert('undefine');
        }
        // const SpeechRecognition = window['SpeechRecognition'] || window['webkitSpeechRecognition'];
        this.recognition.lang = lang;
        this.recognition.interimResults = false;
        this.recognition.continuous = true;
        this.recognition.onresult = function (event) {
            var message = {};
            var word = '';
            var finalTranscript = '';
            if (event.results) {
                var result = event.results[event.resultIndex];
                var firstAlternative = result[0];
                if (result.isFinal) {
                    finalTranscript = firstAlternative.transcript;
                    message = { success: true, message: finalTranscript };
                }
                else {
                    word += result[0].transcript.trim().toLowerCase();
                    message = { success: true, message: word };
                }
            }
            _this.zone.run(function () {
                if (message['error']) {
                    _this.message.error(message);
                }
                else {
                    _this.message.next(message);
                    var context = _this.getContextForWord(word);
                    if (context) {
                        _this.context.next(context);
                    }
                    else {
                        var isCommand = _this.commands[_this.context.value] && _this.commands[_this.context.value][word];
                        if (isCommand) {
                            _this.command.next({ context: _this.context.value, command: word });
                        }
                        else {
                            // try to match a global context command
                            var isGlobalCommand = _this.commands[''] && _this.commands[''][word];
                            if (isGlobalCommand) {
                                _this.command.next({ context: '', command: word });
                            }
                        }
                    }
                }
            });
        };
        this.recognition.onerror = function (error) {
            _this.zone.run(function () {
                _this.message.error(error);
            });
        };
        this.recognition.onstart = function () {
            _this.zone.run(function () {
                _this.started.next(true);
            });
        };
        this.recognition.onend = function () {
            _this.zone.run(function () {
                _this.started.next(false);
            });
        };
        this.refreshGrammar.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(0)).subscribe(function () {
            _this.setGrammar();
        });
    }
    TalkService.prototype.start = function () {
        this.recognition.start();
    };
    TalkService.prototype.stop = function () {
        this.recognition.stop();
    };
    TalkService.prototype.declareContext = function (context) {
        var contextKey = context.map(function (w) { return w.toLowerCase(); }).join('/');
        if (!this.commands[contextKey]) {
            this.commands[contextKey] = {};
        }
        this.refreshGrammar.next(true);
    };
    TalkService.prototype.declareCommand = function (command, context) {
        var contextKey = context.map(function (w) { return w.toLowerCase(); }).join('/');
        if (!this.commands[contextKey]) {
            this.commands[contextKey] = {};
        }
        this.commands[contextKey][command.toLowerCase()] = true;
        this.refreshGrammar.next(true);
    };
    TalkService.prototype.setContext = function (context) {
        var contextKey = context.map(function (w) { return w.toLowerCase(); }).join('/');
        this.context.next(contextKey);
    };
    TalkService.prototype.getContextForWord = function (word) {
        // first try to match a subcontext of the current context
        var context = this.context.value ? this.context.value + '/' + word : word;
        if (this.commands[context]) {
            return context;
        }
        // then try top-level context
        if (this.commands[word]) {
            return word;
        }
    };
    TalkService.prototype.setGrammar = function () {
        var _this = this;
        var SpeechGrammarList = window['SpeechGrammarList'] || window['webkitSpeechGrammarList'];
        var words = {};
        Object.keys(this.commands).forEach(function (context) {
            context.split('/').forEach(function (word) {
                words[word] = true;
            });
            Object.keys(_this.commands[context]).forEach(function (command) { return words[command] = true; });
        });
        var grammar = DEFAULT_GRAMMAR + ' public <command> = ' + Object.keys(words).join(' | ') + ' ;';
        var speechRecognitionList = new SpeechGrammarList();
        speechRecognitionList.addFromString(grammar, 1);
        this.recognition.grammars = speechRecognitionList;
    };
    TalkService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('SPEECH_LANG')),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], String])
    ], TalkService);
    return TalkService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.css":
/*!*********************************************!*\
  !*** ./src/app/signup/signup.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n .panel-heading{\r\n    background-color: #ddd;\r\n}\r\n\r\ninput.ng-invalid.ng-touched{\r\n    border: 1px solid red;\r\n}\r\n\r\n.help-block\r\n{\r\n    color: red;\r\n}\r\n\r\n.container{\r\n    border: 1px solid #ff6600;\r\n}\r\n\r\n/* brandico */\r\n\r\n[class*=\"brandico-\"]:before {\r\n    font-family: 'brandico', sans-serif;\r\n}\r\n\r\ninput.ng-invalid.ng-touched{\r\n    border: 1px solid red;\r\n}\r\n\r\n.help-block\r\n{\r\n    color: red;\r\n}\r\n\r\n/* entypo */\r\n\r\n[class*=\"entypo-\"]:before {\r\n    font-family: 'entypo', sans-serif;\r\n}\r\n\r\n/* openwebicons */\r\n\r\n[class*=\"openwebicons-\"]:before {\r\n    font-family: 'OpenWeb Icons', sans-serif;\r\n}\r\n\r\n/* zocial */\r\n\r\n[class*=\"zocial-\"]:before {\r\n    font-family: 'zocial', sans-serif;\r\n}\r\n\r\n.form-signin{\r\n    max-width: 330px;\r\n    padding: 15px;\r\n    margin: 0 auto;\r\n}\r\n\r\n.login-input {\r\n    margin-bottom: -1px;\r\n    border-bottom-left-radius: 0;\r\n    border-bottom-right-radius: 0;\r\n}\r\n\r\n.login-input-pass {\r\n    margin-bottom: 10px;\r\n    border-top-left-radius: 0;\r\n    border-top-right-radius: 0;\r\n}\r\n\r\n.signup-input {\r\n    margin-bottom: -1px;\r\n    border-bottom-left-radius: 0;\r\n    border-bottom-right-radius: 0;\r\n}\r\n\r\n.signup-input-confirm {\r\n    margin-bottom: 10px;\r\n    border-top-left-radius: 0;\r\n    border-top-right-radius: 0;\r\n}\r\n\r\n.create-account {\r\n    text-align: center;\r\n    width: 100%;\r\n    display: block;\r\n}\r\n\r\n.form-signin .form-control {\r\n    position: relative;\r\n    font-size: 16px;\r\n    height: auto;\r\n    padding: 10px;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.btn-center{\r\n    width: 50%;\r\n    text-align: center;\r\n    margin: inherit;\r\n}\r\n\r\n.social-login-btn {\r\n    margin: 5px;\r\n    width: 5%;\r\n    font-size: 250%;\r\n    padding: 0;\r\n}\r\n\r\n.social-login-more {\r\n    width: 45%;\r\n}\r\n\r\n.social-google {\r\n    background-color: #da573b;\r\n    border-color: #be5238;\r\n    \r\n}\r\n\r\n.social-google:hover{\r\n    background-color: #be5238;\r\n    border-color: #9b4631;\r\n}\r\n\r\n.social-twitter {\r\n    background-color: #1daee3;\r\n    border-color: #3997ba;\r\n}\r\n\r\n.social-twitter:hover {\r\n    background-color: #3997ba;\r\n    border-color: #347b95;\r\n}\r\n\r\n.social-facebook {\r\n    background-color: #4c699e;\r\n    border-color: #47618d;\r\n}\r\n\r\n.social-facebook:hover {\r\n    background-color: #47618d;\r\n    border-color: #3c5173;\r\n}\r\n\r\n.social-linkedin {\r\n    background-color: #4875B4;\r\n    border-color: #466b99;\r\n    \r\n}\r\n\r\n.social-linkedin:hover {\r\n    background-color: #466b99;\r\n    border-color: #3b5a7c;\r\n}\r\n\r\n.img{\r\n    margin-left: 43%;\r\n    margin-top: 2%;\r\n}"

/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css\" rel=\"stylesheet\">\r\n<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\"\r\n        crossorigin=\"anonymous\">\r\n<div style=\"background-color: #ff6600\">\r\n        <img class=\"img\" src=\"../../assets/light_mm_logo.png\">\r\n        <br><br>\r\n</div>\r\n<p class=\"text-center\">\r\n        <a class=\"btn btn-primary social-login-btn social-facebook\" routerLink=\"/login\"><i class=\"fa fa-facebook\"></i></a>\r\n        <a class=\"btn btn-primary social-login-btn social-linkedin\" href=\"\"><i class=\"fa fa-linkedin\"></i></a>\r\n        <a class=\"btn btn-primary social-login-btn social-google\" href=\"\"><i class=\"fa fa-google-plus\"></i></a>\r\n\r\n</p>\r\n<div class=\"container\" style=\"padding-top:47px; position: center\">\r\n        <div class=\"row\">\r\n                <style>\r\n                        .panel-default>.myLoginHeading {\r\n          color: #efebeb;\r\n          background-color: #ff6600;\r\n          border-color: #ddd;\r\n          text-align: center;\r\n          font-size: 31px;\r\n          padding: 3px 15px;\r\n        }\r\n        @media only screen and (max-width: 300px) {\r\n          .panel-default>.myLoginHeading {\r\n            font-size: 19px;\r\n            padding: 9px 15px;\r\n          }\r\n        }\r\n      </style>\r\n                <div class=\"col-md-12\">\r\n                </div>\r\n                <div class=\"col-md-3\"></div>\r\n                <div class=\"col-md-6\">\r\n                        <div class=\"panel panel-default\">\r\n                                <div class=\"panel-heading myLoginHeading\">Signup</div>\r\n                                <div class=\"panel-body\">\r\n                                        <form class=\"form-horizontal\" role=\"form\" [formGroup]=\"myform\">\r\n                                                <div class=\"form-group\">\r\n                                                        <label for=\"email\" class=\"col-md-4 control-label\">E-Mail</label>\r\n\r\n                                                        <div class=\"col-md-8\">\r\n                                                                <input id=\"email\" type=\"email\" class=\"form-control\"\r\n                                                                        name=\"email\" value=\"\" [(ngModel)]=\"user.email\"\r\n                                                                        placeholder=\"Enter Email\" formControlName=\"email\">\r\n                                                                <span class=\"help-block\" *ngIf=\"myform.get('email').touched && myform.get('email').invalid\">Email\r\n                                                                        is Required!</span>\r\n                                                        </div>\r\n                                                </div>\r\n\r\n                                                <div class=\"form-group\">\r\n                                                        <label for=\"password\" class=\"col-md-4 control-label\">Password</label>\r\n\r\n                                                        <div class=\"col-md-8\">\r\n                                                                <input type=\"password\" class=\"form-control\" id=\"password\"\r\n                                                                        placeholder=\"Enter password\" [(ngModel)]=\"user.password\"\r\n                                                                        name=\"password\" formControlName=\"password\">\r\n                                                                <span class=\"help-block\" *ngIf=\"myform.get('password').touched && myform.get('password').invalid\">Password\r\n                                                                        is Required!</span>\r\n                                                        </div>\r\n                                                </div>\r\n                                                <div class=\"form-group\">\r\n                                                        <label for=\"password\" class=\"col-md-4 control-label\">Mobile No</label>\r\n\r\n                                                        <div class=\"col-md-8\">\r\n                                                                <input type=\"text\" class=\"form-control\" id=\"mobileno\"\r\n                                                                        placeholder=\"Enter Mobile No\" [(ngModel)]=\"user.mobileno\"\r\n                                                                        name=\"mobileno\" formControlName=\"mobileno\">\r\n                                                                <span class=\"help-block\" *ngIf=\"myform.get('mobileno').touched && myform.get('mobileno').invalid\">Mobile Number\r\n                                                                        is Required!</span>\r\n                                                        </div>\r\n                                                </div>\r\n                                                <div class=\"form-group\">\r\n                                                                <label for=\"password\" class=\"col-md-4 control-label\">Enter OTP</label>\r\n        \r\n                                                                <div class=\"col-md-8\">\r\n                                                                        <input type=\"text\" class=\"form-control\" id=\"otp\"\r\n                                                                                placeholder=\"Enter Mobile No\" [(ngModel)]=\"user.otp\"\r\n                                                                                name=\"otp\" formControlName=\"otp\">\r\n                                                                        <span class=\"help-block\" *ngIf=\"myform.get('otp').touched && myform.get('otp').invalid\">Enter OTP\r\n                                                                                is Required!</span>\r\n                                                                </div>\r\n                                                        </div>\r\n                                                \r\n                                                <div class=\"form-group\">\r\n                                                        <div class=\"col-md-8 col-md-offset-4\">\r\n                                                                <button type=\"submit\" [disabled]=\"!myform.valid\" class=\"btn btn-primary\"\r\n                                                                        (click)=\"onLogin()\">\r\n                                                                        <i class=\"fa fa-btn fa-sign-in\"></i> Signup\r\n                                                                </button>\r\n                                                         <a routerLink=\"/skill-update\">Register Here</a>\r\n                                                        </div>\r\n                                                </div>\r\n                                        </form>\r\n                                </div>\r\n                                <button (click)=\"getRandomNum(4)\" class=\"btn btn-primary\"> click</button>\r\n                        </div>\r\n                </div>\r\n                <div class=\"col-md-3\"></div>\r\n        </div>\r\n</div>"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupComponent = /** @class */ (function () {
    function SignupComponent(toastr, router, http, auth) {
        this.toastr = toastr;
        this.router = router;
        this.http = http;
        this.auth = auth;
        this.parentMessage = "message from parent";
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.user = { email: "", password: "", mobileno: '', otp: '' };
    }
    SignupComponent.prototype.ngOnInit = function () {
        this.myform = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            mobileno: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            otp: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4)])
        });
    };
    Object.defineProperty(SignupComponent.prototype, "email", {
        get: function () {
            return this.myform.get('email');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SignupComponent.prototype, "password", {
        get: function () {
            return this.myform.get('password');
        },
        enumerable: true,
        configurable: true
    });
    SignupComponent.prototype.onLogin = function () {
        var _this = this;
        this.auth.login(this.user).then(function (data) {
            console.log(data);
            if (data) {
                _this.userData = data.json();
                localStorage.setItem('token', data.json().token);
                _this.token = localStorage.getItem('token');
                console.log(_this.token);
                _this.toastr.success('Welcome Palak!!!');
                _this.router.navigateByUrl('/home');
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    SignupComponent.prototype.getRandomNum = function (length) {
        var randomNum = (Math.pow(10, length).toString().slice(length - 1) +
            Math.floor((Math.random() * Math.pow(10, length)) + 1).toString()).slice(-length);
        console.log(randomNum);
        return randomNum;
    };
    SignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/signup/signup.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/signup/upload-photo/upload-photo.component.css":
/*!****************************************************************!*\
  !*** ./src/app/signup/upload-photo/upload-photo.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/signup/upload-photo/upload-photo.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/signup/upload-photo/upload-photo.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"text-center\">\r\n  <div *ngIf=\"!url\">\r\n    <img src=\"../../../assets/avatar.png\" width=\"150px\" class=\"img-circle\" alt=\"Profile Photo\">\r\n  </div>\r\n  <div *ngIf=\"url\">\r\n  <img [src]=\"url\" class=\"img-circle\" alt=\"Profile Photo\" width=\"150px\">\r\n</div>\r\n</div>\r\n<div class=\"container text-center\">\r\n\r\n\r\n\r\n<!-- <div class=\"img\" *ngIf=\"url!= null\">\r\n  <img  class=\"img-circle\" [src]=\"url\" height=\"200\" style=\"margin-left: -65%;margin-top: 15%\"> <br/>\r\n</div> -->\r\n\r\n<input  type=\"file\"  name=\"photo\" ng2FileSelect [uploader]=\"uploader\" style=\"margin-left:40%\"(change)=\"fileUpload($event)\"/>\r\n\r\n<button type=\"button\" style=\"margin-top:3%;margin-right:8%\"class=\"btn btn-success btn-s\"\r\n(click)=\"uploader.uploadAll()\"\r\n[disabled]=\"!uploader.getNotUploadedItems().length\" >\r\n    Upload an Image\r\n</button>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/signup/upload-photo/upload-photo.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/signup/upload-photo/upload-photo.component.ts ***!
  \***************************************************************/
/*! exports provided: UploadPhotoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadPhotoComponent", function() { return UploadPhotoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var URL = 'http://localhost:3002/api/upload';
var authHeader = [];
var UploadPhotoComponent = /** @class */ (function () {
    function UploadPhotoComponent(auth) {
        this.auth = auth;
        this.fileToUpload = null;
        this.allowedMimeType = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__["FileUploader"]({ url: URL, itemAlias: 'photo', allowedFileType: ["image"], maxFileSize: 9216 * 9216 });
        var currentUser = localStorage.getItem('token');
        console.log(currentUser);
        if (currentUser) {
            authHeader.push({ name: 'Authorization', value: currentUser });
        }
        var uploadOptions = { headers: authHeader };
        this.uploader.setOptions(uploadOptions);
    }
    UploadPhotoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.uploader.onWhenAddingFileFailed = function (item, filter, options) {
            switch (filter.name) {
                case 'fileSize':
                    _this.errorMessage = 'Maximum upload size 500kb allowed.';
                    console.log(_this.errorMessage);
                    break;
                case 'mimeType':
                    var allowedTypes = _this.allowedMimeType.join();
                    _this.errorMessage = "Type \"$ {item.type} is not allowed. Allowed types: \"" + allowedTypes + "\".";
                    console.log(_this.errorMessage);
                    break;
                default:
                    _this.errorMessage = "Unknown error (filter is $ {filter.name}).";
                    console.log(_this.errorMessage);
            }
        };
        this.uploader.onAfterAddingFile = function (file) { file.withCredentials = false; };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            console.log('ImageUpload:uploaded:', item, status, response);
            alert('File uploaded successfully');
        };
    };
    UploadPhotoComponent.prototype.fileUpload = function (event) {
        var _this = this;
        if (this.errorMessage != 'Maximum upload size 500kb allowed.') {
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(event.target.files[0]); // read file as data url
                reader.onload = function (event) {
                    _this.url = event.target.result;
                    console.log(event.target.result);
                };
            }
        }
    };
    UploadPhotoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-upload-photo',
            template: __webpack_require__(/*! ./upload-photo.component.html */ "./src/app/signup/upload-photo/upload-photo.component.html"),
            styles: [__webpack_require__(/*! ./upload-photo.component.css */ "./src/app/signup/upload-photo/upload-photo.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], UploadPhotoComponent);
    return UploadPhotoComponent;
}());



/***/ }),

/***/ "./src/app/validations/backcopypaste.directive.ts":
/*!********************************************************!*\
  !*** ./src/app/validations/backcopypaste.directive.ts ***!
  \********************************************************/
/*! exports provided: BackcopypasteDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackcopypasteDirective", function() { return BackcopypasteDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackcopypasteDirective = /** @class */ (function () {
    function BackcopypasteDirective() {
    }
    BackcopypasteDirective.prototype.blockPaste = function (e) {
        e.preventDefault();
    };
    BackcopypasteDirective.prototype.blockCopy = function (e) {
        e.preventDefault();
    };
    BackcopypasteDirective.prototype.blockCut = function (e) {
        e.preventDefault();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('paste', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], BackcopypasteDirective.prototype, "blockPaste", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('copy', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], BackcopypasteDirective.prototype, "blockCopy", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('cut', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], BackcopypasteDirective.prototype, "blockCut", null);
    BackcopypasteDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appBackcopypaste]'
        }),
        __metadata("design:paramtypes", [])
    ], BackcopypasteDirective);
    return BackcopypasteDirective;
}());



/***/ }),

/***/ "./src/app/validations/no-whitespace.directive.ts":
/*!********************************************************!*\
  !*** ./src/app/validations/no-whitespace.directive.ts ***!
  \********************************************************/
/*! exports provided: NoWhitespaceDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoWhitespaceDirective", function() { return NoWhitespaceDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _no_whitespace_validator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./no-whitespace.validator */ "./src/app/validations/no-whitespace.validator.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



/**
 * This validator works like "required" but it does not allow whitespace either
 *
 * @export
 * @class NoWhitespaceDirective
 * @implements {Validator}
 */
var NoWhitespaceDirective = /** @class */ (function () {
    function NoWhitespaceDirective() {
        this.valFn = Object(_no_whitespace_validator__WEBPACK_IMPORTED_MODULE_1__["NoWhitespaceValidator"])();
    }
    NoWhitespaceDirective_1 = NoWhitespaceDirective;
    NoWhitespaceDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    NoWhitespaceDirective = NoWhitespaceDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appNoWhitespace]',
            providers: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"], useExisting: NoWhitespaceDirective_1, multi: true }]
        })
    ], NoWhitespaceDirective);
    return NoWhitespaceDirective;
    var NoWhitespaceDirective_1;
}());



/***/ }),

/***/ "./src/app/validations/no-whitespace.validator.ts":
/*!********************************************************!*\
  !*** ./src/app/validations/no-whitespace.validator.ts ***!
  \********************************************************/
/*! exports provided: NoWhitespaceValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoWhitespaceValidator", function() { return NoWhitespaceValidator; });
function NoWhitespaceValidator() {
    return function (control) {
        var isWhitespace = (control.value || '').match(/\s/g);
        var isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    };
}


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\12-03-mar\12-03-mar\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map